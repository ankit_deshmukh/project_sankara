﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;
using UnityEngine.XR.ARSubsystems;
using UnityEngine.XR.ARFoundation;
using UnityEngine.UI;
using System.Runtime.InteropServices;

[System.Serializable]
public class PrefabsInVicinity
{
    public string prefabName;
    public GameObject prefabPresent;
    
}

[System.Serializable]
public class MarkerPrefabs
{
    public string prefabName;
    public GameObject targetPrefab;
}

public class ImageRecognitionExample : MonoBehaviour
{

    public MarkerPrefabs[] markerPrefabCombos;

    public PrefabsInVicinity[] prefabsInVicinities;

    private ARTrackedImageManager _arTrackedImageManager;

    private Camera m_MainCamera;
    
    private int count = 0;

    private bool isInTheFrame, shouldObjectsBeShown = false;

    private Vector3 prefabPosition, userPosition;

    private GameObject objectInflatedOnScreen = null;

    private void Awake()
    {
        m_MainCamera = Camera.main;
        _arTrackedImageManager = FindObjectOfType<ARTrackedImageManager>();
    }

    private void OnEnable()
    {
        _arTrackedImageManager.trackedImagesChanged += OnImageChanged;
    }

    private void OnDestroy()
    {
        _arTrackedImageManager.trackedImagesChanged += OnImageChanged;
    }

    private void OnImageChanged(ARTrackedImagesChangedEventArgs args)
    { 

        foreach (var trackedImage in args.updated)
        {
            Debug.Log("Added image name = " + trackedImage.referenceImage.name);
        }

        foreach (var trackedImage in args.updated)
        {
            Debug.Log("Updated Image Name = " + trackedImage.referenceImage.name);

            if (trackedImage.trackingState == TrackingState.Tracking || trackedImage.trackingState == TrackingState.Limited)
            {
                Debug.Log("Tracking State = " + trackedImage.trackingState.ToString());
                /* Loop through image/prefab-combo array */
                for (int i = 0; i < markerPrefabCombos.Length; i++)
                {
                    /* If trackedImage matches an image in the array */
                    if (markerPrefabCombos[i].prefabName == trackedImage.referenceImage.name)
                    {
                        shouldObjectsBeShown = true;

                        /* Set the corresponding prefab to active at the center of the tracked image */
                        objectInflatedOnScreen = markerPrefabCombos[i].targetPrefab;
                        objectInflatedOnScreen.SetActive(true);
                        objectInflatedOnScreen.transform.position = trackedImage.transform.position;

                    
                        prefabPosition = objectInflatedOnScreen.transform.position;


                        //for(int iter=0; iter< prefabsInVicinities.Length; iter++)
                        //{
                        //    float distance = Vector3.Distance(prefabsInVicinities[iter].prefabPresent.transform.position, userPosition);
                        //    Debug.Log("Distance between user and " + prefabsInVicinities[iter].prefabName + " = " + distance);

                        //    if(distance<2.5)
                        //    {
                        //        prefabsInVicinities[iter].prefabPresent.SetActive(true);
                        //    }
                           
                        //    //prefabsInVicinities[iter].prefabPresent.transform.position += userPosition;

                        //    Debug.Log(prefabsInVicinities[iter].prefabName + " position = " + prefabsInVicinities[i].prefabPresent.transform.position);
                        //}

                        Vector3 animPos = prefabPosition;

                        animPos.x = (float)-0.07;

                        
                        Debug.Log("Prefab Position = " + prefabPosition.ToString());

                        var rot = trackedImage.transform.rotation;
                        objectInflatedOnScreen.transform.rotation = rot * Quaternion.Euler(90,0,0);
                        

                    }
                }
                /* If not properly tracked 
                */
            }
            else
            {
                objectInflatedOnScreen.SetActive(false);

                Debug.Log("Tracking State = " + trackedImage.trackingState.ToString());
                /* Deactivate all prefabs */
                for (int i = 0; i < markerPrefabCombos.Length; i++)
                {
                    markerPrefabCombos[i].targetPrefab.SetActive(false);
                }
            }
        }

        foreach( var trackedImage in args.removed)
        {
            Debug.Log("Removed Image Name = " + trackedImage.referenceImage.name);
        }
    }

    void Start()
    {
            
    }

    void Update()
    {
        if(shouldObjectsBeShown)
        {
            inflateObjectsInVicinity();
        }

        userPosition = m_MainCamera.transform.position;

        count++;

        Vector3 newPoint = m_MainCamera.WorldToViewportPoint(prefabPosition);
        Debug.Log("New Point = " + newPoint.ToString());

        if (newPoint.z > 0 && newPoint.x > 0 && newPoint.x < 1 && newPoint.y > 0 && newPoint.y < 1)
            {
                isInTheFrame = true;
        }
        else
            {
                isInTheFrame = false;

            if(objectInflatedOnScreen!=null)
            {
                Debug.Log("Removing Object");
                //objectInflatedOnScreen.SetActive(false);
             
            }

        }
    
    }

    void inflateObjectsInVicinity()
    {
        for (int iter = 0; iter < prefabsInVicinities.Length; iter++)
        {
            float distance = Vector3.Distance(prefabsInVicinities[iter].prefabPresent.transform.position, userPosition);
            Debug.Log("Distance between user and " + prefabsInVicinities[iter].prefabName + " = " + distance);

            if (distance <= 2.0)
            {
                prefabsInVicinities[iter].prefabPresent.SetActive(true);
                //
                Debug.Log("Now showing object: " + prefabsInVicinities[iter].prefabName);
            }
        }
    }


}
