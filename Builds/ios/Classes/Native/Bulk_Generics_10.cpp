﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2;
// System.String
struct String_t;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.Experimental.ISubsystemDescriptor
struct ISubsystemDescriptor_tDF5EB3ED639A15690D2CB9993789BB21F24D3934;
// UnityEngine.XR.ARSubsystems.TrackingSubsystem`2<UnityEngine.XR.ARSubsystems.BoundedPlane,System.Object>
struct TrackingSubsystem_2_t0D6BC19D17058B4CBF6E3E9122F09A5A86A8566C;
// UnityEngine.XR.ARSubsystems.TrackingSubsystem`2<UnityEngine.XR.ARSubsystems.XREnvironmentProbe,System.Object>
struct TrackingSubsystem_2_tF85E28B67FC3325EEB52CE2A8DA0361B2BD80909;
// UnityEngine.XR.ARSubsystems.TrackingSubsystem`2<UnityEngine.XR.ARSubsystems.XRFace,System.Object>
struct TrackingSubsystem_2_t810E285415E1644FA85391E12F95806CEE3C50EB;
// UnityEngine.XR.ARSubsystems.TrackingSubsystem`2<UnityEngine.XR.ARSubsystems.XRHumanBody,System.Object>
struct TrackingSubsystem_2_tBB99F93D6653B71E3A2C1E2116F8599311DC72D6;
// UnityEngine.XR.ARSubsystems.TrackingSubsystem`2<UnityEngine.XR.ARSubsystems.XRPointCloud,System.Object>
struct TrackingSubsystem_2_t1EC759A75AE7EFD4E4446A20009C8472E1C8AED1;
// UnityEngine.XR.ARSubsystems.TrackingSubsystem`2<UnityEngine.XR.ARSubsystems.XRReferencePoint,System.Object>
struct TrackingSubsystem_2_tA3D4B822865BAE0754B253CF8551A3EBB7073851;
// UnityEngine.XR.ARSubsystems.TrackingSubsystem`2<UnityEngine.XR.ARSubsystems.XRTrackedImage,System.Object>
struct TrackingSubsystem_2_t9DCCF84BEF8FF140325BC90B18398D78CACAFF00;
// UnityEngine.XR.ARSubsystems.TrackingSubsystem`2<UnityEngine.XR.ARSubsystems.XRTrackedObject,System.Object>
struct TrackingSubsystem_2_t463AAAB107BF4F04078A98431C680E92C4B53148;

extern const RuntimeMethod* NativeArrayUnsafeUtility_ConvertExistingDataToNativeArray_TisTrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47_m8D7EEF45CE20EB27369597A33879F42E459F2D59_RuntimeMethod_var;
extern const RuntimeMethod* NativeArray_1_CopyFrom_mE2C04369E83ED4726DE1121E7E05342B6A993A97_RuntimeMethod_var;
extern const RuntimeMethod* NativeArray_1_Dispose_mB8124101177299FC76251C4E7F6C98BB0E5503C7_RuntimeMethod_var;
extern const RuntimeMethod* NativeArray_1__ctor_m49FCF39766C38037835E85C0A677B26599D0BE55_RuntimeMethod_var;
extern const uint32_t TrackableChanges_1_CopyFrom_m1EAE1F86BAC4E32BC26A41ED26E154AE34AB853B_MetadataUsageId;
extern const uint32_t TrackableChanges_1_CopyFrom_m23C9F4B5E29C52D8878486A365DE352B9D520117_MetadataUsageId;
extern const uint32_t TrackableChanges_1_CopyFrom_m4393C6018EE4FF47EB0435CA9C6566BB9562652D_MetadataUsageId;
extern const uint32_t TrackableChanges_1_CopyFrom_m588811D004C5B4A0C4D0021DDE64C8F225B45D32_MetadataUsageId;
extern const uint32_t TrackableChanges_1_CopyFrom_m96900FFB6366D3F01F5DADC8AFDC2BF00F7D1329_MetadataUsageId;
extern const uint32_t TrackableChanges_1_CopyFrom_mDD6D3BE97D0DD91EAB4A1112E55755EDB8C11C74_MetadataUsageId;
extern const uint32_t TrackableChanges_1_Dispose_m1C8DF8C1879FD6A22AD7474E15FB2590BB4F9631_MetadataUsageId;
extern const uint32_t TrackableChanges_1_Dispose_m5A25C5A0DF692BA62C0C9E9DD458E28462000702_MetadataUsageId;
extern const uint32_t TrackableChanges_1_Dispose_m7F29A9FDEA683B63D471FB1D5505A6D8EC7A107A_MetadataUsageId;
extern const uint32_t TrackableChanges_1_Dispose_m8CB8D80766E7529B391391E43158D99E39220FF6_MetadataUsageId;
extern const uint32_t TrackableChanges_1_Dispose_mA1365656D4814F94F14E5A34A975A00F5892C790_MetadataUsageId;
extern const uint32_t TrackableChanges_1_Dispose_mE3DC3C0230641A828B6A91F3CB94441797520834_MetadataUsageId;
extern const uint32_t TrackableChanges_1__ctor_m3477ECC2A1A2FF3D7287DF3B5BDFC7F272FDB3EE_MetadataUsageId;
extern const uint32_t TrackableChanges_1__ctor_m47C55466595329D4629E9A78A403D34C94AC30FC_MetadataUsageId;
extern const uint32_t TrackableChanges_1__ctor_m6A057FF02DA6FD8876C995CE890C7D8B3A1EBFB0_MetadataUsageId;
extern const uint32_t TrackableChanges_1__ctor_m74CBCEBF1898779EEA76D9991BB1F62E80E2687B_MetadataUsageId;
extern const uint32_t TrackableChanges_1__ctor_m859CA5B973D4AD250F12979A582B9F5D56007BFD_MetadataUsageId;
extern const uint32_t TrackableChanges_1__ctor_m88DBEB1615B50F97CBE740AA5502C0DFA42D7641_MetadataUsageId;
extern const uint32_t TrackableChanges_1__ctor_m8E2935D6FEAB365BB8BBF74A7D12694909DFB282_MetadataUsageId;
extern const uint32_t TrackableChanges_1__ctor_m9D3306EC438EE09208F98E4F4049E615CC5E801E_MetadataUsageId;
extern const uint32_t TrackableChanges_1__ctor_mA69A666BCF692D5A8660A564A4450ABF6C80FD90_MetadataUsageId;
extern const uint32_t TrackableChanges_1__ctor_mB26E1128CC08E676885A96E341B8CE2DC1228D20_MetadataUsageId;
extern const uint32_t TrackableChanges_1__ctor_mBA293905C9592E097DAC0E30B77D458C7D7E23FD_MetadataUsageId;
extern const uint32_t TrackableChanges_1__ctor_mD3BF9E8193EF890B4977549BF6374BE3D5165CBF_MetadataUsageId;



#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef SUBSYSTEM_T9D40133A9FEB984E2C3695B077DA4DDFC91CA181_H
#define SUBSYSTEM_T9D40133A9FEB984E2C3695B077DA4DDFC91CA181_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Subsystem
struct  Subsystem_t9D40133A9FEB984E2C3695B077DA4DDFC91CA181  : public RuntimeObject
{
public:
	// UnityEngine.Experimental.ISubsystemDescriptor UnityEngine.Experimental.Subsystem::m_subsystemDescriptor
	RuntimeObject* ___m_subsystemDescriptor_0;

public:
	inline static int32_t get_offset_of_m_subsystemDescriptor_0() { return static_cast<int32_t>(offsetof(Subsystem_t9D40133A9FEB984E2C3695B077DA4DDFC91CA181, ___m_subsystemDescriptor_0)); }
	inline RuntimeObject* get_m_subsystemDescriptor_0() const { return ___m_subsystemDescriptor_0; }
	inline RuntimeObject** get_address_of_m_subsystemDescriptor_0() { return &___m_subsystemDescriptor_0; }
	inline void set_m_subsystemDescriptor_0(RuntimeObject* value)
	{
		___m_subsystemDescriptor_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_subsystemDescriptor_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBSYSTEM_T9D40133A9FEB984E2C3695B077DA4DDFC91CA181_H
#ifndef BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#define BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef GUID_T_H
#define GUID_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Guid
struct  Guid_t 
{
public:
	// System.Int32 System.Guid::_a
	int32_t ____a_1;
	// System.Int16 System.Guid::_b
	int16_t ____b_2;
	// System.Int16 System.Guid::_c
	int16_t ____c_3;
	// System.Byte System.Guid::_d
	uint8_t ____d_4;
	// System.Byte System.Guid::_e
	uint8_t ____e_5;
	// System.Byte System.Guid::_f
	uint8_t ____f_6;
	// System.Byte System.Guid::_g
	uint8_t ____g_7;
	// System.Byte System.Guid::_h
	uint8_t ____h_8;
	// System.Byte System.Guid::_i
	uint8_t ____i_9;
	// System.Byte System.Guid::_j
	uint8_t ____j_10;
	// System.Byte System.Guid::_k
	uint8_t ____k_11;

public:
	inline static int32_t get_offset_of__a_1() { return static_cast<int32_t>(offsetof(Guid_t, ____a_1)); }
	inline int32_t get__a_1() const { return ____a_1; }
	inline int32_t* get_address_of__a_1() { return &____a_1; }
	inline void set__a_1(int32_t value)
	{
		____a_1 = value;
	}

	inline static int32_t get_offset_of__b_2() { return static_cast<int32_t>(offsetof(Guid_t, ____b_2)); }
	inline int16_t get__b_2() const { return ____b_2; }
	inline int16_t* get_address_of__b_2() { return &____b_2; }
	inline void set__b_2(int16_t value)
	{
		____b_2 = value;
	}

	inline static int32_t get_offset_of__c_3() { return static_cast<int32_t>(offsetof(Guid_t, ____c_3)); }
	inline int16_t get__c_3() const { return ____c_3; }
	inline int16_t* get_address_of__c_3() { return &____c_3; }
	inline void set__c_3(int16_t value)
	{
		____c_3 = value;
	}

	inline static int32_t get_offset_of__d_4() { return static_cast<int32_t>(offsetof(Guid_t, ____d_4)); }
	inline uint8_t get__d_4() const { return ____d_4; }
	inline uint8_t* get_address_of__d_4() { return &____d_4; }
	inline void set__d_4(uint8_t value)
	{
		____d_4 = value;
	}

	inline static int32_t get_offset_of__e_5() { return static_cast<int32_t>(offsetof(Guid_t, ____e_5)); }
	inline uint8_t get__e_5() const { return ____e_5; }
	inline uint8_t* get_address_of__e_5() { return &____e_5; }
	inline void set__e_5(uint8_t value)
	{
		____e_5 = value;
	}

	inline static int32_t get_offset_of__f_6() { return static_cast<int32_t>(offsetof(Guid_t, ____f_6)); }
	inline uint8_t get__f_6() const { return ____f_6; }
	inline uint8_t* get_address_of__f_6() { return &____f_6; }
	inline void set__f_6(uint8_t value)
	{
		____f_6 = value;
	}

	inline static int32_t get_offset_of__g_7() { return static_cast<int32_t>(offsetof(Guid_t, ____g_7)); }
	inline uint8_t get__g_7() const { return ____g_7; }
	inline uint8_t* get_address_of__g_7() { return &____g_7; }
	inline void set__g_7(uint8_t value)
	{
		____g_7 = value;
	}

	inline static int32_t get_offset_of__h_8() { return static_cast<int32_t>(offsetof(Guid_t, ____h_8)); }
	inline uint8_t get__h_8() const { return ____h_8; }
	inline uint8_t* get_address_of__h_8() { return &____h_8; }
	inline void set__h_8(uint8_t value)
	{
		____h_8 = value;
	}

	inline static int32_t get_offset_of__i_9() { return static_cast<int32_t>(offsetof(Guid_t, ____i_9)); }
	inline uint8_t get__i_9() const { return ____i_9; }
	inline uint8_t* get_address_of__i_9() { return &____i_9; }
	inline void set__i_9(uint8_t value)
	{
		____i_9 = value;
	}

	inline static int32_t get_offset_of__j_10() { return static_cast<int32_t>(offsetof(Guid_t, ____j_10)); }
	inline uint8_t get__j_10() const { return ____j_10; }
	inline uint8_t* get_address_of__j_10() { return &____j_10; }
	inline void set__j_10(uint8_t value)
	{
		____j_10 = value;
	}

	inline static int32_t get_offset_of__k_11() { return static_cast<int32_t>(offsetof(Guid_t, ____k_11)); }
	inline uint8_t get__k_11() const { return ____k_11; }
	inline uint8_t* get_address_of__k_11() { return &____k_11; }
	inline void set__k_11(uint8_t value)
	{
		____k_11 = value;
	}
};

struct Guid_t_StaticFields
{
public:
	// System.Guid System.Guid::Empty
	Guid_t  ___Empty_0;
	// System.Object System.Guid::_rngAccess
	RuntimeObject * ____rngAccess_12;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_rng
	RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * ____rng_13;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_fastRng
	RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * ____fastRng_14;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ___Empty_0)); }
	inline Guid_t  get_Empty_0() const { return ___Empty_0; }
	inline Guid_t * get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(Guid_t  value)
	{
		___Empty_0 = value;
	}

	inline static int32_t get_offset_of__rngAccess_12() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rngAccess_12)); }
	inline RuntimeObject * get__rngAccess_12() const { return ____rngAccess_12; }
	inline RuntimeObject ** get_address_of__rngAccess_12() { return &____rngAccess_12; }
	inline void set__rngAccess_12(RuntimeObject * value)
	{
		____rngAccess_12 = value;
		Il2CppCodeGenWriteBarrier((&____rngAccess_12), value);
	}

	inline static int32_t get_offset_of__rng_13() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rng_13)); }
	inline RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * get__rng_13() const { return ____rng_13; }
	inline RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 ** get_address_of__rng_13() { return &____rng_13; }
	inline void set__rng_13(RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * value)
	{
		____rng_13 = value;
		Il2CppCodeGenWriteBarrier((&____rng_13), value);
	}

	inline static int32_t get_offset_of__fastRng_14() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____fastRng_14)); }
	inline RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * get__fastRng_14() const { return ____fastRng_14; }
	inline RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 ** get_address_of__fastRng_14() { return &____fastRng_14; }
	inline void set__fastRng_14(RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * value)
	{
		____fastRng_14 = value;
		Il2CppCodeGenWriteBarrier((&____fastRng_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUID_T_H
#ifndef INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#define INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t585191389E07734F19F3156FF88FB3EF4800D102 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t585191389E07734F19F3156FF88FB3EF4800D102, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#define VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifndef SUBSYSTEM_1_T3C9464E9C57AFD87BCA00E457B3C2FCB3AFA269E_H
#define SUBSYSTEM_1_T3C9464E9C57AFD87BCA00E457B3C2FCB3AFA269E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Subsystem`1<System.Object>
struct  Subsystem_1_t3C9464E9C57AFD87BCA00E457B3C2FCB3AFA269E  : public Subsystem_t9D40133A9FEB984E2C3695B077DA4DDFC91CA181
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBSYSTEM_1_T3C9464E9C57AFD87BCA00E457B3C2FCB3AFA269E_H
#ifndef QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#define QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifndef VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#define VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifndef VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#define VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifndef TRACKABLEID_TA7E19AFE62176E25E3759548887E9068E1E4AE47_H
#define TRACKABLEID_TA7E19AFE62176E25E3759548887E9068E1E4AE47_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARSubsystems.TrackableId
struct  TrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47 
{
public:
	// System.UInt64 UnityEngine.XR.ARSubsystems.TrackableId::m_SubId1
	uint64_t ___m_SubId1_1;
	// System.UInt64 UnityEngine.XR.ARSubsystems.TrackableId::m_SubId2
	uint64_t ___m_SubId2_2;

public:
	inline static int32_t get_offset_of_m_SubId1_1() { return static_cast<int32_t>(offsetof(TrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47, ___m_SubId1_1)); }
	inline uint64_t get_m_SubId1_1() const { return ___m_SubId1_1; }
	inline uint64_t* get_address_of_m_SubId1_1() { return &___m_SubId1_1; }
	inline void set_m_SubId1_1(uint64_t value)
	{
		___m_SubId1_1 = value;
	}

	inline static int32_t get_offset_of_m_SubId2_2() { return static_cast<int32_t>(offsetof(TrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47, ___m_SubId2_2)); }
	inline uint64_t get_m_SubId2_2() const { return ___m_SubId2_2; }
	inline uint64_t* get_address_of_m_SubId2_2() { return &___m_SubId2_2; }
	inline void set_m_SubId2_2(uint64_t value)
	{
		___m_SubId2_2 = value;
	}
};

struct TrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47_StaticFields
{
public:
	// UnityEngine.XR.ARSubsystems.TrackableId UnityEngine.XR.ARSubsystems.TrackableId::s_InvalidId
	TrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47  ___s_InvalidId_0;

public:
	inline static int32_t get_offset_of_s_InvalidId_0() { return static_cast<int32_t>(offsetof(TrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47_StaticFields, ___s_InvalidId_0)); }
	inline TrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47  get_s_InvalidId_0() const { return ___s_InvalidId_0; }
	inline TrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47 * get_address_of_s_InvalidId_0() { return &___s_InvalidId_0; }
	inline void set_s_InvalidId_0(TrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47  value)
	{
		___s_InvalidId_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLEID_TA7E19AFE62176E25E3759548887E9068E1E4AE47_H
#ifndef ALLOCATOR_T62A091275262E7067EAAD565B67764FA877D58D6_H
#define ALLOCATOR_T62A091275262E7067EAAD565B67764FA877D58D6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Unity.Collections.Allocator
struct  Allocator_t62A091275262E7067EAAD565B67764FA877D58D6 
{
public:
	// System.Int32 Unity.Collections.Allocator::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Allocator_t62A091275262E7067EAAD565B67764FA877D58D6, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ALLOCATOR_T62A091275262E7067EAAD565B67764FA877D58D6_H
#ifndef NATIVEARRAYOPTIONS_T23897F2D7CA2F1B58D2539C64062DD7C77615B6A_H
#define NATIVEARRAYOPTIONS_T23897F2D7CA2F1B58D2539C64062DD7C77615B6A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Unity.Collections.NativeArrayOptions
struct  NativeArrayOptions_t23897F2D7CA2F1B58D2539C64062DD7C77615B6A 
{
public:
	// System.Int32 Unity.Collections.NativeArrayOptions::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(NativeArrayOptions_t23897F2D7CA2F1B58D2539C64062DD7C77615B6A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NATIVEARRAYOPTIONS_T23897F2D7CA2F1B58D2539C64062DD7C77615B6A_H
#ifndef POSE_T2997DE3CB3863E4D78FCF42B46FC481818823F29_H
#define POSE_T2997DE3CB3863E4D78FCF42B46FC481818823F29_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Pose
struct  Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29 
{
public:
	// UnityEngine.Vector3 UnityEngine.Pose::position
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___position_0;
	// UnityEngine.Quaternion UnityEngine.Pose::rotation
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___rotation_1;

public:
	inline static int32_t get_offset_of_position_0() { return static_cast<int32_t>(offsetof(Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29, ___position_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_position_0() const { return ___position_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_position_0() { return &___position_0; }
	inline void set_position_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___position_0 = value;
	}

	inline static int32_t get_offset_of_rotation_1() { return static_cast<int32_t>(offsetof(Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29, ___rotation_1)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_rotation_1() const { return ___rotation_1; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_rotation_1() { return &___rotation_1; }
	inline void set_rotation_1(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___rotation_1 = value;
	}
};

struct Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29_StaticFields
{
public:
	// UnityEngine.Pose UnityEngine.Pose::k_Identity
	Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29  ___k_Identity_2;

public:
	inline static int32_t get_offset_of_k_Identity_2() { return static_cast<int32_t>(offsetof(Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29_StaticFields, ___k_Identity_2)); }
	inline Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29  get_k_Identity_2() const { return ___k_Identity_2; }
	inline Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29 * get_address_of_k_Identity_2() { return &___k_Identity_2; }
	inline void set_k_Identity_2(Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29  value)
	{
		___k_Identity_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSE_T2997DE3CB3863E4D78FCF42B46FC481818823F29_H
#ifndef TRACKINGSTATE_T124D9E603E4E0453A85409CF7762EE8C946233F6_H
#define TRACKINGSTATE_T124D9E603E4E0453A85409CF7762EE8C946233F6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARSubsystems.TrackingState
struct  TrackingState_t124D9E603E4E0453A85409CF7762EE8C946233F6 
{
public:
	// System.Int32 UnityEngine.XR.ARSubsystems.TrackingState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TrackingState_t124D9E603E4E0453A85409CF7762EE8C946233F6, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKINGSTATE_T124D9E603E4E0453A85409CF7762EE8C946233F6_H
#ifndef TRACKINGSUBSYSTEM_2_T0D6BC19D17058B4CBF6E3E9122F09A5A86A8566C_H
#define TRACKINGSUBSYSTEM_2_T0D6BC19D17058B4CBF6E3E9122F09A5A86A8566C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARSubsystems.TrackingSubsystem`2<UnityEngine.XR.ARSubsystems.BoundedPlane,System.Object>
struct  TrackingSubsystem_2_t0D6BC19D17058B4CBF6E3E9122F09A5A86A8566C  : public Subsystem_1_t3C9464E9C57AFD87BCA00E457B3C2FCB3AFA269E
{
public:
	// System.Boolean UnityEngine.XR.ARSubsystems.TrackingSubsystem`2::m_Running
	bool ___m_Running_1;

public:
	inline static int32_t get_offset_of_m_Running_1() { return static_cast<int32_t>(offsetof(TrackingSubsystem_2_t0D6BC19D17058B4CBF6E3E9122F09A5A86A8566C, ___m_Running_1)); }
	inline bool get_m_Running_1() const { return ___m_Running_1; }
	inline bool* get_address_of_m_Running_1() { return &___m_Running_1; }
	inline void set_m_Running_1(bool value)
	{
		___m_Running_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKINGSUBSYSTEM_2_T0D6BC19D17058B4CBF6E3E9122F09A5A86A8566C_H
#ifndef TRACKINGSUBSYSTEM_2_TF85E28B67FC3325EEB52CE2A8DA0361B2BD80909_H
#define TRACKINGSUBSYSTEM_2_TF85E28B67FC3325EEB52CE2A8DA0361B2BD80909_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARSubsystems.TrackingSubsystem`2<UnityEngine.XR.ARSubsystems.XREnvironmentProbe,System.Object>
struct  TrackingSubsystem_2_tF85E28B67FC3325EEB52CE2A8DA0361B2BD80909  : public Subsystem_1_t3C9464E9C57AFD87BCA00E457B3C2FCB3AFA269E
{
public:
	// System.Boolean UnityEngine.XR.ARSubsystems.TrackingSubsystem`2::m_Running
	bool ___m_Running_1;

public:
	inline static int32_t get_offset_of_m_Running_1() { return static_cast<int32_t>(offsetof(TrackingSubsystem_2_tF85E28B67FC3325EEB52CE2A8DA0361B2BD80909, ___m_Running_1)); }
	inline bool get_m_Running_1() const { return ___m_Running_1; }
	inline bool* get_address_of_m_Running_1() { return &___m_Running_1; }
	inline void set_m_Running_1(bool value)
	{
		___m_Running_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKINGSUBSYSTEM_2_TF85E28B67FC3325EEB52CE2A8DA0361B2BD80909_H
#ifndef TRACKINGSUBSYSTEM_2_T810E285415E1644FA85391E12F95806CEE3C50EB_H
#define TRACKINGSUBSYSTEM_2_T810E285415E1644FA85391E12F95806CEE3C50EB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARSubsystems.TrackingSubsystem`2<UnityEngine.XR.ARSubsystems.XRFace,System.Object>
struct  TrackingSubsystem_2_t810E285415E1644FA85391E12F95806CEE3C50EB  : public Subsystem_1_t3C9464E9C57AFD87BCA00E457B3C2FCB3AFA269E
{
public:
	// System.Boolean UnityEngine.XR.ARSubsystems.TrackingSubsystem`2::m_Running
	bool ___m_Running_1;

public:
	inline static int32_t get_offset_of_m_Running_1() { return static_cast<int32_t>(offsetof(TrackingSubsystem_2_t810E285415E1644FA85391E12F95806CEE3C50EB, ___m_Running_1)); }
	inline bool get_m_Running_1() const { return ___m_Running_1; }
	inline bool* get_address_of_m_Running_1() { return &___m_Running_1; }
	inline void set_m_Running_1(bool value)
	{
		___m_Running_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKINGSUBSYSTEM_2_T810E285415E1644FA85391E12F95806CEE3C50EB_H
#ifndef TRACKINGSUBSYSTEM_2_TBB99F93D6653B71E3A2C1E2116F8599311DC72D6_H
#define TRACKINGSUBSYSTEM_2_TBB99F93D6653B71E3A2C1E2116F8599311DC72D6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARSubsystems.TrackingSubsystem`2<UnityEngine.XR.ARSubsystems.XRHumanBody,System.Object>
struct  TrackingSubsystem_2_tBB99F93D6653B71E3A2C1E2116F8599311DC72D6  : public Subsystem_1_t3C9464E9C57AFD87BCA00E457B3C2FCB3AFA269E
{
public:
	// System.Boolean UnityEngine.XR.ARSubsystems.TrackingSubsystem`2::m_Running
	bool ___m_Running_1;

public:
	inline static int32_t get_offset_of_m_Running_1() { return static_cast<int32_t>(offsetof(TrackingSubsystem_2_tBB99F93D6653B71E3A2C1E2116F8599311DC72D6, ___m_Running_1)); }
	inline bool get_m_Running_1() const { return ___m_Running_1; }
	inline bool* get_address_of_m_Running_1() { return &___m_Running_1; }
	inline void set_m_Running_1(bool value)
	{
		___m_Running_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKINGSUBSYSTEM_2_TBB99F93D6653B71E3A2C1E2116F8599311DC72D6_H
#ifndef TRACKINGSUBSYSTEM_2_T1EC759A75AE7EFD4E4446A20009C8472E1C8AED1_H
#define TRACKINGSUBSYSTEM_2_T1EC759A75AE7EFD4E4446A20009C8472E1C8AED1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARSubsystems.TrackingSubsystem`2<UnityEngine.XR.ARSubsystems.XRPointCloud,System.Object>
struct  TrackingSubsystem_2_t1EC759A75AE7EFD4E4446A20009C8472E1C8AED1  : public Subsystem_1_t3C9464E9C57AFD87BCA00E457B3C2FCB3AFA269E
{
public:
	// System.Boolean UnityEngine.XR.ARSubsystems.TrackingSubsystem`2::m_Running
	bool ___m_Running_1;

public:
	inline static int32_t get_offset_of_m_Running_1() { return static_cast<int32_t>(offsetof(TrackingSubsystem_2_t1EC759A75AE7EFD4E4446A20009C8472E1C8AED1, ___m_Running_1)); }
	inline bool get_m_Running_1() const { return ___m_Running_1; }
	inline bool* get_address_of_m_Running_1() { return &___m_Running_1; }
	inline void set_m_Running_1(bool value)
	{
		___m_Running_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKINGSUBSYSTEM_2_T1EC759A75AE7EFD4E4446A20009C8472E1C8AED1_H
#ifndef TRACKINGSUBSYSTEM_2_TA3D4B822865BAE0754B253CF8551A3EBB7073851_H
#define TRACKINGSUBSYSTEM_2_TA3D4B822865BAE0754B253CF8551A3EBB7073851_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARSubsystems.TrackingSubsystem`2<UnityEngine.XR.ARSubsystems.XRReferencePoint,System.Object>
struct  TrackingSubsystem_2_tA3D4B822865BAE0754B253CF8551A3EBB7073851  : public Subsystem_1_t3C9464E9C57AFD87BCA00E457B3C2FCB3AFA269E
{
public:
	// System.Boolean UnityEngine.XR.ARSubsystems.TrackingSubsystem`2::m_Running
	bool ___m_Running_1;

public:
	inline static int32_t get_offset_of_m_Running_1() { return static_cast<int32_t>(offsetof(TrackingSubsystem_2_tA3D4B822865BAE0754B253CF8551A3EBB7073851, ___m_Running_1)); }
	inline bool get_m_Running_1() const { return ___m_Running_1; }
	inline bool* get_address_of_m_Running_1() { return &___m_Running_1; }
	inline void set_m_Running_1(bool value)
	{
		___m_Running_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKINGSUBSYSTEM_2_TA3D4B822865BAE0754B253CF8551A3EBB7073851_H
#ifndef TRACKINGSUBSYSTEM_2_T9DCCF84BEF8FF140325BC90B18398D78CACAFF00_H
#define TRACKINGSUBSYSTEM_2_T9DCCF84BEF8FF140325BC90B18398D78CACAFF00_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARSubsystems.TrackingSubsystem`2<UnityEngine.XR.ARSubsystems.XRTrackedImage,System.Object>
struct  TrackingSubsystem_2_t9DCCF84BEF8FF140325BC90B18398D78CACAFF00  : public Subsystem_1_t3C9464E9C57AFD87BCA00E457B3C2FCB3AFA269E
{
public:
	// System.Boolean UnityEngine.XR.ARSubsystems.TrackingSubsystem`2::m_Running
	bool ___m_Running_1;

public:
	inline static int32_t get_offset_of_m_Running_1() { return static_cast<int32_t>(offsetof(TrackingSubsystem_2_t9DCCF84BEF8FF140325BC90B18398D78CACAFF00, ___m_Running_1)); }
	inline bool get_m_Running_1() const { return ___m_Running_1; }
	inline bool* get_address_of_m_Running_1() { return &___m_Running_1; }
	inline void set_m_Running_1(bool value)
	{
		___m_Running_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKINGSUBSYSTEM_2_T9DCCF84BEF8FF140325BC90B18398D78CACAFF00_H
#ifndef TRACKINGSUBSYSTEM_2_T463AAAB107BF4F04078A98431C680E92C4B53148_H
#define TRACKINGSUBSYSTEM_2_T463AAAB107BF4F04078A98431C680E92C4B53148_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARSubsystems.TrackingSubsystem`2<UnityEngine.XR.ARSubsystems.XRTrackedObject,System.Object>
struct  TrackingSubsystem_2_t463AAAB107BF4F04078A98431C680E92C4B53148  : public Subsystem_1_t3C9464E9C57AFD87BCA00E457B3C2FCB3AFA269E
{
public:
	// System.Boolean UnityEngine.XR.ARSubsystems.TrackingSubsystem`2::m_Running
	bool ___m_Running_1;

public:
	inline static int32_t get_offset_of_m_Running_1() { return static_cast<int32_t>(offsetof(TrackingSubsystem_2_t463AAAB107BF4F04078A98431C680E92C4B53148, ___m_Running_1)); }
	inline bool get_m_Running_1() const { return ___m_Running_1; }
	inline bool* get_address_of_m_Running_1() { return &___m_Running_1; }
	inline void set_m_Running_1(bool value)
	{
		___m_Running_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKINGSUBSYSTEM_2_T463AAAB107BF4F04078A98431C680E92C4B53148_H
#ifndef NATIVEARRAY_1_TD7797CC5848BA9ECBD1172056474C8DD8022B1D6_H
#define NATIVEARRAY_1_TD7797CC5848BA9ECBD1172056474C8DD8022B1D6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.TrackableId>
struct  NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 
{
public:
	// System.Void* Unity.Collections.NativeArray`1::m_Buffer
	void* ___m_Buffer_0;
	// System.Int32 Unity.Collections.NativeArray`1::m_Length
	int32_t ___m_Length_1;
	// Unity.Collections.Allocator Unity.Collections.NativeArray`1::m_AllocatorLabel
	int32_t ___m_AllocatorLabel_2;

public:
	inline static int32_t get_offset_of_m_Buffer_0() { return static_cast<int32_t>(offsetof(NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6, ___m_Buffer_0)); }
	inline void* get_m_Buffer_0() const { return ___m_Buffer_0; }
	inline void** get_address_of_m_Buffer_0() { return &___m_Buffer_0; }
	inline void set_m_Buffer_0(void* value)
	{
		___m_Buffer_0 = value;
	}

	inline static int32_t get_offset_of_m_Length_1() { return static_cast<int32_t>(offsetof(NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6, ___m_Length_1)); }
	inline int32_t get_m_Length_1() const { return ___m_Length_1; }
	inline int32_t* get_address_of_m_Length_1() { return &___m_Length_1; }
	inline void set_m_Length_1(int32_t value)
	{
		___m_Length_1 = value;
	}

	inline static int32_t get_offset_of_m_AllocatorLabel_2() { return static_cast<int32_t>(offsetof(NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6, ___m_AllocatorLabel_2)); }
	inline int32_t get_m_AllocatorLabel_2() const { return ___m_AllocatorLabel_2; }
	inline int32_t* get_address_of_m_AllocatorLabel_2() { return &___m_AllocatorLabel_2; }
	inline void set_m_AllocatorLabel_2(int32_t value)
	{
		___m_AllocatorLabel_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Unity.Collections.NativeArray`1
#ifndef NativeArray_1_t350F3793D2FE9D9CD5A50725BE978ED846FE3098_marshaled_pinvoke_define
#define NativeArray_1_t350F3793D2FE9D9CD5A50725BE978ED846FE3098_marshaled_pinvoke_define
struct NativeArray_1_t350F3793D2FE9D9CD5A50725BE978ED846FE3098_marshaled_pinvoke
{
	void* ___m_Buffer_0;
	int32_t ___m_Length_1;
	int32_t ___m_AllocatorLabel_2;
};
#endif
// Native definition for COM marshalling of Unity.Collections.NativeArray`1
#ifndef NativeArray_1_t350F3793D2FE9D9CD5A50725BE978ED846FE3098_marshaled_com_define
#define NativeArray_1_t350F3793D2FE9D9CD5A50725BE978ED846FE3098_marshaled_com_define
struct NativeArray_1_t350F3793D2FE9D9CD5A50725BE978ED846FE3098_marshaled_com
{
	void* ___m_Buffer_0;
	int32_t ___m_Length_1;
	int32_t ___m_AllocatorLabel_2;
};
#endif
#endif // NATIVEARRAY_1_TD7797CC5848BA9ECBD1172056474C8DD8022B1D6_H
#ifndef NATIVEARRAY_1_TAA72EF264612AEC585CFAA055B86F6B65CA4E2E5_H
#define NATIVEARRAY_1_TAA72EF264612AEC585CFAA055B86F6B65CA4E2E5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.XRFace>
struct  NativeArray_1_tAA72EF264612AEC585CFAA055B86F6B65CA4E2E5 
{
public:
	// System.Void* Unity.Collections.NativeArray`1::m_Buffer
	void* ___m_Buffer_0;
	// System.Int32 Unity.Collections.NativeArray`1::m_Length
	int32_t ___m_Length_1;
	// Unity.Collections.Allocator Unity.Collections.NativeArray`1::m_AllocatorLabel
	int32_t ___m_AllocatorLabel_2;

public:
	inline static int32_t get_offset_of_m_Buffer_0() { return static_cast<int32_t>(offsetof(NativeArray_1_tAA72EF264612AEC585CFAA055B86F6B65CA4E2E5, ___m_Buffer_0)); }
	inline void* get_m_Buffer_0() const { return ___m_Buffer_0; }
	inline void** get_address_of_m_Buffer_0() { return &___m_Buffer_0; }
	inline void set_m_Buffer_0(void* value)
	{
		___m_Buffer_0 = value;
	}

	inline static int32_t get_offset_of_m_Length_1() { return static_cast<int32_t>(offsetof(NativeArray_1_tAA72EF264612AEC585CFAA055B86F6B65CA4E2E5, ___m_Length_1)); }
	inline int32_t get_m_Length_1() const { return ___m_Length_1; }
	inline int32_t* get_address_of_m_Length_1() { return &___m_Length_1; }
	inline void set_m_Length_1(int32_t value)
	{
		___m_Length_1 = value;
	}

	inline static int32_t get_offset_of_m_AllocatorLabel_2() { return static_cast<int32_t>(offsetof(NativeArray_1_tAA72EF264612AEC585CFAA055B86F6B65CA4E2E5, ___m_AllocatorLabel_2)); }
	inline int32_t get_m_AllocatorLabel_2() const { return ___m_AllocatorLabel_2; }
	inline int32_t* get_address_of_m_AllocatorLabel_2() { return &___m_AllocatorLabel_2; }
	inline void set_m_AllocatorLabel_2(int32_t value)
	{
		___m_AllocatorLabel_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Unity.Collections.NativeArray`1
#ifndef NativeArray_1_t350F3793D2FE9D9CD5A50725BE978ED846FE3098_marshaled_pinvoke_define
#define NativeArray_1_t350F3793D2FE9D9CD5A50725BE978ED846FE3098_marshaled_pinvoke_define
struct NativeArray_1_t350F3793D2FE9D9CD5A50725BE978ED846FE3098_marshaled_pinvoke
{
	void* ___m_Buffer_0;
	int32_t ___m_Length_1;
	int32_t ___m_AllocatorLabel_2;
};
#endif
// Native definition for COM marshalling of Unity.Collections.NativeArray`1
#ifndef NativeArray_1_t350F3793D2FE9D9CD5A50725BE978ED846FE3098_marshaled_com_define
#define NativeArray_1_t350F3793D2FE9D9CD5A50725BE978ED846FE3098_marshaled_com_define
struct NativeArray_1_t350F3793D2FE9D9CD5A50725BE978ED846FE3098_marshaled_com
{
	void* ___m_Buffer_0;
	int32_t ___m_Length_1;
	int32_t ___m_AllocatorLabel_2;
};
#endif
#endif // NATIVEARRAY_1_TAA72EF264612AEC585CFAA055B86F6B65CA4E2E5_H
#ifndef NATIVEARRAY_1_T140FCB442630D549508192A9E3737425E84D15F8_H
#define NATIVEARRAY_1_T140FCB442630D549508192A9E3737425E84D15F8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.XRHumanBody>
struct  NativeArray_1_t140FCB442630D549508192A9E3737425E84D15F8 
{
public:
	// System.Void* Unity.Collections.NativeArray`1::m_Buffer
	void* ___m_Buffer_0;
	// System.Int32 Unity.Collections.NativeArray`1::m_Length
	int32_t ___m_Length_1;
	// Unity.Collections.Allocator Unity.Collections.NativeArray`1::m_AllocatorLabel
	int32_t ___m_AllocatorLabel_2;

public:
	inline static int32_t get_offset_of_m_Buffer_0() { return static_cast<int32_t>(offsetof(NativeArray_1_t140FCB442630D549508192A9E3737425E84D15F8, ___m_Buffer_0)); }
	inline void* get_m_Buffer_0() const { return ___m_Buffer_0; }
	inline void** get_address_of_m_Buffer_0() { return &___m_Buffer_0; }
	inline void set_m_Buffer_0(void* value)
	{
		___m_Buffer_0 = value;
	}

	inline static int32_t get_offset_of_m_Length_1() { return static_cast<int32_t>(offsetof(NativeArray_1_t140FCB442630D549508192A9E3737425E84D15F8, ___m_Length_1)); }
	inline int32_t get_m_Length_1() const { return ___m_Length_1; }
	inline int32_t* get_address_of_m_Length_1() { return &___m_Length_1; }
	inline void set_m_Length_1(int32_t value)
	{
		___m_Length_1 = value;
	}

	inline static int32_t get_offset_of_m_AllocatorLabel_2() { return static_cast<int32_t>(offsetof(NativeArray_1_t140FCB442630D549508192A9E3737425E84D15F8, ___m_AllocatorLabel_2)); }
	inline int32_t get_m_AllocatorLabel_2() const { return ___m_AllocatorLabel_2; }
	inline int32_t* get_address_of_m_AllocatorLabel_2() { return &___m_AllocatorLabel_2; }
	inline void set_m_AllocatorLabel_2(int32_t value)
	{
		___m_AllocatorLabel_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Unity.Collections.NativeArray`1
#ifndef NativeArray_1_t350F3793D2FE9D9CD5A50725BE978ED846FE3098_marshaled_pinvoke_define
#define NativeArray_1_t350F3793D2FE9D9CD5A50725BE978ED846FE3098_marshaled_pinvoke_define
struct NativeArray_1_t350F3793D2FE9D9CD5A50725BE978ED846FE3098_marshaled_pinvoke
{
	void* ___m_Buffer_0;
	int32_t ___m_Length_1;
	int32_t ___m_AllocatorLabel_2;
};
#endif
// Native definition for COM marshalling of Unity.Collections.NativeArray`1
#ifndef NativeArray_1_t350F3793D2FE9D9CD5A50725BE978ED846FE3098_marshaled_com_define
#define NativeArray_1_t350F3793D2FE9D9CD5A50725BE978ED846FE3098_marshaled_com_define
struct NativeArray_1_t350F3793D2FE9D9CD5A50725BE978ED846FE3098_marshaled_com
{
	void* ___m_Buffer_0;
	int32_t ___m_Length_1;
	int32_t ___m_AllocatorLabel_2;
};
#endif
#endif // NATIVEARRAY_1_T140FCB442630D549508192A9E3737425E84D15F8_H
#ifndef NATIVEARRAY_1_T5F0698DD293100E2FECB8BC55FCBC3A3F6FEA582_H
#define NATIVEARRAY_1_T5F0698DD293100E2FECB8BC55FCBC3A3F6FEA582_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.XRPointCloud>
struct  NativeArray_1_t5F0698DD293100E2FECB8BC55FCBC3A3F6FEA582 
{
public:
	// System.Void* Unity.Collections.NativeArray`1::m_Buffer
	void* ___m_Buffer_0;
	// System.Int32 Unity.Collections.NativeArray`1::m_Length
	int32_t ___m_Length_1;
	// Unity.Collections.Allocator Unity.Collections.NativeArray`1::m_AllocatorLabel
	int32_t ___m_AllocatorLabel_2;

public:
	inline static int32_t get_offset_of_m_Buffer_0() { return static_cast<int32_t>(offsetof(NativeArray_1_t5F0698DD293100E2FECB8BC55FCBC3A3F6FEA582, ___m_Buffer_0)); }
	inline void* get_m_Buffer_0() const { return ___m_Buffer_0; }
	inline void** get_address_of_m_Buffer_0() { return &___m_Buffer_0; }
	inline void set_m_Buffer_0(void* value)
	{
		___m_Buffer_0 = value;
	}

	inline static int32_t get_offset_of_m_Length_1() { return static_cast<int32_t>(offsetof(NativeArray_1_t5F0698DD293100E2FECB8BC55FCBC3A3F6FEA582, ___m_Length_1)); }
	inline int32_t get_m_Length_1() const { return ___m_Length_1; }
	inline int32_t* get_address_of_m_Length_1() { return &___m_Length_1; }
	inline void set_m_Length_1(int32_t value)
	{
		___m_Length_1 = value;
	}

	inline static int32_t get_offset_of_m_AllocatorLabel_2() { return static_cast<int32_t>(offsetof(NativeArray_1_t5F0698DD293100E2FECB8BC55FCBC3A3F6FEA582, ___m_AllocatorLabel_2)); }
	inline int32_t get_m_AllocatorLabel_2() const { return ___m_AllocatorLabel_2; }
	inline int32_t* get_address_of_m_AllocatorLabel_2() { return &___m_AllocatorLabel_2; }
	inline void set_m_AllocatorLabel_2(int32_t value)
	{
		___m_AllocatorLabel_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Unity.Collections.NativeArray`1
#ifndef NativeArray_1_t350F3793D2FE9D9CD5A50725BE978ED846FE3098_marshaled_pinvoke_define
#define NativeArray_1_t350F3793D2FE9D9CD5A50725BE978ED846FE3098_marshaled_pinvoke_define
struct NativeArray_1_t350F3793D2FE9D9CD5A50725BE978ED846FE3098_marshaled_pinvoke
{
	void* ___m_Buffer_0;
	int32_t ___m_Length_1;
	int32_t ___m_AllocatorLabel_2;
};
#endif
// Native definition for COM marshalling of Unity.Collections.NativeArray`1
#ifndef NativeArray_1_t350F3793D2FE9D9CD5A50725BE978ED846FE3098_marshaled_com_define
#define NativeArray_1_t350F3793D2FE9D9CD5A50725BE978ED846FE3098_marshaled_com_define
struct NativeArray_1_t350F3793D2FE9D9CD5A50725BE978ED846FE3098_marshaled_com
{
	void* ___m_Buffer_0;
	int32_t ___m_Length_1;
	int32_t ___m_AllocatorLabel_2;
};
#endif
#endif // NATIVEARRAY_1_T5F0698DD293100E2FECB8BC55FCBC3A3F6FEA582_H
#ifndef NATIVEARRAY_1_T5AADFB4C72573FE3017795F15B8CBC88625A8876_H
#define NATIVEARRAY_1_T5AADFB4C72573FE3017795F15B8CBC88625A8876_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.XRReferencePoint>
struct  NativeArray_1_t5AADFB4C72573FE3017795F15B8CBC88625A8876 
{
public:
	// System.Void* Unity.Collections.NativeArray`1::m_Buffer
	void* ___m_Buffer_0;
	// System.Int32 Unity.Collections.NativeArray`1::m_Length
	int32_t ___m_Length_1;
	// Unity.Collections.Allocator Unity.Collections.NativeArray`1::m_AllocatorLabel
	int32_t ___m_AllocatorLabel_2;

public:
	inline static int32_t get_offset_of_m_Buffer_0() { return static_cast<int32_t>(offsetof(NativeArray_1_t5AADFB4C72573FE3017795F15B8CBC88625A8876, ___m_Buffer_0)); }
	inline void* get_m_Buffer_0() const { return ___m_Buffer_0; }
	inline void** get_address_of_m_Buffer_0() { return &___m_Buffer_0; }
	inline void set_m_Buffer_0(void* value)
	{
		___m_Buffer_0 = value;
	}

	inline static int32_t get_offset_of_m_Length_1() { return static_cast<int32_t>(offsetof(NativeArray_1_t5AADFB4C72573FE3017795F15B8CBC88625A8876, ___m_Length_1)); }
	inline int32_t get_m_Length_1() const { return ___m_Length_1; }
	inline int32_t* get_address_of_m_Length_1() { return &___m_Length_1; }
	inline void set_m_Length_1(int32_t value)
	{
		___m_Length_1 = value;
	}

	inline static int32_t get_offset_of_m_AllocatorLabel_2() { return static_cast<int32_t>(offsetof(NativeArray_1_t5AADFB4C72573FE3017795F15B8CBC88625A8876, ___m_AllocatorLabel_2)); }
	inline int32_t get_m_AllocatorLabel_2() const { return ___m_AllocatorLabel_2; }
	inline int32_t* get_address_of_m_AllocatorLabel_2() { return &___m_AllocatorLabel_2; }
	inline void set_m_AllocatorLabel_2(int32_t value)
	{
		___m_AllocatorLabel_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Unity.Collections.NativeArray`1
#ifndef NativeArray_1_t350F3793D2FE9D9CD5A50725BE978ED846FE3098_marshaled_pinvoke_define
#define NativeArray_1_t350F3793D2FE9D9CD5A50725BE978ED846FE3098_marshaled_pinvoke_define
struct NativeArray_1_t350F3793D2FE9D9CD5A50725BE978ED846FE3098_marshaled_pinvoke
{
	void* ___m_Buffer_0;
	int32_t ___m_Length_1;
	int32_t ___m_AllocatorLabel_2;
};
#endif
// Native definition for COM marshalling of Unity.Collections.NativeArray`1
#ifndef NativeArray_1_t350F3793D2FE9D9CD5A50725BE978ED846FE3098_marshaled_com_define
#define NativeArray_1_t350F3793D2FE9D9CD5A50725BE978ED846FE3098_marshaled_com_define
struct NativeArray_1_t350F3793D2FE9D9CD5A50725BE978ED846FE3098_marshaled_com
{
	void* ___m_Buffer_0;
	int32_t ___m_Length_1;
	int32_t ___m_AllocatorLabel_2;
};
#endif
#endif // NATIVEARRAY_1_T5AADFB4C72573FE3017795F15B8CBC88625A8876_H
#ifndef NATIVEARRAY_1_T70F653CBE89924503B639B2438D6FF5973388AC1_H
#define NATIVEARRAY_1_T70F653CBE89924503B639B2438D6FF5973388AC1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.XRTrackedImage>
struct  NativeArray_1_t70F653CBE89924503B639B2438D6FF5973388AC1 
{
public:
	// System.Void* Unity.Collections.NativeArray`1::m_Buffer
	void* ___m_Buffer_0;
	// System.Int32 Unity.Collections.NativeArray`1::m_Length
	int32_t ___m_Length_1;
	// Unity.Collections.Allocator Unity.Collections.NativeArray`1::m_AllocatorLabel
	int32_t ___m_AllocatorLabel_2;

public:
	inline static int32_t get_offset_of_m_Buffer_0() { return static_cast<int32_t>(offsetof(NativeArray_1_t70F653CBE89924503B639B2438D6FF5973388AC1, ___m_Buffer_0)); }
	inline void* get_m_Buffer_0() const { return ___m_Buffer_0; }
	inline void** get_address_of_m_Buffer_0() { return &___m_Buffer_0; }
	inline void set_m_Buffer_0(void* value)
	{
		___m_Buffer_0 = value;
	}

	inline static int32_t get_offset_of_m_Length_1() { return static_cast<int32_t>(offsetof(NativeArray_1_t70F653CBE89924503B639B2438D6FF5973388AC1, ___m_Length_1)); }
	inline int32_t get_m_Length_1() const { return ___m_Length_1; }
	inline int32_t* get_address_of_m_Length_1() { return &___m_Length_1; }
	inline void set_m_Length_1(int32_t value)
	{
		___m_Length_1 = value;
	}

	inline static int32_t get_offset_of_m_AllocatorLabel_2() { return static_cast<int32_t>(offsetof(NativeArray_1_t70F653CBE89924503B639B2438D6FF5973388AC1, ___m_AllocatorLabel_2)); }
	inline int32_t get_m_AllocatorLabel_2() const { return ___m_AllocatorLabel_2; }
	inline int32_t* get_address_of_m_AllocatorLabel_2() { return &___m_AllocatorLabel_2; }
	inline void set_m_AllocatorLabel_2(int32_t value)
	{
		___m_AllocatorLabel_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Unity.Collections.NativeArray`1
#ifndef NativeArray_1_t350F3793D2FE9D9CD5A50725BE978ED846FE3098_marshaled_pinvoke_define
#define NativeArray_1_t350F3793D2FE9D9CD5A50725BE978ED846FE3098_marshaled_pinvoke_define
struct NativeArray_1_t350F3793D2FE9D9CD5A50725BE978ED846FE3098_marshaled_pinvoke
{
	void* ___m_Buffer_0;
	int32_t ___m_Length_1;
	int32_t ___m_AllocatorLabel_2;
};
#endif
// Native definition for COM marshalling of Unity.Collections.NativeArray`1
#ifndef NativeArray_1_t350F3793D2FE9D9CD5A50725BE978ED846FE3098_marshaled_com_define
#define NativeArray_1_t350F3793D2FE9D9CD5A50725BE978ED846FE3098_marshaled_com_define
struct NativeArray_1_t350F3793D2FE9D9CD5A50725BE978ED846FE3098_marshaled_com
{
	void* ___m_Buffer_0;
	int32_t ___m_Length_1;
	int32_t ___m_AllocatorLabel_2;
};
#endif
#endif // NATIVEARRAY_1_T70F653CBE89924503B639B2438D6FF5973388AC1_H
#ifndef NATIVEARRAY_1_T738892D030F76D319525B8C000F49D41505DD529_H
#define NATIVEARRAY_1_T738892D030F76D319525B8C000F49D41505DD529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.XRTrackedObject>
struct  NativeArray_1_t738892D030F76D319525B8C000F49D41505DD529 
{
public:
	// System.Void* Unity.Collections.NativeArray`1::m_Buffer
	void* ___m_Buffer_0;
	// System.Int32 Unity.Collections.NativeArray`1::m_Length
	int32_t ___m_Length_1;
	// Unity.Collections.Allocator Unity.Collections.NativeArray`1::m_AllocatorLabel
	int32_t ___m_AllocatorLabel_2;

public:
	inline static int32_t get_offset_of_m_Buffer_0() { return static_cast<int32_t>(offsetof(NativeArray_1_t738892D030F76D319525B8C000F49D41505DD529, ___m_Buffer_0)); }
	inline void* get_m_Buffer_0() const { return ___m_Buffer_0; }
	inline void** get_address_of_m_Buffer_0() { return &___m_Buffer_0; }
	inline void set_m_Buffer_0(void* value)
	{
		___m_Buffer_0 = value;
	}

	inline static int32_t get_offset_of_m_Length_1() { return static_cast<int32_t>(offsetof(NativeArray_1_t738892D030F76D319525B8C000F49D41505DD529, ___m_Length_1)); }
	inline int32_t get_m_Length_1() const { return ___m_Length_1; }
	inline int32_t* get_address_of_m_Length_1() { return &___m_Length_1; }
	inline void set_m_Length_1(int32_t value)
	{
		___m_Length_1 = value;
	}

	inline static int32_t get_offset_of_m_AllocatorLabel_2() { return static_cast<int32_t>(offsetof(NativeArray_1_t738892D030F76D319525B8C000F49D41505DD529, ___m_AllocatorLabel_2)); }
	inline int32_t get_m_AllocatorLabel_2() const { return ___m_AllocatorLabel_2; }
	inline int32_t* get_address_of_m_AllocatorLabel_2() { return &___m_AllocatorLabel_2; }
	inline void set_m_AllocatorLabel_2(int32_t value)
	{
		___m_AllocatorLabel_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Unity.Collections.NativeArray`1
#ifndef NativeArray_1_t350F3793D2FE9D9CD5A50725BE978ED846FE3098_marshaled_pinvoke_define
#define NativeArray_1_t350F3793D2FE9D9CD5A50725BE978ED846FE3098_marshaled_pinvoke_define
struct NativeArray_1_t350F3793D2FE9D9CD5A50725BE978ED846FE3098_marshaled_pinvoke
{
	void* ___m_Buffer_0;
	int32_t ___m_Length_1;
	int32_t ___m_AllocatorLabel_2;
};
#endif
// Native definition for COM marshalling of Unity.Collections.NativeArray`1
#ifndef NativeArray_1_t350F3793D2FE9D9CD5A50725BE978ED846FE3098_marshaled_com_define
#define NativeArray_1_t350F3793D2FE9D9CD5A50725BE978ED846FE3098_marshaled_com_define
struct NativeArray_1_t350F3793D2FE9D9CD5A50725BE978ED846FE3098_marshaled_com
{
	void* ___m_Buffer_0;
	int32_t ___m_Length_1;
	int32_t ___m_AllocatorLabel_2;
};
#endif
#endif // NATIVEARRAY_1_T738892D030F76D319525B8C000F49D41505DD529_H
#ifndef XRFACE_TF2B2E9B06813BA74F5DAFD527FD249DD1002B7C7_H
#define XRFACE_TF2B2E9B06813BA74F5DAFD527FD249DD1002B7C7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARSubsystems.XRFace
struct  XRFace_tF2B2E9B06813BA74F5DAFD527FD249DD1002B7C7 
{
public:
	// UnityEngine.XR.ARSubsystems.TrackableId UnityEngine.XR.ARSubsystems.XRFace::m_TrackableId
	TrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47  ___m_TrackableId_0;
	// UnityEngine.Pose UnityEngine.XR.ARSubsystems.XRFace::m_Pose
	Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29  ___m_Pose_1;
	// UnityEngine.XR.ARSubsystems.TrackingState UnityEngine.XR.ARSubsystems.XRFace::m_TrackingState
	int32_t ___m_TrackingState_2;
	// System.IntPtr UnityEngine.XR.ARSubsystems.XRFace::m_NativePtr
	intptr_t ___m_NativePtr_3;

public:
	inline static int32_t get_offset_of_m_TrackableId_0() { return static_cast<int32_t>(offsetof(XRFace_tF2B2E9B06813BA74F5DAFD527FD249DD1002B7C7, ___m_TrackableId_0)); }
	inline TrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47  get_m_TrackableId_0() const { return ___m_TrackableId_0; }
	inline TrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47 * get_address_of_m_TrackableId_0() { return &___m_TrackableId_0; }
	inline void set_m_TrackableId_0(TrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47  value)
	{
		___m_TrackableId_0 = value;
	}

	inline static int32_t get_offset_of_m_Pose_1() { return static_cast<int32_t>(offsetof(XRFace_tF2B2E9B06813BA74F5DAFD527FD249DD1002B7C7, ___m_Pose_1)); }
	inline Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29  get_m_Pose_1() const { return ___m_Pose_1; }
	inline Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29 * get_address_of_m_Pose_1() { return &___m_Pose_1; }
	inline void set_m_Pose_1(Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29  value)
	{
		___m_Pose_1 = value;
	}

	inline static int32_t get_offset_of_m_TrackingState_2() { return static_cast<int32_t>(offsetof(XRFace_tF2B2E9B06813BA74F5DAFD527FD249DD1002B7C7, ___m_TrackingState_2)); }
	inline int32_t get_m_TrackingState_2() const { return ___m_TrackingState_2; }
	inline int32_t* get_address_of_m_TrackingState_2() { return &___m_TrackingState_2; }
	inline void set_m_TrackingState_2(int32_t value)
	{
		___m_TrackingState_2 = value;
	}

	inline static int32_t get_offset_of_m_NativePtr_3() { return static_cast<int32_t>(offsetof(XRFace_tF2B2E9B06813BA74F5DAFD527FD249DD1002B7C7, ___m_NativePtr_3)); }
	inline intptr_t get_m_NativePtr_3() const { return ___m_NativePtr_3; }
	inline intptr_t* get_address_of_m_NativePtr_3() { return &___m_NativePtr_3; }
	inline void set_m_NativePtr_3(intptr_t value)
	{
		___m_NativePtr_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XRFACE_TF2B2E9B06813BA74F5DAFD527FD249DD1002B7C7_H
#ifndef XRHUMANBODY_T64938399EB40376E99745517E15D42FFC4604BC1_H
#define XRHUMANBODY_T64938399EB40376E99745517E15D42FFC4604BC1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARSubsystems.XRHumanBody
struct  XRHumanBody_t64938399EB40376E99745517E15D42FFC4604BC1 
{
public:
	// UnityEngine.XR.ARSubsystems.TrackableId UnityEngine.XR.ARSubsystems.XRHumanBody::m_TrackableId
	TrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47  ___m_TrackableId_0;
	// UnityEngine.Pose UnityEngine.XR.ARSubsystems.XRHumanBody::m_Pose
	Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29  ___m_Pose_1;
	// UnityEngine.XR.ARSubsystems.TrackingState UnityEngine.XR.ARSubsystems.XRHumanBody::m_TrackingState
	int32_t ___m_TrackingState_2;
	// System.IntPtr UnityEngine.XR.ARSubsystems.XRHumanBody::m_NativePtr
	intptr_t ___m_NativePtr_3;

public:
	inline static int32_t get_offset_of_m_TrackableId_0() { return static_cast<int32_t>(offsetof(XRHumanBody_t64938399EB40376E99745517E15D42FFC4604BC1, ___m_TrackableId_0)); }
	inline TrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47  get_m_TrackableId_0() const { return ___m_TrackableId_0; }
	inline TrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47 * get_address_of_m_TrackableId_0() { return &___m_TrackableId_0; }
	inline void set_m_TrackableId_0(TrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47  value)
	{
		___m_TrackableId_0 = value;
	}

	inline static int32_t get_offset_of_m_Pose_1() { return static_cast<int32_t>(offsetof(XRHumanBody_t64938399EB40376E99745517E15D42FFC4604BC1, ___m_Pose_1)); }
	inline Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29  get_m_Pose_1() const { return ___m_Pose_1; }
	inline Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29 * get_address_of_m_Pose_1() { return &___m_Pose_1; }
	inline void set_m_Pose_1(Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29  value)
	{
		___m_Pose_1 = value;
	}

	inline static int32_t get_offset_of_m_TrackingState_2() { return static_cast<int32_t>(offsetof(XRHumanBody_t64938399EB40376E99745517E15D42FFC4604BC1, ___m_TrackingState_2)); }
	inline int32_t get_m_TrackingState_2() const { return ___m_TrackingState_2; }
	inline int32_t* get_address_of_m_TrackingState_2() { return &___m_TrackingState_2; }
	inline void set_m_TrackingState_2(int32_t value)
	{
		___m_TrackingState_2 = value;
	}

	inline static int32_t get_offset_of_m_NativePtr_3() { return static_cast<int32_t>(offsetof(XRHumanBody_t64938399EB40376E99745517E15D42FFC4604BC1, ___m_NativePtr_3)); }
	inline intptr_t get_m_NativePtr_3() const { return ___m_NativePtr_3; }
	inline intptr_t* get_address_of_m_NativePtr_3() { return &___m_NativePtr_3; }
	inline void set_m_NativePtr_3(intptr_t value)
	{
		___m_NativePtr_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XRHUMANBODY_T64938399EB40376E99745517E15D42FFC4604BC1_H
#ifndef XRPOINTCLOUD_TA4A412DE503530E1B2953919F1463B9B48504ED0_H
#define XRPOINTCLOUD_TA4A412DE503530E1B2953919F1463B9B48504ED0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARSubsystems.XRPointCloud
struct  XRPointCloud_tA4A412DE503530E1B2953919F1463B9B48504ED0 
{
public:
	// UnityEngine.XR.ARSubsystems.TrackableId UnityEngine.XR.ARSubsystems.XRPointCloud::m_TrackableId
	TrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47  ___m_TrackableId_0;
	// UnityEngine.Pose UnityEngine.XR.ARSubsystems.XRPointCloud::m_Pose
	Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29  ___m_Pose_1;
	// UnityEngine.XR.ARSubsystems.TrackingState UnityEngine.XR.ARSubsystems.XRPointCloud::m_TrackingState
	int32_t ___m_TrackingState_2;
	// System.IntPtr UnityEngine.XR.ARSubsystems.XRPointCloud::m_NativePtr
	intptr_t ___m_NativePtr_3;

public:
	inline static int32_t get_offset_of_m_TrackableId_0() { return static_cast<int32_t>(offsetof(XRPointCloud_tA4A412DE503530E1B2953919F1463B9B48504ED0, ___m_TrackableId_0)); }
	inline TrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47  get_m_TrackableId_0() const { return ___m_TrackableId_0; }
	inline TrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47 * get_address_of_m_TrackableId_0() { return &___m_TrackableId_0; }
	inline void set_m_TrackableId_0(TrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47  value)
	{
		___m_TrackableId_0 = value;
	}

	inline static int32_t get_offset_of_m_Pose_1() { return static_cast<int32_t>(offsetof(XRPointCloud_tA4A412DE503530E1B2953919F1463B9B48504ED0, ___m_Pose_1)); }
	inline Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29  get_m_Pose_1() const { return ___m_Pose_1; }
	inline Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29 * get_address_of_m_Pose_1() { return &___m_Pose_1; }
	inline void set_m_Pose_1(Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29  value)
	{
		___m_Pose_1 = value;
	}

	inline static int32_t get_offset_of_m_TrackingState_2() { return static_cast<int32_t>(offsetof(XRPointCloud_tA4A412DE503530E1B2953919F1463B9B48504ED0, ___m_TrackingState_2)); }
	inline int32_t get_m_TrackingState_2() const { return ___m_TrackingState_2; }
	inline int32_t* get_address_of_m_TrackingState_2() { return &___m_TrackingState_2; }
	inline void set_m_TrackingState_2(int32_t value)
	{
		___m_TrackingState_2 = value;
	}

	inline static int32_t get_offset_of_m_NativePtr_3() { return static_cast<int32_t>(offsetof(XRPointCloud_tA4A412DE503530E1B2953919F1463B9B48504ED0, ___m_NativePtr_3)); }
	inline intptr_t get_m_NativePtr_3() const { return ___m_NativePtr_3; }
	inline intptr_t* get_address_of_m_NativePtr_3() { return &___m_NativePtr_3; }
	inline void set_m_NativePtr_3(intptr_t value)
	{
		___m_NativePtr_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XRPOINTCLOUD_TA4A412DE503530E1B2953919F1463B9B48504ED0_H
#ifndef XRREFERENCEPOINT_TA8592C08A27EC91D9B1FB3B083C95C5D372FF1F9_H
#define XRREFERENCEPOINT_TA8592C08A27EC91D9B1FB3B083C95C5D372FF1F9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARSubsystems.XRReferencePoint
struct  XRReferencePoint_tA8592C08A27EC91D9B1FB3B083C95C5D372FF1F9 
{
public:
	// UnityEngine.XR.ARSubsystems.TrackableId UnityEngine.XR.ARSubsystems.XRReferencePoint::m_Id
	TrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47  ___m_Id_0;
	// UnityEngine.Pose UnityEngine.XR.ARSubsystems.XRReferencePoint::m_Pose
	Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29  ___m_Pose_1;
	// UnityEngine.XR.ARSubsystems.TrackingState UnityEngine.XR.ARSubsystems.XRReferencePoint::m_TrackingState
	int32_t ___m_TrackingState_2;
	// System.IntPtr UnityEngine.XR.ARSubsystems.XRReferencePoint::m_NativePtr
	intptr_t ___m_NativePtr_3;
	// System.Guid UnityEngine.XR.ARSubsystems.XRReferencePoint::m_SessionId
	Guid_t  ___m_SessionId_4;

public:
	inline static int32_t get_offset_of_m_Id_0() { return static_cast<int32_t>(offsetof(XRReferencePoint_tA8592C08A27EC91D9B1FB3B083C95C5D372FF1F9, ___m_Id_0)); }
	inline TrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47  get_m_Id_0() const { return ___m_Id_0; }
	inline TrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47 * get_address_of_m_Id_0() { return &___m_Id_0; }
	inline void set_m_Id_0(TrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47  value)
	{
		___m_Id_0 = value;
	}

	inline static int32_t get_offset_of_m_Pose_1() { return static_cast<int32_t>(offsetof(XRReferencePoint_tA8592C08A27EC91D9B1FB3B083C95C5D372FF1F9, ___m_Pose_1)); }
	inline Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29  get_m_Pose_1() const { return ___m_Pose_1; }
	inline Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29 * get_address_of_m_Pose_1() { return &___m_Pose_1; }
	inline void set_m_Pose_1(Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29  value)
	{
		___m_Pose_1 = value;
	}

	inline static int32_t get_offset_of_m_TrackingState_2() { return static_cast<int32_t>(offsetof(XRReferencePoint_tA8592C08A27EC91D9B1FB3B083C95C5D372FF1F9, ___m_TrackingState_2)); }
	inline int32_t get_m_TrackingState_2() const { return ___m_TrackingState_2; }
	inline int32_t* get_address_of_m_TrackingState_2() { return &___m_TrackingState_2; }
	inline void set_m_TrackingState_2(int32_t value)
	{
		___m_TrackingState_2 = value;
	}

	inline static int32_t get_offset_of_m_NativePtr_3() { return static_cast<int32_t>(offsetof(XRReferencePoint_tA8592C08A27EC91D9B1FB3B083C95C5D372FF1F9, ___m_NativePtr_3)); }
	inline intptr_t get_m_NativePtr_3() const { return ___m_NativePtr_3; }
	inline intptr_t* get_address_of_m_NativePtr_3() { return &___m_NativePtr_3; }
	inline void set_m_NativePtr_3(intptr_t value)
	{
		___m_NativePtr_3 = value;
	}

	inline static int32_t get_offset_of_m_SessionId_4() { return static_cast<int32_t>(offsetof(XRReferencePoint_tA8592C08A27EC91D9B1FB3B083C95C5D372FF1F9, ___m_SessionId_4)); }
	inline Guid_t  get_m_SessionId_4() const { return ___m_SessionId_4; }
	inline Guid_t * get_address_of_m_SessionId_4() { return &___m_SessionId_4; }
	inline void set_m_SessionId_4(Guid_t  value)
	{
		___m_SessionId_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XRREFERENCEPOINT_TA8592C08A27EC91D9B1FB3B083C95C5D372FF1F9_H
#ifndef XRTRACKEDIMAGE_T178EACF5BFA4228DF4EB1899685C533F3F296AA8_H
#define XRTRACKEDIMAGE_T178EACF5BFA4228DF4EB1899685C533F3F296AA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARSubsystems.XRTrackedImage
struct  XRTrackedImage_t178EACF5BFA4228DF4EB1899685C533F3F296AA8 
{
public:
	// UnityEngine.XR.ARSubsystems.TrackableId UnityEngine.XR.ARSubsystems.XRTrackedImage::m_Id
	TrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47  ___m_Id_0;
	// System.Guid UnityEngine.XR.ARSubsystems.XRTrackedImage::m_SourceImageId
	Guid_t  ___m_SourceImageId_1;
	// UnityEngine.Pose UnityEngine.XR.ARSubsystems.XRTrackedImage::m_Pose
	Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29  ___m_Pose_2;
	// UnityEngine.Vector2 UnityEngine.XR.ARSubsystems.XRTrackedImage::m_Size
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_Size_3;
	// UnityEngine.XR.ARSubsystems.TrackingState UnityEngine.XR.ARSubsystems.XRTrackedImage::m_TrackingState
	int32_t ___m_TrackingState_4;
	// System.IntPtr UnityEngine.XR.ARSubsystems.XRTrackedImage::m_NativePtr
	intptr_t ___m_NativePtr_5;

public:
	inline static int32_t get_offset_of_m_Id_0() { return static_cast<int32_t>(offsetof(XRTrackedImage_t178EACF5BFA4228DF4EB1899685C533F3F296AA8, ___m_Id_0)); }
	inline TrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47  get_m_Id_0() const { return ___m_Id_0; }
	inline TrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47 * get_address_of_m_Id_0() { return &___m_Id_0; }
	inline void set_m_Id_0(TrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47  value)
	{
		___m_Id_0 = value;
	}

	inline static int32_t get_offset_of_m_SourceImageId_1() { return static_cast<int32_t>(offsetof(XRTrackedImage_t178EACF5BFA4228DF4EB1899685C533F3F296AA8, ___m_SourceImageId_1)); }
	inline Guid_t  get_m_SourceImageId_1() const { return ___m_SourceImageId_1; }
	inline Guid_t * get_address_of_m_SourceImageId_1() { return &___m_SourceImageId_1; }
	inline void set_m_SourceImageId_1(Guid_t  value)
	{
		___m_SourceImageId_1 = value;
	}

	inline static int32_t get_offset_of_m_Pose_2() { return static_cast<int32_t>(offsetof(XRTrackedImage_t178EACF5BFA4228DF4EB1899685C533F3F296AA8, ___m_Pose_2)); }
	inline Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29  get_m_Pose_2() const { return ___m_Pose_2; }
	inline Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29 * get_address_of_m_Pose_2() { return &___m_Pose_2; }
	inline void set_m_Pose_2(Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29  value)
	{
		___m_Pose_2 = value;
	}

	inline static int32_t get_offset_of_m_Size_3() { return static_cast<int32_t>(offsetof(XRTrackedImage_t178EACF5BFA4228DF4EB1899685C533F3F296AA8, ___m_Size_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_Size_3() const { return ___m_Size_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_Size_3() { return &___m_Size_3; }
	inline void set_m_Size_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_Size_3 = value;
	}

	inline static int32_t get_offset_of_m_TrackingState_4() { return static_cast<int32_t>(offsetof(XRTrackedImage_t178EACF5BFA4228DF4EB1899685C533F3F296AA8, ___m_TrackingState_4)); }
	inline int32_t get_m_TrackingState_4() const { return ___m_TrackingState_4; }
	inline int32_t* get_address_of_m_TrackingState_4() { return &___m_TrackingState_4; }
	inline void set_m_TrackingState_4(int32_t value)
	{
		___m_TrackingState_4 = value;
	}

	inline static int32_t get_offset_of_m_NativePtr_5() { return static_cast<int32_t>(offsetof(XRTrackedImage_t178EACF5BFA4228DF4EB1899685C533F3F296AA8, ___m_NativePtr_5)); }
	inline intptr_t get_m_NativePtr_5() const { return ___m_NativePtr_5; }
	inline intptr_t* get_address_of_m_NativePtr_5() { return &___m_NativePtr_5; }
	inline void set_m_NativePtr_5(intptr_t value)
	{
		___m_NativePtr_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XRTRACKEDIMAGE_T178EACF5BFA4228DF4EB1899685C533F3F296AA8_H
#ifndef XRTRACKEDOBJECT_TF08D3523D17E214EC3D7DBE8ED5BFE220BAAE260_H
#define XRTRACKEDOBJECT_TF08D3523D17E214EC3D7DBE8ED5BFE220BAAE260_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARSubsystems.XRTrackedObject
struct  XRTrackedObject_tF08D3523D17E214EC3D7DBE8ED5BFE220BAAE260 
{
public:
	// UnityEngine.XR.ARSubsystems.TrackableId UnityEngine.XR.ARSubsystems.XRTrackedObject::m_TrackableId
	TrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47  ___m_TrackableId_0;
	// UnityEngine.Pose UnityEngine.XR.ARSubsystems.XRTrackedObject::m_Pose
	Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29  ___m_Pose_1;
	// UnityEngine.XR.ARSubsystems.TrackingState UnityEngine.XR.ARSubsystems.XRTrackedObject::m_TrackingState
	int32_t ___m_TrackingState_2;
	// System.IntPtr UnityEngine.XR.ARSubsystems.XRTrackedObject::m_NativePtr
	intptr_t ___m_NativePtr_3;
	// System.Guid UnityEngine.XR.ARSubsystems.XRTrackedObject::m_ReferenceObjectGuid
	Guid_t  ___m_ReferenceObjectGuid_4;

public:
	inline static int32_t get_offset_of_m_TrackableId_0() { return static_cast<int32_t>(offsetof(XRTrackedObject_tF08D3523D17E214EC3D7DBE8ED5BFE220BAAE260, ___m_TrackableId_0)); }
	inline TrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47  get_m_TrackableId_0() const { return ___m_TrackableId_0; }
	inline TrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47 * get_address_of_m_TrackableId_0() { return &___m_TrackableId_0; }
	inline void set_m_TrackableId_0(TrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47  value)
	{
		___m_TrackableId_0 = value;
	}

	inline static int32_t get_offset_of_m_Pose_1() { return static_cast<int32_t>(offsetof(XRTrackedObject_tF08D3523D17E214EC3D7DBE8ED5BFE220BAAE260, ___m_Pose_1)); }
	inline Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29  get_m_Pose_1() const { return ___m_Pose_1; }
	inline Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29 * get_address_of_m_Pose_1() { return &___m_Pose_1; }
	inline void set_m_Pose_1(Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29  value)
	{
		___m_Pose_1 = value;
	}

	inline static int32_t get_offset_of_m_TrackingState_2() { return static_cast<int32_t>(offsetof(XRTrackedObject_tF08D3523D17E214EC3D7DBE8ED5BFE220BAAE260, ___m_TrackingState_2)); }
	inline int32_t get_m_TrackingState_2() const { return ___m_TrackingState_2; }
	inline int32_t* get_address_of_m_TrackingState_2() { return &___m_TrackingState_2; }
	inline void set_m_TrackingState_2(int32_t value)
	{
		___m_TrackingState_2 = value;
	}

	inline static int32_t get_offset_of_m_NativePtr_3() { return static_cast<int32_t>(offsetof(XRTrackedObject_tF08D3523D17E214EC3D7DBE8ED5BFE220BAAE260, ___m_NativePtr_3)); }
	inline intptr_t get_m_NativePtr_3() const { return ___m_NativePtr_3; }
	inline intptr_t* get_address_of_m_NativePtr_3() { return &___m_NativePtr_3; }
	inline void set_m_NativePtr_3(intptr_t value)
	{
		___m_NativePtr_3 = value;
	}

	inline static int32_t get_offset_of_m_ReferenceObjectGuid_4() { return static_cast<int32_t>(offsetof(XRTrackedObject_tF08D3523D17E214EC3D7DBE8ED5BFE220BAAE260, ___m_ReferenceObjectGuid_4)); }
	inline Guid_t  get_m_ReferenceObjectGuid_4() const { return ___m_ReferenceObjectGuid_4; }
	inline Guid_t * get_address_of_m_ReferenceObjectGuid_4() { return &___m_ReferenceObjectGuid_4; }
	inline void set_m_ReferenceObjectGuid_4(Guid_t  value)
	{
		___m_ReferenceObjectGuid_4 = value;
	}
};

struct XRTrackedObject_tF08D3523D17E214EC3D7DBE8ED5BFE220BAAE260_StaticFields
{
public:
	// UnityEngine.XR.ARSubsystems.XRTrackedObject UnityEngine.XR.ARSubsystems.XRTrackedObject::k_Default
	XRTrackedObject_tF08D3523D17E214EC3D7DBE8ED5BFE220BAAE260  ___k_Default_5;

public:
	inline static int32_t get_offset_of_k_Default_5() { return static_cast<int32_t>(offsetof(XRTrackedObject_tF08D3523D17E214EC3D7DBE8ED5BFE220BAAE260_StaticFields, ___k_Default_5)); }
	inline XRTrackedObject_tF08D3523D17E214EC3D7DBE8ED5BFE220BAAE260  get_k_Default_5() const { return ___k_Default_5; }
	inline XRTrackedObject_tF08D3523D17E214EC3D7DBE8ED5BFE220BAAE260 * get_address_of_k_Default_5() { return &___k_Default_5; }
	inline void set_k_Default_5(XRTrackedObject_tF08D3523D17E214EC3D7DBE8ED5BFE220BAAE260  value)
	{
		___k_Default_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XRTRACKEDOBJECT_TF08D3523D17E214EC3D7DBE8ED5BFE220BAAE260_H
#ifndef TRACKABLECHANGES_1_T316F273AB927198D6305E965CDB7B0ED758920E6_H
#define TRACKABLECHANGES_1_T316F273AB927198D6305E965CDB7B0ED758920E6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRFace>
struct  TrackableChanges_1_t316F273AB927198D6305E965CDB7B0ED758920E6 
{
public:
	// System.Boolean UnityEngine.XR.ARSubsystems.TrackableChanges`1::<isCreated>k__BackingField
	bool ___U3CisCreatedU3Ek__BackingField_0;
	// Unity.Collections.NativeArray`1<T> UnityEngine.XR.ARSubsystems.TrackableChanges`1::m_Added
	NativeArray_1_tAA72EF264612AEC585CFAA055B86F6B65CA4E2E5  ___m_Added_1;
	// Unity.Collections.NativeArray`1<T> UnityEngine.XR.ARSubsystems.TrackableChanges`1::m_Updated
	NativeArray_1_tAA72EF264612AEC585CFAA055B86F6B65CA4E2E5  ___m_Updated_2;
	// Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.TrackableId> UnityEngine.XR.ARSubsystems.TrackableChanges`1::m_Removed
	NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  ___m_Removed_3;

public:
	inline static int32_t get_offset_of_U3CisCreatedU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(TrackableChanges_1_t316F273AB927198D6305E965CDB7B0ED758920E6, ___U3CisCreatedU3Ek__BackingField_0)); }
	inline bool get_U3CisCreatedU3Ek__BackingField_0() const { return ___U3CisCreatedU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CisCreatedU3Ek__BackingField_0() { return &___U3CisCreatedU3Ek__BackingField_0; }
	inline void set_U3CisCreatedU3Ek__BackingField_0(bool value)
	{
		___U3CisCreatedU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_m_Added_1() { return static_cast<int32_t>(offsetof(TrackableChanges_1_t316F273AB927198D6305E965CDB7B0ED758920E6, ___m_Added_1)); }
	inline NativeArray_1_tAA72EF264612AEC585CFAA055B86F6B65CA4E2E5  get_m_Added_1() const { return ___m_Added_1; }
	inline NativeArray_1_tAA72EF264612AEC585CFAA055B86F6B65CA4E2E5 * get_address_of_m_Added_1() { return &___m_Added_1; }
	inline void set_m_Added_1(NativeArray_1_tAA72EF264612AEC585CFAA055B86F6B65CA4E2E5  value)
	{
		___m_Added_1 = value;
	}

	inline static int32_t get_offset_of_m_Updated_2() { return static_cast<int32_t>(offsetof(TrackableChanges_1_t316F273AB927198D6305E965CDB7B0ED758920E6, ___m_Updated_2)); }
	inline NativeArray_1_tAA72EF264612AEC585CFAA055B86F6B65CA4E2E5  get_m_Updated_2() const { return ___m_Updated_2; }
	inline NativeArray_1_tAA72EF264612AEC585CFAA055B86F6B65CA4E2E5 * get_address_of_m_Updated_2() { return &___m_Updated_2; }
	inline void set_m_Updated_2(NativeArray_1_tAA72EF264612AEC585CFAA055B86F6B65CA4E2E5  value)
	{
		___m_Updated_2 = value;
	}

	inline static int32_t get_offset_of_m_Removed_3() { return static_cast<int32_t>(offsetof(TrackableChanges_1_t316F273AB927198D6305E965CDB7B0ED758920E6, ___m_Removed_3)); }
	inline NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  get_m_Removed_3() const { return ___m_Removed_3; }
	inline NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 * get_address_of_m_Removed_3() { return &___m_Removed_3; }
	inline void set_m_Removed_3(NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  value)
	{
		___m_Removed_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLECHANGES_1_T316F273AB927198D6305E965CDB7B0ED758920E6_H
#ifndef TRACKABLECHANGES_1_T8F77CF9D37FD0DCA052A7D6B34E1A489AF13C41B_H
#define TRACKABLECHANGES_1_T8F77CF9D37FD0DCA052A7D6B34E1A489AF13C41B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRHumanBody>
struct  TrackableChanges_1_t8F77CF9D37FD0DCA052A7D6B34E1A489AF13C41B 
{
public:
	// System.Boolean UnityEngine.XR.ARSubsystems.TrackableChanges`1::<isCreated>k__BackingField
	bool ___U3CisCreatedU3Ek__BackingField_0;
	// Unity.Collections.NativeArray`1<T> UnityEngine.XR.ARSubsystems.TrackableChanges`1::m_Added
	NativeArray_1_t140FCB442630D549508192A9E3737425E84D15F8  ___m_Added_1;
	// Unity.Collections.NativeArray`1<T> UnityEngine.XR.ARSubsystems.TrackableChanges`1::m_Updated
	NativeArray_1_t140FCB442630D549508192A9E3737425E84D15F8  ___m_Updated_2;
	// Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.TrackableId> UnityEngine.XR.ARSubsystems.TrackableChanges`1::m_Removed
	NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  ___m_Removed_3;

public:
	inline static int32_t get_offset_of_U3CisCreatedU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(TrackableChanges_1_t8F77CF9D37FD0DCA052A7D6B34E1A489AF13C41B, ___U3CisCreatedU3Ek__BackingField_0)); }
	inline bool get_U3CisCreatedU3Ek__BackingField_0() const { return ___U3CisCreatedU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CisCreatedU3Ek__BackingField_0() { return &___U3CisCreatedU3Ek__BackingField_0; }
	inline void set_U3CisCreatedU3Ek__BackingField_0(bool value)
	{
		___U3CisCreatedU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_m_Added_1() { return static_cast<int32_t>(offsetof(TrackableChanges_1_t8F77CF9D37FD0DCA052A7D6B34E1A489AF13C41B, ___m_Added_1)); }
	inline NativeArray_1_t140FCB442630D549508192A9E3737425E84D15F8  get_m_Added_1() const { return ___m_Added_1; }
	inline NativeArray_1_t140FCB442630D549508192A9E3737425E84D15F8 * get_address_of_m_Added_1() { return &___m_Added_1; }
	inline void set_m_Added_1(NativeArray_1_t140FCB442630D549508192A9E3737425E84D15F8  value)
	{
		___m_Added_1 = value;
	}

	inline static int32_t get_offset_of_m_Updated_2() { return static_cast<int32_t>(offsetof(TrackableChanges_1_t8F77CF9D37FD0DCA052A7D6B34E1A489AF13C41B, ___m_Updated_2)); }
	inline NativeArray_1_t140FCB442630D549508192A9E3737425E84D15F8  get_m_Updated_2() const { return ___m_Updated_2; }
	inline NativeArray_1_t140FCB442630D549508192A9E3737425E84D15F8 * get_address_of_m_Updated_2() { return &___m_Updated_2; }
	inline void set_m_Updated_2(NativeArray_1_t140FCB442630D549508192A9E3737425E84D15F8  value)
	{
		___m_Updated_2 = value;
	}

	inline static int32_t get_offset_of_m_Removed_3() { return static_cast<int32_t>(offsetof(TrackableChanges_1_t8F77CF9D37FD0DCA052A7D6B34E1A489AF13C41B, ___m_Removed_3)); }
	inline NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  get_m_Removed_3() const { return ___m_Removed_3; }
	inline NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 * get_address_of_m_Removed_3() { return &___m_Removed_3; }
	inline void set_m_Removed_3(NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  value)
	{
		___m_Removed_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLECHANGES_1_T8F77CF9D37FD0DCA052A7D6B34E1A489AF13C41B_H
#ifndef TRACKABLECHANGES_1_T67F5E50C0B7A063774AEC211C4C81BA7679876BB_H
#define TRACKABLECHANGES_1_T67F5E50C0B7A063774AEC211C4C81BA7679876BB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRPointCloud>
struct  TrackableChanges_1_t67F5E50C0B7A063774AEC211C4C81BA7679876BB 
{
public:
	// System.Boolean UnityEngine.XR.ARSubsystems.TrackableChanges`1::<isCreated>k__BackingField
	bool ___U3CisCreatedU3Ek__BackingField_0;
	// Unity.Collections.NativeArray`1<T> UnityEngine.XR.ARSubsystems.TrackableChanges`1::m_Added
	NativeArray_1_t5F0698DD293100E2FECB8BC55FCBC3A3F6FEA582  ___m_Added_1;
	// Unity.Collections.NativeArray`1<T> UnityEngine.XR.ARSubsystems.TrackableChanges`1::m_Updated
	NativeArray_1_t5F0698DD293100E2FECB8BC55FCBC3A3F6FEA582  ___m_Updated_2;
	// Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.TrackableId> UnityEngine.XR.ARSubsystems.TrackableChanges`1::m_Removed
	NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  ___m_Removed_3;

public:
	inline static int32_t get_offset_of_U3CisCreatedU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(TrackableChanges_1_t67F5E50C0B7A063774AEC211C4C81BA7679876BB, ___U3CisCreatedU3Ek__BackingField_0)); }
	inline bool get_U3CisCreatedU3Ek__BackingField_0() const { return ___U3CisCreatedU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CisCreatedU3Ek__BackingField_0() { return &___U3CisCreatedU3Ek__BackingField_0; }
	inline void set_U3CisCreatedU3Ek__BackingField_0(bool value)
	{
		___U3CisCreatedU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_m_Added_1() { return static_cast<int32_t>(offsetof(TrackableChanges_1_t67F5E50C0B7A063774AEC211C4C81BA7679876BB, ___m_Added_1)); }
	inline NativeArray_1_t5F0698DD293100E2FECB8BC55FCBC3A3F6FEA582  get_m_Added_1() const { return ___m_Added_1; }
	inline NativeArray_1_t5F0698DD293100E2FECB8BC55FCBC3A3F6FEA582 * get_address_of_m_Added_1() { return &___m_Added_1; }
	inline void set_m_Added_1(NativeArray_1_t5F0698DD293100E2FECB8BC55FCBC3A3F6FEA582  value)
	{
		___m_Added_1 = value;
	}

	inline static int32_t get_offset_of_m_Updated_2() { return static_cast<int32_t>(offsetof(TrackableChanges_1_t67F5E50C0B7A063774AEC211C4C81BA7679876BB, ___m_Updated_2)); }
	inline NativeArray_1_t5F0698DD293100E2FECB8BC55FCBC3A3F6FEA582  get_m_Updated_2() const { return ___m_Updated_2; }
	inline NativeArray_1_t5F0698DD293100E2FECB8BC55FCBC3A3F6FEA582 * get_address_of_m_Updated_2() { return &___m_Updated_2; }
	inline void set_m_Updated_2(NativeArray_1_t5F0698DD293100E2FECB8BC55FCBC3A3F6FEA582  value)
	{
		___m_Updated_2 = value;
	}

	inline static int32_t get_offset_of_m_Removed_3() { return static_cast<int32_t>(offsetof(TrackableChanges_1_t67F5E50C0B7A063774AEC211C4C81BA7679876BB, ___m_Removed_3)); }
	inline NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  get_m_Removed_3() const { return ___m_Removed_3; }
	inline NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 * get_address_of_m_Removed_3() { return &___m_Removed_3; }
	inline void set_m_Removed_3(NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  value)
	{
		___m_Removed_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLECHANGES_1_T67F5E50C0B7A063774AEC211C4C81BA7679876BB_H
#ifndef TRACKABLECHANGES_1_T5C8C3FBA23E5BBC147A6991B68520A756EF54FDF_H
#define TRACKABLECHANGES_1_T5C8C3FBA23E5BBC147A6991B68520A756EF54FDF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRReferencePoint>
struct  TrackableChanges_1_t5C8C3FBA23E5BBC147A6991B68520A756EF54FDF 
{
public:
	// System.Boolean UnityEngine.XR.ARSubsystems.TrackableChanges`1::<isCreated>k__BackingField
	bool ___U3CisCreatedU3Ek__BackingField_0;
	// Unity.Collections.NativeArray`1<T> UnityEngine.XR.ARSubsystems.TrackableChanges`1::m_Added
	NativeArray_1_t5AADFB4C72573FE3017795F15B8CBC88625A8876  ___m_Added_1;
	// Unity.Collections.NativeArray`1<T> UnityEngine.XR.ARSubsystems.TrackableChanges`1::m_Updated
	NativeArray_1_t5AADFB4C72573FE3017795F15B8CBC88625A8876  ___m_Updated_2;
	// Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.TrackableId> UnityEngine.XR.ARSubsystems.TrackableChanges`1::m_Removed
	NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  ___m_Removed_3;

public:
	inline static int32_t get_offset_of_U3CisCreatedU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(TrackableChanges_1_t5C8C3FBA23E5BBC147A6991B68520A756EF54FDF, ___U3CisCreatedU3Ek__BackingField_0)); }
	inline bool get_U3CisCreatedU3Ek__BackingField_0() const { return ___U3CisCreatedU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CisCreatedU3Ek__BackingField_0() { return &___U3CisCreatedU3Ek__BackingField_0; }
	inline void set_U3CisCreatedU3Ek__BackingField_0(bool value)
	{
		___U3CisCreatedU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_m_Added_1() { return static_cast<int32_t>(offsetof(TrackableChanges_1_t5C8C3FBA23E5BBC147A6991B68520A756EF54FDF, ___m_Added_1)); }
	inline NativeArray_1_t5AADFB4C72573FE3017795F15B8CBC88625A8876  get_m_Added_1() const { return ___m_Added_1; }
	inline NativeArray_1_t5AADFB4C72573FE3017795F15B8CBC88625A8876 * get_address_of_m_Added_1() { return &___m_Added_1; }
	inline void set_m_Added_1(NativeArray_1_t5AADFB4C72573FE3017795F15B8CBC88625A8876  value)
	{
		___m_Added_1 = value;
	}

	inline static int32_t get_offset_of_m_Updated_2() { return static_cast<int32_t>(offsetof(TrackableChanges_1_t5C8C3FBA23E5BBC147A6991B68520A756EF54FDF, ___m_Updated_2)); }
	inline NativeArray_1_t5AADFB4C72573FE3017795F15B8CBC88625A8876  get_m_Updated_2() const { return ___m_Updated_2; }
	inline NativeArray_1_t5AADFB4C72573FE3017795F15B8CBC88625A8876 * get_address_of_m_Updated_2() { return &___m_Updated_2; }
	inline void set_m_Updated_2(NativeArray_1_t5AADFB4C72573FE3017795F15B8CBC88625A8876  value)
	{
		___m_Updated_2 = value;
	}

	inline static int32_t get_offset_of_m_Removed_3() { return static_cast<int32_t>(offsetof(TrackableChanges_1_t5C8C3FBA23E5BBC147A6991B68520A756EF54FDF, ___m_Removed_3)); }
	inline NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  get_m_Removed_3() const { return ___m_Removed_3; }
	inline NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 * get_address_of_m_Removed_3() { return &___m_Removed_3; }
	inline void set_m_Removed_3(NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  value)
	{
		___m_Removed_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLECHANGES_1_T5C8C3FBA23E5BBC147A6991B68520A756EF54FDF_H
#ifndef TRACKABLECHANGES_1_TE90399F00562881A054A3592F7AF134BA053AF4F_H
#define TRACKABLECHANGES_1_TE90399F00562881A054A3592F7AF134BA053AF4F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRTrackedImage>
struct  TrackableChanges_1_tE90399F00562881A054A3592F7AF134BA053AF4F 
{
public:
	// System.Boolean UnityEngine.XR.ARSubsystems.TrackableChanges`1::<isCreated>k__BackingField
	bool ___U3CisCreatedU3Ek__BackingField_0;
	// Unity.Collections.NativeArray`1<T> UnityEngine.XR.ARSubsystems.TrackableChanges`1::m_Added
	NativeArray_1_t70F653CBE89924503B639B2438D6FF5973388AC1  ___m_Added_1;
	// Unity.Collections.NativeArray`1<T> UnityEngine.XR.ARSubsystems.TrackableChanges`1::m_Updated
	NativeArray_1_t70F653CBE89924503B639B2438D6FF5973388AC1  ___m_Updated_2;
	// Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.TrackableId> UnityEngine.XR.ARSubsystems.TrackableChanges`1::m_Removed
	NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  ___m_Removed_3;

public:
	inline static int32_t get_offset_of_U3CisCreatedU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(TrackableChanges_1_tE90399F00562881A054A3592F7AF134BA053AF4F, ___U3CisCreatedU3Ek__BackingField_0)); }
	inline bool get_U3CisCreatedU3Ek__BackingField_0() const { return ___U3CisCreatedU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CisCreatedU3Ek__BackingField_0() { return &___U3CisCreatedU3Ek__BackingField_0; }
	inline void set_U3CisCreatedU3Ek__BackingField_0(bool value)
	{
		___U3CisCreatedU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_m_Added_1() { return static_cast<int32_t>(offsetof(TrackableChanges_1_tE90399F00562881A054A3592F7AF134BA053AF4F, ___m_Added_1)); }
	inline NativeArray_1_t70F653CBE89924503B639B2438D6FF5973388AC1  get_m_Added_1() const { return ___m_Added_1; }
	inline NativeArray_1_t70F653CBE89924503B639B2438D6FF5973388AC1 * get_address_of_m_Added_1() { return &___m_Added_1; }
	inline void set_m_Added_1(NativeArray_1_t70F653CBE89924503B639B2438D6FF5973388AC1  value)
	{
		___m_Added_1 = value;
	}

	inline static int32_t get_offset_of_m_Updated_2() { return static_cast<int32_t>(offsetof(TrackableChanges_1_tE90399F00562881A054A3592F7AF134BA053AF4F, ___m_Updated_2)); }
	inline NativeArray_1_t70F653CBE89924503B639B2438D6FF5973388AC1  get_m_Updated_2() const { return ___m_Updated_2; }
	inline NativeArray_1_t70F653CBE89924503B639B2438D6FF5973388AC1 * get_address_of_m_Updated_2() { return &___m_Updated_2; }
	inline void set_m_Updated_2(NativeArray_1_t70F653CBE89924503B639B2438D6FF5973388AC1  value)
	{
		___m_Updated_2 = value;
	}

	inline static int32_t get_offset_of_m_Removed_3() { return static_cast<int32_t>(offsetof(TrackableChanges_1_tE90399F00562881A054A3592F7AF134BA053AF4F, ___m_Removed_3)); }
	inline NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  get_m_Removed_3() const { return ___m_Removed_3; }
	inline NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 * get_address_of_m_Removed_3() { return &___m_Removed_3; }
	inline void set_m_Removed_3(NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  value)
	{
		___m_Removed_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLECHANGES_1_TE90399F00562881A054A3592F7AF134BA053AF4F_H
#ifndef TRACKABLECHANGES_1_T03A664B58E58E32B5EC1F7CB7133E87F804EE874_H
#define TRACKABLECHANGES_1_T03A664B58E58E32B5EC1F7CB7133E87F804EE874_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRTrackedObject>
struct  TrackableChanges_1_t03A664B58E58E32B5EC1F7CB7133E87F804EE874 
{
public:
	// System.Boolean UnityEngine.XR.ARSubsystems.TrackableChanges`1::<isCreated>k__BackingField
	bool ___U3CisCreatedU3Ek__BackingField_0;
	// Unity.Collections.NativeArray`1<T> UnityEngine.XR.ARSubsystems.TrackableChanges`1::m_Added
	NativeArray_1_t738892D030F76D319525B8C000F49D41505DD529  ___m_Added_1;
	// Unity.Collections.NativeArray`1<T> UnityEngine.XR.ARSubsystems.TrackableChanges`1::m_Updated
	NativeArray_1_t738892D030F76D319525B8C000F49D41505DD529  ___m_Updated_2;
	// Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.TrackableId> UnityEngine.XR.ARSubsystems.TrackableChanges`1::m_Removed
	NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  ___m_Removed_3;

public:
	inline static int32_t get_offset_of_U3CisCreatedU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(TrackableChanges_1_t03A664B58E58E32B5EC1F7CB7133E87F804EE874, ___U3CisCreatedU3Ek__BackingField_0)); }
	inline bool get_U3CisCreatedU3Ek__BackingField_0() const { return ___U3CisCreatedU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CisCreatedU3Ek__BackingField_0() { return &___U3CisCreatedU3Ek__BackingField_0; }
	inline void set_U3CisCreatedU3Ek__BackingField_0(bool value)
	{
		___U3CisCreatedU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_m_Added_1() { return static_cast<int32_t>(offsetof(TrackableChanges_1_t03A664B58E58E32B5EC1F7CB7133E87F804EE874, ___m_Added_1)); }
	inline NativeArray_1_t738892D030F76D319525B8C000F49D41505DD529  get_m_Added_1() const { return ___m_Added_1; }
	inline NativeArray_1_t738892D030F76D319525B8C000F49D41505DD529 * get_address_of_m_Added_1() { return &___m_Added_1; }
	inline void set_m_Added_1(NativeArray_1_t738892D030F76D319525B8C000F49D41505DD529  value)
	{
		___m_Added_1 = value;
	}

	inline static int32_t get_offset_of_m_Updated_2() { return static_cast<int32_t>(offsetof(TrackableChanges_1_t03A664B58E58E32B5EC1F7CB7133E87F804EE874, ___m_Updated_2)); }
	inline NativeArray_1_t738892D030F76D319525B8C000F49D41505DD529  get_m_Updated_2() const { return ___m_Updated_2; }
	inline NativeArray_1_t738892D030F76D319525B8C000F49D41505DD529 * get_address_of_m_Updated_2() { return &___m_Updated_2; }
	inline void set_m_Updated_2(NativeArray_1_t738892D030F76D319525B8C000F49D41505DD529  value)
	{
		___m_Updated_2 = value;
	}

	inline static int32_t get_offset_of_m_Removed_3() { return static_cast<int32_t>(offsetof(TrackableChanges_1_t03A664B58E58E32B5EC1F7CB7133E87F804EE874, ___m_Removed_3)); }
	inline NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  get_m_Removed_3() const { return ___m_Removed_3; }
	inline NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 * get_address_of_m_Removed_3() { return &___m_Removed_3; }
	inline void set_m_Removed_3(NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  value)
	{
		___m_Removed_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLECHANGES_1_T03A664B58E58E32B5EC1F7CB7133E87F804EE874_H


// Unity.Collections.NativeArray`1<T> UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRFace>::get_added()
extern "C" IL2CPP_METHOD_ATTR NativeArray_1_tAA72EF264612AEC585CFAA055B86F6B65CA4E2E5  TrackableChanges_1_get_added_m6C3AA173D63B8181A147527E5D607363D4D7E3B9_gshared (TrackableChanges_1_t316F273AB927198D6305E965CDB7B0ED758920E6 * __this, const RuntimeMethod* method);
// Unity.Collections.NativeArray`1<T> UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRFace>::get_updated()
extern "C" IL2CPP_METHOD_ATTR NativeArray_1_tAA72EF264612AEC585CFAA055B86F6B65CA4E2E5  TrackableChanges_1_get_updated_m4DD6B31059055267B3F98F003F7891F74052267D_gshared (TrackableChanges_1_t316F273AB927198D6305E965CDB7B0ED758920E6 * __this, const RuntimeMethod* method);
// Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.TrackableId> UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRFace>::get_removed()
extern "C" IL2CPP_METHOD_ATTR NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  TrackableChanges_1_get_removed_m7C323DBD29E884085A21A82E9D5ABC458EF49AED_gshared (TrackableChanges_1_t316F273AB927198D6305E965CDB7B0ED758920E6 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRFace>::get_isCreated()
extern "C" IL2CPP_METHOD_ATTR bool TrackableChanges_1_get_isCreated_mD2D689F21DCB8E4F238E18F88981D142C946CC69_gshared (TrackableChanges_1_t316F273AB927198D6305E965CDB7B0ED758920E6 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRFace>::set_isCreated(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void TrackableChanges_1_set_isCreated_m032D3E111CF433FC5556450F581911649722CA1B_gshared (TrackableChanges_1_t316F273AB927198D6305E965CDB7B0ED758920E6 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.XRFace>::.ctor(System.Int32,Unity.Collections.Allocator,Unity.Collections.NativeArrayOptions)
extern "C" IL2CPP_METHOD_ATTR void NativeArray_1__ctor_m9DDFB3AC72B2DDA9252E82C7D045B360C34BCADF_gshared (NativeArray_1_tAA72EF264612AEC585CFAA055B86F6B65CA4E2E5 * __this, int32_t p0, int32_t p1, int32_t p2, const RuntimeMethod* method);
// System.Void Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.TrackableId>::.ctor(System.Int32,Unity.Collections.Allocator,Unity.Collections.NativeArrayOptions)
extern "C" IL2CPP_METHOD_ATTR void NativeArray_1__ctor_m49FCF39766C38037835E85C0A677B26599D0BE55_gshared (NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 * __this, int32_t p0, int32_t p1, int32_t p2, const RuntimeMethod* method);
// System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRFace>::.ctor(System.Int32,System.Int32,System.Int32,Unity.Collections.Allocator)
extern "C" IL2CPP_METHOD_ATTR void TrackableChanges_1__ctor_m74CBCEBF1898779EEA76D9991BB1F62E80E2687B_gshared (TrackableChanges_1_t316F273AB927198D6305E965CDB7B0ED758920E6 * __this, int32_t ___addedCount0, int32_t ___updatedCount1, int32_t ___removedCount2, int32_t ___allocator3, const RuntimeMethod* method);
// Unity.Collections.NativeArray`1<!!0> Unity.Collections.LowLevel.Unsafe.NativeArrayUnsafeUtility::ConvertExistingDataToNativeArray<UnityEngine.XR.ARSubsystems.TrackableId>(System.Void*,System.Int32,Unity.Collections.Allocator)
extern "C" IL2CPP_METHOD_ATTR NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  NativeArrayUnsafeUtility_ConvertExistingDataToNativeArray_TisTrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47_m8D7EEF45CE20EB27369597A33879F42E459F2D59_gshared (void* p0, int32_t p1, int32_t p2, const RuntimeMethod* method);
// System.Void Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.TrackableId>::CopyFrom(Unity.Collections.NativeArray`1<!0>)
extern "C" IL2CPP_METHOD_ATTR void NativeArray_1_CopyFrom_mE2C04369E83ED4726DE1121E7E05342B6A993A97_gshared (NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 * __this, NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  p0, const RuntimeMethod* method);
// System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRFace>::.ctor(System.Void*,System.Int32,System.Void*,System.Int32,System.Void*,System.Int32,T,System.Int32,Unity.Collections.Allocator)
extern "C" IL2CPP_METHOD_ATTR void TrackableChanges_1__ctor_m9D3306EC438EE09208F98E4F4049E615CC5E801E_gshared (TrackableChanges_1_t316F273AB927198D6305E965CDB7B0ED758920E6 * __this, void* ___addedPtr0, int32_t ___addedCount1, void* ___updatedPtr2, int32_t ___updatedCount3, void* ___removedPtr4, int32_t ___removedCount5, XRFace_tF2B2E9B06813BA74F5DAFD527FD249DD1002B7C7  ___defaultT6, int32_t ___stride7, int32_t ___allocator8, const RuntimeMethod* method);
// System.Void Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.XRFace>::CopyFrom(Unity.Collections.NativeArray`1<!0>)
extern "C" IL2CPP_METHOD_ATTR void NativeArray_1_CopyFrom_mF9D212E6958D4B634ED7644625EC61A9C7605CDB_gshared (NativeArray_1_tAA72EF264612AEC585CFAA055B86F6B65CA4E2E5 * __this, NativeArray_1_tAA72EF264612AEC585CFAA055B86F6B65CA4E2E5  p0, const RuntimeMethod* method);
// System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRFace>::.ctor(Unity.Collections.NativeArray`1<T>,Unity.Collections.NativeArray`1<T>,Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.TrackableId>)
extern "C" IL2CPP_METHOD_ATTR void TrackableChanges_1__ctor_m311F799F3BF5B7CDF7B32E9A6B5CF320D1D0A375_gshared (TrackableChanges_1_t316F273AB927198D6305E965CDB7B0ED758920E6 * __this, NativeArray_1_tAA72EF264612AEC585CFAA055B86F6B65CA4E2E5  p0, NativeArray_1_tAA72EF264612AEC585CFAA055B86F6B65CA4E2E5  p1, NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  p2, const RuntimeMethod* method);
// System.Void Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.XRFace>::Dispose()
extern "C" IL2CPP_METHOD_ATTR void NativeArray_1_Dispose_m343FC4D068AE0BA3141FDF70567F8BD2A752FF2D_gshared (NativeArray_1_tAA72EF264612AEC585CFAA055B86F6B65CA4E2E5 * __this, const RuntimeMethod* method);
// System.Void Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.TrackableId>::Dispose()
extern "C" IL2CPP_METHOD_ATTR void NativeArray_1_Dispose_mB8124101177299FC76251C4E7F6C98BB0E5503C7_gshared (NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRFace>::Dispose()
extern "C" IL2CPP_METHOD_ATTR void TrackableChanges_1_Dispose_m8CB8D80766E7529B391391E43158D99E39220FF6_gshared (TrackableChanges_1_t316F273AB927198D6305E965CDB7B0ED758920E6 * __this, const RuntimeMethod* method);
// Unity.Collections.NativeArray`1<T> UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRHumanBody>::get_added()
extern "C" IL2CPP_METHOD_ATTR NativeArray_1_t140FCB442630D549508192A9E3737425E84D15F8  TrackableChanges_1_get_added_mAC12696B7D12A00A3FE9C34A2FF8F22ED6A3BD6C_gshared (TrackableChanges_1_t8F77CF9D37FD0DCA052A7D6B34E1A489AF13C41B * __this, const RuntimeMethod* method);
// Unity.Collections.NativeArray`1<T> UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRHumanBody>::get_updated()
extern "C" IL2CPP_METHOD_ATTR NativeArray_1_t140FCB442630D549508192A9E3737425E84D15F8  TrackableChanges_1_get_updated_mCD92C9A6154DC58C63992B59E6671BC8AE07F9C8_gshared (TrackableChanges_1_t8F77CF9D37FD0DCA052A7D6B34E1A489AF13C41B * __this, const RuntimeMethod* method);
// Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.TrackableId> UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRHumanBody>::get_removed()
extern "C" IL2CPP_METHOD_ATTR NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  TrackableChanges_1_get_removed_m377C6A3BFE41CC689AFE09CE1529B79113140DD2_gshared (TrackableChanges_1_t8F77CF9D37FD0DCA052A7D6B34E1A489AF13C41B * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRHumanBody>::get_isCreated()
extern "C" IL2CPP_METHOD_ATTR bool TrackableChanges_1_get_isCreated_m68F2E3D10D821918F3C0002EAA07440EC60CB4B0_gshared (TrackableChanges_1_t8F77CF9D37FD0DCA052A7D6B34E1A489AF13C41B * __this, const RuntimeMethod* method);
// System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRHumanBody>::set_isCreated(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void TrackableChanges_1_set_isCreated_m6FF97BC809D88ABBCD88D515340CBC1F1EE27D83_gshared (TrackableChanges_1_t8F77CF9D37FD0DCA052A7D6B34E1A489AF13C41B * __this, bool ___value0, const RuntimeMethod* method);
// System.Void Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.XRHumanBody>::.ctor(System.Int32,Unity.Collections.Allocator,Unity.Collections.NativeArrayOptions)
extern "C" IL2CPP_METHOD_ATTR void NativeArray_1__ctor_mF5A1714C6870A3DF6F61BB87B0700F0A8A67F9CC_gshared (NativeArray_1_t140FCB442630D549508192A9E3737425E84D15F8 * __this, int32_t p0, int32_t p1, int32_t p2, const RuntimeMethod* method);
// System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRHumanBody>::.ctor(System.Int32,System.Int32,System.Int32,Unity.Collections.Allocator)
extern "C" IL2CPP_METHOD_ATTR void TrackableChanges_1__ctor_m6A057FF02DA6FD8876C995CE890C7D8B3A1EBFB0_gshared (TrackableChanges_1_t8F77CF9D37FD0DCA052A7D6B34E1A489AF13C41B * __this, int32_t ___addedCount0, int32_t ___updatedCount1, int32_t ___removedCount2, int32_t ___allocator3, const RuntimeMethod* method);
// System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRHumanBody>::.ctor(System.Void*,System.Int32,System.Void*,System.Int32,System.Void*,System.Int32,T,System.Int32,Unity.Collections.Allocator)
extern "C" IL2CPP_METHOD_ATTR void TrackableChanges_1__ctor_m859CA5B973D4AD250F12979A582B9F5D56007BFD_gshared (TrackableChanges_1_t8F77CF9D37FD0DCA052A7D6B34E1A489AF13C41B * __this, void* ___addedPtr0, int32_t ___addedCount1, void* ___updatedPtr2, int32_t ___updatedCount3, void* ___removedPtr4, int32_t ___removedCount5, XRHumanBody_t64938399EB40376E99745517E15D42FFC4604BC1  ___defaultT6, int32_t ___stride7, int32_t ___allocator8, const RuntimeMethod* method);
// System.Void Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.XRHumanBody>::CopyFrom(Unity.Collections.NativeArray`1<!0>)
extern "C" IL2CPP_METHOD_ATTR void NativeArray_1_CopyFrom_m476CD42B4BF6AFC03517ED3C99ADB1FFC44B5531_gshared (NativeArray_1_t140FCB442630D549508192A9E3737425E84D15F8 * __this, NativeArray_1_t140FCB442630D549508192A9E3737425E84D15F8  p0, const RuntimeMethod* method);
// System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRHumanBody>::.ctor(Unity.Collections.NativeArray`1<T>,Unity.Collections.NativeArray`1<T>,Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.TrackableId>)
extern "C" IL2CPP_METHOD_ATTR void TrackableChanges_1__ctor_mCBFAB9AC1ADBAED983EEC593665EBBE6337E9A3B_gshared (TrackableChanges_1_t8F77CF9D37FD0DCA052A7D6B34E1A489AF13C41B * __this, NativeArray_1_t140FCB442630D549508192A9E3737425E84D15F8  p0, NativeArray_1_t140FCB442630D549508192A9E3737425E84D15F8  p1, NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  p2, const RuntimeMethod* method);
// System.Void Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.XRHumanBody>::Dispose()
extern "C" IL2CPP_METHOD_ATTR void NativeArray_1_Dispose_mF075AE11F3DFF21CA4E5A17B28A0F9D1B52D5926_gshared (NativeArray_1_t140FCB442630D549508192A9E3737425E84D15F8 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRHumanBody>::Dispose()
extern "C" IL2CPP_METHOD_ATTR void TrackableChanges_1_Dispose_m7F29A9FDEA683B63D471FB1D5505A6D8EC7A107A_gshared (TrackableChanges_1_t8F77CF9D37FD0DCA052A7D6B34E1A489AF13C41B * __this, const RuntimeMethod* method);
// Unity.Collections.NativeArray`1<T> UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRPointCloud>::get_added()
extern "C" IL2CPP_METHOD_ATTR NativeArray_1_t5F0698DD293100E2FECB8BC55FCBC3A3F6FEA582  TrackableChanges_1_get_added_mBB3A80E298216A6D8826422F62A11A1A4CB7CBD9_gshared (TrackableChanges_1_t67F5E50C0B7A063774AEC211C4C81BA7679876BB * __this, const RuntimeMethod* method);
// Unity.Collections.NativeArray`1<T> UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRPointCloud>::get_updated()
extern "C" IL2CPP_METHOD_ATTR NativeArray_1_t5F0698DD293100E2FECB8BC55FCBC3A3F6FEA582  TrackableChanges_1_get_updated_m80DB51775906B404AEE4C7454E7A517ECFD6A003_gshared (TrackableChanges_1_t67F5E50C0B7A063774AEC211C4C81BA7679876BB * __this, const RuntimeMethod* method);
// Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.TrackableId> UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRPointCloud>::get_removed()
extern "C" IL2CPP_METHOD_ATTR NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  TrackableChanges_1_get_removed_mC49E91CAF67469CDD76DBCF18D1423DC3971AF99_gshared (TrackableChanges_1_t67F5E50C0B7A063774AEC211C4C81BA7679876BB * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRPointCloud>::get_isCreated()
extern "C" IL2CPP_METHOD_ATTR bool TrackableChanges_1_get_isCreated_m6BDB6E85EF722CE4F115552E754E059F9BBA87B8_gshared (TrackableChanges_1_t67F5E50C0B7A063774AEC211C4C81BA7679876BB * __this, const RuntimeMethod* method);
// System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRPointCloud>::set_isCreated(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void TrackableChanges_1_set_isCreated_mE659C5BCD61914E799B4D6AAE6AD5AD7B9DDCE76_gshared (TrackableChanges_1_t67F5E50C0B7A063774AEC211C4C81BA7679876BB * __this, bool ___value0, const RuntimeMethod* method);
// System.Void Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.XRPointCloud>::.ctor(System.Int32,Unity.Collections.Allocator,Unity.Collections.NativeArrayOptions)
extern "C" IL2CPP_METHOD_ATTR void NativeArray_1__ctor_m0A7431FE284605A389AB455AFBC44304CAE118CF_gshared (NativeArray_1_t5F0698DD293100E2FECB8BC55FCBC3A3F6FEA582 * __this, int32_t p0, int32_t p1, int32_t p2, const RuntimeMethod* method);
// System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRPointCloud>::.ctor(System.Int32,System.Int32,System.Int32,Unity.Collections.Allocator)
extern "C" IL2CPP_METHOD_ATTR void TrackableChanges_1__ctor_mB26E1128CC08E676885A96E341B8CE2DC1228D20_gshared (TrackableChanges_1_t67F5E50C0B7A063774AEC211C4C81BA7679876BB * __this, int32_t ___addedCount0, int32_t ___updatedCount1, int32_t ___removedCount2, int32_t ___allocator3, const RuntimeMethod* method);
// System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRPointCloud>::.ctor(System.Void*,System.Int32,System.Void*,System.Int32,System.Void*,System.Int32,T,System.Int32,Unity.Collections.Allocator)
extern "C" IL2CPP_METHOD_ATTR void TrackableChanges_1__ctor_mA69A666BCF692D5A8660A564A4450ABF6C80FD90_gshared (TrackableChanges_1_t67F5E50C0B7A063774AEC211C4C81BA7679876BB * __this, void* ___addedPtr0, int32_t ___addedCount1, void* ___updatedPtr2, int32_t ___updatedCount3, void* ___removedPtr4, int32_t ___removedCount5, XRPointCloud_tA4A412DE503530E1B2953919F1463B9B48504ED0  ___defaultT6, int32_t ___stride7, int32_t ___allocator8, const RuntimeMethod* method);
// System.Void Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.XRPointCloud>::CopyFrom(Unity.Collections.NativeArray`1<!0>)
extern "C" IL2CPP_METHOD_ATTR void NativeArray_1_CopyFrom_mE62AAA10EC11C27A6A545BEF481C0FC3DB4800B3_gshared (NativeArray_1_t5F0698DD293100E2FECB8BC55FCBC3A3F6FEA582 * __this, NativeArray_1_t5F0698DD293100E2FECB8BC55FCBC3A3F6FEA582  p0, const RuntimeMethod* method);
// System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRPointCloud>::.ctor(Unity.Collections.NativeArray`1<T>,Unity.Collections.NativeArray`1<T>,Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.TrackableId>)
extern "C" IL2CPP_METHOD_ATTR void TrackableChanges_1__ctor_m4961BB84AABB2C5A4D492E4994845A231BFB09BF_gshared (TrackableChanges_1_t67F5E50C0B7A063774AEC211C4C81BA7679876BB * __this, NativeArray_1_t5F0698DD293100E2FECB8BC55FCBC3A3F6FEA582  p0, NativeArray_1_t5F0698DD293100E2FECB8BC55FCBC3A3F6FEA582  p1, NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  p2, const RuntimeMethod* method);
// System.Void Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.XRPointCloud>::Dispose()
extern "C" IL2CPP_METHOD_ATTR void NativeArray_1_Dispose_mAF31B95CE89FF647372855A4ECFB9FCDDE39C38B_gshared (NativeArray_1_t5F0698DD293100E2FECB8BC55FCBC3A3F6FEA582 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRPointCloud>::Dispose()
extern "C" IL2CPP_METHOD_ATTR void TrackableChanges_1_Dispose_mE3DC3C0230641A828B6A91F3CB94441797520834_gshared (TrackableChanges_1_t67F5E50C0B7A063774AEC211C4C81BA7679876BB * __this, const RuntimeMethod* method);
// Unity.Collections.NativeArray`1<T> UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRReferencePoint>::get_added()
extern "C" IL2CPP_METHOD_ATTR NativeArray_1_t5AADFB4C72573FE3017795F15B8CBC88625A8876  TrackableChanges_1_get_added_mC31FEFAAC8F70ABBBC324DC618B0DFAB08AAE934_gshared (TrackableChanges_1_t5C8C3FBA23E5BBC147A6991B68520A756EF54FDF * __this, const RuntimeMethod* method);
// Unity.Collections.NativeArray`1<T> UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRReferencePoint>::get_updated()
extern "C" IL2CPP_METHOD_ATTR NativeArray_1_t5AADFB4C72573FE3017795F15B8CBC88625A8876  TrackableChanges_1_get_updated_m54C710EFD531DFB25ABA289B60FAA4181D479DDF_gshared (TrackableChanges_1_t5C8C3FBA23E5BBC147A6991B68520A756EF54FDF * __this, const RuntimeMethod* method);
// Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.TrackableId> UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRReferencePoint>::get_removed()
extern "C" IL2CPP_METHOD_ATTR NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  TrackableChanges_1_get_removed_m9781A441FA95E17CBDB3C4687247859A562AF077_gshared (TrackableChanges_1_t5C8C3FBA23E5BBC147A6991B68520A756EF54FDF * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRReferencePoint>::get_isCreated()
extern "C" IL2CPP_METHOD_ATTR bool TrackableChanges_1_get_isCreated_m81592B6012FD82C68A023EC52FFAB3B4A35C6A3D_gshared (TrackableChanges_1_t5C8C3FBA23E5BBC147A6991B68520A756EF54FDF * __this, const RuntimeMethod* method);
// System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRReferencePoint>::set_isCreated(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void TrackableChanges_1_set_isCreated_m3B57EAEC482D7B5D700E044B8720F89F25B13DC5_gshared (TrackableChanges_1_t5C8C3FBA23E5BBC147A6991B68520A756EF54FDF * __this, bool ___value0, const RuntimeMethod* method);
// System.Void Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.XRReferencePoint>::.ctor(System.Int32,Unity.Collections.Allocator,Unity.Collections.NativeArrayOptions)
extern "C" IL2CPP_METHOD_ATTR void NativeArray_1__ctor_mBA3667DFD2FA91C71C118ECA43EB22503D4FC2C8_gshared (NativeArray_1_t5AADFB4C72573FE3017795F15B8CBC88625A8876 * __this, int32_t p0, int32_t p1, int32_t p2, const RuntimeMethod* method);
// System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRReferencePoint>::.ctor(System.Int32,System.Int32,System.Int32,Unity.Collections.Allocator)
extern "C" IL2CPP_METHOD_ATTR void TrackableChanges_1__ctor_m8E2935D6FEAB365BB8BBF74A7D12694909DFB282_gshared (TrackableChanges_1_t5C8C3FBA23E5BBC147A6991B68520A756EF54FDF * __this, int32_t ___addedCount0, int32_t ___updatedCount1, int32_t ___removedCount2, int32_t ___allocator3, const RuntimeMethod* method);
// System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRReferencePoint>::.ctor(System.Void*,System.Int32,System.Void*,System.Int32,System.Void*,System.Int32,T,System.Int32,Unity.Collections.Allocator)
extern "C" IL2CPP_METHOD_ATTR void TrackableChanges_1__ctor_m47C55466595329D4629E9A78A403D34C94AC30FC_gshared (TrackableChanges_1_t5C8C3FBA23E5BBC147A6991B68520A756EF54FDF * __this, void* ___addedPtr0, int32_t ___addedCount1, void* ___updatedPtr2, int32_t ___updatedCount3, void* ___removedPtr4, int32_t ___removedCount5, XRReferencePoint_tA8592C08A27EC91D9B1FB3B083C95C5D372FF1F9  ___defaultT6, int32_t ___stride7, int32_t ___allocator8, const RuntimeMethod* method);
// System.Void Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.XRReferencePoint>::CopyFrom(Unity.Collections.NativeArray`1<!0>)
extern "C" IL2CPP_METHOD_ATTR void NativeArray_1_CopyFrom_mC053D3A9DF2FBD4DCB31D2864F4996E9B2DA255A_gshared (NativeArray_1_t5AADFB4C72573FE3017795F15B8CBC88625A8876 * __this, NativeArray_1_t5AADFB4C72573FE3017795F15B8CBC88625A8876  p0, const RuntimeMethod* method);
// System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRReferencePoint>::.ctor(Unity.Collections.NativeArray`1<T>,Unity.Collections.NativeArray`1<T>,Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.TrackableId>)
extern "C" IL2CPP_METHOD_ATTR void TrackableChanges_1__ctor_m5E677E3D08A7D26921EA074FB0A849F566DE5951_gshared (TrackableChanges_1_t5C8C3FBA23E5BBC147A6991B68520A756EF54FDF * __this, NativeArray_1_t5AADFB4C72573FE3017795F15B8CBC88625A8876  p0, NativeArray_1_t5AADFB4C72573FE3017795F15B8CBC88625A8876  p1, NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  p2, const RuntimeMethod* method);
// System.Void Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.XRReferencePoint>::Dispose()
extern "C" IL2CPP_METHOD_ATTR void NativeArray_1_Dispose_m79A1BBDC8A7E6F17C8B48E7CB3ED027A3180039E_gshared (NativeArray_1_t5AADFB4C72573FE3017795F15B8CBC88625A8876 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRReferencePoint>::Dispose()
extern "C" IL2CPP_METHOD_ATTR void TrackableChanges_1_Dispose_m1C8DF8C1879FD6A22AD7474E15FB2590BB4F9631_gshared (TrackableChanges_1_t5C8C3FBA23E5BBC147A6991B68520A756EF54FDF * __this, const RuntimeMethod* method);
// Unity.Collections.NativeArray`1<T> UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRTrackedImage>::get_added()
extern "C" IL2CPP_METHOD_ATTR NativeArray_1_t70F653CBE89924503B639B2438D6FF5973388AC1  TrackableChanges_1_get_added_mC35012A2E03A744ECF4A6E58D2DA1C34D85CA6ED_gshared (TrackableChanges_1_tE90399F00562881A054A3592F7AF134BA053AF4F * __this, const RuntimeMethod* method);
// Unity.Collections.NativeArray`1<T> UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRTrackedImage>::get_updated()
extern "C" IL2CPP_METHOD_ATTR NativeArray_1_t70F653CBE89924503B639B2438D6FF5973388AC1  TrackableChanges_1_get_updated_mF1CCDAA99909A241F138D24B4175AD6806E65323_gshared (TrackableChanges_1_tE90399F00562881A054A3592F7AF134BA053AF4F * __this, const RuntimeMethod* method);
// Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.TrackableId> UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRTrackedImage>::get_removed()
extern "C" IL2CPP_METHOD_ATTR NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  TrackableChanges_1_get_removed_m7CF311E706E890B2CC9281BC392849E4DE880ECA_gshared (TrackableChanges_1_tE90399F00562881A054A3592F7AF134BA053AF4F * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRTrackedImage>::get_isCreated()
extern "C" IL2CPP_METHOD_ATTR bool TrackableChanges_1_get_isCreated_mEEE0707FBE540E3961450502B2C2771AA02ABF83_gshared (TrackableChanges_1_tE90399F00562881A054A3592F7AF134BA053AF4F * __this, const RuntimeMethod* method);
// System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRTrackedImage>::set_isCreated(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void TrackableChanges_1_set_isCreated_m360183150F0DE3BAE45AD1C6C942146BF81F51D4_gshared (TrackableChanges_1_tE90399F00562881A054A3592F7AF134BA053AF4F * __this, bool ___value0, const RuntimeMethod* method);
// System.Void Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.XRTrackedImage>::.ctor(System.Int32,Unity.Collections.Allocator,Unity.Collections.NativeArrayOptions)
extern "C" IL2CPP_METHOD_ATTR void NativeArray_1__ctor_m192CF93170B36E61ACE804945C3B18481D07B1C5_gshared (NativeArray_1_t70F653CBE89924503B639B2438D6FF5973388AC1 * __this, int32_t p0, int32_t p1, int32_t p2, const RuntimeMethod* method);
// System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRTrackedImage>::.ctor(System.Int32,System.Int32,System.Int32,Unity.Collections.Allocator)
extern "C" IL2CPP_METHOD_ATTR void TrackableChanges_1__ctor_m88DBEB1615B50F97CBE740AA5502C0DFA42D7641_gshared (TrackableChanges_1_tE90399F00562881A054A3592F7AF134BA053AF4F * __this, int32_t ___addedCount0, int32_t ___updatedCount1, int32_t ___removedCount2, int32_t ___allocator3, const RuntimeMethod* method);
// System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRTrackedImage>::.ctor(System.Void*,System.Int32,System.Void*,System.Int32,System.Void*,System.Int32,T,System.Int32,Unity.Collections.Allocator)
extern "C" IL2CPP_METHOD_ATTR void TrackableChanges_1__ctor_m3477ECC2A1A2FF3D7287DF3B5BDFC7F272FDB3EE_gshared (TrackableChanges_1_tE90399F00562881A054A3592F7AF134BA053AF4F * __this, void* ___addedPtr0, int32_t ___addedCount1, void* ___updatedPtr2, int32_t ___updatedCount3, void* ___removedPtr4, int32_t ___removedCount5, XRTrackedImage_t178EACF5BFA4228DF4EB1899685C533F3F296AA8  ___defaultT6, int32_t ___stride7, int32_t ___allocator8, const RuntimeMethod* method);
// System.Void Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.XRTrackedImage>::CopyFrom(Unity.Collections.NativeArray`1<!0>)
extern "C" IL2CPP_METHOD_ATTR void NativeArray_1_CopyFrom_mC0B092730DE59B585688BCCF8FA9099731798DCD_gshared (NativeArray_1_t70F653CBE89924503B639B2438D6FF5973388AC1 * __this, NativeArray_1_t70F653CBE89924503B639B2438D6FF5973388AC1  p0, const RuntimeMethod* method);
// System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRTrackedImage>::.ctor(Unity.Collections.NativeArray`1<T>,Unity.Collections.NativeArray`1<T>,Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.TrackableId>)
extern "C" IL2CPP_METHOD_ATTR void TrackableChanges_1__ctor_mC96D4347CE6234FD5E51ECDA265125C19D70BF0C_gshared (TrackableChanges_1_tE90399F00562881A054A3592F7AF134BA053AF4F * __this, NativeArray_1_t70F653CBE89924503B639B2438D6FF5973388AC1  p0, NativeArray_1_t70F653CBE89924503B639B2438D6FF5973388AC1  p1, NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  p2, const RuntimeMethod* method);
// System.Void Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.XRTrackedImage>::Dispose()
extern "C" IL2CPP_METHOD_ATTR void NativeArray_1_Dispose_m8645D533921C8C5BE22919B57D259A4240AB379A_gshared (NativeArray_1_t70F653CBE89924503B639B2438D6FF5973388AC1 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRTrackedImage>::Dispose()
extern "C" IL2CPP_METHOD_ATTR void TrackableChanges_1_Dispose_m5A25C5A0DF692BA62C0C9E9DD458E28462000702_gshared (TrackableChanges_1_tE90399F00562881A054A3592F7AF134BA053AF4F * __this, const RuntimeMethod* method);
// Unity.Collections.NativeArray`1<T> UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRTrackedObject>::get_added()
extern "C" IL2CPP_METHOD_ATTR NativeArray_1_t738892D030F76D319525B8C000F49D41505DD529  TrackableChanges_1_get_added_m6B4AFC77B682299AFAB977EDEAF164E6B63E3670_gshared (TrackableChanges_1_t03A664B58E58E32B5EC1F7CB7133E87F804EE874 * __this, const RuntimeMethod* method);
// Unity.Collections.NativeArray`1<T> UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRTrackedObject>::get_updated()
extern "C" IL2CPP_METHOD_ATTR NativeArray_1_t738892D030F76D319525B8C000F49D41505DD529  TrackableChanges_1_get_updated_mDDB738464599270A745A15C57FC941EEBEC00700_gshared (TrackableChanges_1_t03A664B58E58E32B5EC1F7CB7133E87F804EE874 * __this, const RuntimeMethod* method);
// Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.TrackableId> UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRTrackedObject>::get_removed()
extern "C" IL2CPP_METHOD_ATTR NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  TrackableChanges_1_get_removed_m53E0E551E0ACC552E62D8BA2A0A234D72CAB6C74_gshared (TrackableChanges_1_t03A664B58E58E32B5EC1F7CB7133E87F804EE874 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRTrackedObject>::get_isCreated()
extern "C" IL2CPP_METHOD_ATTR bool TrackableChanges_1_get_isCreated_mBFB9748C1EA377BE047EC13F736E0C8C207E441C_gshared (TrackableChanges_1_t03A664B58E58E32B5EC1F7CB7133E87F804EE874 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRTrackedObject>::set_isCreated(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void TrackableChanges_1_set_isCreated_m085A94096B2D33690423DF6A6777D7586B582D14_gshared (TrackableChanges_1_t03A664B58E58E32B5EC1F7CB7133E87F804EE874 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.XRTrackedObject>::.ctor(System.Int32,Unity.Collections.Allocator,Unity.Collections.NativeArrayOptions)
extern "C" IL2CPP_METHOD_ATTR void NativeArray_1__ctor_m16425BC62E9AC48C88E58870411D0D63CCE985A9_gshared (NativeArray_1_t738892D030F76D319525B8C000F49D41505DD529 * __this, int32_t p0, int32_t p1, int32_t p2, const RuntimeMethod* method);
// System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRTrackedObject>::.ctor(System.Int32,System.Int32,System.Int32,Unity.Collections.Allocator)
extern "C" IL2CPP_METHOD_ATTR void TrackableChanges_1__ctor_mBA293905C9592E097DAC0E30B77D458C7D7E23FD_gshared (TrackableChanges_1_t03A664B58E58E32B5EC1F7CB7133E87F804EE874 * __this, int32_t ___addedCount0, int32_t ___updatedCount1, int32_t ___removedCount2, int32_t ___allocator3, const RuntimeMethod* method);
// System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRTrackedObject>::.ctor(System.Void*,System.Int32,System.Void*,System.Int32,System.Void*,System.Int32,T,System.Int32,Unity.Collections.Allocator)
extern "C" IL2CPP_METHOD_ATTR void TrackableChanges_1__ctor_mD3BF9E8193EF890B4977549BF6374BE3D5165CBF_gshared (TrackableChanges_1_t03A664B58E58E32B5EC1F7CB7133E87F804EE874 * __this, void* ___addedPtr0, int32_t ___addedCount1, void* ___updatedPtr2, int32_t ___updatedCount3, void* ___removedPtr4, int32_t ___removedCount5, XRTrackedObject_tF08D3523D17E214EC3D7DBE8ED5BFE220BAAE260  ___defaultT6, int32_t ___stride7, int32_t ___allocator8, const RuntimeMethod* method);
// System.Void Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.XRTrackedObject>::CopyFrom(Unity.Collections.NativeArray`1<!0>)
extern "C" IL2CPP_METHOD_ATTR void NativeArray_1_CopyFrom_m44A3244484057D36D3F473E72EBE58EEF79C5B92_gshared (NativeArray_1_t738892D030F76D319525B8C000F49D41505DD529 * __this, NativeArray_1_t738892D030F76D319525B8C000F49D41505DD529  p0, const RuntimeMethod* method);
// System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRTrackedObject>::.ctor(Unity.Collections.NativeArray`1<T>,Unity.Collections.NativeArray`1<T>,Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.TrackableId>)
extern "C" IL2CPP_METHOD_ATTR void TrackableChanges_1__ctor_m256B8D469EFDAAA37C6D50B7DB6C76C431A70430_gshared (TrackableChanges_1_t03A664B58E58E32B5EC1F7CB7133E87F804EE874 * __this, NativeArray_1_t738892D030F76D319525B8C000F49D41505DD529  p0, NativeArray_1_t738892D030F76D319525B8C000F49D41505DD529  p1, NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  p2, const RuntimeMethod* method);
// System.Void Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.XRTrackedObject>::Dispose()
extern "C" IL2CPP_METHOD_ATTR void NativeArray_1_Dispose_mA88D5D4DAC3D042D2DBB1DBBC45EC820AE85242A_gshared (NativeArray_1_t738892D030F76D319525B8C000F49D41505DD529 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRTrackedObject>::Dispose()
extern "C" IL2CPP_METHOD_ATTR void TrackableChanges_1_Dispose_mA1365656D4814F94F14E5A34A975A00F5892C790_gshared (TrackableChanges_1_t03A664B58E58E32B5EC1F7CB7133E87F804EE874 * __this, const RuntimeMethod* method);

// Unity.Collections.NativeArray`1<T> UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRFace>::get_added()
inline NativeArray_1_tAA72EF264612AEC585CFAA055B86F6B65CA4E2E5  TrackableChanges_1_get_added_m6C3AA173D63B8181A147527E5D607363D4D7E3B9 (TrackableChanges_1_t316F273AB927198D6305E965CDB7B0ED758920E6 * __this, const RuntimeMethod* method)
{
	return ((  NativeArray_1_tAA72EF264612AEC585CFAA055B86F6B65CA4E2E5  (*) (TrackableChanges_1_t316F273AB927198D6305E965CDB7B0ED758920E6 *, const RuntimeMethod*))TrackableChanges_1_get_added_m6C3AA173D63B8181A147527E5D607363D4D7E3B9_gshared)(__this, method);
}
// Unity.Collections.NativeArray`1<T> UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRFace>::get_updated()
inline NativeArray_1_tAA72EF264612AEC585CFAA055B86F6B65CA4E2E5  TrackableChanges_1_get_updated_m4DD6B31059055267B3F98F003F7891F74052267D (TrackableChanges_1_t316F273AB927198D6305E965CDB7B0ED758920E6 * __this, const RuntimeMethod* method)
{
	return ((  NativeArray_1_tAA72EF264612AEC585CFAA055B86F6B65CA4E2E5  (*) (TrackableChanges_1_t316F273AB927198D6305E965CDB7B0ED758920E6 *, const RuntimeMethod*))TrackableChanges_1_get_updated_m4DD6B31059055267B3F98F003F7891F74052267D_gshared)(__this, method);
}
// Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.TrackableId> UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRFace>::get_removed()
inline NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  TrackableChanges_1_get_removed_m7C323DBD29E884085A21A82E9D5ABC458EF49AED (TrackableChanges_1_t316F273AB927198D6305E965CDB7B0ED758920E6 * __this, const RuntimeMethod* method)
{
	return ((  NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  (*) (TrackableChanges_1_t316F273AB927198D6305E965CDB7B0ED758920E6 *, const RuntimeMethod*))TrackableChanges_1_get_removed_m7C323DBD29E884085A21A82E9D5ABC458EF49AED_gshared)(__this, method);
}
// System.Boolean UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRFace>::get_isCreated()
inline bool TrackableChanges_1_get_isCreated_mD2D689F21DCB8E4F238E18F88981D142C946CC69 (TrackableChanges_1_t316F273AB927198D6305E965CDB7B0ED758920E6 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (TrackableChanges_1_t316F273AB927198D6305E965CDB7B0ED758920E6 *, const RuntimeMethod*))TrackableChanges_1_get_isCreated_mD2D689F21DCB8E4F238E18F88981D142C946CC69_gshared)(__this, method);
}
// System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRFace>::set_isCreated(System.Boolean)
inline void TrackableChanges_1_set_isCreated_m032D3E111CF433FC5556450F581911649722CA1B (TrackableChanges_1_t316F273AB927198D6305E965CDB7B0ED758920E6 * __this, bool ___value0, const RuntimeMethod* method)
{
	((  void (*) (TrackableChanges_1_t316F273AB927198D6305E965CDB7B0ED758920E6 *, bool, const RuntimeMethod*))TrackableChanges_1_set_isCreated_m032D3E111CF433FC5556450F581911649722CA1B_gshared)(__this, ___value0, method);
}
// System.Void Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.XRFace>::.ctor(System.Int32,Unity.Collections.Allocator,Unity.Collections.NativeArrayOptions)
inline void NativeArray_1__ctor_m9DDFB3AC72B2DDA9252E82C7D045B360C34BCADF (NativeArray_1_tAA72EF264612AEC585CFAA055B86F6B65CA4E2E5 * __this, int32_t p0, int32_t p1, int32_t p2, const RuntimeMethod* method)
{
	((  void (*) (NativeArray_1_tAA72EF264612AEC585CFAA055B86F6B65CA4E2E5 *, int32_t, int32_t, int32_t, const RuntimeMethod*))NativeArray_1__ctor_m9DDFB3AC72B2DDA9252E82C7D045B360C34BCADF_gshared)(__this, p0, p1, p2, method);
}
// System.Void Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.TrackableId>::.ctor(System.Int32,Unity.Collections.Allocator,Unity.Collections.NativeArrayOptions)
inline void NativeArray_1__ctor_m49FCF39766C38037835E85C0A677B26599D0BE55 (NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 * __this, int32_t p0, int32_t p1, int32_t p2, const RuntimeMethod* method)
{
	((  void (*) (NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 *, int32_t, int32_t, int32_t, const RuntimeMethod*))NativeArray_1__ctor_m49FCF39766C38037835E85C0A677B26599D0BE55_gshared)(__this, p0, p1, p2, method);
}
// System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRFace>::.ctor(System.Int32,System.Int32,System.Int32,Unity.Collections.Allocator)
inline void TrackableChanges_1__ctor_m74CBCEBF1898779EEA76D9991BB1F62E80E2687B (TrackableChanges_1_t316F273AB927198D6305E965CDB7B0ED758920E6 * __this, int32_t ___addedCount0, int32_t ___updatedCount1, int32_t ___removedCount2, int32_t ___allocator3, const RuntimeMethod* method)
{
	((  void (*) (TrackableChanges_1_t316F273AB927198D6305E965CDB7B0ED758920E6 *, int32_t, int32_t, int32_t, int32_t, const RuntimeMethod*))TrackableChanges_1__ctor_m74CBCEBF1898779EEA76D9991BB1F62E80E2687B_gshared)(__this, ___addedCount0, ___updatedCount1, ___removedCount2, ___allocator3, method);
}
// Unity.Collections.NativeArray`1<!!0> Unity.Collections.LowLevel.Unsafe.NativeArrayUnsafeUtility::ConvertExistingDataToNativeArray<UnityEngine.XR.ARSubsystems.TrackableId>(System.Void*,System.Int32,Unity.Collections.Allocator)
inline NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  NativeArrayUnsafeUtility_ConvertExistingDataToNativeArray_TisTrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47_m8D7EEF45CE20EB27369597A33879F42E459F2D59 (void* p0, int32_t p1, int32_t p2, const RuntimeMethod* method)
{
	return ((  NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  (*) (void*, int32_t, int32_t, const RuntimeMethod*))NativeArrayUnsafeUtility_ConvertExistingDataToNativeArray_TisTrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47_m8D7EEF45CE20EB27369597A33879F42E459F2D59_gshared)(p0, p1, p2, method);
}
// System.Void Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.TrackableId>::CopyFrom(Unity.Collections.NativeArray`1<!0>)
inline void NativeArray_1_CopyFrom_mE2C04369E83ED4726DE1121E7E05342B6A993A97 (NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 * __this, NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  p0, const RuntimeMethod* method)
{
	((  void (*) (NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 *, NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 , const RuntimeMethod*))NativeArray_1_CopyFrom_mE2C04369E83ED4726DE1121E7E05342B6A993A97_gshared)(__this, p0, method);
}
// System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRFace>::.ctor(System.Void*,System.Int32,System.Void*,System.Int32,System.Void*,System.Int32,T,System.Int32,Unity.Collections.Allocator)
inline void TrackableChanges_1__ctor_m9D3306EC438EE09208F98E4F4049E615CC5E801E (TrackableChanges_1_t316F273AB927198D6305E965CDB7B0ED758920E6 * __this, void* ___addedPtr0, int32_t ___addedCount1, void* ___updatedPtr2, int32_t ___updatedCount3, void* ___removedPtr4, int32_t ___removedCount5, XRFace_tF2B2E9B06813BA74F5DAFD527FD249DD1002B7C7  ___defaultT6, int32_t ___stride7, int32_t ___allocator8, const RuntimeMethod* method)
{
	((  void (*) (TrackableChanges_1_t316F273AB927198D6305E965CDB7B0ED758920E6 *, void*, int32_t, void*, int32_t, void*, int32_t, XRFace_tF2B2E9B06813BA74F5DAFD527FD249DD1002B7C7 , int32_t, int32_t, const RuntimeMethod*))TrackableChanges_1__ctor_m9D3306EC438EE09208F98E4F4049E615CC5E801E_gshared)(__this, ___addedPtr0, ___addedCount1, ___updatedPtr2, ___updatedCount3, ___removedPtr4, ___removedCount5, ___defaultT6, ___stride7, ___allocator8, method);
}
// System.Void Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.XRFace>::CopyFrom(Unity.Collections.NativeArray`1<!0>)
inline void NativeArray_1_CopyFrom_mF9D212E6958D4B634ED7644625EC61A9C7605CDB (NativeArray_1_tAA72EF264612AEC585CFAA055B86F6B65CA4E2E5 * __this, NativeArray_1_tAA72EF264612AEC585CFAA055B86F6B65CA4E2E5  p0, const RuntimeMethod* method)
{
	((  void (*) (NativeArray_1_tAA72EF264612AEC585CFAA055B86F6B65CA4E2E5 *, NativeArray_1_tAA72EF264612AEC585CFAA055B86F6B65CA4E2E5 , const RuntimeMethod*))NativeArray_1_CopyFrom_mF9D212E6958D4B634ED7644625EC61A9C7605CDB_gshared)(__this, p0, method);
}
// System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRFace>::.ctor(Unity.Collections.NativeArray`1<T>,Unity.Collections.NativeArray`1<T>,Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.TrackableId>)
inline void TrackableChanges_1__ctor_m311F799F3BF5B7CDF7B32E9A6B5CF320D1D0A375 (TrackableChanges_1_t316F273AB927198D6305E965CDB7B0ED758920E6 * __this, NativeArray_1_tAA72EF264612AEC585CFAA055B86F6B65CA4E2E5  p0, NativeArray_1_tAA72EF264612AEC585CFAA055B86F6B65CA4E2E5  p1, NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  p2, const RuntimeMethod* method)
{
	((  void (*) (TrackableChanges_1_t316F273AB927198D6305E965CDB7B0ED758920E6 *, NativeArray_1_tAA72EF264612AEC585CFAA055B86F6B65CA4E2E5 , NativeArray_1_tAA72EF264612AEC585CFAA055B86F6B65CA4E2E5 , NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 , const RuntimeMethod*))TrackableChanges_1__ctor_m311F799F3BF5B7CDF7B32E9A6B5CF320D1D0A375_gshared)(__this, p0, p1, p2, method);
}
// System.Void Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.XRFace>::Dispose()
inline void NativeArray_1_Dispose_m343FC4D068AE0BA3141FDF70567F8BD2A752FF2D (NativeArray_1_tAA72EF264612AEC585CFAA055B86F6B65CA4E2E5 * __this, const RuntimeMethod* method)
{
	((  void (*) (NativeArray_1_tAA72EF264612AEC585CFAA055B86F6B65CA4E2E5 *, const RuntimeMethod*))NativeArray_1_Dispose_m343FC4D068AE0BA3141FDF70567F8BD2A752FF2D_gshared)(__this, method);
}
// System.Void Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.TrackableId>::Dispose()
inline void NativeArray_1_Dispose_mB8124101177299FC76251C4E7F6C98BB0E5503C7 (NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 * __this, const RuntimeMethod* method)
{
	((  void (*) (NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 *, const RuntimeMethod*))NativeArray_1_Dispose_mB8124101177299FC76251C4E7F6C98BB0E5503C7_gshared)(__this, method);
}
// System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRFace>::Dispose()
inline void TrackableChanges_1_Dispose_m8CB8D80766E7529B391391E43158D99E39220FF6 (TrackableChanges_1_t316F273AB927198D6305E965CDB7B0ED758920E6 * __this, const RuntimeMethod* method)
{
	((  void (*) (TrackableChanges_1_t316F273AB927198D6305E965CDB7B0ED758920E6 *, const RuntimeMethod*))TrackableChanges_1_Dispose_m8CB8D80766E7529B391391E43158D99E39220FF6_gshared)(__this, method);
}
// Unity.Collections.NativeArray`1<T> UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRHumanBody>::get_added()
inline NativeArray_1_t140FCB442630D549508192A9E3737425E84D15F8  TrackableChanges_1_get_added_mAC12696B7D12A00A3FE9C34A2FF8F22ED6A3BD6C (TrackableChanges_1_t8F77CF9D37FD0DCA052A7D6B34E1A489AF13C41B * __this, const RuntimeMethod* method)
{
	return ((  NativeArray_1_t140FCB442630D549508192A9E3737425E84D15F8  (*) (TrackableChanges_1_t8F77CF9D37FD0DCA052A7D6B34E1A489AF13C41B *, const RuntimeMethod*))TrackableChanges_1_get_added_mAC12696B7D12A00A3FE9C34A2FF8F22ED6A3BD6C_gshared)(__this, method);
}
// Unity.Collections.NativeArray`1<T> UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRHumanBody>::get_updated()
inline NativeArray_1_t140FCB442630D549508192A9E3737425E84D15F8  TrackableChanges_1_get_updated_mCD92C9A6154DC58C63992B59E6671BC8AE07F9C8 (TrackableChanges_1_t8F77CF9D37FD0DCA052A7D6B34E1A489AF13C41B * __this, const RuntimeMethod* method)
{
	return ((  NativeArray_1_t140FCB442630D549508192A9E3737425E84D15F8  (*) (TrackableChanges_1_t8F77CF9D37FD0DCA052A7D6B34E1A489AF13C41B *, const RuntimeMethod*))TrackableChanges_1_get_updated_mCD92C9A6154DC58C63992B59E6671BC8AE07F9C8_gshared)(__this, method);
}
// Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.TrackableId> UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRHumanBody>::get_removed()
inline NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  TrackableChanges_1_get_removed_m377C6A3BFE41CC689AFE09CE1529B79113140DD2 (TrackableChanges_1_t8F77CF9D37FD0DCA052A7D6B34E1A489AF13C41B * __this, const RuntimeMethod* method)
{
	return ((  NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  (*) (TrackableChanges_1_t8F77CF9D37FD0DCA052A7D6B34E1A489AF13C41B *, const RuntimeMethod*))TrackableChanges_1_get_removed_m377C6A3BFE41CC689AFE09CE1529B79113140DD2_gshared)(__this, method);
}
// System.Boolean UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRHumanBody>::get_isCreated()
inline bool TrackableChanges_1_get_isCreated_m68F2E3D10D821918F3C0002EAA07440EC60CB4B0 (TrackableChanges_1_t8F77CF9D37FD0DCA052A7D6B34E1A489AF13C41B * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (TrackableChanges_1_t8F77CF9D37FD0DCA052A7D6B34E1A489AF13C41B *, const RuntimeMethod*))TrackableChanges_1_get_isCreated_m68F2E3D10D821918F3C0002EAA07440EC60CB4B0_gshared)(__this, method);
}
// System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRHumanBody>::set_isCreated(System.Boolean)
inline void TrackableChanges_1_set_isCreated_m6FF97BC809D88ABBCD88D515340CBC1F1EE27D83 (TrackableChanges_1_t8F77CF9D37FD0DCA052A7D6B34E1A489AF13C41B * __this, bool ___value0, const RuntimeMethod* method)
{
	((  void (*) (TrackableChanges_1_t8F77CF9D37FD0DCA052A7D6B34E1A489AF13C41B *, bool, const RuntimeMethod*))TrackableChanges_1_set_isCreated_m6FF97BC809D88ABBCD88D515340CBC1F1EE27D83_gshared)(__this, ___value0, method);
}
// System.Void Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.XRHumanBody>::.ctor(System.Int32,Unity.Collections.Allocator,Unity.Collections.NativeArrayOptions)
inline void NativeArray_1__ctor_mF5A1714C6870A3DF6F61BB87B0700F0A8A67F9CC (NativeArray_1_t140FCB442630D549508192A9E3737425E84D15F8 * __this, int32_t p0, int32_t p1, int32_t p2, const RuntimeMethod* method)
{
	((  void (*) (NativeArray_1_t140FCB442630D549508192A9E3737425E84D15F8 *, int32_t, int32_t, int32_t, const RuntimeMethod*))NativeArray_1__ctor_mF5A1714C6870A3DF6F61BB87B0700F0A8A67F9CC_gshared)(__this, p0, p1, p2, method);
}
// System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRHumanBody>::.ctor(System.Int32,System.Int32,System.Int32,Unity.Collections.Allocator)
inline void TrackableChanges_1__ctor_m6A057FF02DA6FD8876C995CE890C7D8B3A1EBFB0 (TrackableChanges_1_t8F77CF9D37FD0DCA052A7D6B34E1A489AF13C41B * __this, int32_t ___addedCount0, int32_t ___updatedCount1, int32_t ___removedCount2, int32_t ___allocator3, const RuntimeMethod* method)
{
	((  void (*) (TrackableChanges_1_t8F77CF9D37FD0DCA052A7D6B34E1A489AF13C41B *, int32_t, int32_t, int32_t, int32_t, const RuntimeMethod*))TrackableChanges_1__ctor_m6A057FF02DA6FD8876C995CE890C7D8B3A1EBFB0_gshared)(__this, ___addedCount0, ___updatedCount1, ___removedCount2, ___allocator3, method);
}
// System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRHumanBody>::.ctor(System.Void*,System.Int32,System.Void*,System.Int32,System.Void*,System.Int32,T,System.Int32,Unity.Collections.Allocator)
inline void TrackableChanges_1__ctor_m859CA5B973D4AD250F12979A582B9F5D56007BFD (TrackableChanges_1_t8F77CF9D37FD0DCA052A7D6B34E1A489AF13C41B * __this, void* ___addedPtr0, int32_t ___addedCount1, void* ___updatedPtr2, int32_t ___updatedCount3, void* ___removedPtr4, int32_t ___removedCount5, XRHumanBody_t64938399EB40376E99745517E15D42FFC4604BC1  ___defaultT6, int32_t ___stride7, int32_t ___allocator8, const RuntimeMethod* method)
{
	((  void (*) (TrackableChanges_1_t8F77CF9D37FD0DCA052A7D6B34E1A489AF13C41B *, void*, int32_t, void*, int32_t, void*, int32_t, XRHumanBody_t64938399EB40376E99745517E15D42FFC4604BC1 , int32_t, int32_t, const RuntimeMethod*))TrackableChanges_1__ctor_m859CA5B973D4AD250F12979A582B9F5D56007BFD_gshared)(__this, ___addedPtr0, ___addedCount1, ___updatedPtr2, ___updatedCount3, ___removedPtr4, ___removedCount5, ___defaultT6, ___stride7, ___allocator8, method);
}
// System.Void Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.XRHumanBody>::CopyFrom(Unity.Collections.NativeArray`1<!0>)
inline void NativeArray_1_CopyFrom_m476CD42B4BF6AFC03517ED3C99ADB1FFC44B5531 (NativeArray_1_t140FCB442630D549508192A9E3737425E84D15F8 * __this, NativeArray_1_t140FCB442630D549508192A9E3737425E84D15F8  p0, const RuntimeMethod* method)
{
	((  void (*) (NativeArray_1_t140FCB442630D549508192A9E3737425E84D15F8 *, NativeArray_1_t140FCB442630D549508192A9E3737425E84D15F8 , const RuntimeMethod*))NativeArray_1_CopyFrom_m476CD42B4BF6AFC03517ED3C99ADB1FFC44B5531_gshared)(__this, p0, method);
}
// System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRHumanBody>::.ctor(Unity.Collections.NativeArray`1<T>,Unity.Collections.NativeArray`1<T>,Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.TrackableId>)
inline void TrackableChanges_1__ctor_mCBFAB9AC1ADBAED983EEC593665EBBE6337E9A3B (TrackableChanges_1_t8F77CF9D37FD0DCA052A7D6B34E1A489AF13C41B * __this, NativeArray_1_t140FCB442630D549508192A9E3737425E84D15F8  p0, NativeArray_1_t140FCB442630D549508192A9E3737425E84D15F8  p1, NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  p2, const RuntimeMethod* method)
{
	((  void (*) (TrackableChanges_1_t8F77CF9D37FD0DCA052A7D6B34E1A489AF13C41B *, NativeArray_1_t140FCB442630D549508192A9E3737425E84D15F8 , NativeArray_1_t140FCB442630D549508192A9E3737425E84D15F8 , NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 , const RuntimeMethod*))TrackableChanges_1__ctor_mCBFAB9AC1ADBAED983EEC593665EBBE6337E9A3B_gshared)(__this, p0, p1, p2, method);
}
// System.Void Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.XRHumanBody>::Dispose()
inline void NativeArray_1_Dispose_mF075AE11F3DFF21CA4E5A17B28A0F9D1B52D5926 (NativeArray_1_t140FCB442630D549508192A9E3737425E84D15F8 * __this, const RuntimeMethod* method)
{
	((  void (*) (NativeArray_1_t140FCB442630D549508192A9E3737425E84D15F8 *, const RuntimeMethod*))NativeArray_1_Dispose_mF075AE11F3DFF21CA4E5A17B28A0F9D1B52D5926_gshared)(__this, method);
}
// System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRHumanBody>::Dispose()
inline void TrackableChanges_1_Dispose_m7F29A9FDEA683B63D471FB1D5505A6D8EC7A107A (TrackableChanges_1_t8F77CF9D37FD0DCA052A7D6B34E1A489AF13C41B * __this, const RuntimeMethod* method)
{
	((  void (*) (TrackableChanges_1_t8F77CF9D37FD0DCA052A7D6B34E1A489AF13C41B *, const RuntimeMethod*))TrackableChanges_1_Dispose_m7F29A9FDEA683B63D471FB1D5505A6D8EC7A107A_gshared)(__this, method);
}
// Unity.Collections.NativeArray`1<T> UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRPointCloud>::get_added()
inline NativeArray_1_t5F0698DD293100E2FECB8BC55FCBC3A3F6FEA582  TrackableChanges_1_get_added_mBB3A80E298216A6D8826422F62A11A1A4CB7CBD9 (TrackableChanges_1_t67F5E50C0B7A063774AEC211C4C81BA7679876BB * __this, const RuntimeMethod* method)
{
	return ((  NativeArray_1_t5F0698DD293100E2FECB8BC55FCBC3A3F6FEA582  (*) (TrackableChanges_1_t67F5E50C0B7A063774AEC211C4C81BA7679876BB *, const RuntimeMethod*))TrackableChanges_1_get_added_mBB3A80E298216A6D8826422F62A11A1A4CB7CBD9_gshared)(__this, method);
}
// Unity.Collections.NativeArray`1<T> UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRPointCloud>::get_updated()
inline NativeArray_1_t5F0698DD293100E2FECB8BC55FCBC3A3F6FEA582  TrackableChanges_1_get_updated_m80DB51775906B404AEE4C7454E7A517ECFD6A003 (TrackableChanges_1_t67F5E50C0B7A063774AEC211C4C81BA7679876BB * __this, const RuntimeMethod* method)
{
	return ((  NativeArray_1_t5F0698DD293100E2FECB8BC55FCBC3A3F6FEA582  (*) (TrackableChanges_1_t67F5E50C0B7A063774AEC211C4C81BA7679876BB *, const RuntimeMethod*))TrackableChanges_1_get_updated_m80DB51775906B404AEE4C7454E7A517ECFD6A003_gshared)(__this, method);
}
// Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.TrackableId> UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRPointCloud>::get_removed()
inline NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  TrackableChanges_1_get_removed_mC49E91CAF67469CDD76DBCF18D1423DC3971AF99 (TrackableChanges_1_t67F5E50C0B7A063774AEC211C4C81BA7679876BB * __this, const RuntimeMethod* method)
{
	return ((  NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  (*) (TrackableChanges_1_t67F5E50C0B7A063774AEC211C4C81BA7679876BB *, const RuntimeMethod*))TrackableChanges_1_get_removed_mC49E91CAF67469CDD76DBCF18D1423DC3971AF99_gshared)(__this, method);
}
// System.Boolean UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRPointCloud>::get_isCreated()
inline bool TrackableChanges_1_get_isCreated_m6BDB6E85EF722CE4F115552E754E059F9BBA87B8 (TrackableChanges_1_t67F5E50C0B7A063774AEC211C4C81BA7679876BB * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (TrackableChanges_1_t67F5E50C0B7A063774AEC211C4C81BA7679876BB *, const RuntimeMethod*))TrackableChanges_1_get_isCreated_m6BDB6E85EF722CE4F115552E754E059F9BBA87B8_gshared)(__this, method);
}
// System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRPointCloud>::set_isCreated(System.Boolean)
inline void TrackableChanges_1_set_isCreated_mE659C5BCD61914E799B4D6AAE6AD5AD7B9DDCE76 (TrackableChanges_1_t67F5E50C0B7A063774AEC211C4C81BA7679876BB * __this, bool ___value0, const RuntimeMethod* method)
{
	((  void (*) (TrackableChanges_1_t67F5E50C0B7A063774AEC211C4C81BA7679876BB *, bool, const RuntimeMethod*))TrackableChanges_1_set_isCreated_mE659C5BCD61914E799B4D6AAE6AD5AD7B9DDCE76_gshared)(__this, ___value0, method);
}
// System.Void Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.XRPointCloud>::.ctor(System.Int32,Unity.Collections.Allocator,Unity.Collections.NativeArrayOptions)
inline void NativeArray_1__ctor_m0A7431FE284605A389AB455AFBC44304CAE118CF (NativeArray_1_t5F0698DD293100E2FECB8BC55FCBC3A3F6FEA582 * __this, int32_t p0, int32_t p1, int32_t p2, const RuntimeMethod* method)
{
	((  void (*) (NativeArray_1_t5F0698DD293100E2FECB8BC55FCBC3A3F6FEA582 *, int32_t, int32_t, int32_t, const RuntimeMethod*))NativeArray_1__ctor_m0A7431FE284605A389AB455AFBC44304CAE118CF_gshared)(__this, p0, p1, p2, method);
}
// System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRPointCloud>::.ctor(System.Int32,System.Int32,System.Int32,Unity.Collections.Allocator)
inline void TrackableChanges_1__ctor_mB26E1128CC08E676885A96E341B8CE2DC1228D20 (TrackableChanges_1_t67F5E50C0B7A063774AEC211C4C81BA7679876BB * __this, int32_t ___addedCount0, int32_t ___updatedCount1, int32_t ___removedCount2, int32_t ___allocator3, const RuntimeMethod* method)
{
	((  void (*) (TrackableChanges_1_t67F5E50C0B7A063774AEC211C4C81BA7679876BB *, int32_t, int32_t, int32_t, int32_t, const RuntimeMethod*))TrackableChanges_1__ctor_mB26E1128CC08E676885A96E341B8CE2DC1228D20_gshared)(__this, ___addedCount0, ___updatedCount1, ___removedCount2, ___allocator3, method);
}
// System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRPointCloud>::.ctor(System.Void*,System.Int32,System.Void*,System.Int32,System.Void*,System.Int32,T,System.Int32,Unity.Collections.Allocator)
inline void TrackableChanges_1__ctor_mA69A666BCF692D5A8660A564A4450ABF6C80FD90 (TrackableChanges_1_t67F5E50C0B7A063774AEC211C4C81BA7679876BB * __this, void* ___addedPtr0, int32_t ___addedCount1, void* ___updatedPtr2, int32_t ___updatedCount3, void* ___removedPtr4, int32_t ___removedCount5, XRPointCloud_tA4A412DE503530E1B2953919F1463B9B48504ED0  ___defaultT6, int32_t ___stride7, int32_t ___allocator8, const RuntimeMethod* method)
{
	((  void (*) (TrackableChanges_1_t67F5E50C0B7A063774AEC211C4C81BA7679876BB *, void*, int32_t, void*, int32_t, void*, int32_t, XRPointCloud_tA4A412DE503530E1B2953919F1463B9B48504ED0 , int32_t, int32_t, const RuntimeMethod*))TrackableChanges_1__ctor_mA69A666BCF692D5A8660A564A4450ABF6C80FD90_gshared)(__this, ___addedPtr0, ___addedCount1, ___updatedPtr2, ___updatedCount3, ___removedPtr4, ___removedCount5, ___defaultT6, ___stride7, ___allocator8, method);
}
// System.Void Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.XRPointCloud>::CopyFrom(Unity.Collections.NativeArray`1<!0>)
inline void NativeArray_1_CopyFrom_mE62AAA10EC11C27A6A545BEF481C0FC3DB4800B3 (NativeArray_1_t5F0698DD293100E2FECB8BC55FCBC3A3F6FEA582 * __this, NativeArray_1_t5F0698DD293100E2FECB8BC55FCBC3A3F6FEA582  p0, const RuntimeMethod* method)
{
	((  void (*) (NativeArray_1_t5F0698DD293100E2FECB8BC55FCBC3A3F6FEA582 *, NativeArray_1_t5F0698DD293100E2FECB8BC55FCBC3A3F6FEA582 , const RuntimeMethod*))NativeArray_1_CopyFrom_mE62AAA10EC11C27A6A545BEF481C0FC3DB4800B3_gshared)(__this, p0, method);
}
// System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRPointCloud>::.ctor(Unity.Collections.NativeArray`1<T>,Unity.Collections.NativeArray`1<T>,Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.TrackableId>)
inline void TrackableChanges_1__ctor_m4961BB84AABB2C5A4D492E4994845A231BFB09BF (TrackableChanges_1_t67F5E50C0B7A063774AEC211C4C81BA7679876BB * __this, NativeArray_1_t5F0698DD293100E2FECB8BC55FCBC3A3F6FEA582  p0, NativeArray_1_t5F0698DD293100E2FECB8BC55FCBC3A3F6FEA582  p1, NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  p2, const RuntimeMethod* method)
{
	((  void (*) (TrackableChanges_1_t67F5E50C0B7A063774AEC211C4C81BA7679876BB *, NativeArray_1_t5F0698DD293100E2FECB8BC55FCBC3A3F6FEA582 , NativeArray_1_t5F0698DD293100E2FECB8BC55FCBC3A3F6FEA582 , NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 , const RuntimeMethod*))TrackableChanges_1__ctor_m4961BB84AABB2C5A4D492E4994845A231BFB09BF_gshared)(__this, p0, p1, p2, method);
}
// System.Void Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.XRPointCloud>::Dispose()
inline void NativeArray_1_Dispose_mAF31B95CE89FF647372855A4ECFB9FCDDE39C38B (NativeArray_1_t5F0698DD293100E2FECB8BC55FCBC3A3F6FEA582 * __this, const RuntimeMethod* method)
{
	((  void (*) (NativeArray_1_t5F0698DD293100E2FECB8BC55FCBC3A3F6FEA582 *, const RuntimeMethod*))NativeArray_1_Dispose_mAF31B95CE89FF647372855A4ECFB9FCDDE39C38B_gshared)(__this, method);
}
// System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRPointCloud>::Dispose()
inline void TrackableChanges_1_Dispose_mE3DC3C0230641A828B6A91F3CB94441797520834 (TrackableChanges_1_t67F5E50C0B7A063774AEC211C4C81BA7679876BB * __this, const RuntimeMethod* method)
{
	((  void (*) (TrackableChanges_1_t67F5E50C0B7A063774AEC211C4C81BA7679876BB *, const RuntimeMethod*))TrackableChanges_1_Dispose_mE3DC3C0230641A828B6A91F3CB94441797520834_gshared)(__this, method);
}
// Unity.Collections.NativeArray`1<T> UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRReferencePoint>::get_added()
inline NativeArray_1_t5AADFB4C72573FE3017795F15B8CBC88625A8876  TrackableChanges_1_get_added_mC31FEFAAC8F70ABBBC324DC618B0DFAB08AAE934 (TrackableChanges_1_t5C8C3FBA23E5BBC147A6991B68520A756EF54FDF * __this, const RuntimeMethod* method)
{
	return ((  NativeArray_1_t5AADFB4C72573FE3017795F15B8CBC88625A8876  (*) (TrackableChanges_1_t5C8C3FBA23E5BBC147A6991B68520A756EF54FDF *, const RuntimeMethod*))TrackableChanges_1_get_added_mC31FEFAAC8F70ABBBC324DC618B0DFAB08AAE934_gshared)(__this, method);
}
// Unity.Collections.NativeArray`1<T> UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRReferencePoint>::get_updated()
inline NativeArray_1_t5AADFB4C72573FE3017795F15B8CBC88625A8876  TrackableChanges_1_get_updated_m54C710EFD531DFB25ABA289B60FAA4181D479DDF (TrackableChanges_1_t5C8C3FBA23E5BBC147A6991B68520A756EF54FDF * __this, const RuntimeMethod* method)
{
	return ((  NativeArray_1_t5AADFB4C72573FE3017795F15B8CBC88625A8876  (*) (TrackableChanges_1_t5C8C3FBA23E5BBC147A6991B68520A756EF54FDF *, const RuntimeMethod*))TrackableChanges_1_get_updated_m54C710EFD531DFB25ABA289B60FAA4181D479DDF_gshared)(__this, method);
}
// Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.TrackableId> UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRReferencePoint>::get_removed()
inline NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  TrackableChanges_1_get_removed_m9781A441FA95E17CBDB3C4687247859A562AF077 (TrackableChanges_1_t5C8C3FBA23E5BBC147A6991B68520A756EF54FDF * __this, const RuntimeMethod* method)
{
	return ((  NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  (*) (TrackableChanges_1_t5C8C3FBA23E5BBC147A6991B68520A756EF54FDF *, const RuntimeMethod*))TrackableChanges_1_get_removed_m9781A441FA95E17CBDB3C4687247859A562AF077_gshared)(__this, method);
}
// System.Boolean UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRReferencePoint>::get_isCreated()
inline bool TrackableChanges_1_get_isCreated_m81592B6012FD82C68A023EC52FFAB3B4A35C6A3D (TrackableChanges_1_t5C8C3FBA23E5BBC147A6991B68520A756EF54FDF * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (TrackableChanges_1_t5C8C3FBA23E5BBC147A6991B68520A756EF54FDF *, const RuntimeMethod*))TrackableChanges_1_get_isCreated_m81592B6012FD82C68A023EC52FFAB3B4A35C6A3D_gshared)(__this, method);
}
// System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRReferencePoint>::set_isCreated(System.Boolean)
inline void TrackableChanges_1_set_isCreated_m3B57EAEC482D7B5D700E044B8720F89F25B13DC5 (TrackableChanges_1_t5C8C3FBA23E5BBC147A6991B68520A756EF54FDF * __this, bool ___value0, const RuntimeMethod* method)
{
	((  void (*) (TrackableChanges_1_t5C8C3FBA23E5BBC147A6991B68520A756EF54FDF *, bool, const RuntimeMethod*))TrackableChanges_1_set_isCreated_m3B57EAEC482D7B5D700E044B8720F89F25B13DC5_gshared)(__this, ___value0, method);
}
// System.Void Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.XRReferencePoint>::.ctor(System.Int32,Unity.Collections.Allocator,Unity.Collections.NativeArrayOptions)
inline void NativeArray_1__ctor_mBA3667DFD2FA91C71C118ECA43EB22503D4FC2C8 (NativeArray_1_t5AADFB4C72573FE3017795F15B8CBC88625A8876 * __this, int32_t p0, int32_t p1, int32_t p2, const RuntimeMethod* method)
{
	((  void (*) (NativeArray_1_t5AADFB4C72573FE3017795F15B8CBC88625A8876 *, int32_t, int32_t, int32_t, const RuntimeMethod*))NativeArray_1__ctor_mBA3667DFD2FA91C71C118ECA43EB22503D4FC2C8_gshared)(__this, p0, p1, p2, method);
}
// System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRReferencePoint>::.ctor(System.Int32,System.Int32,System.Int32,Unity.Collections.Allocator)
inline void TrackableChanges_1__ctor_m8E2935D6FEAB365BB8BBF74A7D12694909DFB282 (TrackableChanges_1_t5C8C3FBA23E5BBC147A6991B68520A756EF54FDF * __this, int32_t ___addedCount0, int32_t ___updatedCount1, int32_t ___removedCount2, int32_t ___allocator3, const RuntimeMethod* method)
{
	((  void (*) (TrackableChanges_1_t5C8C3FBA23E5BBC147A6991B68520A756EF54FDF *, int32_t, int32_t, int32_t, int32_t, const RuntimeMethod*))TrackableChanges_1__ctor_m8E2935D6FEAB365BB8BBF74A7D12694909DFB282_gshared)(__this, ___addedCount0, ___updatedCount1, ___removedCount2, ___allocator3, method);
}
// System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRReferencePoint>::.ctor(System.Void*,System.Int32,System.Void*,System.Int32,System.Void*,System.Int32,T,System.Int32,Unity.Collections.Allocator)
inline void TrackableChanges_1__ctor_m47C55466595329D4629E9A78A403D34C94AC30FC (TrackableChanges_1_t5C8C3FBA23E5BBC147A6991B68520A756EF54FDF * __this, void* ___addedPtr0, int32_t ___addedCount1, void* ___updatedPtr2, int32_t ___updatedCount3, void* ___removedPtr4, int32_t ___removedCount5, XRReferencePoint_tA8592C08A27EC91D9B1FB3B083C95C5D372FF1F9  ___defaultT6, int32_t ___stride7, int32_t ___allocator8, const RuntimeMethod* method)
{
	((  void (*) (TrackableChanges_1_t5C8C3FBA23E5BBC147A6991B68520A756EF54FDF *, void*, int32_t, void*, int32_t, void*, int32_t, XRReferencePoint_tA8592C08A27EC91D9B1FB3B083C95C5D372FF1F9 , int32_t, int32_t, const RuntimeMethod*))TrackableChanges_1__ctor_m47C55466595329D4629E9A78A403D34C94AC30FC_gshared)(__this, ___addedPtr0, ___addedCount1, ___updatedPtr2, ___updatedCount3, ___removedPtr4, ___removedCount5, ___defaultT6, ___stride7, ___allocator8, method);
}
// System.Void Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.XRReferencePoint>::CopyFrom(Unity.Collections.NativeArray`1<!0>)
inline void NativeArray_1_CopyFrom_mC053D3A9DF2FBD4DCB31D2864F4996E9B2DA255A (NativeArray_1_t5AADFB4C72573FE3017795F15B8CBC88625A8876 * __this, NativeArray_1_t5AADFB4C72573FE3017795F15B8CBC88625A8876  p0, const RuntimeMethod* method)
{
	((  void (*) (NativeArray_1_t5AADFB4C72573FE3017795F15B8CBC88625A8876 *, NativeArray_1_t5AADFB4C72573FE3017795F15B8CBC88625A8876 , const RuntimeMethod*))NativeArray_1_CopyFrom_mC053D3A9DF2FBD4DCB31D2864F4996E9B2DA255A_gshared)(__this, p0, method);
}
// System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRReferencePoint>::.ctor(Unity.Collections.NativeArray`1<T>,Unity.Collections.NativeArray`1<T>,Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.TrackableId>)
inline void TrackableChanges_1__ctor_m5E677E3D08A7D26921EA074FB0A849F566DE5951 (TrackableChanges_1_t5C8C3FBA23E5BBC147A6991B68520A756EF54FDF * __this, NativeArray_1_t5AADFB4C72573FE3017795F15B8CBC88625A8876  p0, NativeArray_1_t5AADFB4C72573FE3017795F15B8CBC88625A8876  p1, NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  p2, const RuntimeMethod* method)
{
	((  void (*) (TrackableChanges_1_t5C8C3FBA23E5BBC147A6991B68520A756EF54FDF *, NativeArray_1_t5AADFB4C72573FE3017795F15B8CBC88625A8876 , NativeArray_1_t5AADFB4C72573FE3017795F15B8CBC88625A8876 , NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 , const RuntimeMethod*))TrackableChanges_1__ctor_m5E677E3D08A7D26921EA074FB0A849F566DE5951_gshared)(__this, p0, p1, p2, method);
}
// System.Void Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.XRReferencePoint>::Dispose()
inline void NativeArray_1_Dispose_m79A1BBDC8A7E6F17C8B48E7CB3ED027A3180039E (NativeArray_1_t5AADFB4C72573FE3017795F15B8CBC88625A8876 * __this, const RuntimeMethod* method)
{
	((  void (*) (NativeArray_1_t5AADFB4C72573FE3017795F15B8CBC88625A8876 *, const RuntimeMethod*))NativeArray_1_Dispose_m79A1BBDC8A7E6F17C8B48E7CB3ED027A3180039E_gshared)(__this, method);
}
// System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRReferencePoint>::Dispose()
inline void TrackableChanges_1_Dispose_m1C8DF8C1879FD6A22AD7474E15FB2590BB4F9631 (TrackableChanges_1_t5C8C3FBA23E5BBC147A6991B68520A756EF54FDF * __this, const RuntimeMethod* method)
{
	((  void (*) (TrackableChanges_1_t5C8C3FBA23E5BBC147A6991B68520A756EF54FDF *, const RuntimeMethod*))TrackableChanges_1_Dispose_m1C8DF8C1879FD6A22AD7474E15FB2590BB4F9631_gshared)(__this, method);
}
// Unity.Collections.NativeArray`1<T> UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRTrackedImage>::get_added()
inline NativeArray_1_t70F653CBE89924503B639B2438D6FF5973388AC1  TrackableChanges_1_get_added_mC35012A2E03A744ECF4A6E58D2DA1C34D85CA6ED (TrackableChanges_1_tE90399F00562881A054A3592F7AF134BA053AF4F * __this, const RuntimeMethod* method)
{
	return ((  NativeArray_1_t70F653CBE89924503B639B2438D6FF5973388AC1  (*) (TrackableChanges_1_tE90399F00562881A054A3592F7AF134BA053AF4F *, const RuntimeMethod*))TrackableChanges_1_get_added_mC35012A2E03A744ECF4A6E58D2DA1C34D85CA6ED_gshared)(__this, method);
}
// Unity.Collections.NativeArray`1<T> UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRTrackedImage>::get_updated()
inline NativeArray_1_t70F653CBE89924503B639B2438D6FF5973388AC1  TrackableChanges_1_get_updated_mF1CCDAA99909A241F138D24B4175AD6806E65323 (TrackableChanges_1_tE90399F00562881A054A3592F7AF134BA053AF4F * __this, const RuntimeMethod* method)
{
	return ((  NativeArray_1_t70F653CBE89924503B639B2438D6FF5973388AC1  (*) (TrackableChanges_1_tE90399F00562881A054A3592F7AF134BA053AF4F *, const RuntimeMethod*))TrackableChanges_1_get_updated_mF1CCDAA99909A241F138D24B4175AD6806E65323_gshared)(__this, method);
}
// Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.TrackableId> UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRTrackedImage>::get_removed()
inline NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  TrackableChanges_1_get_removed_m7CF311E706E890B2CC9281BC392849E4DE880ECA (TrackableChanges_1_tE90399F00562881A054A3592F7AF134BA053AF4F * __this, const RuntimeMethod* method)
{
	return ((  NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  (*) (TrackableChanges_1_tE90399F00562881A054A3592F7AF134BA053AF4F *, const RuntimeMethod*))TrackableChanges_1_get_removed_m7CF311E706E890B2CC9281BC392849E4DE880ECA_gshared)(__this, method);
}
// System.Boolean UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRTrackedImage>::get_isCreated()
inline bool TrackableChanges_1_get_isCreated_mEEE0707FBE540E3961450502B2C2771AA02ABF83 (TrackableChanges_1_tE90399F00562881A054A3592F7AF134BA053AF4F * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (TrackableChanges_1_tE90399F00562881A054A3592F7AF134BA053AF4F *, const RuntimeMethod*))TrackableChanges_1_get_isCreated_mEEE0707FBE540E3961450502B2C2771AA02ABF83_gshared)(__this, method);
}
// System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRTrackedImage>::set_isCreated(System.Boolean)
inline void TrackableChanges_1_set_isCreated_m360183150F0DE3BAE45AD1C6C942146BF81F51D4 (TrackableChanges_1_tE90399F00562881A054A3592F7AF134BA053AF4F * __this, bool ___value0, const RuntimeMethod* method)
{
	((  void (*) (TrackableChanges_1_tE90399F00562881A054A3592F7AF134BA053AF4F *, bool, const RuntimeMethod*))TrackableChanges_1_set_isCreated_m360183150F0DE3BAE45AD1C6C942146BF81F51D4_gshared)(__this, ___value0, method);
}
// System.Void Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.XRTrackedImage>::.ctor(System.Int32,Unity.Collections.Allocator,Unity.Collections.NativeArrayOptions)
inline void NativeArray_1__ctor_m192CF93170B36E61ACE804945C3B18481D07B1C5 (NativeArray_1_t70F653CBE89924503B639B2438D6FF5973388AC1 * __this, int32_t p0, int32_t p1, int32_t p2, const RuntimeMethod* method)
{
	((  void (*) (NativeArray_1_t70F653CBE89924503B639B2438D6FF5973388AC1 *, int32_t, int32_t, int32_t, const RuntimeMethod*))NativeArray_1__ctor_m192CF93170B36E61ACE804945C3B18481D07B1C5_gshared)(__this, p0, p1, p2, method);
}
// System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRTrackedImage>::.ctor(System.Int32,System.Int32,System.Int32,Unity.Collections.Allocator)
inline void TrackableChanges_1__ctor_m88DBEB1615B50F97CBE740AA5502C0DFA42D7641 (TrackableChanges_1_tE90399F00562881A054A3592F7AF134BA053AF4F * __this, int32_t ___addedCount0, int32_t ___updatedCount1, int32_t ___removedCount2, int32_t ___allocator3, const RuntimeMethod* method)
{
	((  void (*) (TrackableChanges_1_tE90399F00562881A054A3592F7AF134BA053AF4F *, int32_t, int32_t, int32_t, int32_t, const RuntimeMethod*))TrackableChanges_1__ctor_m88DBEB1615B50F97CBE740AA5502C0DFA42D7641_gshared)(__this, ___addedCount0, ___updatedCount1, ___removedCount2, ___allocator3, method);
}
// System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRTrackedImage>::.ctor(System.Void*,System.Int32,System.Void*,System.Int32,System.Void*,System.Int32,T,System.Int32,Unity.Collections.Allocator)
inline void TrackableChanges_1__ctor_m3477ECC2A1A2FF3D7287DF3B5BDFC7F272FDB3EE (TrackableChanges_1_tE90399F00562881A054A3592F7AF134BA053AF4F * __this, void* ___addedPtr0, int32_t ___addedCount1, void* ___updatedPtr2, int32_t ___updatedCount3, void* ___removedPtr4, int32_t ___removedCount5, XRTrackedImage_t178EACF5BFA4228DF4EB1899685C533F3F296AA8  ___defaultT6, int32_t ___stride7, int32_t ___allocator8, const RuntimeMethod* method)
{
	((  void (*) (TrackableChanges_1_tE90399F00562881A054A3592F7AF134BA053AF4F *, void*, int32_t, void*, int32_t, void*, int32_t, XRTrackedImage_t178EACF5BFA4228DF4EB1899685C533F3F296AA8 , int32_t, int32_t, const RuntimeMethod*))TrackableChanges_1__ctor_m3477ECC2A1A2FF3D7287DF3B5BDFC7F272FDB3EE_gshared)(__this, ___addedPtr0, ___addedCount1, ___updatedPtr2, ___updatedCount3, ___removedPtr4, ___removedCount5, ___defaultT6, ___stride7, ___allocator8, method);
}
// System.Void Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.XRTrackedImage>::CopyFrom(Unity.Collections.NativeArray`1<!0>)
inline void NativeArray_1_CopyFrom_mC0B092730DE59B585688BCCF8FA9099731798DCD (NativeArray_1_t70F653CBE89924503B639B2438D6FF5973388AC1 * __this, NativeArray_1_t70F653CBE89924503B639B2438D6FF5973388AC1  p0, const RuntimeMethod* method)
{
	((  void (*) (NativeArray_1_t70F653CBE89924503B639B2438D6FF5973388AC1 *, NativeArray_1_t70F653CBE89924503B639B2438D6FF5973388AC1 , const RuntimeMethod*))NativeArray_1_CopyFrom_mC0B092730DE59B585688BCCF8FA9099731798DCD_gshared)(__this, p0, method);
}
// System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRTrackedImage>::.ctor(Unity.Collections.NativeArray`1<T>,Unity.Collections.NativeArray`1<T>,Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.TrackableId>)
inline void TrackableChanges_1__ctor_mC96D4347CE6234FD5E51ECDA265125C19D70BF0C (TrackableChanges_1_tE90399F00562881A054A3592F7AF134BA053AF4F * __this, NativeArray_1_t70F653CBE89924503B639B2438D6FF5973388AC1  p0, NativeArray_1_t70F653CBE89924503B639B2438D6FF5973388AC1  p1, NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  p2, const RuntimeMethod* method)
{
	((  void (*) (TrackableChanges_1_tE90399F00562881A054A3592F7AF134BA053AF4F *, NativeArray_1_t70F653CBE89924503B639B2438D6FF5973388AC1 , NativeArray_1_t70F653CBE89924503B639B2438D6FF5973388AC1 , NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 , const RuntimeMethod*))TrackableChanges_1__ctor_mC96D4347CE6234FD5E51ECDA265125C19D70BF0C_gshared)(__this, p0, p1, p2, method);
}
// System.Void Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.XRTrackedImage>::Dispose()
inline void NativeArray_1_Dispose_m8645D533921C8C5BE22919B57D259A4240AB379A (NativeArray_1_t70F653CBE89924503B639B2438D6FF5973388AC1 * __this, const RuntimeMethod* method)
{
	((  void (*) (NativeArray_1_t70F653CBE89924503B639B2438D6FF5973388AC1 *, const RuntimeMethod*))NativeArray_1_Dispose_m8645D533921C8C5BE22919B57D259A4240AB379A_gshared)(__this, method);
}
// System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRTrackedImage>::Dispose()
inline void TrackableChanges_1_Dispose_m5A25C5A0DF692BA62C0C9E9DD458E28462000702 (TrackableChanges_1_tE90399F00562881A054A3592F7AF134BA053AF4F * __this, const RuntimeMethod* method)
{
	((  void (*) (TrackableChanges_1_tE90399F00562881A054A3592F7AF134BA053AF4F *, const RuntimeMethod*))TrackableChanges_1_Dispose_m5A25C5A0DF692BA62C0C9E9DD458E28462000702_gshared)(__this, method);
}
// Unity.Collections.NativeArray`1<T> UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRTrackedObject>::get_added()
inline NativeArray_1_t738892D030F76D319525B8C000F49D41505DD529  TrackableChanges_1_get_added_m6B4AFC77B682299AFAB977EDEAF164E6B63E3670 (TrackableChanges_1_t03A664B58E58E32B5EC1F7CB7133E87F804EE874 * __this, const RuntimeMethod* method)
{
	return ((  NativeArray_1_t738892D030F76D319525B8C000F49D41505DD529  (*) (TrackableChanges_1_t03A664B58E58E32B5EC1F7CB7133E87F804EE874 *, const RuntimeMethod*))TrackableChanges_1_get_added_m6B4AFC77B682299AFAB977EDEAF164E6B63E3670_gshared)(__this, method);
}
// Unity.Collections.NativeArray`1<T> UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRTrackedObject>::get_updated()
inline NativeArray_1_t738892D030F76D319525B8C000F49D41505DD529  TrackableChanges_1_get_updated_mDDB738464599270A745A15C57FC941EEBEC00700 (TrackableChanges_1_t03A664B58E58E32B5EC1F7CB7133E87F804EE874 * __this, const RuntimeMethod* method)
{
	return ((  NativeArray_1_t738892D030F76D319525B8C000F49D41505DD529  (*) (TrackableChanges_1_t03A664B58E58E32B5EC1F7CB7133E87F804EE874 *, const RuntimeMethod*))TrackableChanges_1_get_updated_mDDB738464599270A745A15C57FC941EEBEC00700_gshared)(__this, method);
}
// Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.TrackableId> UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRTrackedObject>::get_removed()
inline NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  TrackableChanges_1_get_removed_m53E0E551E0ACC552E62D8BA2A0A234D72CAB6C74 (TrackableChanges_1_t03A664B58E58E32B5EC1F7CB7133E87F804EE874 * __this, const RuntimeMethod* method)
{
	return ((  NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  (*) (TrackableChanges_1_t03A664B58E58E32B5EC1F7CB7133E87F804EE874 *, const RuntimeMethod*))TrackableChanges_1_get_removed_m53E0E551E0ACC552E62D8BA2A0A234D72CAB6C74_gshared)(__this, method);
}
// System.Boolean UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRTrackedObject>::get_isCreated()
inline bool TrackableChanges_1_get_isCreated_mBFB9748C1EA377BE047EC13F736E0C8C207E441C (TrackableChanges_1_t03A664B58E58E32B5EC1F7CB7133E87F804EE874 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (TrackableChanges_1_t03A664B58E58E32B5EC1F7CB7133E87F804EE874 *, const RuntimeMethod*))TrackableChanges_1_get_isCreated_mBFB9748C1EA377BE047EC13F736E0C8C207E441C_gshared)(__this, method);
}
// System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRTrackedObject>::set_isCreated(System.Boolean)
inline void TrackableChanges_1_set_isCreated_m085A94096B2D33690423DF6A6777D7586B582D14 (TrackableChanges_1_t03A664B58E58E32B5EC1F7CB7133E87F804EE874 * __this, bool ___value0, const RuntimeMethod* method)
{
	((  void (*) (TrackableChanges_1_t03A664B58E58E32B5EC1F7CB7133E87F804EE874 *, bool, const RuntimeMethod*))TrackableChanges_1_set_isCreated_m085A94096B2D33690423DF6A6777D7586B582D14_gshared)(__this, ___value0, method);
}
// System.Void Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.XRTrackedObject>::.ctor(System.Int32,Unity.Collections.Allocator,Unity.Collections.NativeArrayOptions)
inline void NativeArray_1__ctor_m16425BC62E9AC48C88E58870411D0D63CCE985A9 (NativeArray_1_t738892D030F76D319525B8C000F49D41505DD529 * __this, int32_t p0, int32_t p1, int32_t p2, const RuntimeMethod* method)
{
	((  void (*) (NativeArray_1_t738892D030F76D319525B8C000F49D41505DD529 *, int32_t, int32_t, int32_t, const RuntimeMethod*))NativeArray_1__ctor_m16425BC62E9AC48C88E58870411D0D63CCE985A9_gshared)(__this, p0, p1, p2, method);
}
// System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRTrackedObject>::.ctor(System.Int32,System.Int32,System.Int32,Unity.Collections.Allocator)
inline void TrackableChanges_1__ctor_mBA293905C9592E097DAC0E30B77D458C7D7E23FD (TrackableChanges_1_t03A664B58E58E32B5EC1F7CB7133E87F804EE874 * __this, int32_t ___addedCount0, int32_t ___updatedCount1, int32_t ___removedCount2, int32_t ___allocator3, const RuntimeMethod* method)
{
	((  void (*) (TrackableChanges_1_t03A664B58E58E32B5EC1F7CB7133E87F804EE874 *, int32_t, int32_t, int32_t, int32_t, const RuntimeMethod*))TrackableChanges_1__ctor_mBA293905C9592E097DAC0E30B77D458C7D7E23FD_gshared)(__this, ___addedCount0, ___updatedCount1, ___removedCount2, ___allocator3, method);
}
// System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRTrackedObject>::.ctor(System.Void*,System.Int32,System.Void*,System.Int32,System.Void*,System.Int32,T,System.Int32,Unity.Collections.Allocator)
inline void TrackableChanges_1__ctor_mD3BF9E8193EF890B4977549BF6374BE3D5165CBF (TrackableChanges_1_t03A664B58E58E32B5EC1F7CB7133E87F804EE874 * __this, void* ___addedPtr0, int32_t ___addedCount1, void* ___updatedPtr2, int32_t ___updatedCount3, void* ___removedPtr4, int32_t ___removedCount5, XRTrackedObject_tF08D3523D17E214EC3D7DBE8ED5BFE220BAAE260  ___defaultT6, int32_t ___stride7, int32_t ___allocator8, const RuntimeMethod* method)
{
	((  void (*) (TrackableChanges_1_t03A664B58E58E32B5EC1F7CB7133E87F804EE874 *, void*, int32_t, void*, int32_t, void*, int32_t, XRTrackedObject_tF08D3523D17E214EC3D7DBE8ED5BFE220BAAE260 , int32_t, int32_t, const RuntimeMethod*))TrackableChanges_1__ctor_mD3BF9E8193EF890B4977549BF6374BE3D5165CBF_gshared)(__this, ___addedPtr0, ___addedCount1, ___updatedPtr2, ___updatedCount3, ___removedPtr4, ___removedCount5, ___defaultT6, ___stride7, ___allocator8, method);
}
// System.Void Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.XRTrackedObject>::CopyFrom(Unity.Collections.NativeArray`1<!0>)
inline void NativeArray_1_CopyFrom_m44A3244484057D36D3F473E72EBE58EEF79C5B92 (NativeArray_1_t738892D030F76D319525B8C000F49D41505DD529 * __this, NativeArray_1_t738892D030F76D319525B8C000F49D41505DD529  p0, const RuntimeMethod* method)
{
	((  void (*) (NativeArray_1_t738892D030F76D319525B8C000F49D41505DD529 *, NativeArray_1_t738892D030F76D319525B8C000F49D41505DD529 , const RuntimeMethod*))NativeArray_1_CopyFrom_m44A3244484057D36D3F473E72EBE58EEF79C5B92_gshared)(__this, p0, method);
}
// System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRTrackedObject>::.ctor(Unity.Collections.NativeArray`1<T>,Unity.Collections.NativeArray`1<T>,Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.TrackableId>)
inline void TrackableChanges_1__ctor_m256B8D469EFDAAA37C6D50B7DB6C76C431A70430 (TrackableChanges_1_t03A664B58E58E32B5EC1F7CB7133E87F804EE874 * __this, NativeArray_1_t738892D030F76D319525B8C000F49D41505DD529  p0, NativeArray_1_t738892D030F76D319525B8C000F49D41505DD529  p1, NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  p2, const RuntimeMethod* method)
{
	((  void (*) (TrackableChanges_1_t03A664B58E58E32B5EC1F7CB7133E87F804EE874 *, NativeArray_1_t738892D030F76D319525B8C000F49D41505DD529 , NativeArray_1_t738892D030F76D319525B8C000F49D41505DD529 , NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 , const RuntimeMethod*))TrackableChanges_1__ctor_m256B8D469EFDAAA37C6D50B7DB6C76C431A70430_gshared)(__this, p0, p1, p2, method);
}
// System.Void Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.XRTrackedObject>::Dispose()
inline void NativeArray_1_Dispose_mA88D5D4DAC3D042D2DBB1DBBC45EC820AE85242A (NativeArray_1_t738892D030F76D319525B8C000F49D41505DD529 * __this, const RuntimeMethod* method)
{
	((  void (*) (NativeArray_1_t738892D030F76D319525B8C000F49D41505DD529 *, const RuntimeMethod*))NativeArray_1_Dispose_mA88D5D4DAC3D042D2DBB1DBBC45EC820AE85242A_gshared)(__this, method);
}
// System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRTrackedObject>::Dispose()
inline void TrackableChanges_1_Dispose_mA1365656D4814F94F14E5A34A975A00F5892C790 (TrackableChanges_1_t03A664B58E58E32B5EC1F7CB7133E87F804EE874 * __this, const RuntimeMethod* method)
{
	((  void (*) (TrackableChanges_1_t03A664B58E58E32B5EC1F7CB7133E87F804EE874 *, const RuntimeMethod*))TrackableChanges_1_Dispose_mA1365656D4814F94F14E5A34A975A00F5892C790_gshared)(__this, method);
}
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Unity.Collections.NativeArray`1<T> UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRFace>::get_added()
extern "C" IL2CPP_METHOD_ATTR NativeArray_1_tAA72EF264612AEC585CFAA055B86F6B65CA4E2E5  TrackableChanges_1_get_added_m6C3AA173D63B8181A147527E5D607363D4D7E3B9_gshared (TrackableChanges_1_t316F273AB927198D6305E965CDB7B0ED758920E6 * __this, const RuntimeMethod* method)
{
	{
		NativeArray_1_tAA72EF264612AEC585CFAA055B86F6B65CA4E2E5  L_0 = (NativeArray_1_tAA72EF264612AEC585CFAA055B86F6B65CA4E2E5 )__this->get_m_Added_1();
		return L_0;
	}
}
extern "C"  NativeArray_1_tAA72EF264612AEC585CFAA055B86F6B65CA4E2E5  TrackableChanges_1_get_added_m6C3AA173D63B8181A147527E5D607363D4D7E3B9_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	TrackableChanges_1_t316F273AB927198D6305E965CDB7B0ED758920E6 * _thisAdjusted = reinterpret_cast<TrackableChanges_1_t316F273AB927198D6305E965CDB7B0ED758920E6 *>(__this + 1);
	return TrackableChanges_1_get_added_m6C3AA173D63B8181A147527E5D607363D4D7E3B9(_thisAdjusted, method);
}
// Unity.Collections.NativeArray`1<T> UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRFace>::get_updated()
extern "C" IL2CPP_METHOD_ATTR NativeArray_1_tAA72EF264612AEC585CFAA055B86F6B65CA4E2E5  TrackableChanges_1_get_updated_m4DD6B31059055267B3F98F003F7891F74052267D_gshared (TrackableChanges_1_t316F273AB927198D6305E965CDB7B0ED758920E6 * __this, const RuntimeMethod* method)
{
	{
		NativeArray_1_tAA72EF264612AEC585CFAA055B86F6B65CA4E2E5  L_0 = (NativeArray_1_tAA72EF264612AEC585CFAA055B86F6B65CA4E2E5 )__this->get_m_Updated_2();
		return L_0;
	}
}
extern "C"  NativeArray_1_tAA72EF264612AEC585CFAA055B86F6B65CA4E2E5  TrackableChanges_1_get_updated_m4DD6B31059055267B3F98F003F7891F74052267D_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	TrackableChanges_1_t316F273AB927198D6305E965CDB7B0ED758920E6 * _thisAdjusted = reinterpret_cast<TrackableChanges_1_t316F273AB927198D6305E965CDB7B0ED758920E6 *>(__this + 1);
	return TrackableChanges_1_get_updated_m4DD6B31059055267B3F98F003F7891F74052267D(_thisAdjusted, method);
}
// Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.TrackableId> UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRFace>::get_removed()
extern "C" IL2CPP_METHOD_ATTR NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  TrackableChanges_1_get_removed_m7C323DBD29E884085A21A82E9D5ABC458EF49AED_gshared (TrackableChanges_1_t316F273AB927198D6305E965CDB7B0ED758920E6 * __this, const RuntimeMethod* method)
{
	{
		NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  L_0 = (NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 )__this->get_m_Removed_3();
		return L_0;
	}
}
extern "C"  NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  TrackableChanges_1_get_removed_m7C323DBD29E884085A21A82E9D5ABC458EF49AED_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	TrackableChanges_1_t316F273AB927198D6305E965CDB7B0ED758920E6 * _thisAdjusted = reinterpret_cast<TrackableChanges_1_t316F273AB927198D6305E965CDB7B0ED758920E6 *>(__this + 1);
	return TrackableChanges_1_get_removed_m7C323DBD29E884085A21A82E9D5ABC458EF49AED(_thisAdjusted, method);
}
// System.Boolean UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRFace>::get_isCreated()
extern "C" IL2CPP_METHOD_ATTR bool TrackableChanges_1_get_isCreated_mD2D689F21DCB8E4F238E18F88981D142C946CC69_gshared (TrackableChanges_1_t316F273AB927198D6305E965CDB7B0ED758920E6 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = (bool)__this->get_U3CisCreatedU3Ek__BackingField_0();
		return L_0;
	}
}
extern "C"  bool TrackableChanges_1_get_isCreated_mD2D689F21DCB8E4F238E18F88981D142C946CC69_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	TrackableChanges_1_t316F273AB927198D6305E965CDB7B0ED758920E6 * _thisAdjusted = reinterpret_cast<TrackableChanges_1_t316F273AB927198D6305E965CDB7B0ED758920E6 *>(__this + 1);
	return TrackableChanges_1_get_isCreated_mD2D689F21DCB8E4F238E18F88981D142C946CC69(_thisAdjusted, method);
}
// System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRFace>::set_isCreated(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void TrackableChanges_1_set_isCreated_m032D3E111CF433FC5556450F581911649722CA1B_gshared (TrackableChanges_1_t316F273AB927198D6305E965CDB7B0ED758920E6 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3CisCreatedU3Ek__BackingField_0(L_0);
		return;
	}
}
extern "C"  void TrackableChanges_1_set_isCreated_m032D3E111CF433FC5556450F581911649722CA1B_AdjustorThunk (RuntimeObject * __this, bool ___value0, const RuntimeMethod* method)
{
	TrackableChanges_1_t316F273AB927198D6305E965CDB7B0ED758920E6 * _thisAdjusted = reinterpret_cast<TrackableChanges_1_t316F273AB927198D6305E965CDB7B0ED758920E6 *>(__this + 1);
	TrackableChanges_1_set_isCreated_m032D3E111CF433FC5556450F581911649722CA1B(_thisAdjusted, ___value0, method);
}
// System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRFace>::.ctor(System.Int32,System.Int32,System.Int32,Unity.Collections.Allocator)
extern "C" IL2CPP_METHOD_ATTR void TrackableChanges_1__ctor_m74CBCEBF1898779EEA76D9991BB1F62E80E2687B_gshared (TrackableChanges_1_t316F273AB927198D6305E965CDB7B0ED758920E6 * __this, int32_t ___addedCount0, int32_t ___updatedCount1, int32_t ___removedCount2, int32_t ___allocator3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TrackableChanges_1__ctor_m74CBCEBF1898779EEA76D9991BB1F62E80E2687B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___addedCount0;
		int32_t L_1 = ___allocator3;
		NativeArray_1_tAA72EF264612AEC585CFAA055B86F6B65CA4E2E5  L_2;
		memset(&L_2, 0, sizeof(L_2));
		NativeArray_1__ctor_m9DDFB3AC72B2DDA9252E82C7D045B360C34BCADF((&L_2), (int32_t)L_0, (int32_t)L_1, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		__this->set_m_Added_1(L_2);
		int32_t L_3 = ___updatedCount1;
		int32_t L_4 = ___allocator3;
		NativeArray_1_tAA72EF264612AEC585CFAA055B86F6B65CA4E2E5  L_5;
		memset(&L_5, 0, sizeof(L_5));
		NativeArray_1__ctor_m9DDFB3AC72B2DDA9252E82C7D045B360C34BCADF((&L_5), (int32_t)L_3, (int32_t)L_4, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		__this->set_m_Updated_2(L_5);
		int32_t L_6 = ___removedCount2;
		int32_t L_7 = ___allocator3;
		NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  L_8;
		memset(&L_8, 0, sizeof(L_8));
		NativeArray_1__ctor_m49FCF39766C38037835E85C0A677B26599D0BE55((&L_8), (int32_t)L_6, (int32_t)L_7, (int32_t)1, /*hidden argument*/NativeArray_1__ctor_m49FCF39766C38037835E85C0A677B26599D0BE55_RuntimeMethod_var);
		__this->set_m_Removed_3(L_8);
		TrackableChanges_1_set_isCreated_m032D3E111CF433FC5556450F581911649722CA1B((TrackableChanges_1_t316F273AB927198D6305E965CDB7B0ED758920E6 *)(TrackableChanges_1_t316F273AB927198D6305E965CDB7B0ED758920E6 *)__this, (bool)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 2));
		return;
	}
}
extern "C"  void TrackableChanges_1__ctor_m74CBCEBF1898779EEA76D9991BB1F62E80E2687B_AdjustorThunk (RuntimeObject * __this, int32_t ___addedCount0, int32_t ___updatedCount1, int32_t ___removedCount2, int32_t ___allocator3, const RuntimeMethod* method)
{
	TrackableChanges_1_t316F273AB927198D6305E965CDB7B0ED758920E6 * _thisAdjusted = reinterpret_cast<TrackableChanges_1_t316F273AB927198D6305E965CDB7B0ED758920E6 *>(__this + 1);
	TrackableChanges_1__ctor_m74CBCEBF1898779EEA76D9991BB1F62E80E2687B(_thisAdjusted, ___addedCount0, ___updatedCount1, ___removedCount2, ___allocator3, method);
}
// System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRFace>::.ctor(System.Void*,System.Int32,System.Void*,System.Int32,System.Void*,System.Int32,T,System.Int32,Unity.Collections.Allocator)
extern "C" IL2CPP_METHOD_ATTR void TrackableChanges_1__ctor_m9D3306EC438EE09208F98E4F4049E615CC5E801E_gshared (TrackableChanges_1_t316F273AB927198D6305E965CDB7B0ED758920E6 * __this, void* ___addedPtr0, int32_t ___addedCount1, void* ___updatedPtr2, int32_t ___updatedCount3, void* ___removedPtr4, int32_t ___removedCount5, XRFace_tF2B2E9B06813BA74F5DAFD527FD249DD1002B7C7  ___defaultT6, int32_t ___stride7, int32_t ___allocator8, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TrackableChanges_1__ctor_m9D3306EC438EE09208F98E4F4049E615CC5E801E_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		XRFace_tF2B2E9B06813BA74F5DAFD527FD249DD1002B7C7  L_0 = ___defaultT6;
		void* L_1 = ___addedPtr0;
		int32_t L_2 = ___stride7;
		int32_t L_3 = ___addedCount1;
		int32_t L_4 = ___allocator8;
		NativeArray_1_tAA72EF264612AEC585CFAA055B86F6B65CA4E2E5  L_5 = ((  NativeArray_1_tAA72EF264612AEC585CFAA055B86F6B65CA4E2E5  (*) (XRFace_tF2B2E9B06813BA74F5DAFD527FD249DD1002B7C7 , void*, int32_t, int32_t, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3)->methodPointer)((XRFace_tF2B2E9B06813BA74F5DAFD527FD249DD1002B7C7 )L_0, (void*)(void*)L_1, (int32_t)L_2, (int32_t)L_3, (int32_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3));
		__this->set_m_Added_1(L_5);
		XRFace_tF2B2E9B06813BA74F5DAFD527FD249DD1002B7C7  L_6 = ___defaultT6;
		void* L_7 = ___updatedPtr2;
		int32_t L_8 = ___stride7;
		int32_t L_9 = ___updatedCount3;
		int32_t L_10 = ___allocator8;
		NativeArray_1_tAA72EF264612AEC585CFAA055B86F6B65CA4E2E5  L_11 = ((  NativeArray_1_tAA72EF264612AEC585CFAA055B86F6B65CA4E2E5  (*) (XRFace_tF2B2E9B06813BA74F5DAFD527FD249DD1002B7C7 , void*, int32_t, int32_t, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3)->methodPointer)((XRFace_tF2B2E9B06813BA74F5DAFD527FD249DD1002B7C7 )L_6, (void*)(void*)L_7, (int32_t)L_8, (int32_t)L_9, (int32_t)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3));
		__this->set_m_Updated_2(L_11);
		int32_t L_12 = ___removedCount5;
		int32_t L_13 = ___allocator8;
		NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  L_14;
		memset(&L_14, 0, sizeof(L_14));
		NativeArray_1__ctor_m49FCF39766C38037835E85C0A677B26599D0BE55((&L_14), (int32_t)L_12, (int32_t)L_13, (int32_t)1, /*hidden argument*/NativeArray_1__ctor_m49FCF39766C38037835E85C0A677B26599D0BE55_RuntimeMethod_var);
		__this->set_m_Removed_3(L_14);
		int32_t L_15 = ___removedCount5;
		if ((((int32_t)L_15) <= ((int32_t)0)))
		{
			goto IL_0051;
		}
	}
	{
		NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 * L_16 = (NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 *)__this->get_address_of_m_Removed_3();
		void* L_17 = ___removedPtr4;
		int32_t L_18 = ___removedCount5;
		NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  L_19 = NativeArrayUnsafeUtility_ConvertExistingDataToNativeArray_TisTrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47_m8D7EEF45CE20EB27369597A33879F42E459F2D59((void*)(void*)L_17, (int32_t)L_18, (int32_t)1, /*hidden argument*/NativeArrayUnsafeUtility_ConvertExistingDataToNativeArray_TisTrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47_m8D7EEF45CE20EB27369597A33879F42E459F2D59_RuntimeMethod_var);
		NativeArray_1_CopyFrom_mE2C04369E83ED4726DE1121E7E05342B6A993A97((NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 *)(NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 *)L_16, (NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 )L_19, /*hidden argument*/NativeArray_1_CopyFrom_mE2C04369E83ED4726DE1121E7E05342B6A993A97_RuntimeMethod_var);
	}

IL_0051:
	{
		TrackableChanges_1_set_isCreated_m032D3E111CF433FC5556450F581911649722CA1B((TrackableChanges_1_t316F273AB927198D6305E965CDB7B0ED758920E6 *)(TrackableChanges_1_t316F273AB927198D6305E965CDB7B0ED758920E6 *)__this, (bool)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 2));
		return;
	}
}
extern "C"  void TrackableChanges_1__ctor_m9D3306EC438EE09208F98E4F4049E615CC5E801E_AdjustorThunk (RuntimeObject * __this, void* ___addedPtr0, int32_t ___addedCount1, void* ___updatedPtr2, int32_t ___updatedCount3, void* ___removedPtr4, int32_t ___removedCount5, XRFace_tF2B2E9B06813BA74F5DAFD527FD249DD1002B7C7  ___defaultT6, int32_t ___stride7, int32_t ___allocator8, const RuntimeMethod* method)
{
	TrackableChanges_1_t316F273AB927198D6305E965CDB7B0ED758920E6 * _thisAdjusted = reinterpret_cast<TrackableChanges_1_t316F273AB927198D6305E965CDB7B0ED758920E6 *>(__this + 1);
	TrackableChanges_1__ctor_m9D3306EC438EE09208F98E4F4049E615CC5E801E(_thisAdjusted, ___addedPtr0, ___addedCount1, ___updatedPtr2, ___updatedCount3, ___removedPtr4, ___removedCount5, ___defaultT6, ___stride7, ___allocator8, method);
}
// UnityEngine.XR.ARSubsystems.TrackableChanges`1<T> UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRFace>::CopyFrom(Unity.Collections.NativeArray`1<T>,Unity.Collections.NativeArray`1<T>,Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.TrackableId>,Unity.Collections.Allocator)
extern "C" IL2CPP_METHOD_ATTR TrackableChanges_1_t316F273AB927198D6305E965CDB7B0ED758920E6  TrackableChanges_1_CopyFrom_m588811D004C5B4A0C4D0021DDE64C8F225B45D32_gshared (NativeArray_1_tAA72EF264612AEC585CFAA055B86F6B65CA4E2E5  ___added0, NativeArray_1_tAA72EF264612AEC585CFAA055B86F6B65CA4E2E5  ___updated1, NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  ___removed2, int32_t ___allocator3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TrackableChanges_1_CopyFrom_m588811D004C5B4A0C4D0021DDE64C8F225B45D32_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	NativeArray_1_tAA72EF264612AEC585CFAA055B86F6B65CA4E2E5  V_0;
	memset(&V_0, 0, sizeof(V_0));
	NativeArray_1_tAA72EF264612AEC585CFAA055B86F6B65CA4E2E5  V_1;
	memset(&V_1, 0, sizeof(V_1));
	NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		int32_t L_0 = IL2CPP_NATIVEARRAY_GET_LENGTH(((NativeArray_1_tAA72EF264612AEC585CFAA055B86F6B65CA4E2E5 *)(NativeArray_1_tAA72EF264612AEC585CFAA055B86F6B65CA4E2E5 *)(&___added0))->___m_Length_1);
		int32_t L_1 = ___allocator3;
		NativeArray_1__ctor_m9DDFB3AC72B2DDA9252E82C7D045B360C34BCADF((NativeArray_1_tAA72EF264612AEC585CFAA055B86F6B65CA4E2E5 *)(NativeArray_1_tAA72EF264612AEC585CFAA055B86F6B65CA4E2E5 *)(&V_0), (int32_t)L_0, (int32_t)L_1, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		NativeArray_1_tAA72EF264612AEC585CFAA055B86F6B65CA4E2E5  L_2 = ___added0;
		NativeArray_1_CopyFrom_mF9D212E6958D4B634ED7644625EC61A9C7605CDB((NativeArray_1_tAA72EF264612AEC585CFAA055B86F6B65CA4E2E5 *)(NativeArray_1_tAA72EF264612AEC585CFAA055B86F6B65CA4E2E5 *)(&V_0), (NativeArray_1_tAA72EF264612AEC585CFAA055B86F6B65CA4E2E5 )L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 5));
		int32_t L_3 = IL2CPP_NATIVEARRAY_GET_LENGTH(((NativeArray_1_tAA72EF264612AEC585CFAA055B86F6B65CA4E2E5 *)(NativeArray_1_tAA72EF264612AEC585CFAA055B86F6B65CA4E2E5 *)(&___updated1))->___m_Length_1);
		int32_t L_4 = ___allocator3;
		NativeArray_1__ctor_m9DDFB3AC72B2DDA9252E82C7D045B360C34BCADF((NativeArray_1_tAA72EF264612AEC585CFAA055B86F6B65CA4E2E5 *)(NativeArray_1_tAA72EF264612AEC585CFAA055B86F6B65CA4E2E5 *)(&V_1), (int32_t)L_3, (int32_t)L_4, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		NativeArray_1_tAA72EF264612AEC585CFAA055B86F6B65CA4E2E5  L_5 = ___updated1;
		NativeArray_1_CopyFrom_mF9D212E6958D4B634ED7644625EC61A9C7605CDB((NativeArray_1_tAA72EF264612AEC585CFAA055B86F6B65CA4E2E5 *)(NativeArray_1_tAA72EF264612AEC585CFAA055B86F6B65CA4E2E5 *)(&V_1), (NativeArray_1_tAA72EF264612AEC585CFAA055B86F6B65CA4E2E5 )L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 5));
		int32_t L_6 = IL2CPP_NATIVEARRAY_GET_LENGTH(((NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 *)(NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 *)(&___removed2))->___m_Length_1);
		int32_t L_7 = ___allocator3;
		NativeArray_1__ctor_m49FCF39766C38037835E85C0A677B26599D0BE55((NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 *)(NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 *)(&V_2), (int32_t)L_6, (int32_t)L_7, (int32_t)1, /*hidden argument*/NativeArray_1__ctor_m49FCF39766C38037835E85C0A677B26599D0BE55_RuntimeMethod_var);
		NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  L_8 = ___removed2;
		NativeArray_1_CopyFrom_mE2C04369E83ED4726DE1121E7E05342B6A993A97((NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 *)(NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 *)(&V_2), (NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 )L_8, /*hidden argument*/NativeArray_1_CopyFrom_mE2C04369E83ED4726DE1121E7E05342B6A993A97_RuntimeMethod_var);
		NativeArray_1_tAA72EF264612AEC585CFAA055B86F6B65CA4E2E5  L_9 = V_0;
		NativeArray_1_tAA72EF264612AEC585CFAA055B86F6B65CA4E2E5  L_10 = V_1;
		NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  L_11 = V_2;
		TrackableChanges_1_t316F273AB927198D6305E965CDB7B0ED758920E6  L_12;
		memset(&L_12, 0, sizeof(L_12));
		TrackableChanges_1__ctor_m311F799F3BF5B7CDF7B32E9A6B5CF320D1D0A375((&L_12), (NativeArray_1_tAA72EF264612AEC585CFAA055B86F6B65CA4E2E5 )L_9, (NativeArray_1_tAA72EF264612AEC585CFAA055B86F6B65CA4E2E5 )L_10, (NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 )L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 7));
		return L_12;
	}
}
// System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRFace>::Dispose()
extern "C" IL2CPP_METHOD_ATTR void TrackableChanges_1_Dispose_m8CB8D80766E7529B391391E43158D99E39220FF6_gshared (TrackableChanges_1_t316F273AB927198D6305E965CDB7B0ED758920E6 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TrackableChanges_1_Dispose_m8CB8D80766E7529B391391E43158D99E39220FF6_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = TrackableChanges_1_get_isCreated_mD2D689F21DCB8E4F238E18F88981D142C946CC69((TrackableChanges_1_t316F273AB927198D6305E965CDB7B0ED758920E6 *)(TrackableChanges_1_t316F273AB927198D6305E965CDB7B0ED758920E6 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 8));
		if (!L_0)
		{
			goto IL_0029;
		}
	}
	{
		NativeArray_1_tAA72EF264612AEC585CFAA055B86F6B65CA4E2E5 * L_1 = (NativeArray_1_tAA72EF264612AEC585CFAA055B86F6B65CA4E2E5 *)__this->get_address_of_m_Added_1();
		NativeArray_1_Dispose_m343FC4D068AE0BA3141FDF70567F8BD2A752FF2D((NativeArray_1_tAA72EF264612AEC585CFAA055B86F6B65CA4E2E5 *)(NativeArray_1_tAA72EF264612AEC585CFAA055B86F6B65CA4E2E5 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 9));
		NativeArray_1_tAA72EF264612AEC585CFAA055B86F6B65CA4E2E5 * L_2 = (NativeArray_1_tAA72EF264612AEC585CFAA055B86F6B65CA4E2E5 *)__this->get_address_of_m_Updated_2();
		NativeArray_1_Dispose_m343FC4D068AE0BA3141FDF70567F8BD2A752FF2D((NativeArray_1_tAA72EF264612AEC585CFAA055B86F6B65CA4E2E5 *)(NativeArray_1_tAA72EF264612AEC585CFAA055B86F6B65CA4E2E5 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 9));
		NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 * L_3 = (NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 *)__this->get_address_of_m_Removed_3();
		NativeArray_1_Dispose_mB8124101177299FC76251C4E7F6C98BB0E5503C7((NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 *)(NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 *)L_3, /*hidden argument*/NativeArray_1_Dispose_mB8124101177299FC76251C4E7F6C98BB0E5503C7_RuntimeMethod_var);
	}

IL_0029:
	{
		return;
	}
}
extern "C"  void TrackableChanges_1_Dispose_m8CB8D80766E7529B391391E43158D99E39220FF6_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	TrackableChanges_1_t316F273AB927198D6305E965CDB7B0ED758920E6 * _thisAdjusted = reinterpret_cast<TrackableChanges_1_t316F273AB927198D6305E965CDB7B0ED758920E6 *>(__this + 1);
	TrackableChanges_1_Dispose_m8CB8D80766E7529B391391E43158D99E39220FF6(_thisAdjusted, method);
}
// System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRFace>::.ctor(Unity.Collections.NativeArray`1<T>,Unity.Collections.NativeArray`1<T>,Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.TrackableId>)
extern "C" IL2CPP_METHOD_ATTR void TrackableChanges_1__ctor_m311F799F3BF5B7CDF7B32E9A6B5CF320D1D0A375_gshared (TrackableChanges_1_t316F273AB927198D6305E965CDB7B0ED758920E6 * __this, NativeArray_1_tAA72EF264612AEC585CFAA055B86F6B65CA4E2E5  ___added0, NativeArray_1_tAA72EF264612AEC585CFAA055B86F6B65CA4E2E5  ___updated1, NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  ___removed2, const RuntimeMethod* method)
{
	{
		NativeArray_1_tAA72EF264612AEC585CFAA055B86F6B65CA4E2E5  L_0 = ___added0;
		__this->set_m_Added_1(L_0);
		NativeArray_1_tAA72EF264612AEC585CFAA055B86F6B65CA4E2E5  L_1 = ___updated1;
		__this->set_m_Updated_2(L_1);
		NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  L_2 = ___removed2;
		__this->set_m_Removed_3(L_2);
		TrackableChanges_1_set_isCreated_m032D3E111CF433FC5556450F581911649722CA1B((TrackableChanges_1_t316F273AB927198D6305E965CDB7B0ED758920E6 *)(TrackableChanges_1_t316F273AB927198D6305E965CDB7B0ED758920E6 *)__this, (bool)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 2));
		return;
	}
}
extern "C"  void TrackableChanges_1__ctor_m311F799F3BF5B7CDF7B32E9A6B5CF320D1D0A375_AdjustorThunk (RuntimeObject * __this, NativeArray_1_tAA72EF264612AEC585CFAA055B86F6B65CA4E2E5  ___added0, NativeArray_1_tAA72EF264612AEC585CFAA055B86F6B65CA4E2E5  ___updated1, NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  ___removed2, const RuntimeMethod* method)
{
	TrackableChanges_1_t316F273AB927198D6305E965CDB7B0ED758920E6 * _thisAdjusted = reinterpret_cast<TrackableChanges_1_t316F273AB927198D6305E965CDB7B0ED758920E6 *>(__this + 1);
	TrackableChanges_1__ctor_m311F799F3BF5B7CDF7B32E9A6B5CF320D1D0A375(_thisAdjusted, ___added0, ___updated1, ___removed2, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Unity.Collections.NativeArray`1<T> UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRHumanBody>::get_added()
extern "C" IL2CPP_METHOD_ATTR NativeArray_1_t140FCB442630D549508192A9E3737425E84D15F8  TrackableChanges_1_get_added_mAC12696B7D12A00A3FE9C34A2FF8F22ED6A3BD6C_gshared (TrackableChanges_1_t8F77CF9D37FD0DCA052A7D6B34E1A489AF13C41B * __this, const RuntimeMethod* method)
{
	{
		NativeArray_1_t140FCB442630D549508192A9E3737425E84D15F8  L_0 = (NativeArray_1_t140FCB442630D549508192A9E3737425E84D15F8 )__this->get_m_Added_1();
		return L_0;
	}
}
extern "C"  NativeArray_1_t140FCB442630D549508192A9E3737425E84D15F8  TrackableChanges_1_get_added_mAC12696B7D12A00A3FE9C34A2FF8F22ED6A3BD6C_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	TrackableChanges_1_t8F77CF9D37FD0DCA052A7D6B34E1A489AF13C41B * _thisAdjusted = reinterpret_cast<TrackableChanges_1_t8F77CF9D37FD0DCA052A7D6B34E1A489AF13C41B *>(__this + 1);
	return TrackableChanges_1_get_added_mAC12696B7D12A00A3FE9C34A2FF8F22ED6A3BD6C(_thisAdjusted, method);
}
// Unity.Collections.NativeArray`1<T> UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRHumanBody>::get_updated()
extern "C" IL2CPP_METHOD_ATTR NativeArray_1_t140FCB442630D549508192A9E3737425E84D15F8  TrackableChanges_1_get_updated_mCD92C9A6154DC58C63992B59E6671BC8AE07F9C8_gshared (TrackableChanges_1_t8F77CF9D37FD0DCA052A7D6B34E1A489AF13C41B * __this, const RuntimeMethod* method)
{
	{
		NativeArray_1_t140FCB442630D549508192A9E3737425E84D15F8  L_0 = (NativeArray_1_t140FCB442630D549508192A9E3737425E84D15F8 )__this->get_m_Updated_2();
		return L_0;
	}
}
extern "C"  NativeArray_1_t140FCB442630D549508192A9E3737425E84D15F8  TrackableChanges_1_get_updated_mCD92C9A6154DC58C63992B59E6671BC8AE07F9C8_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	TrackableChanges_1_t8F77CF9D37FD0DCA052A7D6B34E1A489AF13C41B * _thisAdjusted = reinterpret_cast<TrackableChanges_1_t8F77CF9D37FD0DCA052A7D6B34E1A489AF13C41B *>(__this + 1);
	return TrackableChanges_1_get_updated_mCD92C9A6154DC58C63992B59E6671BC8AE07F9C8(_thisAdjusted, method);
}
// Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.TrackableId> UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRHumanBody>::get_removed()
extern "C" IL2CPP_METHOD_ATTR NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  TrackableChanges_1_get_removed_m377C6A3BFE41CC689AFE09CE1529B79113140DD2_gshared (TrackableChanges_1_t8F77CF9D37FD0DCA052A7D6B34E1A489AF13C41B * __this, const RuntimeMethod* method)
{
	{
		NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  L_0 = (NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 )__this->get_m_Removed_3();
		return L_0;
	}
}
extern "C"  NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  TrackableChanges_1_get_removed_m377C6A3BFE41CC689AFE09CE1529B79113140DD2_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	TrackableChanges_1_t8F77CF9D37FD0DCA052A7D6B34E1A489AF13C41B * _thisAdjusted = reinterpret_cast<TrackableChanges_1_t8F77CF9D37FD0DCA052A7D6B34E1A489AF13C41B *>(__this + 1);
	return TrackableChanges_1_get_removed_m377C6A3BFE41CC689AFE09CE1529B79113140DD2(_thisAdjusted, method);
}
// System.Boolean UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRHumanBody>::get_isCreated()
extern "C" IL2CPP_METHOD_ATTR bool TrackableChanges_1_get_isCreated_m68F2E3D10D821918F3C0002EAA07440EC60CB4B0_gshared (TrackableChanges_1_t8F77CF9D37FD0DCA052A7D6B34E1A489AF13C41B * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = (bool)__this->get_U3CisCreatedU3Ek__BackingField_0();
		return L_0;
	}
}
extern "C"  bool TrackableChanges_1_get_isCreated_m68F2E3D10D821918F3C0002EAA07440EC60CB4B0_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	TrackableChanges_1_t8F77CF9D37FD0DCA052A7D6B34E1A489AF13C41B * _thisAdjusted = reinterpret_cast<TrackableChanges_1_t8F77CF9D37FD0DCA052A7D6B34E1A489AF13C41B *>(__this + 1);
	return TrackableChanges_1_get_isCreated_m68F2E3D10D821918F3C0002EAA07440EC60CB4B0(_thisAdjusted, method);
}
// System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRHumanBody>::set_isCreated(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void TrackableChanges_1_set_isCreated_m6FF97BC809D88ABBCD88D515340CBC1F1EE27D83_gshared (TrackableChanges_1_t8F77CF9D37FD0DCA052A7D6B34E1A489AF13C41B * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3CisCreatedU3Ek__BackingField_0(L_0);
		return;
	}
}
extern "C"  void TrackableChanges_1_set_isCreated_m6FF97BC809D88ABBCD88D515340CBC1F1EE27D83_AdjustorThunk (RuntimeObject * __this, bool ___value0, const RuntimeMethod* method)
{
	TrackableChanges_1_t8F77CF9D37FD0DCA052A7D6B34E1A489AF13C41B * _thisAdjusted = reinterpret_cast<TrackableChanges_1_t8F77CF9D37FD0DCA052A7D6B34E1A489AF13C41B *>(__this + 1);
	TrackableChanges_1_set_isCreated_m6FF97BC809D88ABBCD88D515340CBC1F1EE27D83(_thisAdjusted, ___value0, method);
}
// System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRHumanBody>::.ctor(System.Int32,System.Int32,System.Int32,Unity.Collections.Allocator)
extern "C" IL2CPP_METHOD_ATTR void TrackableChanges_1__ctor_m6A057FF02DA6FD8876C995CE890C7D8B3A1EBFB0_gshared (TrackableChanges_1_t8F77CF9D37FD0DCA052A7D6B34E1A489AF13C41B * __this, int32_t ___addedCount0, int32_t ___updatedCount1, int32_t ___removedCount2, int32_t ___allocator3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TrackableChanges_1__ctor_m6A057FF02DA6FD8876C995CE890C7D8B3A1EBFB0_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___addedCount0;
		int32_t L_1 = ___allocator3;
		NativeArray_1_t140FCB442630D549508192A9E3737425E84D15F8  L_2;
		memset(&L_2, 0, sizeof(L_2));
		NativeArray_1__ctor_mF5A1714C6870A3DF6F61BB87B0700F0A8A67F9CC((&L_2), (int32_t)L_0, (int32_t)L_1, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		__this->set_m_Added_1(L_2);
		int32_t L_3 = ___updatedCount1;
		int32_t L_4 = ___allocator3;
		NativeArray_1_t140FCB442630D549508192A9E3737425E84D15F8  L_5;
		memset(&L_5, 0, sizeof(L_5));
		NativeArray_1__ctor_mF5A1714C6870A3DF6F61BB87B0700F0A8A67F9CC((&L_5), (int32_t)L_3, (int32_t)L_4, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		__this->set_m_Updated_2(L_5);
		int32_t L_6 = ___removedCount2;
		int32_t L_7 = ___allocator3;
		NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  L_8;
		memset(&L_8, 0, sizeof(L_8));
		NativeArray_1__ctor_m49FCF39766C38037835E85C0A677B26599D0BE55((&L_8), (int32_t)L_6, (int32_t)L_7, (int32_t)1, /*hidden argument*/NativeArray_1__ctor_m49FCF39766C38037835E85C0A677B26599D0BE55_RuntimeMethod_var);
		__this->set_m_Removed_3(L_8);
		TrackableChanges_1_set_isCreated_m6FF97BC809D88ABBCD88D515340CBC1F1EE27D83((TrackableChanges_1_t8F77CF9D37FD0DCA052A7D6B34E1A489AF13C41B *)(TrackableChanges_1_t8F77CF9D37FD0DCA052A7D6B34E1A489AF13C41B *)__this, (bool)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 2));
		return;
	}
}
extern "C"  void TrackableChanges_1__ctor_m6A057FF02DA6FD8876C995CE890C7D8B3A1EBFB0_AdjustorThunk (RuntimeObject * __this, int32_t ___addedCount0, int32_t ___updatedCount1, int32_t ___removedCount2, int32_t ___allocator3, const RuntimeMethod* method)
{
	TrackableChanges_1_t8F77CF9D37FD0DCA052A7D6B34E1A489AF13C41B * _thisAdjusted = reinterpret_cast<TrackableChanges_1_t8F77CF9D37FD0DCA052A7D6B34E1A489AF13C41B *>(__this + 1);
	TrackableChanges_1__ctor_m6A057FF02DA6FD8876C995CE890C7D8B3A1EBFB0(_thisAdjusted, ___addedCount0, ___updatedCount1, ___removedCount2, ___allocator3, method);
}
// System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRHumanBody>::.ctor(System.Void*,System.Int32,System.Void*,System.Int32,System.Void*,System.Int32,T,System.Int32,Unity.Collections.Allocator)
extern "C" IL2CPP_METHOD_ATTR void TrackableChanges_1__ctor_m859CA5B973D4AD250F12979A582B9F5D56007BFD_gshared (TrackableChanges_1_t8F77CF9D37FD0DCA052A7D6B34E1A489AF13C41B * __this, void* ___addedPtr0, int32_t ___addedCount1, void* ___updatedPtr2, int32_t ___updatedCount3, void* ___removedPtr4, int32_t ___removedCount5, XRHumanBody_t64938399EB40376E99745517E15D42FFC4604BC1  ___defaultT6, int32_t ___stride7, int32_t ___allocator8, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TrackableChanges_1__ctor_m859CA5B973D4AD250F12979A582B9F5D56007BFD_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		XRHumanBody_t64938399EB40376E99745517E15D42FFC4604BC1  L_0 = ___defaultT6;
		void* L_1 = ___addedPtr0;
		int32_t L_2 = ___stride7;
		int32_t L_3 = ___addedCount1;
		int32_t L_4 = ___allocator8;
		NativeArray_1_t140FCB442630D549508192A9E3737425E84D15F8  L_5 = ((  NativeArray_1_t140FCB442630D549508192A9E3737425E84D15F8  (*) (XRHumanBody_t64938399EB40376E99745517E15D42FFC4604BC1 , void*, int32_t, int32_t, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3)->methodPointer)((XRHumanBody_t64938399EB40376E99745517E15D42FFC4604BC1 )L_0, (void*)(void*)L_1, (int32_t)L_2, (int32_t)L_3, (int32_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3));
		__this->set_m_Added_1(L_5);
		XRHumanBody_t64938399EB40376E99745517E15D42FFC4604BC1  L_6 = ___defaultT6;
		void* L_7 = ___updatedPtr2;
		int32_t L_8 = ___stride7;
		int32_t L_9 = ___updatedCount3;
		int32_t L_10 = ___allocator8;
		NativeArray_1_t140FCB442630D549508192A9E3737425E84D15F8  L_11 = ((  NativeArray_1_t140FCB442630D549508192A9E3737425E84D15F8  (*) (XRHumanBody_t64938399EB40376E99745517E15D42FFC4604BC1 , void*, int32_t, int32_t, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3)->methodPointer)((XRHumanBody_t64938399EB40376E99745517E15D42FFC4604BC1 )L_6, (void*)(void*)L_7, (int32_t)L_8, (int32_t)L_9, (int32_t)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3));
		__this->set_m_Updated_2(L_11);
		int32_t L_12 = ___removedCount5;
		int32_t L_13 = ___allocator8;
		NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  L_14;
		memset(&L_14, 0, sizeof(L_14));
		NativeArray_1__ctor_m49FCF39766C38037835E85C0A677B26599D0BE55((&L_14), (int32_t)L_12, (int32_t)L_13, (int32_t)1, /*hidden argument*/NativeArray_1__ctor_m49FCF39766C38037835E85C0A677B26599D0BE55_RuntimeMethod_var);
		__this->set_m_Removed_3(L_14);
		int32_t L_15 = ___removedCount5;
		if ((((int32_t)L_15) <= ((int32_t)0)))
		{
			goto IL_0051;
		}
	}
	{
		NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 * L_16 = (NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 *)__this->get_address_of_m_Removed_3();
		void* L_17 = ___removedPtr4;
		int32_t L_18 = ___removedCount5;
		NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  L_19 = NativeArrayUnsafeUtility_ConvertExistingDataToNativeArray_TisTrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47_m8D7EEF45CE20EB27369597A33879F42E459F2D59((void*)(void*)L_17, (int32_t)L_18, (int32_t)1, /*hidden argument*/NativeArrayUnsafeUtility_ConvertExistingDataToNativeArray_TisTrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47_m8D7EEF45CE20EB27369597A33879F42E459F2D59_RuntimeMethod_var);
		NativeArray_1_CopyFrom_mE2C04369E83ED4726DE1121E7E05342B6A993A97((NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 *)(NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 *)L_16, (NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 )L_19, /*hidden argument*/NativeArray_1_CopyFrom_mE2C04369E83ED4726DE1121E7E05342B6A993A97_RuntimeMethod_var);
	}

IL_0051:
	{
		TrackableChanges_1_set_isCreated_m6FF97BC809D88ABBCD88D515340CBC1F1EE27D83((TrackableChanges_1_t8F77CF9D37FD0DCA052A7D6B34E1A489AF13C41B *)(TrackableChanges_1_t8F77CF9D37FD0DCA052A7D6B34E1A489AF13C41B *)__this, (bool)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 2));
		return;
	}
}
extern "C"  void TrackableChanges_1__ctor_m859CA5B973D4AD250F12979A582B9F5D56007BFD_AdjustorThunk (RuntimeObject * __this, void* ___addedPtr0, int32_t ___addedCount1, void* ___updatedPtr2, int32_t ___updatedCount3, void* ___removedPtr4, int32_t ___removedCount5, XRHumanBody_t64938399EB40376E99745517E15D42FFC4604BC1  ___defaultT6, int32_t ___stride7, int32_t ___allocator8, const RuntimeMethod* method)
{
	TrackableChanges_1_t8F77CF9D37FD0DCA052A7D6B34E1A489AF13C41B * _thisAdjusted = reinterpret_cast<TrackableChanges_1_t8F77CF9D37FD0DCA052A7D6B34E1A489AF13C41B *>(__this + 1);
	TrackableChanges_1__ctor_m859CA5B973D4AD250F12979A582B9F5D56007BFD(_thisAdjusted, ___addedPtr0, ___addedCount1, ___updatedPtr2, ___updatedCount3, ___removedPtr4, ___removedCount5, ___defaultT6, ___stride7, ___allocator8, method);
}
// UnityEngine.XR.ARSubsystems.TrackableChanges`1<T> UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRHumanBody>::CopyFrom(Unity.Collections.NativeArray`1<T>,Unity.Collections.NativeArray`1<T>,Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.TrackableId>,Unity.Collections.Allocator)
extern "C" IL2CPP_METHOD_ATTR TrackableChanges_1_t8F77CF9D37FD0DCA052A7D6B34E1A489AF13C41B  TrackableChanges_1_CopyFrom_mDD6D3BE97D0DD91EAB4A1112E55755EDB8C11C74_gshared (NativeArray_1_t140FCB442630D549508192A9E3737425E84D15F8  ___added0, NativeArray_1_t140FCB442630D549508192A9E3737425E84D15F8  ___updated1, NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  ___removed2, int32_t ___allocator3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TrackableChanges_1_CopyFrom_mDD6D3BE97D0DD91EAB4A1112E55755EDB8C11C74_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	NativeArray_1_t140FCB442630D549508192A9E3737425E84D15F8  V_0;
	memset(&V_0, 0, sizeof(V_0));
	NativeArray_1_t140FCB442630D549508192A9E3737425E84D15F8  V_1;
	memset(&V_1, 0, sizeof(V_1));
	NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		int32_t L_0 = IL2CPP_NATIVEARRAY_GET_LENGTH(((NativeArray_1_t140FCB442630D549508192A9E3737425E84D15F8 *)(NativeArray_1_t140FCB442630D549508192A9E3737425E84D15F8 *)(&___added0))->___m_Length_1);
		int32_t L_1 = ___allocator3;
		NativeArray_1__ctor_mF5A1714C6870A3DF6F61BB87B0700F0A8A67F9CC((NativeArray_1_t140FCB442630D549508192A9E3737425E84D15F8 *)(NativeArray_1_t140FCB442630D549508192A9E3737425E84D15F8 *)(&V_0), (int32_t)L_0, (int32_t)L_1, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		NativeArray_1_t140FCB442630D549508192A9E3737425E84D15F8  L_2 = ___added0;
		NativeArray_1_CopyFrom_m476CD42B4BF6AFC03517ED3C99ADB1FFC44B5531((NativeArray_1_t140FCB442630D549508192A9E3737425E84D15F8 *)(NativeArray_1_t140FCB442630D549508192A9E3737425E84D15F8 *)(&V_0), (NativeArray_1_t140FCB442630D549508192A9E3737425E84D15F8 )L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 5));
		int32_t L_3 = IL2CPP_NATIVEARRAY_GET_LENGTH(((NativeArray_1_t140FCB442630D549508192A9E3737425E84D15F8 *)(NativeArray_1_t140FCB442630D549508192A9E3737425E84D15F8 *)(&___updated1))->___m_Length_1);
		int32_t L_4 = ___allocator3;
		NativeArray_1__ctor_mF5A1714C6870A3DF6F61BB87B0700F0A8A67F9CC((NativeArray_1_t140FCB442630D549508192A9E3737425E84D15F8 *)(NativeArray_1_t140FCB442630D549508192A9E3737425E84D15F8 *)(&V_1), (int32_t)L_3, (int32_t)L_4, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		NativeArray_1_t140FCB442630D549508192A9E3737425E84D15F8  L_5 = ___updated1;
		NativeArray_1_CopyFrom_m476CD42B4BF6AFC03517ED3C99ADB1FFC44B5531((NativeArray_1_t140FCB442630D549508192A9E3737425E84D15F8 *)(NativeArray_1_t140FCB442630D549508192A9E3737425E84D15F8 *)(&V_1), (NativeArray_1_t140FCB442630D549508192A9E3737425E84D15F8 )L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 5));
		int32_t L_6 = IL2CPP_NATIVEARRAY_GET_LENGTH(((NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 *)(NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 *)(&___removed2))->___m_Length_1);
		int32_t L_7 = ___allocator3;
		NativeArray_1__ctor_m49FCF39766C38037835E85C0A677B26599D0BE55((NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 *)(NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 *)(&V_2), (int32_t)L_6, (int32_t)L_7, (int32_t)1, /*hidden argument*/NativeArray_1__ctor_m49FCF39766C38037835E85C0A677B26599D0BE55_RuntimeMethod_var);
		NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  L_8 = ___removed2;
		NativeArray_1_CopyFrom_mE2C04369E83ED4726DE1121E7E05342B6A993A97((NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 *)(NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 *)(&V_2), (NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 )L_8, /*hidden argument*/NativeArray_1_CopyFrom_mE2C04369E83ED4726DE1121E7E05342B6A993A97_RuntimeMethod_var);
		NativeArray_1_t140FCB442630D549508192A9E3737425E84D15F8  L_9 = V_0;
		NativeArray_1_t140FCB442630D549508192A9E3737425E84D15F8  L_10 = V_1;
		NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  L_11 = V_2;
		TrackableChanges_1_t8F77CF9D37FD0DCA052A7D6B34E1A489AF13C41B  L_12;
		memset(&L_12, 0, sizeof(L_12));
		TrackableChanges_1__ctor_mCBFAB9AC1ADBAED983EEC593665EBBE6337E9A3B((&L_12), (NativeArray_1_t140FCB442630D549508192A9E3737425E84D15F8 )L_9, (NativeArray_1_t140FCB442630D549508192A9E3737425E84D15F8 )L_10, (NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 )L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 7));
		return L_12;
	}
}
// System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRHumanBody>::Dispose()
extern "C" IL2CPP_METHOD_ATTR void TrackableChanges_1_Dispose_m7F29A9FDEA683B63D471FB1D5505A6D8EC7A107A_gshared (TrackableChanges_1_t8F77CF9D37FD0DCA052A7D6B34E1A489AF13C41B * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TrackableChanges_1_Dispose_m7F29A9FDEA683B63D471FB1D5505A6D8EC7A107A_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = TrackableChanges_1_get_isCreated_m68F2E3D10D821918F3C0002EAA07440EC60CB4B0((TrackableChanges_1_t8F77CF9D37FD0DCA052A7D6B34E1A489AF13C41B *)(TrackableChanges_1_t8F77CF9D37FD0DCA052A7D6B34E1A489AF13C41B *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 8));
		if (!L_0)
		{
			goto IL_0029;
		}
	}
	{
		NativeArray_1_t140FCB442630D549508192A9E3737425E84D15F8 * L_1 = (NativeArray_1_t140FCB442630D549508192A9E3737425E84D15F8 *)__this->get_address_of_m_Added_1();
		NativeArray_1_Dispose_mF075AE11F3DFF21CA4E5A17B28A0F9D1B52D5926((NativeArray_1_t140FCB442630D549508192A9E3737425E84D15F8 *)(NativeArray_1_t140FCB442630D549508192A9E3737425E84D15F8 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 9));
		NativeArray_1_t140FCB442630D549508192A9E3737425E84D15F8 * L_2 = (NativeArray_1_t140FCB442630D549508192A9E3737425E84D15F8 *)__this->get_address_of_m_Updated_2();
		NativeArray_1_Dispose_mF075AE11F3DFF21CA4E5A17B28A0F9D1B52D5926((NativeArray_1_t140FCB442630D549508192A9E3737425E84D15F8 *)(NativeArray_1_t140FCB442630D549508192A9E3737425E84D15F8 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 9));
		NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 * L_3 = (NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 *)__this->get_address_of_m_Removed_3();
		NativeArray_1_Dispose_mB8124101177299FC76251C4E7F6C98BB0E5503C7((NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 *)(NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 *)L_3, /*hidden argument*/NativeArray_1_Dispose_mB8124101177299FC76251C4E7F6C98BB0E5503C7_RuntimeMethod_var);
	}

IL_0029:
	{
		return;
	}
}
extern "C"  void TrackableChanges_1_Dispose_m7F29A9FDEA683B63D471FB1D5505A6D8EC7A107A_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	TrackableChanges_1_t8F77CF9D37FD0DCA052A7D6B34E1A489AF13C41B * _thisAdjusted = reinterpret_cast<TrackableChanges_1_t8F77CF9D37FD0DCA052A7D6B34E1A489AF13C41B *>(__this + 1);
	TrackableChanges_1_Dispose_m7F29A9FDEA683B63D471FB1D5505A6D8EC7A107A(_thisAdjusted, method);
}
// System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRHumanBody>::.ctor(Unity.Collections.NativeArray`1<T>,Unity.Collections.NativeArray`1<T>,Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.TrackableId>)
extern "C" IL2CPP_METHOD_ATTR void TrackableChanges_1__ctor_mCBFAB9AC1ADBAED983EEC593665EBBE6337E9A3B_gshared (TrackableChanges_1_t8F77CF9D37FD0DCA052A7D6B34E1A489AF13C41B * __this, NativeArray_1_t140FCB442630D549508192A9E3737425E84D15F8  ___added0, NativeArray_1_t140FCB442630D549508192A9E3737425E84D15F8  ___updated1, NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  ___removed2, const RuntimeMethod* method)
{
	{
		NativeArray_1_t140FCB442630D549508192A9E3737425E84D15F8  L_0 = ___added0;
		__this->set_m_Added_1(L_0);
		NativeArray_1_t140FCB442630D549508192A9E3737425E84D15F8  L_1 = ___updated1;
		__this->set_m_Updated_2(L_1);
		NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  L_2 = ___removed2;
		__this->set_m_Removed_3(L_2);
		TrackableChanges_1_set_isCreated_m6FF97BC809D88ABBCD88D515340CBC1F1EE27D83((TrackableChanges_1_t8F77CF9D37FD0DCA052A7D6B34E1A489AF13C41B *)(TrackableChanges_1_t8F77CF9D37FD0DCA052A7D6B34E1A489AF13C41B *)__this, (bool)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 2));
		return;
	}
}
extern "C"  void TrackableChanges_1__ctor_mCBFAB9AC1ADBAED983EEC593665EBBE6337E9A3B_AdjustorThunk (RuntimeObject * __this, NativeArray_1_t140FCB442630D549508192A9E3737425E84D15F8  ___added0, NativeArray_1_t140FCB442630D549508192A9E3737425E84D15F8  ___updated1, NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  ___removed2, const RuntimeMethod* method)
{
	TrackableChanges_1_t8F77CF9D37FD0DCA052A7D6B34E1A489AF13C41B * _thisAdjusted = reinterpret_cast<TrackableChanges_1_t8F77CF9D37FD0DCA052A7D6B34E1A489AF13C41B *>(__this + 1);
	TrackableChanges_1__ctor_mCBFAB9AC1ADBAED983EEC593665EBBE6337E9A3B(_thisAdjusted, ___added0, ___updated1, ___removed2, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Unity.Collections.NativeArray`1<T> UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRPointCloud>::get_added()
extern "C" IL2CPP_METHOD_ATTR NativeArray_1_t5F0698DD293100E2FECB8BC55FCBC3A3F6FEA582  TrackableChanges_1_get_added_mBB3A80E298216A6D8826422F62A11A1A4CB7CBD9_gshared (TrackableChanges_1_t67F5E50C0B7A063774AEC211C4C81BA7679876BB * __this, const RuntimeMethod* method)
{
	{
		NativeArray_1_t5F0698DD293100E2FECB8BC55FCBC3A3F6FEA582  L_0 = (NativeArray_1_t5F0698DD293100E2FECB8BC55FCBC3A3F6FEA582 )__this->get_m_Added_1();
		return L_0;
	}
}
extern "C"  NativeArray_1_t5F0698DD293100E2FECB8BC55FCBC3A3F6FEA582  TrackableChanges_1_get_added_mBB3A80E298216A6D8826422F62A11A1A4CB7CBD9_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	TrackableChanges_1_t67F5E50C0B7A063774AEC211C4C81BA7679876BB * _thisAdjusted = reinterpret_cast<TrackableChanges_1_t67F5E50C0B7A063774AEC211C4C81BA7679876BB *>(__this + 1);
	return TrackableChanges_1_get_added_mBB3A80E298216A6D8826422F62A11A1A4CB7CBD9(_thisAdjusted, method);
}
// Unity.Collections.NativeArray`1<T> UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRPointCloud>::get_updated()
extern "C" IL2CPP_METHOD_ATTR NativeArray_1_t5F0698DD293100E2FECB8BC55FCBC3A3F6FEA582  TrackableChanges_1_get_updated_m80DB51775906B404AEE4C7454E7A517ECFD6A003_gshared (TrackableChanges_1_t67F5E50C0B7A063774AEC211C4C81BA7679876BB * __this, const RuntimeMethod* method)
{
	{
		NativeArray_1_t5F0698DD293100E2FECB8BC55FCBC3A3F6FEA582  L_0 = (NativeArray_1_t5F0698DD293100E2FECB8BC55FCBC3A3F6FEA582 )__this->get_m_Updated_2();
		return L_0;
	}
}
extern "C"  NativeArray_1_t5F0698DD293100E2FECB8BC55FCBC3A3F6FEA582  TrackableChanges_1_get_updated_m80DB51775906B404AEE4C7454E7A517ECFD6A003_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	TrackableChanges_1_t67F5E50C0B7A063774AEC211C4C81BA7679876BB * _thisAdjusted = reinterpret_cast<TrackableChanges_1_t67F5E50C0B7A063774AEC211C4C81BA7679876BB *>(__this + 1);
	return TrackableChanges_1_get_updated_m80DB51775906B404AEE4C7454E7A517ECFD6A003(_thisAdjusted, method);
}
// Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.TrackableId> UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRPointCloud>::get_removed()
extern "C" IL2CPP_METHOD_ATTR NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  TrackableChanges_1_get_removed_mC49E91CAF67469CDD76DBCF18D1423DC3971AF99_gshared (TrackableChanges_1_t67F5E50C0B7A063774AEC211C4C81BA7679876BB * __this, const RuntimeMethod* method)
{
	{
		NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  L_0 = (NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 )__this->get_m_Removed_3();
		return L_0;
	}
}
extern "C"  NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  TrackableChanges_1_get_removed_mC49E91CAF67469CDD76DBCF18D1423DC3971AF99_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	TrackableChanges_1_t67F5E50C0B7A063774AEC211C4C81BA7679876BB * _thisAdjusted = reinterpret_cast<TrackableChanges_1_t67F5E50C0B7A063774AEC211C4C81BA7679876BB *>(__this + 1);
	return TrackableChanges_1_get_removed_mC49E91CAF67469CDD76DBCF18D1423DC3971AF99(_thisAdjusted, method);
}
// System.Boolean UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRPointCloud>::get_isCreated()
extern "C" IL2CPP_METHOD_ATTR bool TrackableChanges_1_get_isCreated_m6BDB6E85EF722CE4F115552E754E059F9BBA87B8_gshared (TrackableChanges_1_t67F5E50C0B7A063774AEC211C4C81BA7679876BB * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = (bool)__this->get_U3CisCreatedU3Ek__BackingField_0();
		return L_0;
	}
}
extern "C"  bool TrackableChanges_1_get_isCreated_m6BDB6E85EF722CE4F115552E754E059F9BBA87B8_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	TrackableChanges_1_t67F5E50C0B7A063774AEC211C4C81BA7679876BB * _thisAdjusted = reinterpret_cast<TrackableChanges_1_t67F5E50C0B7A063774AEC211C4C81BA7679876BB *>(__this + 1);
	return TrackableChanges_1_get_isCreated_m6BDB6E85EF722CE4F115552E754E059F9BBA87B8(_thisAdjusted, method);
}
// System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRPointCloud>::set_isCreated(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void TrackableChanges_1_set_isCreated_mE659C5BCD61914E799B4D6AAE6AD5AD7B9DDCE76_gshared (TrackableChanges_1_t67F5E50C0B7A063774AEC211C4C81BA7679876BB * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3CisCreatedU3Ek__BackingField_0(L_0);
		return;
	}
}
extern "C"  void TrackableChanges_1_set_isCreated_mE659C5BCD61914E799B4D6AAE6AD5AD7B9DDCE76_AdjustorThunk (RuntimeObject * __this, bool ___value0, const RuntimeMethod* method)
{
	TrackableChanges_1_t67F5E50C0B7A063774AEC211C4C81BA7679876BB * _thisAdjusted = reinterpret_cast<TrackableChanges_1_t67F5E50C0B7A063774AEC211C4C81BA7679876BB *>(__this + 1);
	TrackableChanges_1_set_isCreated_mE659C5BCD61914E799B4D6AAE6AD5AD7B9DDCE76(_thisAdjusted, ___value0, method);
}
// System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRPointCloud>::.ctor(System.Int32,System.Int32,System.Int32,Unity.Collections.Allocator)
extern "C" IL2CPP_METHOD_ATTR void TrackableChanges_1__ctor_mB26E1128CC08E676885A96E341B8CE2DC1228D20_gshared (TrackableChanges_1_t67F5E50C0B7A063774AEC211C4C81BA7679876BB * __this, int32_t ___addedCount0, int32_t ___updatedCount1, int32_t ___removedCount2, int32_t ___allocator3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TrackableChanges_1__ctor_mB26E1128CC08E676885A96E341B8CE2DC1228D20_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___addedCount0;
		int32_t L_1 = ___allocator3;
		NativeArray_1_t5F0698DD293100E2FECB8BC55FCBC3A3F6FEA582  L_2;
		memset(&L_2, 0, sizeof(L_2));
		NativeArray_1__ctor_m0A7431FE284605A389AB455AFBC44304CAE118CF((&L_2), (int32_t)L_0, (int32_t)L_1, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		__this->set_m_Added_1(L_2);
		int32_t L_3 = ___updatedCount1;
		int32_t L_4 = ___allocator3;
		NativeArray_1_t5F0698DD293100E2FECB8BC55FCBC3A3F6FEA582  L_5;
		memset(&L_5, 0, sizeof(L_5));
		NativeArray_1__ctor_m0A7431FE284605A389AB455AFBC44304CAE118CF((&L_5), (int32_t)L_3, (int32_t)L_4, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		__this->set_m_Updated_2(L_5);
		int32_t L_6 = ___removedCount2;
		int32_t L_7 = ___allocator3;
		NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  L_8;
		memset(&L_8, 0, sizeof(L_8));
		NativeArray_1__ctor_m49FCF39766C38037835E85C0A677B26599D0BE55((&L_8), (int32_t)L_6, (int32_t)L_7, (int32_t)1, /*hidden argument*/NativeArray_1__ctor_m49FCF39766C38037835E85C0A677B26599D0BE55_RuntimeMethod_var);
		__this->set_m_Removed_3(L_8);
		TrackableChanges_1_set_isCreated_mE659C5BCD61914E799B4D6AAE6AD5AD7B9DDCE76((TrackableChanges_1_t67F5E50C0B7A063774AEC211C4C81BA7679876BB *)(TrackableChanges_1_t67F5E50C0B7A063774AEC211C4C81BA7679876BB *)__this, (bool)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 2));
		return;
	}
}
extern "C"  void TrackableChanges_1__ctor_mB26E1128CC08E676885A96E341B8CE2DC1228D20_AdjustorThunk (RuntimeObject * __this, int32_t ___addedCount0, int32_t ___updatedCount1, int32_t ___removedCount2, int32_t ___allocator3, const RuntimeMethod* method)
{
	TrackableChanges_1_t67F5E50C0B7A063774AEC211C4C81BA7679876BB * _thisAdjusted = reinterpret_cast<TrackableChanges_1_t67F5E50C0B7A063774AEC211C4C81BA7679876BB *>(__this + 1);
	TrackableChanges_1__ctor_mB26E1128CC08E676885A96E341B8CE2DC1228D20(_thisAdjusted, ___addedCount0, ___updatedCount1, ___removedCount2, ___allocator3, method);
}
// System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRPointCloud>::.ctor(System.Void*,System.Int32,System.Void*,System.Int32,System.Void*,System.Int32,T,System.Int32,Unity.Collections.Allocator)
extern "C" IL2CPP_METHOD_ATTR void TrackableChanges_1__ctor_mA69A666BCF692D5A8660A564A4450ABF6C80FD90_gshared (TrackableChanges_1_t67F5E50C0B7A063774AEC211C4C81BA7679876BB * __this, void* ___addedPtr0, int32_t ___addedCount1, void* ___updatedPtr2, int32_t ___updatedCount3, void* ___removedPtr4, int32_t ___removedCount5, XRPointCloud_tA4A412DE503530E1B2953919F1463B9B48504ED0  ___defaultT6, int32_t ___stride7, int32_t ___allocator8, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TrackableChanges_1__ctor_mA69A666BCF692D5A8660A564A4450ABF6C80FD90_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		XRPointCloud_tA4A412DE503530E1B2953919F1463B9B48504ED0  L_0 = ___defaultT6;
		void* L_1 = ___addedPtr0;
		int32_t L_2 = ___stride7;
		int32_t L_3 = ___addedCount1;
		int32_t L_4 = ___allocator8;
		NativeArray_1_t5F0698DD293100E2FECB8BC55FCBC3A3F6FEA582  L_5 = ((  NativeArray_1_t5F0698DD293100E2FECB8BC55FCBC3A3F6FEA582  (*) (XRPointCloud_tA4A412DE503530E1B2953919F1463B9B48504ED0 , void*, int32_t, int32_t, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3)->methodPointer)((XRPointCloud_tA4A412DE503530E1B2953919F1463B9B48504ED0 )L_0, (void*)(void*)L_1, (int32_t)L_2, (int32_t)L_3, (int32_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3));
		__this->set_m_Added_1(L_5);
		XRPointCloud_tA4A412DE503530E1B2953919F1463B9B48504ED0  L_6 = ___defaultT6;
		void* L_7 = ___updatedPtr2;
		int32_t L_8 = ___stride7;
		int32_t L_9 = ___updatedCount3;
		int32_t L_10 = ___allocator8;
		NativeArray_1_t5F0698DD293100E2FECB8BC55FCBC3A3F6FEA582  L_11 = ((  NativeArray_1_t5F0698DD293100E2FECB8BC55FCBC3A3F6FEA582  (*) (XRPointCloud_tA4A412DE503530E1B2953919F1463B9B48504ED0 , void*, int32_t, int32_t, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3)->methodPointer)((XRPointCloud_tA4A412DE503530E1B2953919F1463B9B48504ED0 )L_6, (void*)(void*)L_7, (int32_t)L_8, (int32_t)L_9, (int32_t)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3));
		__this->set_m_Updated_2(L_11);
		int32_t L_12 = ___removedCount5;
		int32_t L_13 = ___allocator8;
		NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  L_14;
		memset(&L_14, 0, sizeof(L_14));
		NativeArray_1__ctor_m49FCF39766C38037835E85C0A677B26599D0BE55((&L_14), (int32_t)L_12, (int32_t)L_13, (int32_t)1, /*hidden argument*/NativeArray_1__ctor_m49FCF39766C38037835E85C0A677B26599D0BE55_RuntimeMethod_var);
		__this->set_m_Removed_3(L_14);
		int32_t L_15 = ___removedCount5;
		if ((((int32_t)L_15) <= ((int32_t)0)))
		{
			goto IL_0051;
		}
	}
	{
		NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 * L_16 = (NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 *)__this->get_address_of_m_Removed_3();
		void* L_17 = ___removedPtr4;
		int32_t L_18 = ___removedCount5;
		NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  L_19 = NativeArrayUnsafeUtility_ConvertExistingDataToNativeArray_TisTrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47_m8D7EEF45CE20EB27369597A33879F42E459F2D59((void*)(void*)L_17, (int32_t)L_18, (int32_t)1, /*hidden argument*/NativeArrayUnsafeUtility_ConvertExistingDataToNativeArray_TisTrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47_m8D7EEF45CE20EB27369597A33879F42E459F2D59_RuntimeMethod_var);
		NativeArray_1_CopyFrom_mE2C04369E83ED4726DE1121E7E05342B6A993A97((NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 *)(NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 *)L_16, (NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 )L_19, /*hidden argument*/NativeArray_1_CopyFrom_mE2C04369E83ED4726DE1121E7E05342B6A993A97_RuntimeMethod_var);
	}

IL_0051:
	{
		TrackableChanges_1_set_isCreated_mE659C5BCD61914E799B4D6AAE6AD5AD7B9DDCE76((TrackableChanges_1_t67F5E50C0B7A063774AEC211C4C81BA7679876BB *)(TrackableChanges_1_t67F5E50C0B7A063774AEC211C4C81BA7679876BB *)__this, (bool)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 2));
		return;
	}
}
extern "C"  void TrackableChanges_1__ctor_mA69A666BCF692D5A8660A564A4450ABF6C80FD90_AdjustorThunk (RuntimeObject * __this, void* ___addedPtr0, int32_t ___addedCount1, void* ___updatedPtr2, int32_t ___updatedCount3, void* ___removedPtr4, int32_t ___removedCount5, XRPointCloud_tA4A412DE503530E1B2953919F1463B9B48504ED0  ___defaultT6, int32_t ___stride7, int32_t ___allocator8, const RuntimeMethod* method)
{
	TrackableChanges_1_t67F5E50C0B7A063774AEC211C4C81BA7679876BB * _thisAdjusted = reinterpret_cast<TrackableChanges_1_t67F5E50C0B7A063774AEC211C4C81BA7679876BB *>(__this + 1);
	TrackableChanges_1__ctor_mA69A666BCF692D5A8660A564A4450ABF6C80FD90(_thisAdjusted, ___addedPtr0, ___addedCount1, ___updatedPtr2, ___updatedCount3, ___removedPtr4, ___removedCount5, ___defaultT6, ___stride7, ___allocator8, method);
}
// UnityEngine.XR.ARSubsystems.TrackableChanges`1<T> UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRPointCloud>::CopyFrom(Unity.Collections.NativeArray`1<T>,Unity.Collections.NativeArray`1<T>,Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.TrackableId>,Unity.Collections.Allocator)
extern "C" IL2CPP_METHOD_ATTR TrackableChanges_1_t67F5E50C0B7A063774AEC211C4C81BA7679876BB  TrackableChanges_1_CopyFrom_m23C9F4B5E29C52D8878486A365DE352B9D520117_gshared (NativeArray_1_t5F0698DD293100E2FECB8BC55FCBC3A3F6FEA582  ___added0, NativeArray_1_t5F0698DD293100E2FECB8BC55FCBC3A3F6FEA582  ___updated1, NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  ___removed2, int32_t ___allocator3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TrackableChanges_1_CopyFrom_m23C9F4B5E29C52D8878486A365DE352B9D520117_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	NativeArray_1_t5F0698DD293100E2FECB8BC55FCBC3A3F6FEA582  V_0;
	memset(&V_0, 0, sizeof(V_0));
	NativeArray_1_t5F0698DD293100E2FECB8BC55FCBC3A3F6FEA582  V_1;
	memset(&V_1, 0, sizeof(V_1));
	NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		int32_t L_0 = IL2CPP_NATIVEARRAY_GET_LENGTH(((NativeArray_1_t5F0698DD293100E2FECB8BC55FCBC3A3F6FEA582 *)(NativeArray_1_t5F0698DD293100E2FECB8BC55FCBC3A3F6FEA582 *)(&___added0))->___m_Length_1);
		int32_t L_1 = ___allocator3;
		NativeArray_1__ctor_m0A7431FE284605A389AB455AFBC44304CAE118CF((NativeArray_1_t5F0698DD293100E2FECB8BC55FCBC3A3F6FEA582 *)(NativeArray_1_t5F0698DD293100E2FECB8BC55FCBC3A3F6FEA582 *)(&V_0), (int32_t)L_0, (int32_t)L_1, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		NativeArray_1_t5F0698DD293100E2FECB8BC55FCBC3A3F6FEA582  L_2 = ___added0;
		NativeArray_1_CopyFrom_mE62AAA10EC11C27A6A545BEF481C0FC3DB4800B3((NativeArray_1_t5F0698DD293100E2FECB8BC55FCBC3A3F6FEA582 *)(NativeArray_1_t5F0698DD293100E2FECB8BC55FCBC3A3F6FEA582 *)(&V_0), (NativeArray_1_t5F0698DD293100E2FECB8BC55FCBC3A3F6FEA582 )L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 5));
		int32_t L_3 = IL2CPP_NATIVEARRAY_GET_LENGTH(((NativeArray_1_t5F0698DD293100E2FECB8BC55FCBC3A3F6FEA582 *)(NativeArray_1_t5F0698DD293100E2FECB8BC55FCBC3A3F6FEA582 *)(&___updated1))->___m_Length_1);
		int32_t L_4 = ___allocator3;
		NativeArray_1__ctor_m0A7431FE284605A389AB455AFBC44304CAE118CF((NativeArray_1_t5F0698DD293100E2FECB8BC55FCBC3A3F6FEA582 *)(NativeArray_1_t5F0698DD293100E2FECB8BC55FCBC3A3F6FEA582 *)(&V_1), (int32_t)L_3, (int32_t)L_4, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		NativeArray_1_t5F0698DD293100E2FECB8BC55FCBC3A3F6FEA582  L_5 = ___updated1;
		NativeArray_1_CopyFrom_mE62AAA10EC11C27A6A545BEF481C0FC3DB4800B3((NativeArray_1_t5F0698DD293100E2FECB8BC55FCBC3A3F6FEA582 *)(NativeArray_1_t5F0698DD293100E2FECB8BC55FCBC3A3F6FEA582 *)(&V_1), (NativeArray_1_t5F0698DD293100E2FECB8BC55FCBC3A3F6FEA582 )L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 5));
		int32_t L_6 = IL2CPP_NATIVEARRAY_GET_LENGTH(((NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 *)(NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 *)(&___removed2))->___m_Length_1);
		int32_t L_7 = ___allocator3;
		NativeArray_1__ctor_m49FCF39766C38037835E85C0A677B26599D0BE55((NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 *)(NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 *)(&V_2), (int32_t)L_6, (int32_t)L_7, (int32_t)1, /*hidden argument*/NativeArray_1__ctor_m49FCF39766C38037835E85C0A677B26599D0BE55_RuntimeMethod_var);
		NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  L_8 = ___removed2;
		NativeArray_1_CopyFrom_mE2C04369E83ED4726DE1121E7E05342B6A993A97((NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 *)(NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 *)(&V_2), (NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 )L_8, /*hidden argument*/NativeArray_1_CopyFrom_mE2C04369E83ED4726DE1121E7E05342B6A993A97_RuntimeMethod_var);
		NativeArray_1_t5F0698DD293100E2FECB8BC55FCBC3A3F6FEA582  L_9 = V_0;
		NativeArray_1_t5F0698DD293100E2FECB8BC55FCBC3A3F6FEA582  L_10 = V_1;
		NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  L_11 = V_2;
		TrackableChanges_1_t67F5E50C0B7A063774AEC211C4C81BA7679876BB  L_12;
		memset(&L_12, 0, sizeof(L_12));
		TrackableChanges_1__ctor_m4961BB84AABB2C5A4D492E4994845A231BFB09BF((&L_12), (NativeArray_1_t5F0698DD293100E2FECB8BC55FCBC3A3F6FEA582 )L_9, (NativeArray_1_t5F0698DD293100E2FECB8BC55FCBC3A3F6FEA582 )L_10, (NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 )L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 7));
		return L_12;
	}
}
// System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRPointCloud>::Dispose()
extern "C" IL2CPP_METHOD_ATTR void TrackableChanges_1_Dispose_mE3DC3C0230641A828B6A91F3CB94441797520834_gshared (TrackableChanges_1_t67F5E50C0B7A063774AEC211C4C81BA7679876BB * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TrackableChanges_1_Dispose_mE3DC3C0230641A828B6A91F3CB94441797520834_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = TrackableChanges_1_get_isCreated_m6BDB6E85EF722CE4F115552E754E059F9BBA87B8((TrackableChanges_1_t67F5E50C0B7A063774AEC211C4C81BA7679876BB *)(TrackableChanges_1_t67F5E50C0B7A063774AEC211C4C81BA7679876BB *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 8));
		if (!L_0)
		{
			goto IL_0029;
		}
	}
	{
		NativeArray_1_t5F0698DD293100E2FECB8BC55FCBC3A3F6FEA582 * L_1 = (NativeArray_1_t5F0698DD293100E2FECB8BC55FCBC3A3F6FEA582 *)__this->get_address_of_m_Added_1();
		NativeArray_1_Dispose_mAF31B95CE89FF647372855A4ECFB9FCDDE39C38B((NativeArray_1_t5F0698DD293100E2FECB8BC55FCBC3A3F6FEA582 *)(NativeArray_1_t5F0698DD293100E2FECB8BC55FCBC3A3F6FEA582 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 9));
		NativeArray_1_t5F0698DD293100E2FECB8BC55FCBC3A3F6FEA582 * L_2 = (NativeArray_1_t5F0698DD293100E2FECB8BC55FCBC3A3F6FEA582 *)__this->get_address_of_m_Updated_2();
		NativeArray_1_Dispose_mAF31B95CE89FF647372855A4ECFB9FCDDE39C38B((NativeArray_1_t5F0698DD293100E2FECB8BC55FCBC3A3F6FEA582 *)(NativeArray_1_t5F0698DD293100E2FECB8BC55FCBC3A3F6FEA582 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 9));
		NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 * L_3 = (NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 *)__this->get_address_of_m_Removed_3();
		NativeArray_1_Dispose_mB8124101177299FC76251C4E7F6C98BB0E5503C7((NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 *)(NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 *)L_3, /*hidden argument*/NativeArray_1_Dispose_mB8124101177299FC76251C4E7F6C98BB0E5503C7_RuntimeMethod_var);
	}

IL_0029:
	{
		return;
	}
}
extern "C"  void TrackableChanges_1_Dispose_mE3DC3C0230641A828B6A91F3CB94441797520834_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	TrackableChanges_1_t67F5E50C0B7A063774AEC211C4C81BA7679876BB * _thisAdjusted = reinterpret_cast<TrackableChanges_1_t67F5E50C0B7A063774AEC211C4C81BA7679876BB *>(__this + 1);
	TrackableChanges_1_Dispose_mE3DC3C0230641A828B6A91F3CB94441797520834(_thisAdjusted, method);
}
// System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRPointCloud>::.ctor(Unity.Collections.NativeArray`1<T>,Unity.Collections.NativeArray`1<T>,Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.TrackableId>)
extern "C" IL2CPP_METHOD_ATTR void TrackableChanges_1__ctor_m4961BB84AABB2C5A4D492E4994845A231BFB09BF_gshared (TrackableChanges_1_t67F5E50C0B7A063774AEC211C4C81BA7679876BB * __this, NativeArray_1_t5F0698DD293100E2FECB8BC55FCBC3A3F6FEA582  ___added0, NativeArray_1_t5F0698DD293100E2FECB8BC55FCBC3A3F6FEA582  ___updated1, NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  ___removed2, const RuntimeMethod* method)
{
	{
		NativeArray_1_t5F0698DD293100E2FECB8BC55FCBC3A3F6FEA582  L_0 = ___added0;
		__this->set_m_Added_1(L_0);
		NativeArray_1_t5F0698DD293100E2FECB8BC55FCBC3A3F6FEA582  L_1 = ___updated1;
		__this->set_m_Updated_2(L_1);
		NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  L_2 = ___removed2;
		__this->set_m_Removed_3(L_2);
		TrackableChanges_1_set_isCreated_mE659C5BCD61914E799B4D6AAE6AD5AD7B9DDCE76((TrackableChanges_1_t67F5E50C0B7A063774AEC211C4C81BA7679876BB *)(TrackableChanges_1_t67F5E50C0B7A063774AEC211C4C81BA7679876BB *)__this, (bool)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 2));
		return;
	}
}
extern "C"  void TrackableChanges_1__ctor_m4961BB84AABB2C5A4D492E4994845A231BFB09BF_AdjustorThunk (RuntimeObject * __this, NativeArray_1_t5F0698DD293100E2FECB8BC55FCBC3A3F6FEA582  ___added0, NativeArray_1_t5F0698DD293100E2FECB8BC55FCBC3A3F6FEA582  ___updated1, NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  ___removed2, const RuntimeMethod* method)
{
	TrackableChanges_1_t67F5E50C0B7A063774AEC211C4C81BA7679876BB * _thisAdjusted = reinterpret_cast<TrackableChanges_1_t67F5E50C0B7A063774AEC211C4C81BA7679876BB *>(__this + 1);
	TrackableChanges_1__ctor_m4961BB84AABB2C5A4D492E4994845A231BFB09BF(_thisAdjusted, ___added0, ___updated1, ___removed2, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Unity.Collections.NativeArray`1<T> UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRReferencePoint>::get_added()
extern "C" IL2CPP_METHOD_ATTR NativeArray_1_t5AADFB4C72573FE3017795F15B8CBC88625A8876  TrackableChanges_1_get_added_mC31FEFAAC8F70ABBBC324DC618B0DFAB08AAE934_gshared (TrackableChanges_1_t5C8C3FBA23E5BBC147A6991B68520A756EF54FDF * __this, const RuntimeMethod* method)
{
	{
		NativeArray_1_t5AADFB4C72573FE3017795F15B8CBC88625A8876  L_0 = (NativeArray_1_t5AADFB4C72573FE3017795F15B8CBC88625A8876 )__this->get_m_Added_1();
		return L_0;
	}
}
extern "C"  NativeArray_1_t5AADFB4C72573FE3017795F15B8CBC88625A8876  TrackableChanges_1_get_added_mC31FEFAAC8F70ABBBC324DC618B0DFAB08AAE934_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	TrackableChanges_1_t5C8C3FBA23E5BBC147A6991B68520A756EF54FDF * _thisAdjusted = reinterpret_cast<TrackableChanges_1_t5C8C3FBA23E5BBC147A6991B68520A756EF54FDF *>(__this + 1);
	return TrackableChanges_1_get_added_mC31FEFAAC8F70ABBBC324DC618B0DFAB08AAE934(_thisAdjusted, method);
}
// Unity.Collections.NativeArray`1<T> UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRReferencePoint>::get_updated()
extern "C" IL2CPP_METHOD_ATTR NativeArray_1_t5AADFB4C72573FE3017795F15B8CBC88625A8876  TrackableChanges_1_get_updated_m54C710EFD531DFB25ABA289B60FAA4181D479DDF_gshared (TrackableChanges_1_t5C8C3FBA23E5BBC147A6991B68520A756EF54FDF * __this, const RuntimeMethod* method)
{
	{
		NativeArray_1_t5AADFB4C72573FE3017795F15B8CBC88625A8876  L_0 = (NativeArray_1_t5AADFB4C72573FE3017795F15B8CBC88625A8876 )__this->get_m_Updated_2();
		return L_0;
	}
}
extern "C"  NativeArray_1_t5AADFB4C72573FE3017795F15B8CBC88625A8876  TrackableChanges_1_get_updated_m54C710EFD531DFB25ABA289B60FAA4181D479DDF_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	TrackableChanges_1_t5C8C3FBA23E5BBC147A6991B68520A756EF54FDF * _thisAdjusted = reinterpret_cast<TrackableChanges_1_t5C8C3FBA23E5BBC147A6991B68520A756EF54FDF *>(__this + 1);
	return TrackableChanges_1_get_updated_m54C710EFD531DFB25ABA289B60FAA4181D479DDF(_thisAdjusted, method);
}
// Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.TrackableId> UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRReferencePoint>::get_removed()
extern "C" IL2CPP_METHOD_ATTR NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  TrackableChanges_1_get_removed_m9781A441FA95E17CBDB3C4687247859A562AF077_gshared (TrackableChanges_1_t5C8C3FBA23E5BBC147A6991B68520A756EF54FDF * __this, const RuntimeMethod* method)
{
	{
		NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  L_0 = (NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 )__this->get_m_Removed_3();
		return L_0;
	}
}
extern "C"  NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  TrackableChanges_1_get_removed_m9781A441FA95E17CBDB3C4687247859A562AF077_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	TrackableChanges_1_t5C8C3FBA23E5BBC147A6991B68520A756EF54FDF * _thisAdjusted = reinterpret_cast<TrackableChanges_1_t5C8C3FBA23E5BBC147A6991B68520A756EF54FDF *>(__this + 1);
	return TrackableChanges_1_get_removed_m9781A441FA95E17CBDB3C4687247859A562AF077(_thisAdjusted, method);
}
// System.Boolean UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRReferencePoint>::get_isCreated()
extern "C" IL2CPP_METHOD_ATTR bool TrackableChanges_1_get_isCreated_m81592B6012FD82C68A023EC52FFAB3B4A35C6A3D_gshared (TrackableChanges_1_t5C8C3FBA23E5BBC147A6991B68520A756EF54FDF * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = (bool)__this->get_U3CisCreatedU3Ek__BackingField_0();
		return L_0;
	}
}
extern "C"  bool TrackableChanges_1_get_isCreated_m81592B6012FD82C68A023EC52FFAB3B4A35C6A3D_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	TrackableChanges_1_t5C8C3FBA23E5BBC147A6991B68520A756EF54FDF * _thisAdjusted = reinterpret_cast<TrackableChanges_1_t5C8C3FBA23E5BBC147A6991B68520A756EF54FDF *>(__this + 1);
	return TrackableChanges_1_get_isCreated_m81592B6012FD82C68A023EC52FFAB3B4A35C6A3D(_thisAdjusted, method);
}
// System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRReferencePoint>::set_isCreated(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void TrackableChanges_1_set_isCreated_m3B57EAEC482D7B5D700E044B8720F89F25B13DC5_gshared (TrackableChanges_1_t5C8C3FBA23E5BBC147A6991B68520A756EF54FDF * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3CisCreatedU3Ek__BackingField_0(L_0);
		return;
	}
}
extern "C"  void TrackableChanges_1_set_isCreated_m3B57EAEC482D7B5D700E044B8720F89F25B13DC5_AdjustorThunk (RuntimeObject * __this, bool ___value0, const RuntimeMethod* method)
{
	TrackableChanges_1_t5C8C3FBA23E5BBC147A6991B68520A756EF54FDF * _thisAdjusted = reinterpret_cast<TrackableChanges_1_t5C8C3FBA23E5BBC147A6991B68520A756EF54FDF *>(__this + 1);
	TrackableChanges_1_set_isCreated_m3B57EAEC482D7B5D700E044B8720F89F25B13DC5(_thisAdjusted, ___value0, method);
}
// System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRReferencePoint>::.ctor(System.Int32,System.Int32,System.Int32,Unity.Collections.Allocator)
extern "C" IL2CPP_METHOD_ATTR void TrackableChanges_1__ctor_m8E2935D6FEAB365BB8BBF74A7D12694909DFB282_gshared (TrackableChanges_1_t5C8C3FBA23E5BBC147A6991B68520A756EF54FDF * __this, int32_t ___addedCount0, int32_t ___updatedCount1, int32_t ___removedCount2, int32_t ___allocator3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TrackableChanges_1__ctor_m8E2935D6FEAB365BB8BBF74A7D12694909DFB282_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___addedCount0;
		int32_t L_1 = ___allocator3;
		NativeArray_1_t5AADFB4C72573FE3017795F15B8CBC88625A8876  L_2;
		memset(&L_2, 0, sizeof(L_2));
		NativeArray_1__ctor_mBA3667DFD2FA91C71C118ECA43EB22503D4FC2C8((&L_2), (int32_t)L_0, (int32_t)L_1, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		__this->set_m_Added_1(L_2);
		int32_t L_3 = ___updatedCount1;
		int32_t L_4 = ___allocator3;
		NativeArray_1_t5AADFB4C72573FE3017795F15B8CBC88625A8876  L_5;
		memset(&L_5, 0, sizeof(L_5));
		NativeArray_1__ctor_mBA3667DFD2FA91C71C118ECA43EB22503D4FC2C8((&L_5), (int32_t)L_3, (int32_t)L_4, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		__this->set_m_Updated_2(L_5);
		int32_t L_6 = ___removedCount2;
		int32_t L_7 = ___allocator3;
		NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  L_8;
		memset(&L_8, 0, sizeof(L_8));
		NativeArray_1__ctor_m49FCF39766C38037835E85C0A677B26599D0BE55((&L_8), (int32_t)L_6, (int32_t)L_7, (int32_t)1, /*hidden argument*/NativeArray_1__ctor_m49FCF39766C38037835E85C0A677B26599D0BE55_RuntimeMethod_var);
		__this->set_m_Removed_3(L_8);
		TrackableChanges_1_set_isCreated_m3B57EAEC482D7B5D700E044B8720F89F25B13DC5((TrackableChanges_1_t5C8C3FBA23E5BBC147A6991B68520A756EF54FDF *)(TrackableChanges_1_t5C8C3FBA23E5BBC147A6991B68520A756EF54FDF *)__this, (bool)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 2));
		return;
	}
}
extern "C"  void TrackableChanges_1__ctor_m8E2935D6FEAB365BB8BBF74A7D12694909DFB282_AdjustorThunk (RuntimeObject * __this, int32_t ___addedCount0, int32_t ___updatedCount1, int32_t ___removedCount2, int32_t ___allocator3, const RuntimeMethod* method)
{
	TrackableChanges_1_t5C8C3FBA23E5BBC147A6991B68520A756EF54FDF * _thisAdjusted = reinterpret_cast<TrackableChanges_1_t5C8C3FBA23E5BBC147A6991B68520A756EF54FDF *>(__this + 1);
	TrackableChanges_1__ctor_m8E2935D6FEAB365BB8BBF74A7D12694909DFB282(_thisAdjusted, ___addedCount0, ___updatedCount1, ___removedCount2, ___allocator3, method);
}
// System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRReferencePoint>::.ctor(System.Void*,System.Int32,System.Void*,System.Int32,System.Void*,System.Int32,T,System.Int32,Unity.Collections.Allocator)
extern "C" IL2CPP_METHOD_ATTR void TrackableChanges_1__ctor_m47C55466595329D4629E9A78A403D34C94AC30FC_gshared (TrackableChanges_1_t5C8C3FBA23E5BBC147A6991B68520A756EF54FDF * __this, void* ___addedPtr0, int32_t ___addedCount1, void* ___updatedPtr2, int32_t ___updatedCount3, void* ___removedPtr4, int32_t ___removedCount5, XRReferencePoint_tA8592C08A27EC91D9B1FB3B083C95C5D372FF1F9  ___defaultT6, int32_t ___stride7, int32_t ___allocator8, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TrackableChanges_1__ctor_m47C55466595329D4629E9A78A403D34C94AC30FC_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		XRReferencePoint_tA8592C08A27EC91D9B1FB3B083C95C5D372FF1F9  L_0 = ___defaultT6;
		void* L_1 = ___addedPtr0;
		int32_t L_2 = ___stride7;
		int32_t L_3 = ___addedCount1;
		int32_t L_4 = ___allocator8;
		NativeArray_1_t5AADFB4C72573FE3017795F15B8CBC88625A8876  L_5 = ((  NativeArray_1_t5AADFB4C72573FE3017795F15B8CBC88625A8876  (*) (XRReferencePoint_tA8592C08A27EC91D9B1FB3B083C95C5D372FF1F9 , void*, int32_t, int32_t, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3)->methodPointer)((XRReferencePoint_tA8592C08A27EC91D9B1FB3B083C95C5D372FF1F9 )L_0, (void*)(void*)L_1, (int32_t)L_2, (int32_t)L_3, (int32_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3));
		__this->set_m_Added_1(L_5);
		XRReferencePoint_tA8592C08A27EC91D9B1FB3B083C95C5D372FF1F9  L_6 = ___defaultT6;
		void* L_7 = ___updatedPtr2;
		int32_t L_8 = ___stride7;
		int32_t L_9 = ___updatedCount3;
		int32_t L_10 = ___allocator8;
		NativeArray_1_t5AADFB4C72573FE3017795F15B8CBC88625A8876  L_11 = ((  NativeArray_1_t5AADFB4C72573FE3017795F15B8CBC88625A8876  (*) (XRReferencePoint_tA8592C08A27EC91D9B1FB3B083C95C5D372FF1F9 , void*, int32_t, int32_t, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3)->methodPointer)((XRReferencePoint_tA8592C08A27EC91D9B1FB3B083C95C5D372FF1F9 )L_6, (void*)(void*)L_7, (int32_t)L_8, (int32_t)L_9, (int32_t)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3));
		__this->set_m_Updated_2(L_11);
		int32_t L_12 = ___removedCount5;
		int32_t L_13 = ___allocator8;
		NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  L_14;
		memset(&L_14, 0, sizeof(L_14));
		NativeArray_1__ctor_m49FCF39766C38037835E85C0A677B26599D0BE55((&L_14), (int32_t)L_12, (int32_t)L_13, (int32_t)1, /*hidden argument*/NativeArray_1__ctor_m49FCF39766C38037835E85C0A677B26599D0BE55_RuntimeMethod_var);
		__this->set_m_Removed_3(L_14);
		int32_t L_15 = ___removedCount5;
		if ((((int32_t)L_15) <= ((int32_t)0)))
		{
			goto IL_0051;
		}
	}
	{
		NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 * L_16 = (NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 *)__this->get_address_of_m_Removed_3();
		void* L_17 = ___removedPtr4;
		int32_t L_18 = ___removedCount5;
		NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  L_19 = NativeArrayUnsafeUtility_ConvertExistingDataToNativeArray_TisTrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47_m8D7EEF45CE20EB27369597A33879F42E459F2D59((void*)(void*)L_17, (int32_t)L_18, (int32_t)1, /*hidden argument*/NativeArrayUnsafeUtility_ConvertExistingDataToNativeArray_TisTrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47_m8D7EEF45CE20EB27369597A33879F42E459F2D59_RuntimeMethod_var);
		NativeArray_1_CopyFrom_mE2C04369E83ED4726DE1121E7E05342B6A993A97((NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 *)(NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 *)L_16, (NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 )L_19, /*hidden argument*/NativeArray_1_CopyFrom_mE2C04369E83ED4726DE1121E7E05342B6A993A97_RuntimeMethod_var);
	}

IL_0051:
	{
		TrackableChanges_1_set_isCreated_m3B57EAEC482D7B5D700E044B8720F89F25B13DC5((TrackableChanges_1_t5C8C3FBA23E5BBC147A6991B68520A756EF54FDF *)(TrackableChanges_1_t5C8C3FBA23E5BBC147A6991B68520A756EF54FDF *)__this, (bool)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 2));
		return;
	}
}
extern "C"  void TrackableChanges_1__ctor_m47C55466595329D4629E9A78A403D34C94AC30FC_AdjustorThunk (RuntimeObject * __this, void* ___addedPtr0, int32_t ___addedCount1, void* ___updatedPtr2, int32_t ___updatedCount3, void* ___removedPtr4, int32_t ___removedCount5, XRReferencePoint_tA8592C08A27EC91D9B1FB3B083C95C5D372FF1F9  ___defaultT6, int32_t ___stride7, int32_t ___allocator8, const RuntimeMethod* method)
{
	TrackableChanges_1_t5C8C3FBA23E5BBC147A6991B68520A756EF54FDF * _thisAdjusted = reinterpret_cast<TrackableChanges_1_t5C8C3FBA23E5BBC147A6991B68520A756EF54FDF *>(__this + 1);
	TrackableChanges_1__ctor_m47C55466595329D4629E9A78A403D34C94AC30FC(_thisAdjusted, ___addedPtr0, ___addedCount1, ___updatedPtr2, ___updatedCount3, ___removedPtr4, ___removedCount5, ___defaultT6, ___stride7, ___allocator8, method);
}
// UnityEngine.XR.ARSubsystems.TrackableChanges`1<T> UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRReferencePoint>::CopyFrom(Unity.Collections.NativeArray`1<T>,Unity.Collections.NativeArray`1<T>,Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.TrackableId>,Unity.Collections.Allocator)
extern "C" IL2CPP_METHOD_ATTR TrackableChanges_1_t5C8C3FBA23E5BBC147A6991B68520A756EF54FDF  TrackableChanges_1_CopyFrom_m4393C6018EE4FF47EB0435CA9C6566BB9562652D_gshared (NativeArray_1_t5AADFB4C72573FE3017795F15B8CBC88625A8876  ___added0, NativeArray_1_t5AADFB4C72573FE3017795F15B8CBC88625A8876  ___updated1, NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  ___removed2, int32_t ___allocator3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TrackableChanges_1_CopyFrom_m4393C6018EE4FF47EB0435CA9C6566BB9562652D_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	NativeArray_1_t5AADFB4C72573FE3017795F15B8CBC88625A8876  V_0;
	memset(&V_0, 0, sizeof(V_0));
	NativeArray_1_t5AADFB4C72573FE3017795F15B8CBC88625A8876  V_1;
	memset(&V_1, 0, sizeof(V_1));
	NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		int32_t L_0 = IL2CPP_NATIVEARRAY_GET_LENGTH(((NativeArray_1_t5AADFB4C72573FE3017795F15B8CBC88625A8876 *)(NativeArray_1_t5AADFB4C72573FE3017795F15B8CBC88625A8876 *)(&___added0))->___m_Length_1);
		int32_t L_1 = ___allocator3;
		NativeArray_1__ctor_mBA3667DFD2FA91C71C118ECA43EB22503D4FC2C8((NativeArray_1_t5AADFB4C72573FE3017795F15B8CBC88625A8876 *)(NativeArray_1_t5AADFB4C72573FE3017795F15B8CBC88625A8876 *)(&V_0), (int32_t)L_0, (int32_t)L_1, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		NativeArray_1_t5AADFB4C72573FE3017795F15B8CBC88625A8876  L_2 = ___added0;
		NativeArray_1_CopyFrom_mC053D3A9DF2FBD4DCB31D2864F4996E9B2DA255A((NativeArray_1_t5AADFB4C72573FE3017795F15B8CBC88625A8876 *)(NativeArray_1_t5AADFB4C72573FE3017795F15B8CBC88625A8876 *)(&V_0), (NativeArray_1_t5AADFB4C72573FE3017795F15B8CBC88625A8876 )L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 5));
		int32_t L_3 = IL2CPP_NATIVEARRAY_GET_LENGTH(((NativeArray_1_t5AADFB4C72573FE3017795F15B8CBC88625A8876 *)(NativeArray_1_t5AADFB4C72573FE3017795F15B8CBC88625A8876 *)(&___updated1))->___m_Length_1);
		int32_t L_4 = ___allocator3;
		NativeArray_1__ctor_mBA3667DFD2FA91C71C118ECA43EB22503D4FC2C8((NativeArray_1_t5AADFB4C72573FE3017795F15B8CBC88625A8876 *)(NativeArray_1_t5AADFB4C72573FE3017795F15B8CBC88625A8876 *)(&V_1), (int32_t)L_3, (int32_t)L_4, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		NativeArray_1_t5AADFB4C72573FE3017795F15B8CBC88625A8876  L_5 = ___updated1;
		NativeArray_1_CopyFrom_mC053D3A9DF2FBD4DCB31D2864F4996E9B2DA255A((NativeArray_1_t5AADFB4C72573FE3017795F15B8CBC88625A8876 *)(NativeArray_1_t5AADFB4C72573FE3017795F15B8CBC88625A8876 *)(&V_1), (NativeArray_1_t5AADFB4C72573FE3017795F15B8CBC88625A8876 )L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 5));
		int32_t L_6 = IL2CPP_NATIVEARRAY_GET_LENGTH(((NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 *)(NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 *)(&___removed2))->___m_Length_1);
		int32_t L_7 = ___allocator3;
		NativeArray_1__ctor_m49FCF39766C38037835E85C0A677B26599D0BE55((NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 *)(NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 *)(&V_2), (int32_t)L_6, (int32_t)L_7, (int32_t)1, /*hidden argument*/NativeArray_1__ctor_m49FCF39766C38037835E85C0A677B26599D0BE55_RuntimeMethod_var);
		NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  L_8 = ___removed2;
		NativeArray_1_CopyFrom_mE2C04369E83ED4726DE1121E7E05342B6A993A97((NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 *)(NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 *)(&V_2), (NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 )L_8, /*hidden argument*/NativeArray_1_CopyFrom_mE2C04369E83ED4726DE1121E7E05342B6A993A97_RuntimeMethod_var);
		NativeArray_1_t5AADFB4C72573FE3017795F15B8CBC88625A8876  L_9 = V_0;
		NativeArray_1_t5AADFB4C72573FE3017795F15B8CBC88625A8876  L_10 = V_1;
		NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  L_11 = V_2;
		TrackableChanges_1_t5C8C3FBA23E5BBC147A6991B68520A756EF54FDF  L_12;
		memset(&L_12, 0, sizeof(L_12));
		TrackableChanges_1__ctor_m5E677E3D08A7D26921EA074FB0A849F566DE5951((&L_12), (NativeArray_1_t5AADFB4C72573FE3017795F15B8CBC88625A8876 )L_9, (NativeArray_1_t5AADFB4C72573FE3017795F15B8CBC88625A8876 )L_10, (NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 )L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 7));
		return L_12;
	}
}
// System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRReferencePoint>::Dispose()
extern "C" IL2CPP_METHOD_ATTR void TrackableChanges_1_Dispose_m1C8DF8C1879FD6A22AD7474E15FB2590BB4F9631_gshared (TrackableChanges_1_t5C8C3FBA23E5BBC147A6991B68520A756EF54FDF * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TrackableChanges_1_Dispose_m1C8DF8C1879FD6A22AD7474E15FB2590BB4F9631_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = TrackableChanges_1_get_isCreated_m81592B6012FD82C68A023EC52FFAB3B4A35C6A3D((TrackableChanges_1_t5C8C3FBA23E5BBC147A6991B68520A756EF54FDF *)(TrackableChanges_1_t5C8C3FBA23E5BBC147A6991B68520A756EF54FDF *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 8));
		if (!L_0)
		{
			goto IL_0029;
		}
	}
	{
		NativeArray_1_t5AADFB4C72573FE3017795F15B8CBC88625A8876 * L_1 = (NativeArray_1_t5AADFB4C72573FE3017795F15B8CBC88625A8876 *)__this->get_address_of_m_Added_1();
		NativeArray_1_Dispose_m79A1BBDC8A7E6F17C8B48E7CB3ED027A3180039E((NativeArray_1_t5AADFB4C72573FE3017795F15B8CBC88625A8876 *)(NativeArray_1_t5AADFB4C72573FE3017795F15B8CBC88625A8876 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 9));
		NativeArray_1_t5AADFB4C72573FE3017795F15B8CBC88625A8876 * L_2 = (NativeArray_1_t5AADFB4C72573FE3017795F15B8CBC88625A8876 *)__this->get_address_of_m_Updated_2();
		NativeArray_1_Dispose_m79A1BBDC8A7E6F17C8B48E7CB3ED027A3180039E((NativeArray_1_t5AADFB4C72573FE3017795F15B8CBC88625A8876 *)(NativeArray_1_t5AADFB4C72573FE3017795F15B8CBC88625A8876 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 9));
		NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 * L_3 = (NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 *)__this->get_address_of_m_Removed_3();
		NativeArray_1_Dispose_mB8124101177299FC76251C4E7F6C98BB0E5503C7((NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 *)(NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 *)L_3, /*hidden argument*/NativeArray_1_Dispose_mB8124101177299FC76251C4E7F6C98BB0E5503C7_RuntimeMethod_var);
	}

IL_0029:
	{
		return;
	}
}
extern "C"  void TrackableChanges_1_Dispose_m1C8DF8C1879FD6A22AD7474E15FB2590BB4F9631_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	TrackableChanges_1_t5C8C3FBA23E5BBC147A6991B68520A756EF54FDF * _thisAdjusted = reinterpret_cast<TrackableChanges_1_t5C8C3FBA23E5BBC147A6991B68520A756EF54FDF *>(__this + 1);
	TrackableChanges_1_Dispose_m1C8DF8C1879FD6A22AD7474E15FB2590BB4F9631(_thisAdjusted, method);
}
// System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRReferencePoint>::.ctor(Unity.Collections.NativeArray`1<T>,Unity.Collections.NativeArray`1<T>,Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.TrackableId>)
extern "C" IL2CPP_METHOD_ATTR void TrackableChanges_1__ctor_m5E677E3D08A7D26921EA074FB0A849F566DE5951_gshared (TrackableChanges_1_t5C8C3FBA23E5BBC147A6991B68520A756EF54FDF * __this, NativeArray_1_t5AADFB4C72573FE3017795F15B8CBC88625A8876  ___added0, NativeArray_1_t5AADFB4C72573FE3017795F15B8CBC88625A8876  ___updated1, NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  ___removed2, const RuntimeMethod* method)
{
	{
		NativeArray_1_t5AADFB4C72573FE3017795F15B8CBC88625A8876  L_0 = ___added0;
		__this->set_m_Added_1(L_0);
		NativeArray_1_t5AADFB4C72573FE3017795F15B8CBC88625A8876  L_1 = ___updated1;
		__this->set_m_Updated_2(L_1);
		NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  L_2 = ___removed2;
		__this->set_m_Removed_3(L_2);
		TrackableChanges_1_set_isCreated_m3B57EAEC482D7B5D700E044B8720F89F25B13DC5((TrackableChanges_1_t5C8C3FBA23E5BBC147A6991B68520A756EF54FDF *)(TrackableChanges_1_t5C8C3FBA23E5BBC147A6991B68520A756EF54FDF *)__this, (bool)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 2));
		return;
	}
}
extern "C"  void TrackableChanges_1__ctor_m5E677E3D08A7D26921EA074FB0A849F566DE5951_AdjustorThunk (RuntimeObject * __this, NativeArray_1_t5AADFB4C72573FE3017795F15B8CBC88625A8876  ___added0, NativeArray_1_t5AADFB4C72573FE3017795F15B8CBC88625A8876  ___updated1, NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  ___removed2, const RuntimeMethod* method)
{
	TrackableChanges_1_t5C8C3FBA23E5BBC147A6991B68520A756EF54FDF * _thisAdjusted = reinterpret_cast<TrackableChanges_1_t5C8C3FBA23E5BBC147A6991B68520A756EF54FDF *>(__this + 1);
	TrackableChanges_1__ctor_m5E677E3D08A7D26921EA074FB0A849F566DE5951(_thisAdjusted, ___added0, ___updated1, ___removed2, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Unity.Collections.NativeArray`1<T> UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRTrackedImage>::get_added()
extern "C" IL2CPP_METHOD_ATTR NativeArray_1_t70F653CBE89924503B639B2438D6FF5973388AC1  TrackableChanges_1_get_added_mC35012A2E03A744ECF4A6E58D2DA1C34D85CA6ED_gshared (TrackableChanges_1_tE90399F00562881A054A3592F7AF134BA053AF4F * __this, const RuntimeMethod* method)
{
	{
		NativeArray_1_t70F653CBE89924503B639B2438D6FF5973388AC1  L_0 = (NativeArray_1_t70F653CBE89924503B639B2438D6FF5973388AC1 )__this->get_m_Added_1();
		return L_0;
	}
}
extern "C"  NativeArray_1_t70F653CBE89924503B639B2438D6FF5973388AC1  TrackableChanges_1_get_added_mC35012A2E03A744ECF4A6E58D2DA1C34D85CA6ED_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	TrackableChanges_1_tE90399F00562881A054A3592F7AF134BA053AF4F * _thisAdjusted = reinterpret_cast<TrackableChanges_1_tE90399F00562881A054A3592F7AF134BA053AF4F *>(__this + 1);
	return TrackableChanges_1_get_added_mC35012A2E03A744ECF4A6E58D2DA1C34D85CA6ED(_thisAdjusted, method);
}
// Unity.Collections.NativeArray`1<T> UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRTrackedImage>::get_updated()
extern "C" IL2CPP_METHOD_ATTR NativeArray_1_t70F653CBE89924503B639B2438D6FF5973388AC1  TrackableChanges_1_get_updated_mF1CCDAA99909A241F138D24B4175AD6806E65323_gshared (TrackableChanges_1_tE90399F00562881A054A3592F7AF134BA053AF4F * __this, const RuntimeMethod* method)
{
	{
		NativeArray_1_t70F653CBE89924503B639B2438D6FF5973388AC1  L_0 = (NativeArray_1_t70F653CBE89924503B639B2438D6FF5973388AC1 )__this->get_m_Updated_2();
		return L_0;
	}
}
extern "C"  NativeArray_1_t70F653CBE89924503B639B2438D6FF5973388AC1  TrackableChanges_1_get_updated_mF1CCDAA99909A241F138D24B4175AD6806E65323_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	TrackableChanges_1_tE90399F00562881A054A3592F7AF134BA053AF4F * _thisAdjusted = reinterpret_cast<TrackableChanges_1_tE90399F00562881A054A3592F7AF134BA053AF4F *>(__this + 1);
	return TrackableChanges_1_get_updated_mF1CCDAA99909A241F138D24B4175AD6806E65323(_thisAdjusted, method);
}
// Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.TrackableId> UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRTrackedImage>::get_removed()
extern "C" IL2CPP_METHOD_ATTR NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  TrackableChanges_1_get_removed_m7CF311E706E890B2CC9281BC392849E4DE880ECA_gshared (TrackableChanges_1_tE90399F00562881A054A3592F7AF134BA053AF4F * __this, const RuntimeMethod* method)
{
	{
		NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  L_0 = (NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 )__this->get_m_Removed_3();
		return L_0;
	}
}
extern "C"  NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  TrackableChanges_1_get_removed_m7CF311E706E890B2CC9281BC392849E4DE880ECA_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	TrackableChanges_1_tE90399F00562881A054A3592F7AF134BA053AF4F * _thisAdjusted = reinterpret_cast<TrackableChanges_1_tE90399F00562881A054A3592F7AF134BA053AF4F *>(__this + 1);
	return TrackableChanges_1_get_removed_m7CF311E706E890B2CC9281BC392849E4DE880ECA(_thisAdjusted, method);
}
// System.Boolean UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRTrackedImage>::get_isCreated()
extern "C" IL2CPP_METHOD_ATTR bool TrackableChanges_1_get_isCreated_mEEE0707FBE540E3961450502B2C2771AA02ABF83_gshared (TrackableChanges_1_tE90399F00562881A054A3592F7AF134BA053AF4F * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = (bool)__this->get_U3CisCreatedU3Ek__BackingField_0();
		return L_0;
	}
}
extern "C"  bool TrackableChanges_1_get_isCreated_mEEE0707FBE540E3961450502B2C2771AA02ABF83_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	TrackableChanges_1_tE90399F00562881A054A3592F7AF134BA053AF4F * _thisAdjusted = reinterpret_cast<TrackableChanges_1_tE90399F00562881A054A3592F7AF134BA053AF4F *>(__this + 1);
	return TrackableChanges_1_get_isCreated_mEEE0707FBE540E3961450502B2C2771AA02ABF83(_thisAdjusted, method);
}
// System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRTrackedImage>::set_isCreated(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void TrackableChanges_1_set_isCreated_m360183150F0DE3BAE45AD1C6C942146BF81F51D4_gshared (TrackableChanges_1_tE90399F00562881A054A3592F7AF134BA053AF4F * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3CisCreatedU3Ek__BackingField_0(L_0);
		return;
	}
}
extern "C"  void TrackableChanges_1_set_isCreated_m360183150F0DE3BAE45AD1C6C942146BF81F51D4_AdjustorThunk (RuntimeObject * __this, bool ___value0, const RuntimeMethod* method)
{
	TrackableChanges_1_tE90399F00562881A054A3592F7AF134BA053AF4F * _thisAdjusted = reinterpret_cast<TrackableChanges_1_tE90399F00562881A054A3592F7AF134BA053AF4F *>(__this + 1);
	TrackableChanges_1_set_isCreated_m360183150F0DE3BAE45AD1C6C942146BF81F51D4(_thisAdjusted, ___value0, method);
}
// System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRTrackedImage>::.ctor(System.Int32,System.Int32,System.Int32,Unity.Collections.Allocator)
extern "C" IL2CPP_METHOD_ATTR void TrackableChanges_1__ctor_m88DBEB1615B50F97CBE740AA5502C0DFA42D7641_gshared (TrackableChanges_1_tE90399F00562881A054A3592F7AF134BA053AF4F * __this, int32_t ___addedCount0, int32_t ___updatedCount1, int32_t ___removedCount2, int32_t ___allocator3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TrackableChanges_1__ctor_m88DBEB1615B50F97CBE740AA5502C0DFA42D7641_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___addedCount0;
		int32_t L_1 = ___allocator3;
		NativeArray_1_t70F653CBE89924503B639B2438D6FF5973388AC1  L_2;
		memset(&L_2, 0, sizeof(L_2));
		NativeArray_1__ctor_m192CF93170B36E61ACE804945C3B18481D07B1C5((&L_2), (int32_t)L_0, (int32_t)L_1, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		__this->set_m_Added_1(L_2);
		int32_t L_3 = ___updatedCount1;
		int32_t L_4 = ___allocator3;
		NativeArray_1_t70F653CBE89924503B639B2438D6FF5973388AC1  L_5;
		memset(&L_5, 0, sizeof(L_5));
		NativeArray_1__ctor_m192CF93170B36E61ACE804945C3B18481D07B1C5((&L_5), (int32_t)L_3, (int32_t)L_4, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		__this->set_m_Updated_2(L_5);
		int32_t L_6 = ___removedCount2;
		int32_t L_7 = ___allocator3;
		NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  L_8;
		memset(&L_8, 0, sizeof(L_8));
		NativeArray_1__ctor_m49FCF39766C38037835E85C0A677B26599D0BE55((&L_8), (int32_t)L_6, (int32_t)L_7, (int32_t)1, /*hidden argument*/NativeArray_1__ctor_m49FCF39766C38037835E85C0A677B26599D0BE55_RuntimeMethod_var);
		__this->set_m_Removed_3(L_8);
		TrackableChanges_1_set_isCreated_m360183150F0DE3BAE45AD1C6C942146BF81F51D4((TrackableChanges_1_tE90399F00562881A054A3592F7AF134BA053AF4F *)(TrackableChanges_1_tE90399F00562881A054A3592F7AF134BA053AF4F *)__this, (bool)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 2));
		return;
	}
}
extern "C"  void TrackableChanges_1__ctor_m88DBEB1615B50F97CBE740AA5502C0DFA42D7641_AdjustorThunk (RuntimeObject * __this, int32_t ___addedCount0, int32_t ___updatedCount1, int32_t ___removedCount2, int32_t ___allocator3, const RuntimeMethod* method)
{
	TrackableChanges_1_tE90399F00562881A054A3592F7AF134BA053AF4F * _thisAdjusted = reinterpret_cast<TrackableChanges_1_tE90399F00562881A054A3592F7AF134BA053AF4F *>(__this + 1);
	TrackableChanges_1__ctor_m88DBEB1615B50F97CBE740AA5502C0DFA42D7641(_thisAdjusted, ___addedCount0, ___updatedCount1, ___removedCount2, ___allocator3, method);
}
// System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRTrackedImage>::.ctor(System.Void*,System.Int32,System.Void*,System.Int32,System.Void*,System.Int32,T,System.Int32,Unity.Collections.Allocator)
extern "C" IL2CPP_METHOD_ATTR void TrackableChanges_1__ctor_m3477ECC2A1A2FF3D7287DF3B5BDFC7F272FDB3EE_gshared (TrackableChanges_1_tE90399F00562881A054A3592F7AF134BA053AF4F * __this, void* ___addedPtr0, int32_t ___addedCount1, void* ___updatedPtr2, int32_t ___updatedCount3, void* ___removedPtr4, int32_t ___removedCount5, XRTrackedImage_t178EACF5BFA4228DF4EB1899685C533F3F296AA8  ___defaultT6, int32_t ___stride7, int32_t ___allocator8, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TrackableChanges_1__ctor_m3477ECC2A1A2FF3D7287DF3B5BDFC7F272FDB3EE_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		XRTrackedImage_t178EACF5BFA4228DF4EB1899685C533F3F296AA8  L_0 = ___defaultT6;
		void* L_1 = ___addedPtr0;
		int32_t L_2 = ___stride7;
		int32_t L_3 = ___addedCount1;
		int32_t L_4 = ___allocator8;
		NativeArray_1_t70F653CBE89924503B639B2438D6FF5973388AC1  L_5 = ((  NativeArray_1_t70F653CBE89924503B639B2438D6FF5973388AC1  (*) (XRTrackedImage_t178EACF5BFA4228DF4EB1899685C533F3F296AA8 , void*, int32_t, int32_t, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3)->methodPointer)((XRTrackedImage_t178EACF5BFA4228DF4EB1899685C533F3F296AA8 )L_0, (void*)(void*)L_1, (int32_t)L_2, (int32_t)L_3, (int32_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3));
		__this->set_m_Added_1(L_5);
		XRTrackedImage_t178EACF5BFA4228DF4EB1899685C533F3F296AA8  L_6 = ___defaultT6;
		void* L_7 = ___updatedPtr2;
		int32_t L_8 = ___stride7;
		int32_t L_9 = ___updatedCount3;
		int32_t L_10 = ___allocator8;
		NativeArray_1_t70F653CBE89924503B639B2438D6FF5973388AC1  L_11 = ((  NativeArray_1_t70F653CBE89924503B639B2438D6FF5973388AC1  (*) (XRTrackedImage_t178EACF5BFA4228DF4EB1899685C533F3F296AA8 , void*, int32_t, int32_t, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3)->methodPointer)((XRTrackedImage_t178EACF5BFA4228DF4EB1899685C533F3F296AA8 )L_6, (void*)(void*)L_7, (int32_t)L_8, (int32_t)L_9, (int32_t)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3));
		__this->set_m_Updated_2(L_11);
		int32_t L_12 = ___removedCount5;
		int32_t L_13 = ___allocator8;
		NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  L_14;
		memset(&L_14, 0, sizeof(L_14));
		NativeArray_1__ctor_m49FCF39766C38037835E85C0A677B26599D0BE55((&L_14), (int32_t)L_12, (int32_t)L_13, (int32_t)1, /*hidden argument*/NativeArray_1__ctor_m49FCF39766C38037835E85C0A677B26599D0BE55_RuntimeMethod_var);
		__this->set_m_Removed_3(L_14);
		int32_t L_15 = ___removedCount5;
		if ((((int32_t)L_15) <= ((int32_t)0)))
		{
			goto IL_0051;
		}
	}
	{
		NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 * L_16 = (NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 *)__this->get_address_of_m_Removed_3();
		void* L_17 = ___removedPtr4;
		int32_t L_18 = ___removedCount5;
		NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  L_19 = NativeArrayUnsafeUtility_ConvertExistingDataToNativeArray_TisTrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47_m8D7EEF45CE20EB27369597A33879F42E459F2D59((void*)(void*)L_17, (int32_t)L_18, (int32_t)1, /*hidden argument*/NativeArrayUnsafeUtility_ConvertExistingDataToNativeArray_TisTrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47_m8D7EEF45CE20EB27369597A33879F42E459F2D59_RuntimeMethod_var);
		NativeArray_1_CopyFrom_mE2C04369E83ED4726DE1121E7E05342B6A993A97((NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 *)(NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 *)L_16, (NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 )L_19, /*hidden argument*/NativeArray_1_CopyFrom_mE2C04369E83ED4726DE1121E7E05342B6A993A97_RuntimeMethod_var);
	}

IL_0051:
	{
		TrackableChanges_1_set_isCreated_m360183150F0DE3BAE45AD1C6C942146BF81F51D4((TrackableChanges_1_tE90399F00562881A054A3592F7AF134BA053AF4F *)(TrackableChanges_1_tE90399F00562881A054A3592F7AF134BA053AF4F *)__this, (bool)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 2));
		return;
	}
}
extern "C"  void TrackableChanges_1__ctor_m3477ECC2A1A2FF3D7287DF3B5BDFC7F272FDB3EE_AdjustorThunk (RuntimeObject * __this, void* ___addedPtr0, int32_t ___addedCount1, void* ___updatedPtr2, int32_t ___updatedCount3, void* ___removedPtr4, int32_t ___removedCount5, XRTrackedImage_t178EACF5BFA4228DF4EB1899685C533F3F296AA8  ___defaultT6, int32_t ___stride7, int32_t ___allocator8, const RuntimeMethod* method)
{
	TrackableChanges_1_tE90399F00562881A054A3592F7AF134BA053AF4F * _thisAdjusted = reinterpret_cast<TrackableChanges_1_tE90399F00562881A054A3592F7AF134BA053AF4F *>(__this + 1);
	TrackableChanges_1__ctor_m3477ECC2A1A2FF3D7287DF3B5BDFC7F272FDB3EE(_thisAdjusted, ___addedPtr0, ___addedCount1, ___updatedPtr2, ___updatedCount3, ___removedPtr4, ___removedCount5, ___defaultT6, ___stride7, ___allocator8, method);
}
// UnityEngine.XR.ARSubsystems.TrackableChanges`1<T> UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRTrackedImage>::CopyFrom(Unity.Collections.NativeArray`1<T>,Unity.Collections.NativeArray`1<T>,Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.TrackableId>,Unity.Collections.Allocator)
extern "C" IL2CPP_METHOD_ATTR TrackableChanges_1_tE90399F00562881A054A3592F7AF134BA053AF4F  TrackableChanges_1_CopyFrom_m1EAE1F86BAC4E32BC26A41ED26E154AE34AB853B_gshared (NativeArray_1_t70F653CBE89924503B639B2438D6FF5973388AC1  ___added0, NativeArray_1_t70F653CBE89924503B639B2438D6FF5973388AC1  ___updated1, NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  ___removed2, int32_t ___allocator3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TrackableChanges_1_CopyFrom_m1EAE1F86BAC4E32BC26A41ED26E154AE34AB853B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	NativeArray_1_t70F653CBE89924503B639B2438D6FF5973388AC1  V_0;
	memset(&V_0, 0, sizeof(V_0));
	NativeArray_1_t70F653CBE89924503B639B2438D6FF5973388AC1  V_1;
	memset(&V_1, 0, sizeof(V_1));
	NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		int32_t L_0 = IL2CPP_NATIVEARRAY_GET_LENGTH(((NativeArray_1_t70F653CBE89924503B639B2438D6FF5973388AC1 *)(NativeArray_1_t70F653CBE89924503B639B2438D6FF5973388AC1 *)(&___added0))->___m_Length_1);
		int32_t L_1 = ___allocator3;
		NativeArray_1__ctor_m192CF93170B36E61ACE804945C3B18481D07B1C5((NativeArray_1_t70F653CBE89924503B639B2438D6FF5973388AC1 *)(NativeArray_1_t70F653CBE89924503B639B2438D6FF5973388AC1 *)(&V_0), (int32_t)L_0, (int32_t)L_1, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		NativeArray_1_t70F653CBE89924503B639B2438D6FF5973388AC1  L_2 = ___added0;
		NativeArray_1_CopyFrom_mC0B092730DE59B585688BCCF8FA9099731798DCD((NativeArray_1_t70F653CBE89924503B639B2438D6FF5973388AC1 *)(NativeArray_1_t70F653CBE89924503B639B2438D6FF5973388AC1 *)(&V_0), (NativeArray_1_t70F653CBE89924503B639B2438D6FF5973388AC1 )L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 5));
		int32_t L_3 = IL2CPP_NATIVEARRAY_GET_LENGTH(((NativeArray_1_t70F653CBE89924503B639B2438D6FF5973388AC1 *)(NativeArray_1_t70F653CBE89924503B639B2438D6FF5973388AC1 *)(&___updated1))->___m_Length_1);
		int32_t L_4 = ___allocator3;
		NativeArray_1__ctor_m192CF93170B36E61ACE804945C3B18481D07B1C5((NativeArray_1_t70F653CBE89924503B639B2438D6FF5973388AC1 *)(NativeArray_1_t70F653CBE89924503B639B2438D6FF5973388AC1 *)(&V_1), (int32_t)L_3, (int32_t)L_4, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		NativeArray_1_t70F653CBE89924503B639B2438D6FF5973388AC1  L_5 = ___updated1;
		NativeArray_1_CopyFrom_mC0B092730DE59B585688BCCF8FA9099731798DCD((NativeArray_1_t70F653CBE89924503B639B2438D6FF5973388AC1 *)(NativeArray_1_t70F653CBE89924503B639B2438D6FF5973388AC1 *)(&V_1), (NativeArray_1_t70F653CBE89924503B639B2438D6FF5973388AC1 )L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 5));
		int32_t L_6 = IL2CPP_NATIVEARRAY_GET_LENGTH(((NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 *)(NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 *)(&___removed2))->___m_Length_1);
		int32_t L_7 = ___allocator3;
		NativeArray_1__ctor_m49FCF39766C38037835E85C0A677B26599D0BE55((NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 *)(NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 *)(&V_2), (int32_t)L_6, (int32_t)L_7, (int32_t)1, /*hidden argument*/NativeArray_1__ctor_m49FCF39766C38037835E85C0A677B26599D0BE55_RuntimeMethod_var);
		NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  L_8 = ___removed2;
		NativeArray_1_CopyFrom_mE2C04369E83ED4726DE1121E7E05342B6A993A97((NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 *)(NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 *)(&V_2), (NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 )L_8, /*hidden argument*/NativeArray_1_CopyFrom_mE2C04369E83ED4726DE1121E7E05342B6A993A97_RuntimeMethod_var);
		NativeArray_1_t70F653CBE89924503B639B2438D6FF5973388AC1  L_9 = V_0;
		NativeArray_1_t70F653CBE89924503B639B2438D6FF5973388AC1  L_10 = V_1;
		NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  L_11 = V_2;
		TrackableChanges_1_tE90399F00562881A054A3592F7AF134BA053AF4F  L_12;
		memset(&L_12, 0, sizeof(L_12));
		TrackableChanges_1__ctor_mC96D4347CE6234FD5E51ECDA265125C19D70BF0C((&L_12), (NativeArray_1_t70F653CBE89924503B639B2438D6FF5973388AC1 )L_9, (NativeArray_1_t70F653CBE89924503B639B2438D6FF5973388AC1 )L_10, (NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 )L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 7));
		return L_12;
	}
}
// System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRTrackedImage>::Dispose()
extern "C" IL2CPP_METHOD_ATTR void TrackableChanges_1_Dispose_m5A25C5A0DF692BA62C0C9E9DD458E28462000702_gshared (TrackableChanges_1_tE90399F00562881A054A3592F7AF134BA053AF4F * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TrackableChanges_1_Dispose_m5A25C5A0DF692BA62C0C9E9DD458E28462000702_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = TrackableChanges_1_get_isCreated_mEEE0707FBE540E3961450502B2C2771AA02ABF83((TrackableChanges_1_tE90399F00562881A054A3592F7AF134BA053AF4F *)(TrackableChanges_1_tE90399F00562881A054A3592F7AF134BA053AF4F *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 8));
		if (!L_0)
		{
			goto IL_0029;
		}
	}
	{
		NativeArray_1_t70F653CBE89924503B639B2438D6FF5973388AC1 * L_1 = (NativeArray_1_t70F653CBE89924503B639B2438D6FF5973388AC1 *)__this->get_address_of_m_Added_1();
		NativeArray_1_Dispose_m8645D533921C8C5BE22919B57D259A4240AB379A((NativeArray_1_t70F653CBE89924503B639B2438D6FF5973388AC1 *)(NativeArray_1_t70F653CBE89924503B639B2438D6FF5973388AC1 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 9));
		NativeArray_1_t70F653CBE89924503B639B2438D6FF5973388AC1 * L_2 = (NativeArray_1_t70F653CBE89924503B639B2438D6FF5973388AC1 *)__this->get_address_of_m_Updated_2();
		NativeArray_1_Dispose_m8645D533921C8C5BE22919B57D259A4240AB379A((NativeArray_1_t70F653CBE89924503B639B2438D6FF5973388AC1 *)(NativeArray_1_t70F653CBE89924503B639B2438D6FF5973388AC1 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 9));
		NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 * L_3 = (NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 *)__this->get_address_of_m_Removed_3();
		NativeArray_1_Dispose_mB8124101177299FC76251C4E7F6C98BB0E5503C7((NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 *)(NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 *)L_3, /*hidden argument*/NativeArray_1_Dispose_mB8124101177299FC76251C4E7F6C98BB0E5503C7_RuntimeMethod_var);
	}

IL_0029:
	{
		return;
	}
}
extern "C"  void TrackableChanges_1_Dispose_m5A25C5A0DF692BA62C0C9E9DD458E28462000702_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	TrackableChanges_1_tE90399F00562881A054A3592F7AF134BA053AF4F * _thisAdjusted = reinterpret_cast<TrackableChanges_1_tE90399F00562881A054A3592F7AF134BA053AF4F *>(__this + 1);
	TrackableChanges_1_Dispose_m5A25C5A0DF692BA62C0C9E9DD458E28462000702(_thisAdjusted, method);
}
// System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRTrackedImage>::.ctor(Unity.Collections.NativeArray`1<T>,Unity.Collections.NativeArray`1<T>,Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.TrackableId>)
extern "C" IL2CPP_METHOD_ATTR void TrackableChanges_1__ctor_mC96D4347CE6234FD5E51ECDA265125C19D70BF0C_gshared (TrackableChanges_1_tE90399F00562881A054A3592F7AF134BA053AF4F * __this, NativeArray_1_t70F653CBE89924503B639B2438D6FF5973388AC1  ___added0, NativeArray_1_t70F653CBE89924503B639B2438D6FF5973388AC1  ___updated1, NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  ___removed2, const RuntimeMethod* method)
{
	{
		NativeArray_1_t70F653CBE89924503B639B2438D6FF5973388AC1  L_0 = ___added0;
		__this->set_m_Added_1(L_0);
		NativeArray_1_t70F653CBE89924503B639B2438D6FF5973388AC1  L_1 = ___updated1;
		__this->set_m_Updated_2(L_1);
		NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  L_2 = ___removed2;
		__this->set_m_Removed_3(L_2);
		TrackableChanges_1_set_isCreated_m360183150F0DE3BAE45AD1C6C942146BF81F51D4((TrackableChanges_1_tE90399F00562881A054A3592F7AF134BA053AF4F *)(TrackableChanges_1_tE90399F00562881A054A3592F7AF134BA053AF4F *)__this, (bool)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 2));
		return;
	}
}
extern "C"  void TrackableChanges_1__ctor_mC96D4347CE6234FD5E51ECDA265125C19D70BF0C_AdjustorThunk (RuntimeObject * __this, NativeArray_1_t70F653CBE89924503B639B2438D6FF5973388AC1  ___added0, NativeArray_1_t70F653CBE89924503B639B2438D6FF5973388AC1  ___updated1, NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  ___removed2, const RuntimeMethod* method)
{
	TrackableChanges_1_tE90399F00562881A054A3592F7AF134BA053AF4F * _thisAdjusted = reinterpret_cast<TrackableChanges_1_tE90399F00562881A054A3592F7AF134BA053AF4F *>(__this + 1);
	TrackableChanges_1__ctor_mC96D4347CE6234FD5E51ECDA265125C19D70BF0C(_thisAdjusted, ___added0, ___updated1, ___removed2, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Unity.Collections.NativeArray`1<T> UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRTrackedObject>::get_added()
extern "C" IL2CPP_METHOD_ATTR NativeArray_1_t738892D030F76D319525B8C000F49D41505DD529  TrackableChanges_1_get_added_m6B4AFC77B682299AFAB977EDEAF164E6B63E3670_gshared (TrackableChanges_1_t03A664B58E58E32B5EC1F7CB7133E87F804EE874 * __this, const RuntimeMethod* method)
{
	{
		NativeArray_1_t738892D030F76D319525B8C000F49D41505DD529  L_0 = (NativeArray_1_t738892D030F76D319525B8C000F49D41505DD529 )__this->get_m_Added_1();
		return L_0;
	}
}
extern "C"  NativeArray_1_t738892D030F76D319525B8C000F49D41505DD529  TrackableChanges_1_get_added_m6B4AFC77B682299AFAB977EDEAF164E6B63E3670_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	TrackableChanges_1_t03A664B58E58E32B5EC1F7CB7133E87F804EE874 * _thisAdjusted = reinterpret_cast<TrackableChanges_1_t03A664B58E58E32B5EC1F7CB7133E87F804EE874 *>(__this + 1);
	return TrackableChanges_1_get_added_m6B4AFC77B682299AFAB977EDEAF164E6B63E3670(_thisAdjusted, method);
}
// Unity.Collections.NativeArray`1<T> UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRTrackedObject>::get_updated()
extern "C" IL2CPP_METHOD_ATTR NativeArray_1_t738892D030F76D319525B8C000F49D41505DD529  TrackableChanges_1_get_updated_mDDB738464599270A745A15C57FC941EEBEC00700_gshared (TrackableChanges_1_t03A664B58E58E32B5EC1F7CB7133E87F804EE874 * __this, const RuntimeMethod* method)
{
	{
		NativeArray_1_t738892D030F76D319525B8C000F49D41505DD529  L_0 = (NativeArray_1_t738892D030F76D319525B8C000F49D41505DD529 )__this->get_m_Updated_2();
		return L_0;
	}
}
extern "C"  NativeArray_1_t738892D030F76D319525B8C000F49D41505DD529  TrackableChanges_1_get_updated_mDDB738464599270A745A15C57FC941EEBEC00700_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	TrackableChanges_1_t03A664B58E58E32B5EC1F7CB7133E87F804EE874 * _thisAdjusted = reinterpret_cast<TrackableChanges_1_t03A664B58E58E32B5EC1F7CB7133E87F804EE874 *>(__this + 1);
	return TrackableChanges_1_get_updated_mDDB738464599270A745A15C57FC941EEBEC00700(_thisAdjusted, method);
}
// Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.TrackableId> UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRTrackedObject>::get_removed()
extern "C" IL2CPP_METHOD_ATTR NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  TrackableChanges_1_get_removed_m53E0E551E0ACC552E62D8BA2A0A234D72CAB6C74_gshared (TrackableChanges_1_t03A664B58E58E32B5EC1F7CB7133E87F804EE874 * __this, const RuntimeMethod* method)
{
	{
		NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  L_0 = (NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 )__this->get_m_Removed_3();
		return L_0;
	}
}
extern "C"  NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  TrackableChanges_1_get_removed_m53E0E551E0ACC552E62D8BA2A0A234D72CAB6C74_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	TrackableChanges_1_t03A664B58E58E32B5EC1F7CB7133E87F804EE874 * _thisAdjusted = reinterpret_cast<TrackableChanges_1_t03A664B58E58E32B5EC1F7CB7133E87F804EE874 *>(__this + 1);
	return TrackableChanges_1_get_removed_m53E0E551E0ACC552E62D8BA2A0A234D72CAB6C74(_thisAdjusted, method);
}
// System.Boolean UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRTrackedObject>::get_isCreated()
extern "C" IL2CPP_METHOD_ATTR bool TrackableChanges_1_get_isCreated_mBFB9748C1EA377BE047EC13F736E0C8C207E441C_gshared (TrackableChanges_1_t03A664B58E58E32B5EC1F7CB7133E87F804EE874 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = (bool)__this->get_U3CisCreatedU3Ek__BackingField_0();
		return L_0;
	}
}
extern "C"  bool TrackableChanges_1_get_isCreated_mBFB9748C1EA377BE047EC13F736E0C8C207E441C_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	TrackableChanges_1_t03A664B58E58E32B5EC1F7CB7133E87F804EE874 * _thisAdjusted = reinterpret_cast<TrackableChanges_1_t03A664B58E58E32B5EC1F7CB7133E87F804EE874 *>(__this + 1);
	return TrackableChanges_1_get_isCreated_mBFB9748C1EA377BE047EC13F736E0C8C207E441C(_thisAdjusted, method);
}
// System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRTrackedObject>::set_isCreated(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void TrackableChanges_1_set_isCreated_m085A94096B2D33690423DF6A6777D7586B582D14_gshared (TrackableChanges_1_t03A664B58E58E32B5EC1F7CB7133E87F804EE874 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3CisCreatedU3Ek__BackingField_0(L_0);
		return;
	}
}
extern "C"  void TrackableChanges_1_set_isCreated_m085A94096B2D33690423DF6A6777D7586B582D14_AdjustorThunk (RuntimeObject * __this, bool ___value0, const RuntimeMethod* method)
{
	TrackableChanges_1_t03A664B58E58E32B5EC1F7CB7133E87F804EE874 * _thisAdjusted = reinterpret_cast<TrackableChanges_1_t03A664B58E58E32B5EC1F7CB7133E87F804EE874 *>(__this + 1);
	TrackableChanges_1_set_isCreated_m085A94096B2D33690423DF6A6777D7586B582D14(_thisAdjusted, ___value0, method);
}
// System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRTrackedObject>::.ctor(System.Int32,System.Int32,System.Int32,Unity.Collections.Allocator)
extern "C" IL2CPP_METHOD_ATTR void TrackableChanges_1__ctor_mBA293905C9592E097DAC0E30B77D458C7D7E23FD_gshared (TrackableChanges_1_t03A664B58E58E32B5EC1F7CB7133E87F804EE874 * __this, int32_t ___addedCount0, int32_t ___updatedCount1, int32_t ___removedCount2, int32_t ___allocator3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TrackableChanges_1__ctor_mBA293905C9592E097DAC0E30B77D458C7D7E23FD_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___addedCount0;
		int32_t L_1 = ___allocator3;
		NativeArray_1_t738892D030F76D319525B8C000F49D41505DD529  L_2;
		memset(&L_2, 0, sizeof(L_2));
		NativeArray_1__ctor_m16425BC62E9AC48C88E58870411D0D63CCE985A9((&L_2), (int32_t)L_0, (int32_t)L_1, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		__this->set_m_Added_1(L_2);
		int32_t L_3 = ___updatedCount1;
		int32_t L_4 = ___allocator3;
		NativeArray_1_t738892D030F76D319525B8C000F49D41505DD529  L_5;
		memset(&L_5, 0, sizeof(L_5));
		NativeArray_1__ctor_m16425BC62E9AC48C88E58870411D0D63CCE985A9((&L_5), (int32_t)L_3, (int32_t)L_4, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		__this->set_m_Updated_2(L_5);
		int32_t L_6 = ___removedCount2;
		int32_t L_7 = ___allocator3;
		NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  L_8;
		memset(&L_8, 0, sizeof(L_8));
		NativeArray_1__ctor_m49FCF39766C38037835E85C0A677B26599D0BE55((&L_8), (int32_t)L_6, (int32_t)L_7, (int32_t)1, /*hidden argument*/NativeArray_1__ctor_m49FCF39766C38037835E85C0A677B26599D0BE55_RuntimeMethod_var);
		__this->set_m_Removed_3(L_8);
		TrackableChanges_1_set_isCreated_m085A94096B2D33690423DF6A6777D7586B582D14((TrackableChanges_1_t03A664B58E58E32B5EC1F7CB7133E87F804EE874 *)(TrackableChanges_1_t03A664B58E58E32B5EC1F7CB7133E87F804EE874 *)__this, (bool)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 2));
		return;
	}
}
extern "C"  void TrackableChanges_1__ctor_mBA293905C9592E097DAC0E30B77D458C7D7E23FD_AdjustorThunk (RuntimeObject * __this, int32_t ___addedCount0, int32_t ___updatedCount1, int32_t ___removedCount2, int32_t ___allocator3, const RuntimeMethod* method)
{
	TrackableChanges_1_t03A664B58E58E32B5EC1F7CB7133E87F804EE874 * _thisAdjusted = reinterpret_cast<TrackableChanges_1_t03A664B58E58E32B5EC1F7CB7133E87F804EE874 *>(__this + 1);
	TrackableChanges_1__ctor_mBA293905C9592E097DAC0E30B77D458C7D7E23FD(_thisAdjusted, ___addedCount0, ___updatedCount1, ___removedCount2, ___allocator3, method);
}
// System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRTrackedObject>::.ctor(System.Void*,System.Int32,System.Void*,System.Int32,System.Void*,System.Int32,T,System.Int32,Unity.Collections.Allocator)
extern "C" IL2CPP_METHOD_ATTR void TrackableChanges_1__ctor_mD3BF9E8193EF890B4977549BF6374BE3D5165CBF_gshared (TrackableChanges_1_t03A664B58E58E32B5EC1F7CB7133E87F804EE874 * __this, void* ___addedPtr0, int32_t ___addedCount1, void* ___updatedPtr2, int32_t ___updatedCount3, void* ___removedPtr4, int32_t ___removedCount5, XRTrackedObject_tF08D3523D17E214EC3D7DBE8ED5BFE220BAAE260  ___defaultT6, int32_t ___stride7, int32_t ___allocator8, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TrackableChanges_1__ctor_mD3BF9E8193EF890B4977549BF6374BE3D5165CBF_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		XRTrackedObject_tF08D3523D17E214EC3D7DBE8ED5BFE220BAAE260  L_0 = ___defaultT6;
		void* L_1 = ___addedPtr0;
		int32_t L_2 = ___stride7;
		int32_t L_3 = ___addedCount1;
		int32_t L_4 = ___allocator8;
		NativeArray_1_t738892D030F76D319525B8C000F49D41505DD529  L_5 = ((  NativeArray_1_t738892D030F76D319525B8C000F49D41505DD529  (*) (XRTrackedObject_tF08D3523D17E214EC3D7DBE8ED5BFE220BAAE260 , void*, int32_t, int32_t, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3)->methodPointer)((XRTrackedObject_tF08D3523D17E214EC3D7DBE8ED5BFE220BAAE260 )L_0, (void*)(void*)L_1, (int32_t)L_2, (int32_t)L_3, (int32_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3));
		__this->set_m_Added_1(L_5);
		XRTrackedObject_tF08D3523D17E214EC3D7DBE8ED5BFE220BAAE260  L_6 = ___defaultT6;
		void* L_7 = ___updatedPtr2;
		int32_t L_8 = ___stride7;
		int32_t L_9 = ___updatedCount3;
		int32_t L_10 = ___allocator8;
		NativeArray_1_t738892D030F76D319525B8C000F49D41505DD529  L_11 = ((  NativeArray_1_t738892D030F76D319525B8C000F49D41505DD529  (*) (XRTrackedObject_tF08D3523D17E214EC3D7DBE8ED5BFE220BAAE260 , void*, int32_t, int32_t, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3)->methodPointer)((XRTrackedObject_tF08D3523D17E214EC3D7DBE8ED5BFE220BAAE260 )L_6, (void*)(void*)L_7, (int32_t)L_8, (int32_t)L_9, (int32_t)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3));
		__this->set_m_Updated_2(L_11);
		int32_t L_12 = ___removedCount5;
		int32_t L_13 = ___allocator8;
		NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  L_14;
		memset(&L_14, 0, sizeof(L_14));
		NativeArray_1__ctor_m49FCF39766C38037835E85C0A677B26599D0BE55((&L_14), (int32_t)L_12, (int32_t)L_13, (int32_t)1, /*hidden argument*/NativeArray_1__ctor_m49FCF39766C38037835E85C0A677B26599D0BE55_RuntimeMethod_var);
		__this->set_m_Removed_3(L_14);
		int32_t L_15 = ___removedCount5;
		if ((((int32_t)L_15) <= ((int32_t)0)))
		{
			goto IL_0051;
		}
	}
	{
		NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 * L_16 = (NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 *)__this->get_address_of_m_Removed_3();
		void* L_17 = ___removedPtr4;
		int32_t L_18 = ___removedCount5;
		NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  L_19 = NativeArrayUnsafeUtility_ConvertExistingDataToNativeArray_TisTrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47_m8D7EEF45CE20EB27369597A33879F42E459F2D59((void*)(void*)L_17, (int32_t)L_18, (int32_t)1, /*hidden argument*/NativeArrayUnsafeUtility_ConvertExistingDataToNativeArray_TisTrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47_m8D7EEF45CE20EB27369597A33879F42E459F2D59_RuntimeMethod_var);
		NativeArray_1_CopyFrom_mE2C04369E83ED4726DE1121E7E05342B6A993A97((NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 *)(NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 *)L_16, (NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 )L_19, /*hidden argument*/NativeArray_1_CopyFrom_mE2C04369E83ED4726DE1121E7E05342B6A993A97_RuntimeMethod_var);
	}

IL_0051:
	{
		TrackableChanges_1_set_isCreated_m085A94096B2D33690423DF6A6777D7586B582D14((TrackableChanges_1_t03A664B58E58E32B5EC1F7CB7133E87F804EE874 *)(TrackableChanges_1_t03A664B58E58E32B5EC1F7CB7133E87F804EE874 *)__this, (bool)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 2));
		return;
	}
}
extern "C"  void TrackableChanges_1__ctor_mD3BF9E8193EF890B4977549BF6374BE3D5165CBF_AdjustorThunk (RuntimeObject * __this, void* ___addedPtr0, int32_t ___addedCount1, void* ___updatedPtr2, int32_t ___updatedCount3, void* ___removedPtr4, int32_t ___removedCount5, XRTrackedObject_tF08D3523D17E214EC3D7DBE8ED5BFE220BAAE260  ___defaultT6, int32_t ___stride7, int32_t ___allocator8, const RuntimeMethod* method)
{
	TrackableChanges_1_t03A664B58E58E32B5EC1F7CB7133E87F804EE874 * _thisAdjusted = reinterpret_cast<TrackableChanges_1_t03A664B58E58E32B5EC1F7CB7133E87F804EE874 *>(__this + 1);
	TrackableChanges_1__ctor_mD3BF9E8193EF890B4977549BF6374BE3D5165CBF(_thisAdjusted, ___addedPtr0, ___addedCount1, ___updatedPtr2, ___updatedCount3, ___removedPtr4, ___removedCount5, ___defaultT6, ___stride7, ___allocator8, method);
}
// UnityEngine.XR.ARSubsystems.TrackableChanges`1<T> UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRTrackedObject>::CopyFrom(Unity.Collections.NativeArray`1<T>,Unity.Collections.NativeArray`1<T>,Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.TrackableId>,Unity.Collections.Allocator)
extern "C" IL2CPP_METHOD_ATTR TrackableChanges_1_t03A664B58E58E32B5EC1F7CB7133E87F804EE874  TrackableChanges_1_CopyFrom_m96900FFB6366D3F01F5DADC8AFDC2BF00F7D1329_gshared (NativeArray_1_t738892D030F76D319525B8C000F49D41505DD529  ___added0, NativeArray_1_t738892D030F76D319525B8C000F49D41505DD529  ___updated1, NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  ___removed2, int32_t ___allocator3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TrackableChanges_1_CopyFrom_m96900FFB6366D3F01F5DADC8AFDC2BF00F7D1329_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	NativeArray_1_t738892D030F76D319525B8C000F49D41505DD529  V_0;
	memset(&V_0, 0, sizeof(V_0));
	NativeArray_1_t738892D030F76D319525B8C000F49D41505DD529  V_1;
	memset(&V_1, 0, sizeof(V_1));
	NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		int32_t L_0 = IL2CPP_NATIVEARRAY_GET_LENGTH(((NativeArray_1_t738892D030F76D319525B8C000F49D41505DD529 *)(NativeArray_1_t738892D030F76D319525B8C000F49D41505DD529 *)(&___added0))->___m_Length_1);
		int32_t L_1 = ___allocator3;
		NativeArray_1__ctor_m16425BC62E9AC48C88E58870411D0D63CCE985A9((NativeArray_1_t738892D030F76D319525B8C000F49D41505DD529 *)(NativeArray_1_t738892D030F76D319525B8C000F49D41505DD529 *)(&V_0), (int32_t)L_0, (int32_t)L_1, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		NativeArray_1_t738892D030F76D319525B8C000F49D41505DD529  L_2 = ___added0;
		NativeArray_1_CopyFrom_m44A3244484057D36D3F473E72EBE58EEF79C5B92((NativeArray_1_t738892D030F76D319525B8C000F49D41505DD529 *)(NativeArray_1_t738892D030F76D319525B8C000F49D41505DD529 *)(&V_0), (NativeArray_1_t738892D030F76D319525B8C000F49D41505DD529 )L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 5));
		int32_t L_3 = IL2CPP_NATIVEARRAY_GET_LENGTH(((NativeArray_1_t738892D030F76D319525B8C000F49D41505DD529 *)(NativeArray_1_t738892D030F76D319525B8C000F49D41505DD529 *)(&___updated1))->___m_Length_1);
		int32_t L_4 = ___allocator3;
		NativeArray_1__ctor_m16425BC62E9AC48C88E58870411D0D63CCE985A9((NativeArray_1_t738892D030F76D319525B8C000F49D41505DD529 *)(NativeArray_1_t738892D030F76D319525B8C000F49D41505DD529 *)(&V_1), (int32_t)L_3, (int32_t)L_4, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		NativeArray_1_t738892D030F76D319525B8C000F49D41505DD529  L_5 = ___updated1;
		NativeArray_1_CopyFrom_m44A3244484057D36D3F473E72EBE58EEF79C5B92((NativeArray_1_t738892D030F76D319525B8C000F49D41505DD529 *)(NativeArray_1_t738892D030F76D319525B8C000F49D41505DD529 *)(&V_1), (NativeArray_1_t738892D030F76D319525B8C000F49D41505DD529 )L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 5));
		int32_t L_6 = IL2CPP_NATIVEARRAY_GET_LENGTH(((NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 *)(NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 *)(&___removed2))->___m_Length_1);
		int32_t L_7 = ___allocator3;
		NativeArray_1__ctor_m49FCF39766C38037835E85C0A677B26599D0BE55((NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 *)(NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 *)(&V_2), (int32_t)L_6, (int32_t)L_7, (int32_t)1, /*hidden argument*/NativeArray_1__ctor_m49FCF39766C38037835E85C0A677B26599D0BE55_RuntimeMethod_var);
		NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  L_8 = ___removed2;
		NativeArray_1_CopyFrom_mE2C04369E83ED4726DE1121E7E05342B6A993A97((NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 *)(NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 *)(&V_2), (NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 )L_8, /*hidden argument*/NativeArray_1_CopyFrom_mE2C04369E83ED4726DE1121E7E05342B6A993A97_RuntimeMethod_var);
		NativeArray_1_t738892D030F76D319525B8C000F49D41505DD529  L_9 = V_0;
		NativeArray_1_t738892D030F76D319525B8C000F49D41505DD529  L_10 = V_1;
		NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  L_11 = V_2;
		TrackableChanges_1_t03A664B58E58E32B5EC1F7CB7133E87F804EE874  L_12;
		memset(&L_12, 0, sizeof(L_12));
		TrackableChanges_1__ctor_m256B8D469EFDAAA37C6D50B7DB6C76C431A70430((&L_12), (NativeArray_1_t738892D030F76D319525B8C000F49D41505DD529 )L_9, (NativeArray_1_t738892D030F76D319525B8C000F49D41505DD529 )L_10, (NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 )L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 7));
		return L_12;
	}
}
// System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRTrackedObject>::Dispose()
extern "C" IL2CPP_METHOD_ATTR void TrackableChanges_1_Dispose_mA1365656D4814F94F14E5A34A975A00F5892C790_gshared (TrackableChanges_1_t03A664B58E58E32B5EC1F7CB7133E87F804EE874 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TrackableChanges_1_Dispose_mA1365656D4814F94F14E5A34A975A00F5892C790_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = TrackableChanges_1_get_isCreated_mBFB9748C1EA377BE047EC13F736E0C8C207E441C((TrackableChanges_1_t03A664B58E58E32B5EC1F7CB7133E87F804EE874 *)(TrackableChanges_1_t03A664B58E58E32B5EC1F7CB7133E87F804EE874 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 8));
		if (!L_0)
		{
			goto IL_0029;
		}
	}
	{
		NativeArray_1_t738892D030F76D319525B8C000F49D41505DD529 * L_1 = (NativeArray_1_t738892D030F76D319525B8C000F49D41505DD529 *)__this->get_address_of_m_Added_1();
		NativeArray_1_Dispose_mA88D5D4DAC3D042D2DBB1DBBC45EC820AE85242A((NativeArray_1_t738892D030F76D319525B8C000F49D41505DD529 *)(NativeArray_1_t738892D030F76D319525B8C000F49D41505DD529 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 9));
		NativeArray_1_t738892D030F76D319525B8C000F49D41505DD529 * L_2 = (NativeArray_1_t738892D030F76D319525B8C000F49D41505DD529 *)__this->get_address_of_m_Updated_2();
		NativeArray_1_Dispose_mA88D5D4DAC3D042D2DBB1DBBC45EC820AE85242A((NativeArray_1_t738892D030F76D319525B8C000F49D41505DD529 *)(NativeArray_1_t738892D030F76D319525B8C000F49D41505DD529 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 9));
		NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 * L_3 = (NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 *)__this->get_address_of_m_Removed_3();
		NativeArray_1_Dispose_mB8124101177299FC76251C4E7F6C98BB0E5503C7((NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 *)(NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6 *)L_3, /*hidden argument*/NativeArray_1_Dispose_mB8124101177299FC76251C4E7F6C98BB0E5503C7_RuntimeMethod_var);
	}

IL_0029:
	{
		return;
	}
}
extern "C"  void TrackableChanges_1_Dispose_mA1365656D4814F94F14E5A34A975A00F5892C790_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	TrackableChanges_1_t03A664B58E58E32B5EC1F7CB7133E87F804EE874 * _thisAdjusted = reinterpret_cast<TrackableChanges_1_t03A664B58E58E32B5EC1F7CB7133E87F804EE874 *>(__this + 1);
	TrackableChanges_1_Dispose_mA1365656D4814F94F14E5A34A975A00F5892C790(_thisAdjusted, method);
}
// System.Void UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRTrackedObject>::.ctor(Unity.Collections.NativeArray`1<T>,Unity.Collections.NativeArray`1<T>,Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.TrackableId>)
extern "C" IL2CPP_METHOD_ATTR void TrackableChanges_1__ctor_m256B8D469EFDAAA37C6D50B7DB6C76C431A70430_gshared (TrackableChanges_1_t03A664B58E58E32B5EC1F7CB7133E87F804EE874 * __this, NativeArray_1_t738892D030F76D319525B8C000F49D41505DD529  ___added0, NativeArray_1_t738892D030F76D319525B8C000F49D41505DD529  ___updated1, NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  ___removed2, const RuntimeMethod* method)
{
	{
		NativeArray_1_t738892D030F76D319525B8C000F49D41505DD529  L_0 = ___added0;
		__this->set_m_Added_1(L_0);
		NativeArray_1_t738892D030F76D319525B8C000F49D41505DD529  L_1 = ___updated1;
		__this->set_m_Updated_2(L_1);
		NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  L_2 = ___removed2;
		__this->set_m_Removed_3(L_2);
		TrackableChanges_1_set_isCreated_m085A94096B2D33690423DF6A6777D7586B582D14((TrackableChanges_1_t03A664B58E58E32B5EC1F7CB7133E87F804EE874 *)(TrackableChanges_1_t03A664B58E58E32B5EC1F7CB7133E87F804EE874 *)__this, (bool)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 2));
		return;
	}
}
extern "C"  void TrackableChanges_1__ctor_m256B8D469EFDAAA37C6D50B7DB6C76C431A70430_AdjustorThunk (RuntimeObject * __this, NativeArray_1_t738892D030F76D319525B8C000F49D41505DD529  ___added0, NativeArray_1_t738892D030F76D319525B8C000F49D41505DD529  ___updated1, NativeArray_1_tD7797CC5848BA9ECBD1172056474C8DD8022B1D6  ___removed2, const RuntimeMethod* method)
{
	TrackableChanges_1_t03A664B58E58E32B5EC1F7CB7133E87F804EE874 * _thisAdjusted = reinterpret_cast<TrackableChanges_1_t03A664B58E58E32B5EC1F7CB7133E87F804EE874 *>(__this + 1);
	TrackableChanges_1__ctor_m256B8D469EFDAAA37C6D50B7DB6C76C431A70430(_thisAdjusted, ___added0, ___updated1, ___removed2, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean UnityEngine.XR.ARSubsystems.TrackingSubsystem`2<UnityEngine.XR.ARSubsystems.BoundedPlane,System.Object>::get_running()
extern "C" IL2CPP_METHOD_ATTR bool TrackingSubsystem_2_get_running_mE178F5D0CDEEDF356425AD63AC753114DE793AD7_gshared (TrackingSubsystem_2_t0D6BC19D17058B4CBF6E3E9122F09A5A86A8566C * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = (bool)__this->get_m_Running_1();
		return L_0;
	}
}
// System.Void UnityEngine.XR.ARSubsystems.TrackingSubsystem`2<UnityEngine.XR.ARSubsystems.BoundedPlane,System.Object>::.ctor()
extern "C" IL2CPP_METHOD_ATTR void TrackingSubsystem_2__ctor_mF1761D396F2FC0B6BA3AA3F0DCDF7577A946B13A_gshared (TrackingSubsystem_2_t0D6BC19D17058B4CBF6E3E9122F09A5A86A8566C * __this, const RuntimeMethod* method)
{
	{
		NullCheck((Subsystem_1_t3C9464E9C57AFD87BCA00E457B3C2FCB3AFA269E *)__this);
		((  void (*) (Subsystem_1_t3C9464E9C57AFD87BCA00E457B3C2FCB3AFA269E *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Subsystem_1_t3C9464E9C57AFD87BCA00E457B3C2FCB3AFA269E *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean UnityEngine.XR.ARSubsystems.TrackingSubsystem`2<UnityEngine.XR.ARSubsystems.XREnvironmentProbe,System.Object>::get_running()
extern "C" IL2CPP_METHOD_ATTR bool TrackingSubsystem_2_get_running_m2AD16B5083036540DC1435F3081329B7DD36BADB_gshared (TrackingSubsystem_2_tF85E28B67FC3325EEB52CE2A8DA0361B2BD80909 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = (bool)__this->get_m_Running_1();
		return L_0;
	}
}
// System.Void UnityEngine.XR.ARSubsystems.TrackingSubsystem`2<UnityEngine.XR.ARSubsystems.XREnvironmentProbe,System.Object>::.ctor()
extern "C" IL2CPP_METHOD_ATTR void TrackingSubsystem_2__ctor_mD1B4842F29FB022CE1B973296BB4D7888A93835E_gshared (TrackingSubsystem_2_tF85E28B67FC3325EEB52CE2A8DA0361B2BD80909 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((Subsystem_1_t3C9464E9C57AFD87BCA00E457B3C2FCB3AFA269E *)__this);
		((  void (*) (Subsystem_1_t3C9464E9C57AFD87BCA00E457B3C2FCB3AFA269E *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Subsystem_1_t3C9464E9C57AFD87BCA00E457B3C2FCB3AFA269E *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean UnityEngine.XR.ARSubsystems.TrackingSubsystem`2<UnityEngine.XR.ARSubsystems.XRFace,System.Object>::get_running()
extern "C" IL2CPP_METHOD_ATTR bool TrackingSubsystem_2_get_running_mA3B8A8D9F7E99E4EE1C1BDE3FD8B408C3825E6D8_gshared (TrackingSubsystem_2_t810E285415E1644FA85391E12F95806CEE3C50EB * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = (bool)__this->get_m_Running_1();
		return L_0;
	}
}
// System.Void UnityEngine.XR.ARSubsystems.TrackingSubsystem`2<UnityEngine.XR.ARSubsystems.XRFace,System.Object>::.ctor()
extern "C" IL2CPP_METHOD_ATTR void TrackingSubsystem_2__ctor_m7625CF45A30E7BA1C410ADCDD6556266E9BDE509_gshared (TrackingSubsystem_2_t810E285415E1644FA85391E12F95806CEE3C50EB * __this, const RuntimeMethod* method)
{
	{
		NullCheck((Subsystem_1_t3C9464E9C57AFD87BCA00E457B3C2FCB3AFA269E *)__this);
		((  void (*) (Subsystem_1_t3C9464E9C57AFD87BCA00E457B3C2FCB3AFA269E *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Subsystem_1_t3C9464E9C57AFD87BCA00E457B3C2FCB3AFA269E *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean UnityEngine.XR.ARSubsystems.TrackingSubsystem`2<UnityEngine.XR.ARSubsystems.XRHumanBody,System.Object>::get_running()
extern "C" IL2CPP_METHOD_ATTR bool TrackingSubsystem_2_get_running_m360F2AE9A4FCD72E5E94E1164F4B697F8E5B29F8_gshared (TrackingSubsystem_2_tBB99F93D6653B71E3A2C1E2116F8599311DC72D6 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = (bool)__this->get_m_Running_1();
		return L_0;
	}
}
// System.Void UnityEngine.XR.ARSubsystems.TrackingSubsystem`2<UnityEngine.XR.ARSubsystems.XRHumanBody,System.Object>::.ctor()
extern "C" IL2CPP_METHOD_ATTR void TrackingSubsystem_2__ctor_mB17216C41B97F6316E4463418DA55CA2F56FB8AD_gshared (TrackingSubsystem_2_tBB99F93D6653B71E3A2C1E2116F8599311DC72D6 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((Subsystem_1_t3C9464E9C57AFD87BCA00E457B3C2FCB3AFA269E *)__this);
		((  void (*) (Subsystem_1_t3C9464E9C57AFD87BCA00E457B3C2FCB3AFA269E *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Subsystem_1_t3C9464E9C57AFD87BCA00E457B3C2FCB3AFA269E *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean UnityEngine.XR.ARSubsystems.TrackingSubsystem`2<UnityEngine.XR.ARSubsystems.XRPointCloud,System.Object>::get_running()
extern "C" IL2CPP_METHOD_ATTR bool TrackingSubsystem_2_get_running_m6C093A1DAC5ED78C236F9B420478D248D4A38281_gshared (TrackingSubsystem_2_t1EC759A75AE7EFD4E4446A20009C8472E1C8AED1 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = (bool)__this->get_m_Running_1();
		return L_0;
	}
}
// System.Void UnityEngine.XR.ARSubsystems.TrackingSubsystem`2<UnityEngine.XR.ARSubsystems.XRPointCloud,System.Object>::.ctor()
extern "C" IL2CPP_METHOD_ATTR void TrackingSubsystem_2__ctor_m90E4D38CB60953BC61F7EFCD9B846A9071C9DF4B_gshared (TrackingSubsystem_2_t1EC759A75AE7EFD4E4446A20009C8472E1C8AED1 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((Subsystem_1_t3C9464E9C57AFD87BCA00E457B3C2FCB3AFA269E *)__this);
		((  void (*) (Subsystem_1_t3C9464E9C57AFD87BCA00E457B3C2FCB3AFA269E *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Subsystem_1_t3C9464E9C57AFD87BCA00E457B3C2FCB3AFA269E *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean UnityEngine.XR.ARSubsystems.TrackingSubsystem`2<UnityEngine.XR.ARSubsystems.XRReferencePoint,System.Object>::get_running()
extern "C" IL2CPP_METHOD_ATTR bool TrackingSubsystem_2_get_running_m2102054ECF2F148EF7E5285FF8113FDDB6F29FBE_gshared (TrackingSubsystem_2_tA3D4B822865BAE0754B253CF8551A3EBB7073851 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = (bool)__this->get_m_Running_1();
		return L_0;
	}
}
// System.Void UnityEngine.XR.ARSubsystems.TrackingSubsystem`2<UnityEngine.XR.ARSubsystems.XRReferencePoint,System.Object>::.ctor()
extern "C" IL2CPP_METHOD_ATTR void TrackingSubsystem_2__ctor_m7BED931B501BCE6534C3BC2DC0CA30C73E917F90_gshared (TrackingSubsystem_2_tA3D4B822865BAE0754B253CF8551A3EBB7073851 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((Subsystem_1_t3C9464E9C57AFD87BCA00E457B3C2FCB3AFA269E *)__this);
		((  void (*) (Subsystem_1_t3C9464E9C57AFD87BCA00E457B3C2FCB3AFA269E *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Subsystem_1_t3C9464E9C57AFD87BCA00E457B3C2FCB3AFA269E *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean UnityEngine.XR.ARSubsystems.TrackingSubsystem`2<UnityEngine.XR.ARSubsystems.XRTrackedImage,System.Object>::get_running()
extern "C" IL2CPP_METHOD_ATTR bool TrackingSubsystem_2_get_running_m6BB5084930534ADADDE2E3CB979D6833C916D31A_gshared (TrackingSubsystem_2_t9DCCF84BEF8FF140325BC90B18398D78CACAFF00 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = (bool)__this->get_m_Running_1();
		return L_0;
	}
}
// System.Void UnityEngine.XR.ARSubsystems.TrackingSubsystem`2<UnityEngine.XR.ARSubsystems.XRTrackedImage,System.Object>::.ctor()
extern "C" IL2CPP_METHOD_ATTR void TrackingSubsystem_2__ctor_mE7AF1FD3521B4B3FE7D13D6BA44F324F0C0AC745_gshared (TrackingSubsystem_2_t9DCCF84BEF8FF140325BC90B18398D78CACAFF00 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((Subsystem_1_t3C9464E9C57AFD87BCA00E457B3C2FCB3AFA269E *)__this);
		((  void (*) (Subsystem_1_t3C9464E9C57AFD87BCA00E457B3C2FCB3AFA269E *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Subsystem_1_t3C9464E9C57AFD87BCA00E457B3C2FCB3AFA269E *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean UnityEngine.XR.ARSubsystems.TrackingSubsystem`2<UnityEngine.XR.ARSubsystems.XRTrackedObject,System.Object>::get_running()
extern "C" IL2CPP_METHOD_ATTR bool TrackingSubsystem_2_get_running_m5897145D6AEC678C705A97D5E76456F46764CDB3_gshared (TrackingSubsystem_2_t463AAAB107BF4F04078A98431C680E92C4B53148 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = (bool)__this->get_m_Running_1();
		return L_0;
	}
}
// System.Void UnityEngine.XR.ARSubsystems.TrackingSubsystem`2<UnityEngine.XR.ARSubsystems.XRTrackedObject,System.Object>::.ctor()
extern "C" IL2CPP_METHOD_ATTR void TrackingSubsystem_2__ctor_mFD6F43BFD4E9884C5B789EA27DF50136997A057B_gshared (TrackingSubsystem_2_t463AAAB107BF4F04078A98431C680E92C4B53148 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((Subsystem_1_t3C9464E9C57AFD87BCA00E457B3C2FCB3AFA269E *)__this);
		((  void (*) (Subsystem_1_t3C9464E9C57AFD87BCA00E457B3C2FCB3AFA269E *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Subsystem_1_t3C9464E9C57AFD87BCA00E457B3C2FCB3AFA269E *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
