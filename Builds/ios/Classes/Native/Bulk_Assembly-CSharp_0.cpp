﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"

template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R>
struct InterfaceFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
struct InterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};

// CharacterControllerThirdPerson
struct CharacterControllerThirdPerson_t2CC0D121855408DC72A1CB71940065D5C220C886;
// ImageRecognitionExample
struct ImageRecognitionExample_t911A7F2FD5BFFF23A30C97F8619C18A1F0757110;
// MarkerPrefabs
struct MarkerPrefabs_t227A4AF77E7197AEFE5B31A217749784AA27AE6B;
// MarkerPrefabs[]
struct MarkerPrefabsU5BU5D_t7F40C670B36EE25921E51F171FD6A68A7A1BC336;
// PrefabsInVicinity
struct PrefabsInVicinity_tF56E3155685D05BFC0529D6C62A92A5C2B88A039;
// PrefabsInVicinity[]
struct PrefabsInVicinityU5BU5D_tCF6E2464E3202CBD16C9DEA043361EA7DB3A49A9;
// System.Action`1<UnityEngine.XR.ARFoundation.ARTrackedImagesChangedEventArgs>
struct Action_1_t825CEDF2F074FBD059900D81615EFDE0AFD02CA0;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<System.Guid,UnityEngine.XR.ARSubsystems.XRReferenceImage>
struct Dictionary_2_tE3586D05DB2E27BC21247078C0A121A82B014D91;
// System.Collections.Generic.Dictionary`2<UnityEngine.XR.ARSubsystems.TrackableId,UnityEngine.XR.ARFoundation.ARTrackedImage>
struct Dictionary_2_t9780DA231BA452BFBD83FA24408472862BDD8A9F;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D;
// System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARTrackedImage>
struct List_1_t85456E82777B8C7F9CFFED2C540D0613542A8C8D;
// System.Collections.Generic.List`1<UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystemDescriptor>
struct List_1_t19F02B33C2CBC6A12544FBB09A8173859BCBCCBE;
// System.Collections.IEnumerator
struct IEnumerator_t8789118187258CC88B77AFAC6315B5AF87D3E18A;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
// System.Reflection.Binder
struct Binder_t4D5CB06963501D32847C057B57157D6DC49CA759;
// System.Reflection.MemberFilter
struct MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// TurnTheGameOn.ArrowWaypointer.Waypoint
struct Waypoint_t7B48E7B71CF48791746A5B866BCCB7F9FABFCA0A;
// TurnTheGameOn.ArrowWaypointer.WaypointController
struct WaypointController_tD4E5882DF3A666D2E1C97E9E73061816C123AB20;
// TurnTheGameOn.ArrowWaypointer.WaypointController/WaypointComponents
struct WaypointComponents_t3DE523CAE0F5855ADF2B740DBBA68569E9034301;
// TurnTheGameOn.ArrowWaypointer.WaypointController/WaypointComponents[]
struct WaypointComponentsU5BU5D_t995379FD5B9F2912BDD0CA0EC38E68B64A9839FD;
// TurnTheGameOn.NPCChat.SmoothFollow
struct SmoothFollow_t216FB57B20D1783DDB83147F7D75C14735C98316;
// UnityEngine.Animator
struct Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A;
// UnityEngine.Camera
struct Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34;
// UnityEngine.Camera/CameraCallback
struct CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0;
// UnityEngine.CapsuleCollider
struct CapsuleCollider_t5FD15B9E7BEEC4FFA8A2071E9FD2B8DEB3A826D1;
// UnityEngine.Collider
struct Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF;
// UnityEngine.Component
struct Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F;
// UnityEngine.Events.UnityEvent
struct UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429;
// UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0;
// UnityEngine.Rigidbody
struct Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5;
// UnityEngine.Texture2D
struct Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// UnityEngine.XR.ARFoundation.ARSessionOrigin
struct ARSessionOrigin_t61463C0A24AF925CF219B6C3F1720325C96720EF;
// UnityEngine.XR.ARFoundation.ARTrackable`2<UnityEngine.XR.ARSubsystems.XRTrackedImage,System.Object>
struct ARTrackable_2_tDC22DD8BD2173402258A323598A3378BE19FACCB;
// UnityEngine.XR.ARFoundation.ARTrackable`2<UnityEngine.XR.ARSubsystems.XRTrackedImage,UnityEngine.XR.ARFoundation.ARTrackedImage>
struct ARTrackable_2_t1EDE6011EE2879288E62C947FC7EF0BBD638DDF8;
// UnityEngine.XR.ARFoundation.ARTrackedImage
struct ARTrackedImage_tD699A1D6CA301ECE200D07D1CF1722E94AFEF01A;
// UnityEngine.XR.ARFoundation.ARTrackedImageManager
struct ARTrackedImageManager_tC1F6C093163D1D4A5CCE83613BFCA465E2DABA9D;
// UnityEngine.XR.ARFoundation.ARTrackedImage[]
struct ARTrackedImageU5BU5D_tA6A5BBBE1F5B60CE74B711598C5941C9FA9C8AD7;
// UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystem
struct XRImageTrackingSubsystem_t1DC56F01ABB0CC32D9AC26AA77E097AD9B0ECEB5;
// UnityEngine.XR.ARSubsystems.XRReferenceImageLibrary
struct XRReferenceImageLibrary_t38B21DC6650EADA892125F045DCBF71EBC3F6A8F;
// UserControlThirdPerson
struct UserControlThirdPerson_tA3F8F919E01EC0A7D458E6F2F3346D2BD3878DAB;

extern RuntimeClass* Action_1_t825CEDF2F074FBD059900D81615EFDE0AFD02CA0_il2cpp_TypeInfo_var;
extern RuntimeClass* Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var;
extern RuntimeClass* GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_il2cpp_TypeInfo_var;
extern RuntimeClass* IDisposable_t7218B22548186B208D65EA5B7870503810A2D15A_il2cpp_TypeInfo_var;
extern RuntimeClass* IEnumerator_t8789118187258CC88B77AFAC6315B5AF87D3E18A_il2cpp_TypeInfo_var;
extern RuntimeClass* Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var;
extern RuntimeClass* Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var;
extern RuntimeClass* ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A_il2cpp_TypeInfo_var;
extern RuntimeClass* Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var;
extern RuntimeClass* Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_il2cpp_TypeInfo_var;
extern RuntimeClass* Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_il2cpp_TypeInfo_var;
extern RuntimeClass* TrackingState_t124D9E603E4E0453A85409CF7762EE8C946233F6_il2cpp_TypeInfo_var;
extern RuntimeClass* Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA_il2cpp_TypeInfo_var;
extern RuntimeClass* Type_t_il2cpp_TypeInfo_var;
extern RuntimeClass* Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral08DEE733AAADFC66234A4C80E52749BADDA1805D;
extern String_t* _stringLiteral09EF5102C999B2492459D0CDEBA4014825ED07F4;
extern String_t* _stringLiteral1070D410481ACDBAF8E92F351242DA3D26ABF17A;
extern String_t* _stringLiteral10720C73909715E6E3CAD0C6F55EAF64E3E4168A;
extern String_t* _stringLiteral110BE4F4AA851DA91271D7D722097AD2AEEAD525;
extern String_t* _stringLiteral1EBA140FDD9C6860A1730C408E3064AA417CA2A3;
extern String_t* _stringLiteral34AC72CAAD5414CF545E1146885DC571771361D9;
extern String_t* _stringLiteral368E8081B781AAC177FD07C2415B694B1F878911;
extern String_t* _stringLiteral36FEC5D5ACCA82E76ED3E6089026A917559F8440;
extern String_t* _stringLiteral3E5E4666125140F59F616547A0648DBF5EBE1B36;
extern String_t* _stringLiteral4B937CC841D82F8936CEF1EFB88708AB5B0F1EE5;
extern String_t* _stringLiteral4F45D2FEA2B49D472D55F53F0CFAC6EDD8065D8D;
extern String_t* _stringLiteral4F57A1CE99E68A7B05C42D0A7EA0070EAFABD31C;
extern String_t* _stringLiteral56B0664945A46455085CDE2AE967C641B320E757;
extern String_t* _stringLiteral6AE43D92255E63B6460986C81E8F68153762E449;
extern String_t* _stringLiteral71648DD8C0E0AB094578F2E030DB26C525C7BA67;
extern String_t* _stringLiteral8C3B68BB4A07759ACDAC90DBED3433E8AADDA35B;
extern String_t* _stringLiteral97D3E6EADAC2258F84B327212E05F248C6B354A1;
extern String_t* _stringLiteral9F5957ECF885109B5CDB03117A55B5CAA759AB53;
extern String_t* _stringLiteralA30F013D6B164520670D1289B00C9D08C846B231;
extern String_t* _stringLiteralA3EADB39916A68BB98BCFA2F32BE511DBD91782E;
extern String_t* _stringLiteralB50A7D3B0F99300A231D81BA74CEE18CD3BF5EC3;
extern String_t* _stringLiteralBA4E72261283258434788542BA397135C10F39D8;
extern String_t* _stringLiteralD32923FEEDA5736C136C007634BE98707D80C8DA;
extern String_t* _stringLiteralD5580B73BFB2949AC6D9534C49ECD66CEBA94E8C;
extern String_t* _stringLiteralE53407CFE1A5156B9F0D1EED3BAB5EF3AE75CFD8;
extern String_t* _stringLiteralEC315164409BDAD2E1AB675BF92EFBF8B3EB019E;
extern String_t* _stringLiteralF79EAD34F827E5E12513B175430AD4243748F1E0;
extern const RuntimeMethod* ARTrackable_2_get_trackingState_mE6C9A0305C0A41DDF81E1330094981C9C1A00BCA_RuntimeMethod_var;
extern const RuntimeMethod* Action_1__ctor_m685B9CA8548AB276638A829E0898E1BDF2A74388_RuntimeMethod_var;
extern const RuntimeMethod* Array_Resize_TisWaypointComponents_t3DE523CAE0F5855ADF2B740DBBA68569E9034301_m9D1CC9E22BEE7FA8386256BA820CC2B8D7CAA87A_RuntimeMethod_var;
extern const RuntimeMethod* Component_GetComponent_TisAnimator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A_mEA6C8FBABE309599A828D6BB8777278BCFB28719_RuntimeMethod_var;
extern const RuntimeMethod* Component_GetComponent_TisCapsuleCollider_t5FD15B9E7BEEC4FFA8A2071E9FD2B8DEB3A826D1_m73D2BA64E56A00BAEDB982029C042CB4CF1A0D37_RuntimeMethod_var;
extern const RuntimeMethod* Component_GetComponent_TisCharacterControllerThirdPerson_t2CC0D121855408DC72A1CB71940065D5C220C886_mBED54F2A08F701B5F685DC0A1DDCE406FE052B2D_RuntimeMethod_var;
extern const RuntimeMethod* Component_GetComponent_TisRigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5_m3F58A77E3F62D9C80D7B49BA04E3D4809264FD5C_RuntimeMethod_var;
extern const RuntimeMethod* Component_GetComponent_TisWaypoint_t7B48E7B71CF48791746A5B866BCCB7F9FABFCA0A_m7AB24D399574ADB4EAD0A31828AAFDA12A50233F_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_Dispose_m46C5EE3170D6F427C4379CE89AFE659555B786A7_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_MoveNext_m75BAC688340F0794E4C6C98B495BB4B9A02E6522_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_get_Current_m800C03D423EC5FCFD560CC7A2B3FF44087CEE0BF_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_GetComponent_TisWaypoint_t7B48E7B71CF48791746A5B866BCCB7F9FABFCA0A_m8DDD1ED52C9E382D4753784215506A29EDA35B32_RuntimeMethod_var;
extern const RuntimeMethod* ImageRecognitionExample_OnImageChanged_m0C926016A8C152B5DBDE7AAEF8C8884EB07951D7_RuntimeMethod_var;
extern const RuntimeMethod* List_1_GetEnumerator_m06B9ADCB9085604C5418BD1AC24ADA9237C95CF9_RuntimeMethod_var;
extern const RuntimeMethod* Object_FindObjectOfType_TisARTrackedImageManager_tC1F6C093163D1D4A5CCE83613BFCA465E2DABA9D_m8E2DA26A9425734CF006C5373BEB1A4C5A896180_RuntimeMethod_var;
extern const RuntimeType* GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_0_0_0_var;
extern const uint32_t CharacterControllerThirdPerson_ApplyExtraTurnRotation_mF7EA79A525D0D136B16D4AA00A524BA395FFBFD1_MetadataUsageId;
extern const uint32_t CharacterControllerThirdPerson_CheckGroundStatus_mDC821F214A629FE6A8E67AFFE8DE88E6B9ED6FBC_MetadataUsageId;
extern const uint32_t CharacterControllerThirdPerson_HandleAirborneMovement_m0DF8799B3B9814A782A10AD5434F9B21A14D8B33_MetadataUsageId;
extern const uint32_t CharacterControllerThirdPerson_HandleGroundedMovement_m8BC178F170EF39BDD6F26A0B4889A3AF6C11BCEA_MetadataUsageId;
extern const uint32_t CharacterControllerThirdPerson_Move_mB818BF9313F1CC2571D963FEBBBBFC1765E43EF4_MetadataUsageId;
extern const uint32_t CharacterControllerThirdPerson_OnAnimatorMove_m68A2195DA3689D9043DA247A18E7941EF0A53309_MetadataUsageId;
extern const uint32_t CharacterControllerThirdPerson_PreventStandingInLowHeadroom_mFA01E5F165FACD66CB419F8439B763621CF2C5EF_MetadataUsageId;
extern const uint32_t CharacterControllerThirdPerson_ScaleCapsuleForCrouching_m7EA7CA090344079B74749A8D6FE2A06E9946D231_MetadataUsageId;
extern const uint32_t CharacterControllerThirdPerson_Start_m54B47B65EDA1E3776CF4EC4BAA6C297F67A44C31_MetadataUsageId;
extern const uint32_t CharacterControllerThirdPerson_UpdateAnimator_m2CCBBD592478AA52D15022D00AC3B5765466BFEA_MetadataUsageId;
extern const uint32_t ImageRecognitionExample_Awake_m3A4E749AFA71E5C19CF14EAE82F8755449972A14_MetadataUsageId;
extern const uint32_t ImageRecognitionExample_OnDestroy_m0DCA2A78CFCA2C055BD4CEAAF268E22E30E08BFC_MetadataUsageId;
extern const uint32_t ImageRecognitionExample_OnEnable_m04082E6E1E765E17296B0ECB5FAEBF1FD83A1342_MetadataUsageId;
extern const uint32_t ImageRecognitionExample_OnImageChanged_m0C926016A8C152B5DBDE7AAEF8C8884EB07951D7_MetadataUsageId;
extern const uint32_t ImageRecognitionExample_Update_mC2498424F35CEF863FFF644441D85ED82E90E09E_MetadataUsageId;
extern const uint32_t ImageRecognitionExample_inflateObjectsInVicinity_mFEF7B58384ED8DAD668118E514CFEBE3B52635C1_MetadataUsageId;
extern const uint32_t SmoothFollow_LateUpdate_mC6D45143622DF2CA6DB06FA1053C6F973E59FA3B_MetadataUsageId;
extern const uint32_t UserControlThirdPerson_FixedUpdate_mD8844CAD5BD762F1E6CF9624AA695F0E1CA770A7_MetadataUsageId;
extern const uint32_t UserControlThirdPerson_Start_mDF67BE424627A659DF4048B044008D31CFEB8F79_MetadataUsageId;
extern const uint32_t UserControlThirdPerson_Update_mB28C29866F8A6825FAF4D8D2CBE82A0F20F4E096_MetadataUsageId;
extern const uint32_t WaypointComponents__ctor_m1F01D8B6FA5D14EA5F49E6BC0E11998407C61499_MetadataUsageId;
extern const uint32_t WaypointController_CalculateWaypoints_m88CB2133663AC7FF257CED5CACCE8641523D7910_MetadataUsageId;
extern const uint32_t WaypointController_ChangeTarget_m8D5F516033E81CC08980BAA7CE9FE4187456B63B_MetadataUsageId;
extern const uint32_t WaypointController_CleanUpWaypoints_m7B8D3FEB7086B037AB945021C1D8B9753FB8C172_MetadataUsageId;
extern const uint32_t WaypointController_CreateArrow_m3E17F297C402A2F304D0FA081808552E733DC7BC_MetadataUsageId;
extern const uint32_t WaypointController_FindArrow_m95F170F1E2ADB8A718A914339C326FD87324661F_MetadataUsageId;
extern const uint32_t WaypointController_Start_m1D3C90981C9D75F4796FF21DBF8DEF2202B3D6BD_MetadataUsageId;
extern const uint32_t WaypointController_Update_mDBBE6585C09BAF5459BDD47D3891570E3DEC5C6E_MetadataUsageId;
extern const uint32_t Waypoint_OnTriggerEnter_mE727467660A29DBBAD9152E21605AF2879F2AC28_MetadataUsageId;
extern const uint32_t Waypoint_Update_mB8767078754CB1D93447A1142A45618F7AE8B3F6_MetadataUsageId;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;

struct MarkerPrefabsU5BU5D_t7F40C670B36EE25921E51F171FD6A68A7A1BC336;
struct PrefabsInVicinityU5BU5D_tCF6E2464E3202CBD16C9DEA043361EA7DB3A49A9;
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
struct WaypointComponentsU5BU5D_t995379FD5B9F2912BDD0CA0EC38E68B64A9839FD;


#ifndef U3CMODULEU3E_T6CDDDF959E7E18A6744E43B613F41CDAC780256A_H
#define U3CMODULEU3E_T6CDDDF959E7E18A6744E43B613F41CDAC780256A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t6CDDDF959E7E18A6744E43B613F41CDAC780256A 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T6CDDDF959E7E18A6744E43B613F41CDAC780256A_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef MARKERPREFABS_T227A4AF77E7197AEFE5B31A217749784AA27AE6B_H
#define MARKERPREFABS_T227A4AF77E7197AEFE5B31A217749784AA27AE6B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MarkerPrefabs
struct  MarkerPrefabs_t227A4AF77E7197AEFE5B31A217749784AA27AE6B  : public RuntimeObject
{
public:
	// System.String MarkerPrefabs::prefabName
	String_t* ___prefabName_0;
	// UnityEngine.GameObject MarkerPrefabs::targetPrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___targetPrefab_1;

public:
	inline static int32_t get_offset_of_prefabName_0() { return static_cast<int32_t>(offsetof(MarkerPrefabs_t227A4AF77E7197AEFE5B31A217749784AA27AE6B, ___prefabName_0)); }
	inline String_t* get_prefabName_0() const { return ___prefabName_0; }
	inline String_t** get_address_of_prefabName_0() { return &___prefabName_0; }
	inline void set_prefabName_0(String_t* value)
	{
		___prefabName_0 = value;
		Il2CppCodeGenWriteBarrier((&___prefabName_0), value);
	}

	inline static int32_t get_offset_of_targetPrefab_1() { return static_cast<int32_t>(offsetof(MarkerPrefabs_t227A4AF77E7197AEFE5B31A217749784AA27AE6B, ___targetPrefab_1)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_targetPrefab_1() const { return ___targetPrefab_1; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_targetPrefab_1() { return &___targetPrefab_1; }
	inline void set_targetPrefab_1(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___targetPrefab_1 = value;
		Il2CppCodeGenWriteBarrier((&___targetPrefab_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MARKERPREFABS_T227A4AF77E7197AEFE5B31A217749784AA27AE6B_H
#ifndef PREFABSINVICINITY_TF56E3155685D05BFC0529D6C62A92A5C2B88A039_H
#define PREFABSINVICINITY_TF56E3155685D05BFC0529D6C62A92A5C2B88A039_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PrefabsInVicinity
struct  PrefabsInVicinity_tF56E3155685D05BFC0529D6C62A92A5C2B88A039  : public RuntimeObject
{
public:
	// System.String PrefabsInVicinity::prefabName
	String_t* ___prefabName_0;
	// UnityEngine.GameObject PrefabsInVicinity::prefabPresent
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___prefabPresent_1;

public:
	inline static int32_t get_offset_of_prefabName_0() { return static_cast<int32_t>(offsetof(PrefabsInVicinity_tF56E3155685D05BFC0529D6C62A92A5C2B88A039, ___prefabName_0)); }
	inline String_t* get_prefabName_0() const { return ___prefabName_0; }
	inline String_t** get_address_of_prefabName_0() { return &___prefabName_0; }
	inline void set_prefabName_0(String_t* value)
	{
		___prefabName_0 = value;
		Il2CppCodeGenWriteBarrier((&___prefabName_0), value);
	}

	inline static int32_t get_offset_of_prefabPresent_1() { return static_cast<int32_t>(offsetof(PrefabsInVicinity_tF56E3155685D05BFC0529D6C62A92A5C2B88A039, ___prefabPresent_1)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_prefabPresent_1() const { return ___prefabPresent_1; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_prefabPresent_1() { return &___prefabPresent_1; }
	inline void set_prefabPresent_1(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___prefabPresent_1 = value;
		Il2CppCodeGenWriteBarrier((&___prefabPresent_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREFABSINVICINITY_TF56E3155685D05BFC0529D6C62A92A5C2B88A039_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef LIST_1_T85456E82777B8C7F9CFFED2C540D0613542A8C8D_H
#define LIST_1_T85456E82777B8C7F9CFFED2C540D0613542A8C8D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARTrackedImage>
struct  List_1_t85456E82777B8C7F9CFFED2C540D0613542A8C8D  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ARTrackedImageU5BU5D_tA6A5BBBE1F5B60CE74B711598C5941C9FA9C8AD7* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t85456E82777B8C7F9CFFED2C540D0613542A8C8D, ____items_1)); }
	inline ARTrackedImageU5BU5D_tA6A5BBBE1F5B60CE74B711598C5941C9FA9C8AD7* get__items_1() const { return ____items_1; }
	inline ARTrackedImageU5BU5D_tA6A5BBBE1F5B60CE74B711598C5941C9FA9C8AD7** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ARTrackedImageU5BU5D_tA6A5BBBE1F5B60CE74B711598C5941C9FA9C8AD7* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t85456E82777B8C7F9CFFED2C540D0613542A8C8D, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t85456E82777B8C7F9CFFED2C540D0613542A8C8D, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t85456E82777B8C7F9CFFED2C540D0613542A8C8D, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_4), value);
	}
};

struct List_1_t85456E82777B8C7F9CFFED2C540D0613542A8C8D_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	ARTrackedImageU5BU5D_tA6A5BBBE1F5B60CE74B711598C5941C9FA9C8AD7* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t85456E82777B8C7F9CFFED2C540D0613542A8C8D_StaticFields, ____emptyArray_5)); }
	inline ARTrackedImageU5BU5D_tA6A5BBBE1F5B60CE74B711598C5941C9FA9C8AD7* get__emptyArray_5() const { return ____emptyArray_5; }
	inline ARTrackedImageU5BU5D_tA6A5BBBE1F5B60CE74B711598C5941C9FA9C8AD7** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(ARTrackedImageU5BU5D_tA6A5BBBE1F5B60CE74B711598C5941C9FA9C8AD7* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((&____emptyArray_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T85456E82777B8C7F9CFFED2C540D0613542A8C8D_H
#ifndef MEMBERINFO_T_H
#define MEMBERINFO_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERINFO_T_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef WAYPOINTCOMPONENTS_T3DE523CAE0F5855ADF2B740DBBA68569E9034301_H
#define WAYPOINTCOMPONENTS_T3DE523CAE0F5855ADF2B740DBBA68569E9034301_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TurnTheGameOn.ArrowWaypointer.WaypointController_WaypointComponents
struct  WaypointComponents_t3DE523CAE0F5855ADF2B740DBBA68569E9034301  : public RuntimeObject
{
public:
	// System.String TurnTheGameOn.ArrowWaypointer.WaypointController_WaypointComponents::waypointName
	String_t* ___waypointName_0;
	// TurnTheGameOn.ArrowWaypointer.Waypoint TurnTheGameOn.ArrowWaypointer.WaypointController_WaypointComponents::waypoint
	Waypoint_t7B48E7B71CF48791746A5B866BCCB7F9FABFCA0A * ___waypoint_1;
	// UnityEngine.Events.UnityEvent TurnTheGameOn.ArrowWaypointer.WaypointController_WaypointComponents::waypointEvent
	UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * ___waypointEvent_2;

public:
	inline static int32_t get_offset_of_waypointName_0() { return static_cast<int32_t>(offsetof(WaypointComponents_t3DE523CAE0F5855ADF2B740DBBA68569E9034301, ___waypointName_0)); }
	inline String_t* get_waypointName_0() const { return ___waypointName_0; }
	inline String_t** get_address_of_waypointName_0() { return &___waypointName_0; }
	inline void set_waypointName_0(String_t* value)
	{
		___waypointName_0 = value;
		Il2CppCodeGenWriteBarrier((&___waypointName_0), value);
	}

	inline static int32_t get_offset_of_waypoint_1() { return static_cast<int32_t>(offsetof(WaypointComponents_t3DE523CAE0F5855ADF2B740DBBA68569E9034301, ___waypoint_1)); }
	inline Waypoint_t7B48E7B71CF48791746A5B866BCCB7F9FABFCA0A * get_waypoint_1() const { return ___waypoint_1; }
	inline Waypoint_t7B48E7B71CF48791746A5B866BCCB7F9FABFCA0A ** get_address_of_waypoint_1() { return &___waypoint_1; }
	inline void set_waypoint_1(Waypoint_t7B48E7B71CF48791746A5B866BCCB7F9FABFCA0A * value)
	{
		___waypoint_1 = value;
		Il2CppCodeGenWriteBarrier((&___waypoint_1), value);
	}

	inline static int32_t get_offset_of_waypointEvent_2() { return static_cast<int32_t>(offsetof(WaypointComponents_t3DE523CAE0F5855ADF2B740DBBA68569E9034301, ___waypointEvent_2)); }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * get_waypointEvent_2() const { return ___waypointEvent_2; }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F ** get_address_of_waypointEvent_2() { return &___waypointEvent_2; }
	inline void set_waypointEvent_2(UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * value)
	{
		___waypointEvent_2 = value;
		Il2CppCodeGenWriteBarrier((&___waypointEvent_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAYPOINTCOMPONENTS_T3DE523CAE0F5855ADF2B740DBBA68569E9034301_H
#ifndef UNITYEVENTBASE_T6E0F7823762EE94BB8489B5AE41C7802A266D3D5_H
#define UNITYEVENTBASE_T6E0F7823762EE94BB8489B5AE41C7802A266D3D5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * ___m_PersistentCalls_1;
	// System.String UnityEngine.Events.UnityEventBase::m_TypeName
	String_t* ___m_TypeName_2;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_3;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_Calls_0)); }
	inline InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Calls_0), value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentCalls_1), value);
	}

	inline static int32_t get_offset_of_m_TypeName_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_TypeName_2)); }
	inline String_t* get_m_TypeName_2() const { return ___m_TypeName_2; }
	inline String_t** get_address_of_m_TypeName_2() { return &___m_TypeName_2; }
	inline void set_m_TypeName_2(String_t* value)
	{
		___m_TypeName_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeName_2), value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_3() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_CallsDirty_3)); }
	inline bool get_m_CallsDirty_3() const { return ___m_CallsDirty_3; }
	inline bool* get_address_of_m_CallsDirty_3() { return &___m_CallsDirty_3; }
	inline void set_m_CallsDirty_3(bool value)
	{
		___m_CallsDirty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTBASE_T6E0F7823762EE94BB8489B5AE41C7802A266D3D5_H
#ifndef BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#define BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifndef ENUMERATOR_TE0C99528D3DCE5863566CE37BD94162A4C0431CD_H
#define ENUMERATOR_TE0C99528D3DCE5863566CE37BD94162A4C0431CD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1_Enumerator<System.Object>
struct  Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1_Enumerator::list
	List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * ___list_0;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1_Enumerator::current
	RuntimeObject * ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD, ___list_0)); }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * get_list_0() const { return ___list_0; }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD, ___current_3)); }
	inline RuntimeObject * get_current_3() const { return ___current_3; }
	inline RuntimeObject ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(RuntimeObject * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_TE0C99528D3DCE5863566CE37BD94162A4C0431CD_H
#ifndef ENUMERATOR_T6754DF054050219F46CEED0786A14A987A139408_H
#define ENUMERATOR_T6754DF054050219F46CEED0786A14A987A139408_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1_Enumerator<UnityEngine.XR.ARFoundation.ARTrackedImage>
struct  Enumerator_t6754DF054050219F46CEED0786A14A987A139408 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1_Enumerator::list
	List_1_t85456E82777B8C7F9CFFED2C540D0613542A8C8D * ___list_0;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1_Enumerator::current
	ARTrackedImage_tD699A1D6CA301ECE200D07D1CF1722E94AFEF01A * ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_t6754DF054050219F46CEED0786A14A987A139408, ___list_0)); }
	inline List_1_t85456E82777B8C7F9CFFED2C540D0613542A8C8D * get_list_0() const { return ___list_0; }
	inline List_1_t85456E82777B8C7F9CFFED2C540D0613542A8C8D ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t85456E82777B8C7F9CFFED2C540D0613542A8C8D * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_t6754DF054050219F46CEED0786A14A987A139408, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_t6754DF054050219F46CEED0786A14A987A139408, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t6754DF054050219F46CEED0786A14A987A139408, ___current_3)); }
	inline ARTrackedImage_tD699A1D6CA301ECE200D07D1CF1722E94AFEF01A * get_current_3() const { return ___current_3; }
	inline ARTrackedImage_tD699A1D6CA301ECE200D07D1CF1722E94AFEF01A ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(ARTrackedImage_tD699A1D6CA301ECE200D07D1CF1722E94AFEF01A * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T6754DF054050219F46CEED0786A14A987A139408_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef GUID_T_H
#define GUID_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Guid
struct  Guid_t 
{
public:
	// System.Int32 System.Guid::_a
	int32_t ____a_1;
	// System.Int16 System.Guid::_b
	int16_t ____b_2;
	// System.Int16 System.Guid::_c
	int16_t ____c_3;
	// System.Byte System.Guid::_d
	uint8_t ____d_4;
	// System.Byte System.Guid::_e
	uint8_t ____e_5;
	// System.Byte System.Guid::_f
	uint8_t ____f_6;
	// System.Byte System.Guid::_g
	uint8_t ____g_7;
	// System.Byte System.Guid::_h
	uint8_t ____h_8;
	// System.Byte System.Guid::_i
	uint8_t ____i_9;
	// System.Byte System.Guid::_j
	uint8_t ____j_10;
	// System.Byte System.Guid::_k
	uint8_t ____k_11;

public:
	inline static int32_t get_offset_of__a_1() { return static_cast<int32_t>(offsetof(Guid_t, ____a_1)); }
	inline int32_t get__a_1() const { return ____a_1; }
	inline int32_t* get_address_of__a_1() { return &____a_1; }
	inline void set__a_1(int32_t value)
	{
		____a_1 = value;
	}

	inline static int32_t get_offset_of__b_2() { return static_cast<int32_t>(offsetof(Guid_t, ____b_2)); }
	inline int16_t get__b_2() const { return ____b_2; }
	inline int16_t* get_address_of__b_2() { return &____b_2; }
	inline void set__b_2(int16_t value)
	{
		____b_2 = value;
	}

	inline static int32_t get_offset_of__c_3() { return static_cast<int32_t>(offsetof(Guid_t, ____c_3)); }
	inline int16_t get__c_3() const { return ____c_3; }
	inline int16_t* get_address_of__c_3() { return &____c_3; }
	inline void set__c_3(int16_t value)
	{
		____c_3 = value;
	}

	inline static int32_t get_offset_of__d_4() { return static_cast<int32_t>(offsetof(Guid_t, ____d_4)); }
	inline uint8_t get__d_4() const { return ____d_4; }
	inline uint8_t* get_address_of__d_4() { return &____d_4; }
	inline void set__d_4(uint8_t value)
	{
		____d_4 = value;
	}

	inline static int32_t get_offset_of__e_5() { return static_cast<int32_t>(offsetof(Guid_t, ____e_5)); }
	inline uint8_t get__e_5() const { return ____e_5; }
	inline uint8_t* get_address_of__e_5() { return &____e_5; }
	inline void set__e_5(uint8_t value)
	{
		____e_5 = value;
	}

	inline static int32_t get_offset_of__f_6() { return static_cast<int32_t>(offsetof(Guid_t, ____f_6)); }
	inline uint8_t get__f_6() const { return ____f_6; }
	inline uint8_t* get_address_of__f_6() { return &____f_6; }
	inline void set__f_6(uint8_t value)
	{
		____f_6 = value;
	}

	inline static int32_t get_offset_of__g_7() { return static_cast<int32_t>(offsetof(Guid_t, ____g_7)); }
	inline uint8_t get__g_7() const { return ____g_7; }
	inline uint8_t* get_address_of__g_7() { return &____g_7; }
	inline void set__g_7(uint8_t value)
	{
		____g_7 = value;
	}

	inline static int32_t get_offset_of__h_8() { return static_cast<int32_t>(offsetof(Guid_t, ____h_8)); }
	inline uint8_t get__h_8() const { return ____h_8; }
	inline uint8_t* get_address_of__h_8() { return &____h_8; }
	inline void set__h_8(uint8_t value)
	{
		____h_8 = value;
	}

	inline static int32_t get_offset_of__i_9() { return static_cast<int32_t>(offsetof(Guid_t, ____i_9)); }
	inline uint8_t get__i_9() const { return ____i_9; }
	inline uint8_t* get_address_of__i_9() { return &____i_9; }
	inline void set__i_9(uint8_t value)
	{
		____i_9 = value;
	}

	inline static int32_t get_offset_of__j_10() { return static_cast<int32_t>(offsetof(Guid_t, ____j_10)); }
	inline uint8_t get__j_10() const { return ____j_10; }
	inline uint8_t* get_address_of__j_10() { return &____j_10; }
	inline void set__j_10(uint8_t value)
	{
		____j_10 = value;
	}

	inline static int32_t get_offset_of__k_11() { return static_cast<int32_t>(offsetof(Guid_t, ____k_11)); }
	inline uint8_t get__k_11() const { return ____k_11; }
	inline uint8_t* get_address_of__k_11() { return &____k_11; }
	inline void set__k_11(uint8_t value)
	{
		____k_11 = value;
	}
};

struct Guid_t_StaticFields
{
public:
	// System.Guid System.Guid::Empty
	Guid_t  ___Empty_0;
	// System.Object System.Guid::_rngAccess
	RuntimeObject * ____rngAccess_12;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_rng
	RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * ____rng_13;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_fastRng
	RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * ____fastRng_14;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ___Empty_0)); }
	inline Guid_t  get_Empty_0() const { return ___Empty_0; }
	inline Guid_t * get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(Guid_t  value)
	{
		___Empty_0 = value;
	}

	inline static int32_t get_offset_of__rngAccess_12() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rngAccess_12)); }
	inline RuntimeObject * get__rngAccess_12() const { return ____rngAccess_12; }
	inline RuntimeObject ** get_address_of__rngAccess_12() { return &____rngAccess_12; }
	inline void set__rngAccess_12(RuntimeObject * value)
	{
		____rngAccess_12 = value;
		Il2CppCodeGenWriteBarrier((&____rngAccess_12), value);
	}

	inline static int32_t get_offset_of__rng_13() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rng_13)); }
	inline RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * get__rng_13() const { return ____rng_13; }
	inline RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 ** get_address_of__rng_13() { return &____rng_13; }
	inline void set__rng_13(RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * value)
	{
		____rng_13 = value;
		Il2CppCodeGenWriteBarrier((&____rng_13), value);
	}

	inline static int32_t get_offset_of__fastRng_14() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____fastRng_14)); }
	inline RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * get__fastRng_14() const { return ____fastRng_14; }
	inline RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 ** get_address_of__fastRng_14() { return &____fastRng_14; }
	inline void set__fastRng_14(RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * value)
	{
		____fastRng_14 = value;
		Il2CppCodeGenWriteBarrier((&____fastRng_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUID_T_H
#ifndef INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#define INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t585191389E07734F19F3156FF88FB3EF4800D102 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t585191389E07734F19F3156FF88FB3EF4800D102, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef SINGLE_TDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_H
#define SINGLE_TDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_TDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_H
#ifndef VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#define VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifndef ANIMATORSTATEINFO_TF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2_H
#define ANIMATORSTATEINFO_TF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AnimatorStateInfo
struct  AnimatorStateInfo_tF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2 
{
public:
	// System.Int32 UnityEngine.AnimatorStateInfo::m_Name
	int32_t ___m_Name_0;
	// System.Int32 UnityEngine.AnimatorStateInfo::m_Path
	int32_t ___m_Path_1;
	// System.Int32 UnityEngine.AnimatorStateInfo::m_FullPath
	int32_t ___m_FullPath_2;
	// System.Single UnityEngine.AnimatorStateInfo::m_NormalizedTime
	float ___m_NormalizedTime_3;
	// System.Single UnityEngine.AnimatorStateInfo::m_Length
	float ___m_Length_4;
	// System.Single UnityEngine.AnimatorStateInfo::m_Speed
	float ___m_Speed_5;
	// System.Single UnityEngine.AnimatorStateInfo::m_SpeedMultiplier
	float ___m_SpeedMultiplier_6;
	// System.Int32 UnityEngine.AnimatorStateInfo::m_Tag
	int32_t ___m_Tag_7;
	// System.Int32 UnityEngine.AnimatorStateInfo::m_Loop
	int32_t ___m_Loop_8;

public:
	inline static int32_t get_offset_of_m_Name_0() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_tF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2, ___m_Name_0)); }
	inline int32_t get_m_Name_0() const { return ___m_Name_0; }
	inline int32_t* get_address_of_m_Name_0() { return &___m_Name_0; }
	inline void set_m_Name_0(int32_t value)
	{
		___m_Name_0 = value;
	}

	inline static int32_t get_offset_of_m_Path_1() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_tF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2, ___m_Path_1)); }
	inline int32_t get_m_Path_1() const { return ___m_Path_1; }
	inline int32_t* get_address_of_m_Path_1() { return &___m_Path_1; }
	inline void set_m_Path_1(int32_t value)
	{
		___m_Path_1 = value;
	}

	inline static int32_t get_offset_of_m_FullPath_2() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_tF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2, ___m_FullPath_2)); }
	inline int32_t get_m_FullPath_2() const { return ___m_FullPath_2; }
	inline int32_t* get_address_of_m_FullPath_2() { return &___m_FullPath_2; }
	inline void set_m_FullPath_2(int32_t value)
	{
		___m_FullPath_2 = value;
	}

	inline static int32_t get_offset_of_m_NormalizedTime_3() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_tF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2, ___m_NormalizedTime_3)); }
	inline float get_m_NormalizedTime_3() const { return ___m_NormalizedTime_3; }
	inline float* get_address_of_m_NormalizedTime_3() { return &___m_NormalizedTime_3; }
	inline void set_m_NormalizedTime_3(float value)
	{
		___m_NormalizedTime_3 = value;
	}

	inline static int32_t get_offset_of_m_Length_4() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_tF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2, ___m_Length_4)); }
	inline float get_m_Length_4() const { return ___m_Length_4; }
	inline float* get_address_of_m_Length_4() { return &___m_Length_4; }
	inline void set_m_Length_4(float value)
	{
		___m_Length_4 = value;
	}

	inline static int32_t get_offset_of_m_Speed_5() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_tF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2, ___m_Speed_5)); }
	inline float get_m_Speed_5() const { return ___m_Speed_5; }
	inline float* get_address_of_m_Speed_5() { return &___m_Speed_5; }
	inline void set_m_Speed_5(float value)
	{
		___m_Speed_5 = value;
	}

	inline static int32_t get_offset_of_m_SpeedMultiplier_6() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_tF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2, ___m_SpeedMultiplier_6)); }
	inline float get_m_SpeedMultiplier_6() const { return ___m_SpeedMultiplier_6; }
	inline float* get_address_of_m_SpeedMultiplier_6() { return &___m_SpeedMultiplier_6; }
	inline void set_m_SpeedMultiplier_6(float value)
	{
		___m_SpeedMultiplier_6 = value;
	}

	inline static int32_t get_offset_of_m_Tag_7() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_tF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2, ___m_Tag_7)); }
	inline int32_t get_m_Tag_7() const { return ___m_Tag_7; }
	inline int32_t* get_address_of_m_Tag_7() { return &___m_Tag_7; }
	inline void set_m_Tag_7(int32_t value)
	{
		___m_Tag_7 = value;
	}

	inline static int32_t get_offset_of_m_Loop_8() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_tF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2, ___m_Loop_8)); }
	inline int32_t get_m_Loop_8() const { return ___m_Loop_8; }
	inline int32_t* get_address_of_m_Loop_8() { return &___m_Loop_8; }
	inline void set_m_Loop_8(int32_t value)
	{
		___m_Loop_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATORSTATEINFO_TF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2_H
#ifndef UNITYEVENT_T5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F_H
#define UNITYEVENT_T5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent
struct  UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_T5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F_H
#ifndef QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#define QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifndef VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#define VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifndef VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#define VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifndef ARTRACKEDIMAGESCHANGEDEVENTARGS_T64F82ABE51189C62E3811D96F2C2143DB91669E4_H
#define ARTRACKEDIMAGESCHANGEDEVENTARGS_T64F82ABE51189C62E3811D96F2C2143DB91669E4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARFoundation.ARTrackedImagesChangedEventArgs
struct  ARTrackedImagesChangedEventArgs_t64F82ABE51189C62E3811D96F2C2143DB91669E4 
{
public:
	// System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARTrackedImage> UnityEngine.XR.ARFoundation.ARTrackedImagesChangedEventArgs::<added>k__BackingField
	List_1_t85456E82777B8C7F9CFFED2C540D0613542A8C8D * ___U3CaddedU3Ek__BackingField_0;
	// System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARTrackedImage> UnityEngine.XR.ARFoundation.ARTrackedImagesChangedEventArgs::<updated>k__BackingField
	List_1_t85456E82777B8C7F9CFFED2C540D0613542A8C8D * ___U3CupdatedU3Ek__BackingField_1;
	// System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARTrackedImage> UnityEngine.XR.ARFoundation.ARTrackedImagesChangedEventArgs::<removed>k__BackingField
	List_1_t85456E82777B8C7F9CFFED2C540D0613542A8C8D * ___U3CremovedU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CaddedU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ARTrackedImagesChangedEventArgs_t64F82ABE51189C62E3811D96F2C2143DB91669E4, ___U3CaddedU3Ek__BackingField_0)); }
	inline List_1_t85456E82777B8C7F9CFFED2C540D0613542A8C8D * get_U3CaddedU3Ek__BackingField_0() const { return ___U3CaddedU3Ek__BackingField_0; }
	inline List_1_t85456E82777B8C7F9CFFED2C540D0613542A8C8D ** get_address_of_U3CaddedU3Ek__BackingField_0() { return &___U3CaddedU3Ek__BackingField_0; }
	inline void set_U3CaddedU3Ek__BackingField_0(List_1_t85456E82777B8C7F9CFFED2C540D0613542A8C8D * value)
	{
		___U3CaddedU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CaddedU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CupdatedU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ARTrackedImagesChangedEventArgs_t64F82ABE51189C62E3811D96F2C2143DB91669E4, ___U3CupdatedU3Ek__BackingField_1)); }
	inline List_1_t85456E82777B8C7F9CFFED2C540D0613542A8C8D * get_U3CupdatedU3Ek__BackingField_1() const { return ___U3CupdatedU3Ek__BackingField_1; }
	inline List_1_t85456E82777B8C7F9CFFED2C540D0613542A8C8D ** get_address_of_U3CupdatedU3Ek__BackingField_1() { return &___U3CupdatedU3Ek__BackingField_1; }
	inline void set_U3CupdatedU3Ek__BackingField_1(List_1_t85456E82777B8C7F9CFFED2C540D0613542A8C8D * value)
	{
		___U3CupdatedU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CupdatedU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CremovedU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ARTrackedImagesChangedEventArgs_t64F82ABE51189C62E3811D96F2C2143DB91669E4, ___U3CremovedU3Ek__BackingField_2)); }
	inline List_1_t85456E82777B8C7F9CFFED2C540D0613542A8C8D * get_U3CremovedU3Ek__BackingField_2() const { return ___U3CremovedU3Ek__BackingField_2; }
	inline List_1_t85456E82777B8C7F9CFFED2C540D0613542A8C8D ** get_address_of_U3CremovedU3Ek__BackingField_2() { return &___U3CremovedU3Ek__BackingField_2; }
	inline void set_U3CremovedU3Ek__BackingField_2(List_1_t85456E82777B8C7F9CFFED2C540D0613542A8C8D * value)
	{
		___U3CremovedU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CremovedU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.XR.ARFoundation.ARTrackedImagesChangedEventArgs
struct ARTrackedImagesChangedEventArgs_t64F82ABE51189C62E3811D96F2C2143DB91669E4_marshaled_pinvoke
{
	List_1_t85456E82777B8C7F9CFFED2C540D0613542A8C8D * ___U3CaddedU3Ek__BackingField_0;
	List_1_t85456E82777B8C7F9CFFED2C540D0613542A8C8D * ___U3CupdatedU3Ek__BackingField_1;
	List_1_t85456E82777B8C7F9CFFED2C540D0613542A8C8D * ___U3CremovedU3Ek__BackingField_2;
};
// Native definition for COM marshalling of UnityEngine.XR.ARFoundation.ARTrackedImagesChangedEventArgs
struct ARTrackedImagesChangedEventArgs_t64F82ABE51189C62E3811D96F2C2143DB91669E4_marshaled_com
{
	List_1_t85456E82777B8C7F9CFFED2C540D0613542A8C8D * ___U3CaddedU3Ek__BackingField_0;
	List_1_t85456E82777B8C7F9CFFED2C540D0613542A8C8D * ___U3CupdatedU3Ek__BackingField_1;
	List_1_t85456E82777B8C7F9CFFED2C540D0613542A8C8D * ___U3CremovedU3Ek__BackingField_2;
};
#endif // ARTRACKEDIMAGESCHANGEDEVENTARGS_T64F82ABE51189C62E3811D96F2C2143DB91669E4_H
#ifndef SERIALIZABLEGUID_TF7CD988878BEBB3281FA1C06B4457569DFD75821_H
#define SERIALIZABLEGUID_TF7CD988878BEBB3281FA1C06B4457569DFD75821_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARSubsystems.SerializableGuid
struct  SerializableGuid_tF7CD988878BEBB3281FA1C06B4457569DFD75821 
{
public:
	// System.UInt64 UnityEngine.XR.ARSubsystems.SerializableGuid::m_GuidLow
	uint64_t ___m_GuidLow_0;
	// System.UInt64 UnityEngine.XR.ARSubsystems.SerializableGuid::m_GuidHigh
	uint64_t ___m_GuidHigh_1;

public:
	inline static int32_t get_offset_of_m_GuidLow_0() { return static_cast<int32_t>(offsetof(SerializableGuid_tF7CD988878BEBB3281FA1C06B4457569DFD75821, ___m_GuidLow_0)); }
	inline uint64_t get_m_GuidLow_0() const { return ___m_GuidLow_0; }
	inline uint64_t* get_address_of_m_GuidLow_0() { return &___m_GuidLow_0; }
	inline void set_m_GuidLow_0(uint64_t value)
	{
		___m_GuidLow_0 = value;
	}

	inline static int32_t get_offset_of_m_GuidHigh_1() { return static_cast<int32_t>(offsetof(SerializableGuid_tF7CD988878BEBB3281FA1C06B4457569DFD75821, ___m_GuidHigh_1)); }
	inline uint64_t get_m_GuidHigh_1() const { return ___m_GuidHigh_1; }
	inline uint64_t* get_address_of_m_GuidHigh_1() { return &___m_GuidHigh_1; }
	inline void set_m_GuidHigh_1(uint64_t value)
	{
		___m_GuidHigh_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEGUID_TF7CD988878BEBB3281FA1C06B4457569DFD75821_H
#ifndef TRACKABLEID_TA7E19AFE62176E25E3759548887E9068E1E4AE47_H
#define TRACKABLEID_TA7E19AFE62176E25E3759548887E9068E1E4AE47_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARSubsystems.TrackableId
struct  TrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47 
{
public:
	// System.UInt64 UnityEngine.XR.ARSubsystems.TrackableId::m_SubId1
	uint64_t ___m_SubId1_1;
	// System.UInt64 UnityEngine.XR.ARSubsystems.TrackableId::m_SubId2
	uint64_t ___m_SubId2_2;

public:
	inline static int32_t get_offset_of_m_SubId1_1() { return static_cast<int32_t>(offsetof(TrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47, ___m_SubId1_1)); }
	inline uint64_t get_m_SubId1_1() const { return ___m_SubId1_1; }
	inline uint64_t* get_address_of_m_SubId1_1() { return &___m_SubId1_1; }
	inline void set_m_SubId1_1(uint64_t value)
	{
		___m_SubId1_1 = value;
	}

	inline static int32_t get_offset_of_m_SubId2_2() { return static_cast<int32_t>(offsetof(TrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47, ___m_SubId2_2)); }
	inline uint64_t get_m_SubId2_2() const { return ___m_SubId2_2; }
	inline uint64_t* get_address_of_m_SubId2_2() { return &___m_SubId2_2; }
	inline void set_m_SubId2_2(uint64_t value)
	{
		___m_SubId2_2 = value;
	}
};

struct TrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47_StaticFields
{
public:
	// UnityEngine.XR.ARSubsystems.TrackableId UnityEngine.XR.ARSubsystems.TrackableId::s_InvalidId
	TrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47  ___s_InvalidId_0;

public:
	inline static int32_t get_offset_of_s_InvalidId_0() { return static_cast<int32_t>(offsetof(TrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47_StaticFields, ___s_InvalidId_0)); }
	inline TrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47  get_s_InvalidId_0() const { return ___s_InvalidId_0; }
	inline TrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47 * get_address_of_s_InvalidId_0() { return &___s_InvalidId_0; }
	inline void set_s_InvalidId_0(TrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47  value)
	{
		___s_InvalidId_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLEID_TA7E19AFE62176E25E3759548887E9068E1E4AE47_H
#ifndef DELEGATE_T_H
#define DELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T_H
#ifndef BINDINGFLAGS_TE35C91D046E63A1B92BB9AB909FCF9DA84379ED0_H
#define BINDINGFLAGS_TE35C91D046E63A1B92BB9AB909FCF9DA84379ED0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.BindingFlags
struct  BindingFlags_tE35C91D046E63A1B92BB9AB909FCF9DA84379ED0 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tE35C91D046E63A1B92BB9AB909FCF9DA84379ED0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDINGFLAGS_TE35C91D046E63A1B92BB9AB909FCF9DA84379ED0_H
#ifndef RUNTIMETYPEHANDLE_T7B542280A22F0EC4EAC2061C29178845847A8B2D_H
#define RUNTIMETYPEHANDLE_T7B542280A22F0EC4EAC2061C29178845847A8B2D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.RuntimeTypeHandle
struct  RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMETYPEHANDLE_T7B542280A22F0EC4EAC2061C29178845847A8B2D_H
#ifndef SWITCH_T477EAF7EDB3539E13C2AB958F50647A0D1892299_H
#define SWITCH_T477EAF7EDB3539E13C2AB958F50647A0D1892299_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TurnTheGameOn.ArrowWaypointer.WaypointController_Switch
struct  Switch_t477EAF7EDB3539E13C2AB958F50647A0D1892299 
{
public:
	// System.Int32 TurnTheGameOn.ArrowWaypointer.WaypointController_Switch::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Switch_t477EAF7EDB3539E13C2AB958F50647A0D1892299, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWITCH_T477EAF7EDB3539E13C2AB958F50647A0D1892299_H
#ifndef KEYCODE_TC93EA87C5A6901160B583ADFCD3EF6726570DC3C_H
#define KEYCODE_TC93EA87C5A6901160B583ADFCD3EF6726570DC3C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.KeyCode
struct  KeyCode_tC93EA87C5A6901160B583ADFCD3EF6726570DC3C 
{
public:
	// System.Int32 UnityEngine.KeyCode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(KeyCode_tC93EA87C5A6901160B583ADFCD3EF6726570DC3C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYCODE_TC93EA87C5A6901160B583ADFCD3EF6726570DC3C_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef POSE_T2997DE3CB3863E4D78FCF42B46FC481818823F29_H
#define POSE_T2997DE3CB3863E4D78FCF42B46FC481818823F29_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Pose
struct  Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29 
{
public:
	// UnityEngine.Vector3 UnityEngine.Pose::position
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___position_0;
	// UnityEngine.Quaternion UnityEngine.Pose::rotation
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___rotation_1;

public:
	inline static int32_t get_offset_of_position_0() { return static_cast<int32_t>(offsetof(Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29, ___position_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_position_0() const { return ___position_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_position_0() { return &___position_0; }
	inline void set_position_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___position_0 = value;
	}

	inline static int32_t get_offset_of_rotation_1() { return static_cast<int32_t>(offsetof(Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29, ___rotation_1)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_rotation_1() const { return ___rotation_1; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_rotation_1() { return &___rotation_1; }
	inline void set_rotation_1(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___rotation_1 = value;
	}
};

struct Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29_StaticFields
{
public:
	// UnityEngine.Pose UnityEngine.Pose::k_Identity
	Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29  ___k_Identity_2;

public:
	inline static int32_t get_offset_of_k_Identity_2() { return static_cast<int32_t>(offsetof(Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29_StaticFields, ___k_Identity_2)); }
	inline Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29  get_k_Identity_2() const { return ___k_Identity_2; }
	inline Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29 * get_address_of_k_Identity_2() { return &___k_Identity_2; }
	inline void set_k_Identity_2(Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29  value)
	{
		___k_Identity_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSE_T2997DE3CB3863E4D78FCF42B46FC481818823F29_H
#ifndef RAY_TE2163D4CB3E6B267E29F8ABE41684490E4A614B2_H
#define RAY_TE2163D4CB3E6B267E29F8ABE41684490E4A614B2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Ray
struct  Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2 
{
public:
	// UnityEngine.Vector3 UnityEngine.Ray::m_Origin
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Origin_0;
	// UnityEngine.Vector3 UnityEngine.Ray::m_Direction
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Direction_1;

public:
	inline static int32_t get_offset_of_m_Origin_0() { return static_cast<int32_t>(offsetof(Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2, ___m_Origin_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Origin_0() const { return ___m_Origin_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Origin_0() { return &___m_Origin_0; }
	inline void set_m_Origin_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Origin_0 = value;
	}

	inline static int32_t get_offset_of_m_Direction_1() { return static_cast<int32_t>(offsetof(Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2, ___m_Direction_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Direction_1() const { return ___m_Direction_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Direction_1() { return &___m_Direction_1; }
	inline void set_m_Direction_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Direction_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAY_TE2163D4CB3E6B267E29F8ABE41684490E4A614B2_H
#ifndef RAYCASTHIT_T19695F18F9265FE5425062BBA6A4D330480538C3_H
#define RAYCASTHIT_T19695F18F9265FE5425062BBA6A4D330480538C3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RaycastHit
struct  RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 
{
public:
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Point
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Point_0;
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Normal
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Normal_1;
	// System.UInt32 UnityEngine.RaycastHit::m_FaceID
	uint32_t ___m_FaceID_2;
	// System.Single UnityEngine.RaycastHit::m_Distance
	float ___m_Distance_3;
	// UnityEngine.Vector2 UnityEngine.RaycastHit::m_UV
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_UV_4;
	// System.Int32 UnityEngine.RaycastHit::m_Collider
	int32_t ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Point_0() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_Point_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Point_0() const { return ___m_Point_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Point_0() { return &___m_Point_0; }
	inline void set_m_Point_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Point_0 = value;
	}

	inline static int32_t get_offset_of_m_Normal_1() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_Normal_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Normal_1() const { return ___m_Normal_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Normal_1() { return &___m_Normal_1; }
	inline void set_m_Normal_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Normal_1 = value;
	}

	inline static int32_t get_offset_of_m_FaceID_2() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_FaceID_2)); }
	inline uint32_t get_m_FaceID_2() const { return ___m_FaceID_2; }
	inline uint32_t* get_address_of_m_FaceID_2() { return &___m_FaceID_2; }
	inline void set_m_FaceID_2(uint32_t value)
	{
		___m_FaceID_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_UV_4() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_UV_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_UV_4() const { return ___m_UV_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_UV_4() { return &___m_UV_4; }
	inline void set_m_UV_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_UV_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_Collider_5)); }
	inline int32_t get_m_Collider_5() const { return ___m_Collider_5; }
	inline int32_t* get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(int32_t value)
	{
		___m_Collider_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCASTHIT_T19695F18F9265FE5425062BBA6A4D330480538C3_H
#ifndef RIGIDBODYCONSTRAINTS_TC57FF2BFB454674CE1FDEF9EEDAF5FCFAB23FFC8_H
#define RIGIDBODYCONSTRAINTS_TC57FF2BFB454674CE1FDEF9EEDAF5FCFAB23FFC8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RigidbodyConstraints
struct  RigidbodyConstraints_tC57FF2BFB454674CE1FDEF9EEDAF5FCFAB23FFC8 
{
public:
	// System.Int32 UnityEngine.RigidbodyConstraints::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RigidbodyConstraints_tC57FF2BFB454674CE1FDEF9EEDAF5FCFAB23FFC8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RIGIDBODYCONSTRAINTS_TC57FF2BFB454674CE1FDEF9EEDAF5FCFAB23FFC8_H
#ifndef TRACKINGSTATE_T124D9E603E4E0453A85409CF7762EE8C946233F6_H
#define TRACKINGSTATE_T124D9E603E4E0453A85409CF7762EE8C946233F6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARSubsystems.TrackingState
struct  TrackingState_t124D9E603E4E0453A85409CF7762EE8C946233F6 
{
public:
	// System.Int32 UnityEngine.XR.ARSubsystems.TrackingState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TrackingState_t124D9E603E4E0453A85409CF7762EE8C946233F6, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKINGSTATE_T124D9E603E4E0453A85409CF7762EE8C946233F6_H
#ifndef XRREFERENCEIMAGE_T5A53387AC6253D5D3DFD62BC583A45BBDFC1347E_H
#define XRREFERENCEIMAGE_T5A53387AC6253D5D3DFD62BC583A45BBDFC1347E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARSubsystems.XRReferenceImage
struct  XRReferenceImage_t5A53387AC6253D5D3DFD62BC583A45BBDFC1347E 
{
public:
	// UnityEngine.XR.ARSubsystems.SerializableGuid UnityEngine.XR.ARSubsystems.XRReferenceImage::m_SerializedGuid
	SerializableGuid_tF7CD988878BEBB3281FA1C06B4457569DFD75821  ___m_SerializedGuid_0;
	// UnityEngine.XR.ARSubsystems.SerializableGuid UnityEngine.XR.ARSubsystems.XRReferenceImage::m_SerializedTextureGuid
	SerializableGuid_tF7CD988878BEBB3281FA1C06B4457569DFD75821  ___m_SerializedTextureGuid_1;
	// UnityEngine.Vector2 UnityEngine.XR.ARSubsystems.XRReferenceImage::m_Size
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_Size_2;
	// System.Boolean UnityEngine.XR.ARSubsystems.XRReferenceImage::m_SpecifySize
	bool ___m_SpecifySize_3;
	// System.String UnityEngine.XR.ARSubsystems.XRReferenceImage::m_Name
	String_t* ___m_Name_4;
	// UnityEngine.Texture2D UnityEngine.XR.ARSubsystems.XRReferenceImage::m_Texture
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___m_Texture_5;

public:
	inline static int32_t get_offset_of_m_SerializedGuid_0() { return static_cast<int32_t>(offsetof(XRReferenceImage_t5A53387AC6253D5D3DFD62BC583A45BBDFC1347E, ___m_SerializedGuid_0)); }
	inline SerializableGuid_tF7CD988878BEBB3281FA1C06B4457569DFD75821  get_m_SerializedGuid_0() const { return ___m_SerializedGuid_0; }
	inline SerializableGuid_tF7CD988878BEBB3281FA1C06B4457569DFD75821 * get_address_of_m_SerializedGuid_0() { return &___m_SerializedGuid_0; }
	inline void set_m_SerializedGuid_0(SerializableGuid_tF7CD988878BEBB3281FA1C06B4457569DFD75821  value)
	{
		___m_SerializedGuid_0 = value;
	}

	inline static int32_t get_offset_of_m_SerializedTextureGuid_1() { return static_cast<int32_t>(offsetof(XRReferenceImage_t5A53387AC6253D5D3DFD62BC583A45BBDFC1347E, ___m_SerializedTextureGuid_1)); }
	inline SerializableGuid_tF7CD988878BEBB3281FA1C06B4457569DFD75821  get_m_SerializedTextureGuid_1() const { return ___m_SerializedTextureGuid_1; }
	inline SerializableGuid_tF7CD988878BEBB3281FA1C06B4457569DFD75821 * get_address_of_m_SerializedTextureGuid_1() { return &___m_SerializedTextureGuid_1; }
	inline void set_m_SerializedTextureGuid_1(SerializableGuid_tF7CD988878BEBB3281FA1C06B4457569DFD75821  value)
	{
		___m_SerializedTextureGuid_1 = value;
	}

	inline static int32_t get_offset_of_m_Size_2() { return static_cast<int32_t>(offsetof(XRReferenceImage_t5A53387AC6253D5D3DFD62BC583A45BBDFC1347E, ___m_Size_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_Size_2() const { return ___m_Size_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_Size_2() { return &___m_Size_2; }
	inline void set_m_Size_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_Size_2 = value;
	}

	inline static int32_t get_offset_of_m_SpecifySize_3() { return static_cast<int32_t>(offsetof(XRReferenceImage_t5A53387AC6253D5D3DFD62BC583A45BBDFC1347E, ___m_SpecifySize_3)); }
	inline bool get_m_SpecifySize_3() const { return ___m_SpecifySize_3; }
	inline bool* get_address_of_m_SpecifySize_3() { return &___m_SpecifySize_3; }
	inline void set_m_SpecifySize_3(bool value)
	{
		___m_SpecifySize_3 = value;
	}

	inline static int32_t get_offset_of_m_Name_4() { return static_cast<int32_t>(offsetof(XRReferenceImage_t5A53387AC6253D5D3DFD62BC583A45BBDFC1347E, ___m_Name_4)); }
	inline String_t* get_m_Name_4() const { return ___m_Name_4; }
	inline String_t** get_address_of_m_Name_4() { return &___m_Name_4; }
	inline void set_m_Name_4(String_t* value)
	{
		___m_Name_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Name_4), value);
	}

	inline static int32_t get_offset_of_m_Texture_5() { return static_cast<int32_t>(offsetof(XRReferenceImage_t5A53387AC6253D5D3DFD62BC583A45BBDFC1347E, ___m_Texture_5)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_m_Texture_5() const { return ___m_Texture_5; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_m_Texture_5() { return &___m_Texture_5; }
	inline void set_m_Texture_5(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___m_Texture_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Texture_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.XR.ARSubsystems.XRReferenceImage
struct XRReferenceImage_t5A53387AC6253D5D3DFD62BC583A45BBDFC1347E_marshaled_pinvoke
{
	SerializableGuid_tF7CD988878BEBB3281FA1C06B4457569DFD75821  ___m_SerializedGuid_0;
	SerializableGuid_tF7CD988878BEBB3281FA1C06B4457569DFD75821  ___m_SerializedTextureGuid_1;
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_Size_2;
	int32_t ___m_SpecifySize_3;
	char* ___m_Name_4;
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___m_Texture_5;
};
// Native definition for COM marshalling of UnityEngine.XR.ARSubsystems.XRReferenceImage
struct XRReferenceImage_t5A53387AC6253D5D3DFD62BC583A45BBDFC1347E_marshaled_com
{
	SerializableGuid_tF7CD988878BEBB3281FA1C06B4457569DFD75821  ___m_SerializedGuid_0;
	SerializableGuid_tF7CD988878BEBB3281FA1C06B4457569DFD75821  ___m_SerializedTextureGuid_1;
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_Size_2;
	int32_t ___m_SpecifySize_3;
	Il2CppChar* ___m_Name_4;
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___m_Texture_5;
};
#endif // XRREFERENCEIMAGE_T5A53387AC6253D5D3DFD62BC583A45BBDFC1347E_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef TYPE_T_H
#define TYPE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Type
struct  Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t4D5CB06963501D32847C057B57157D6DC49CA759 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((&___FilterAttribute_0), value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((&___FilterName_1), value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((&___FilterNameIgnoreCase_2), value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((&___Missing_3), value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyTypes_5), value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t4D5CB06963501D32847C057B57157D6DC49CA759 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t4D5CB06963501D32847C057B57157D6DC49CA759 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t4D5CB06963501D32847C057B57157D6DC49CA759 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((&___defaultBinder_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef GAMEOBJECT_TBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_H
#define GAMEOBJECT_TBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GameObject
struct  GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECT_TBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_H
#ifndef XRTRACKEDIMAGE_T178EACF5BFA4228DF4EB1899685C533F3F296AA8_H
#define XRTRACKEDIMAGE_T178EACF5BFA4228DF4EB1899685C533F3F296AA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARSubsystems.XRTrackedImage
struct  XRTrackedImage_t178EACF5BFA4228DF4EB1899685C533F3F296AA8 
{
public:
	// UnityEngine.XR.ARSubsystems.TrackableId UnityEngine.XR.ARSubsystems.XRTrackedImage::m_Id
	TrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47  ___m_Id_0;
	// System.Guid UnityEngine.XR.ARSubsystems.XRTrackedImage::m_SourceImageId
	Guid_t  ___m_SourceImageId_1;
	// UnityEngine.Pose UnityEngine.XR.ARSubsystems.XRTrackedImage::m_Pose
	Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29  ___m_Pose_2;
	// UnityEngine.Vector2 UnityEngine.XR.ARSubsystems.XRTrackedImage::m_Size
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_Size_3;
	// UnityEngine.XR.ARSubsystems.TrackingState UnityEngine.XR.ARSubsystems.XRTrackedImage::m_TrackingState
	int32_t ___m_TrackingState_4;
	// System.IntPtr UnityEngine.XR.ARSubsystems.XRTrackedImage::m_NativePtr
	intptr_t ___m_NativePtr_5;

public:
	inline static int32_t get_offset_of_m_Id_0() { return static_cast<int32_t>(offsetof(XRTrackedImage_t178EACF5BFA4228DF4EB1899685C533F3F296AA8, ___m_Id_0)); }
	inline TrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47  get_m_Id_0() const { return ___m_Id_0; }
	inline TrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47 * get_address_of_m_Id_0() { return &___m_Id_0; }
	inline void set_m_Id_0(TrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47  value)
	{
		___m_Id_0 = value;
	}

	inline static int32_t get_offset_of_m_SourceImageId_1() { return static_cast<int32_t>(offsetof(XRTrackedImage_t178EACF5BFA4228DF4EB1899685C533F3F296AA8, ___m_SourceImageId_1)); }
	inline Guid_t  get_m_SourceImageId_1() const { return ___m_SourceImageId_1; }
	inline Guid_t * get_address_of_m_SourceImageId_1() { return &___m_SourceImageId_1; }
	inline void set_m_SourceImageId_1(Guid_t  value)
	{
		___m_SourceImageId_1 = value;
	}

	inline static int32_t get_offset_of_m_Pose_2() { return static_cast<int32_t>(offsetof(XRTrackedImage_t178EACF5BFA4228DF4EB1899685C533F3F296AA8, ___m_Pose_2)); }
	inline Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29  get_m_Pose_2() const { return ___m_Pose_2; }
	inline Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29 * get_address_of_m_Pose_2() { return &___m_Pose_2; }
	inline void set_m_Pose_2(Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29  value)
	{
		___m_Pose_2 = value;
	}

	inline static int32_t get_offset_of_m_Size_3() { return static_cast<int32_t>(offsetof(XRTrackedImage_t178EACF5BFA4228DF4EB1899685C533F3F296AA8, ___m_Size_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_Size_3() const { return ___m_Size_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_Size_3() { return &___m_Size_3; }
	inline void set_m_Size_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_Size_3 = value;
	}

	inline static int32_t get_offset_of_m_TrackingState_4() { return static_cast<int32_t>(offsetof(XRTrackedImage_t178EACF5BFA4228DF4EB1899685C533F3F296AA8, ___m_TrackingState_4)); }
	inline int32_t get_m_TrackingState_4() const { return ___m_TrackingState_4; }
	inline int32_t* get_address_of_m_TrackingState_4() { return &___m_TrackingState_4; }
	inline void set_m_TrackingState_4(int32_t value)
	{
		___m_TrackingState_4 = value;
	}

	inline static int32_t get_offset_of_m_NativePtr_5() { return static_cast<int32_t>(offsetof(XRTrackedImage_t178EACF5BFA4228DF4EB1899685C533F3F296AA8, ___m_NativePtr_5)); }
	inline intptr_t get_m_NativePtr_5() const { return ___m_NativePtr_5; }
	inline intptr_t* get_address_of_m_NativePtr_5() { return &___m_NativePtr_5; }
	inline void set_m_NativePtr_5(intptr_t value)
	{
		___m_NativePtr_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XRTRACKEDIMAGE_T178EACF5BFA4228DF4EB1899685C533F3F296AA8_H
#ifndef ACTION_1_T825CEDF2F074FBD059900D81615EFDE0AFD02CA0_H
#define ACTION_1_T825CEDF2F074FBD059900D81615EFDE0AFD02CA0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Action`1<UnityEngine.XR.ARFoundation.ARTrackedImagesChangedEventArgs>
struct  Action_1_t825CEDF2F074FBD059900D81615EFDE0AFD02CA0  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_1_T825CEDF2F074FBD059900D81615EFDE0AFD02CA0_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef COLLIDER_T0FEEB36760860AD21B3B1F0509C365B393EC4BDF_H
#define COLLIDER_T0FEEB36760860AD21B3B1F0509C365B393EC4BDF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Collider
struct  Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLIDER_T0FEEB36760860AD21B3B1F0509C365B393EC4BDF_H
#ifndef RIGIDBODY_TE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5_H
#define RIGIDBODY_TE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rigidbody
struct  Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RIGIDBODY_TE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5_H
#ifndef TRANSFORM_TBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA_H
#define TRANSFORM_TBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Transform
struct  Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORM_TBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA_H
#ifndef ANIMATOR_TF1A88E66B3B731DDA75A066DBAE9C55837660F5A_H
#define ANIMATOR_TF1A88E66B3B731DDA75A066DBAE9C55837660F5A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Animator
struct  Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATOR_TF1A88E66B3B731DDA75A066DBAE9C55837660F5A_H
#ifndef CAMERA_T48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_H
#define CAMERA_T48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Camera
struct  Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

struct Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_StaticFields
{
public:
	// UnityEngine.Camera_CameraCallback UnityEngine.Camera::onPreCull
	CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * ___onPreCull_4;
	// UnityEngine.Camera_CameraCallback UnityEngine.Camera::onPreRender
	CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * ___onPreRender_5;
	// UnityEngine.Camera_CameraCallback UnityEngine.Camera::onPostRender
	CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * ___onPostRender_6;

public:
	inline static int32_t get_offset_of_onPreCull_4() { return static_cast<int32_t>(offsetof(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_StaticFields, ___onPreCull_4)); }
	inline CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * get_onPreCull_4() const { return ___onPreCull_4; }
	inline CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 ** get_address_of_onPreCull_4() { return &___onPreCull_4; }
	inline void set_onPreCull_4(CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * value)
	{
		___onPreCull_4 = value;
		Il2CppCodeGenWriteBarrier((&___onPreCull_4), value);
	}

	inline static int32_t get_offset_of_onPreRender_5() { return static_cast<int32_t>(offsetof(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_StaticFields, ___onPreRender_5)); }
	inline CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * get_onPreRender_5() const { return ___onPreRender_5; }
	inline CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 ** get_address_of_onPreRender_5() { return &___onPreRender_5; }
	inline void set_onPreRender_5(CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * value)
	{
		___onPreRender_5 = value;
		Il2CppCodeGenWriteBarrier((&___onPreRender_5), value);
	}

	inline static int32_t get_offset_of_onPostRender_6() { return static_cast<int32_t>(offsetof(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_StaticFields, ___onPostRender_6)); }
	inline CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * get_onPostRender_6() const { return ___onPostRender_6; }
	inline CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 ** get_address_of_onPostRender_6() { return &___onPostRender_6; }
	inline void set_onPostRender_6(CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * value)
	{
		___onPostRender_6 = value;
		Il2CppCodeGenWriteBarrier((&___onPostRender_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERA_T48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_H
#ifndef CAPSULECOLLIDER_T5FD15B9E7BEEC4FFA8A2071E9FD2B8DEB3A826D1_H
#define CAPSULECOLLIDER_T5FD15B9E7BEEC4FFA8A2071E9FD2B8DEB3A826D1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CapsuleCollider
struct  CapsuleCollider_t5FD15B9E7BEEC4FFA8A2071E9FD2B8DEB3A826D1  : public Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAPSULECOLLIDER_T5FD15B9E7BEEC4FFA8A2071E9FD2B8DEB3A826D1_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef CHARACTERCONTROLLERTHIRDPERSON_T2CC0D121855408DC72A1CB71940065D5C220C886_H
#define CHARACTERCONTROLLERTHIRDPERSON_T2CC0D121855408DC72A1CB71940065D5C220C886_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CharacterControllerThirdPerson
struct  CharacterControllerThirdPerson_t2CC0D121855408DC72A1CB71940065D5C220C886  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single CharacterControllerThirdPerson::m_MovingTurnSpeed
	float ___m_MovingTurnSpeed_4;
	// System.Single CharacterControllerThirdPerson::m_StationaryTurnSpeed
	float ___m_StationaryTurnSpeed_5;
	// System.Single CharacterControllerThirdPerson::m_JumpPower
	float ___m_JumpPower_6;
	// System.Single CharacterControllerThirdPerson::m_GravityMultiplier
	float ___m_GravityMultiplier_7;
	// System.Single CharacterControllerThirdPerson::m_RunCycleLegOffset
	float ___m_RunCycleLegOffset_8;
	// System.Single CharacterControllerThirdPerson::m_MoveSpeedMultiplier
	float ___m_MoveSpeedMultiplier_9;
	// System.Single CharacterControllerThirdPerson::m_AnimSpeedMultiplier
	float ___m_AnimSpeedMultiplier_10;
	// System.Single CharacterControllerThirdPerson::m_GroundCheckDistance
	float ___m_GroundCheckDistance_11;
	// UnityEngine.Rigidbody CharacterControllerThirdPerson::m_Rigidbody
	Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * ___m_Rigidbody_12;
	// UnityEngine.Animator CharacterControllerThirdPerson::m_Animator
	Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * ___m_Animator_13;
	// System.Boolean CharacterControllerThirdPerson::m_IsGrounded
	bool ___m_IsGrounded_14;
	// System.Single CharacterControllerThirdPerson::m_OrigGroundCheckDistance
	float ___m_OrigGroundCheckDistance_15;
	// System.Single CharacterControllerThirdPerson::m_TurnAmount
	float ___m_TurnAmount_17;
	// System.Single CharacterControllerThirdPerson::m_ForwardAmount
	float ___m_ForwardAmount_18;
	// UnityEngine.Vector3 CharacterControllerThirdPerson::m_GroundNormal
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_GroundNormal_19;
	// System.Single CharacterControllerThirdPerson::m_CapsuleHeight
	float ___m_CapsuleHeight_20;
	// UnityEngine.Vector3 CharacterControllerThirdPerson::m_CapsuleCenter
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_CapsuleCenter_21;
	// UnityEngine.CapsuleCollider CharacterControllerThirdPerson::m_Capsule
	CapsuleCollider_t5FD15B9E7BEEC4FFA8A2071E9FD2B8DEB3A826D1 * ___m_Capsule_22;
	// System.Boolean CharacterControllerThirdPerson::m_Crouching
	bool ___m_Crouching_23;

public:
	inline static int32_t get_offset_of_m_MovingTurnSpeed_4() { return static_cast<int32_t>(offsetof(CharacterControllerThirdPerson_t2CC0D121855408DC72A1CB71940065D5C220C886, ___m_MovingTurnSpeed_4)); }
	inline float get_m_MovingTurnSpeed_4() const { return ___m_MovingTurnSpeed_4; }
	inline float* get_address_of_m_MovingTurnSpeed_4() { return &___m_MovingTurnSpeed_4; }
	inline void set_m_MovingTurnSpeed_4(float value)
	{
		___m_MovingTurnSpeed_4 = value;
	}

	inline static int32_t get_offset_of_m_StationaryTurnSpeed_5() { return static_cast<int32_t>(offsetof(CharacterControllerThirdPerson_t2CC0D121855408DC72A1CB71940065D5C220C886, ___m_StationaryTurnSpeed_5)); }
	inline float get_m_StationaryTurnSpeed_5() const { return ___m_StationaryTurnSpeed_5; }
	inline float* get_address_of_m_StationaryTurnSpeed_5() { return &___m_StationaryTurnSpeed_5; }
	inline void set_m_StationaryTurnSpeed_5(float value)
	{
		___m_StationaryTurnSpeed_5 = value;
	}

	inline static int32_t get_offset_of_m_JumpPower_6() { return static_cast<int32_t>(offsetof(CharacterControllerThirdPerson_t2CC0D121855408DC72A1CB71940065D5C220C886, ___m_JumpPower_6)); }
	inline float get_m_JumpPower_6() const { return ___m_JumpPower_6; }
	inline float* get_address_of_m_JumpPower_6() { return &___m_JumpPower_6; }
	inline void set_m_JumpPower_6(float value)
	{
		___m_JumpPower_6 = value;
	}

	inline static int32_t get_offset_of_m_GravityMultiplier_7() { return static_cast<int32_t>(offsetof(CharacterControllerThirdPerson_t2CC0D121855408DC72A1CB71940065D5C220C886, ___m_GravityMultiplier_7)); }
	inline float get_m_GravityMultiplier_7() const { return ___m_GravityMultiplier_7; }
	inline float* get_address_of_m_GravityMultiplier_7() { return &___m_GravityMultiplier_7; }
	inline void set_m_GravityMultiplier_7(float value)
	{
		___m_GravityMultiplier_7 = value;
	}

	inline static int32_t get_offset_of_m_RunCycleLegOffset_8() { return static_cast<int32_t>(offsetof(CharacterControllerThirdPerson_t2CC0D121855408DC72A1CB71940065D5C220C886, ___m_RunCycleLegOffset_8)); }
	inline float get_m_RunCycleLegOffset_8() const { return ___m_RunCycleLegOffset_8; }
	inline float* get_address_of_m_RunCycleLegOffset_8() { return &___m_RunCycleLegOffset_8; }
	inline void set_m_RunCycleLegOffset_8(float value)
	{
		___m_RunCycleLegOffset_8 = value;
	}

	inline static int32_t get_offset_of_m_MoveSpeedMultiplier_9() { return static_cast<int32_t>(offsetof(CharacterControllerThirdPerson_t2CC0D121855408DC72A1CB71940065D5C220C886, ___m_MoveSpeedMultiplier_9)); }
	inline float get_m_MoveSpeedMultiplier_9() const { return ___m_MoveSpeedMultiplier_9; }
	inline float* get_address_of_m_MoveSpeedMultiplier_9() { return &___m_MoveSpeedMultiplier_9; }
	inline void set_m_MoveSpeedMultiplier_9(float value)
	{
		___m_MoveSpeedMultiplier_9 = value;
	}

	inline static int32_t get_offset_of_m_AnimSpeedMultiplier_10() { return static_cast<int32_t>(offsetof(CharacterControllerThirdPerson_t2CC0D121855408DC72A1CB71940065D5C220C886, ___m_AnimSpeedMultiplier_10)); }
	inline float get_m_AnimSpeedMultiplier_10() const { return ___m_AnimSpeedMultiplier_10; }
	inline float* get_address_of_m_AnimSpeedMultiplier_10() { return &___m_AnimSpeedMultiplier_10; }
	inline void set_m_AnimSpeedMultiplier_10(float value)
	{
		___m_AnimSpeedMultiplier_10 = value;
	}

	inline static int32_t get_offset_of_m_GroundCheckDistance_11() { return static_cast<int32_t>(offsetof(CharacterControllerThirdPerson_t2CC0D121855408DC72A1CB71940065D5C220C886, ___m_GroundCheckDistance_11)); }
	inline float get_m_GroundCheckDistance_11() const { return ___m_GroundCheckDistance_11; }
	inline float* get_address_of_m_GroundCheckDistance_11() { return &___m_GroundCheckDistance_11; }
	inline void set_m_GroundCheckDistance_11(float value)
	{
		___m_GroundCheckDistance_11 = value;
	}

	inline static int32_t get_offset_of_m_Rigidbody_12() { return static_cast<int32_t>(offsetof(CharacterControllerThirdPerson_t2CC0D121855408DC72A1CB71940065D5C220C886, ___m_Rigidbody_12)); }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * get_m_Rigidbody_12() const { return ___m_Rigidbody_12; }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 ** get_address_of_m_Rigidbody_12() { return &___m_Rigidbody_12; }
	inline void set_m_Rigidbody_12(Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * value)
	{
		___m_Rigidbody_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rigidbody_12), value);
	}

	inline static int32_t get_offset_of_m_Animator_13() { return static_cast<int32_t>(offsetof(CharacterControllerThirdPerson_t2CC0D121855408DC72A1CB71940065D5C220C886, ___m_Animator_13)); }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * get_m_Animator_13() const { return ___m_Animator_13; }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A ** get_address_of_m_Animator_13() { return &___m_Animator_13; }
	inline void set_m_Animator_13(Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * value)
	{
		___m_Animator_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_Animator_13), value);
	}

	inline static int32_t get_offset_of_m_IsGrounded_14() { return static_cast<int32_t>(offsetof(CharacterControllerThirdPerson_t2CC0D121855408DC72A1CB71940065D5C220C886, ___m_IsGrounded_14)); }
	inline bool get_m_IsGrounded_14() const { return ___m_IsGrounded_14; }
	inline bool* get_address_of_m_IsGrounded_14() { return &___m_IsGrounded_14; }
	inline void set_m_IsGrounded_14(bool value)
	{
		___m_IsGrounded_14 = value;
	}

	inline static int32_t get_offset_of_m_OrigGroundCheckDistance_15() { return static_cast<int32_t>(offsetof(CharacterControllerThirdPerson_t2CC0D121855408DC72A1CB71940065D5C220C886, ___m_OrigGroundCheckDistance_15)); }
	inline float get_m_OrigGroundCheckDistance_15() const { return ___m_OrigGroundCheckDistance_15; }
	inline float* get_address_of_m_OrigGroundCheckDistance_15() { return &___m_OrigGroundCheckDistance_15; }
	inline void set_m_OrigGroundCheckDistance_15(float value)
	{
		___m_OrigGroundCheckDistance_15 = value;
	}

	inline static int32_t get_offset_of_m_TurnAmount_17() { return static_cast<int32_t>(offsetof(CharacterControllerThirdPerson_t2CC0D121855408DC72A1CB71940065D5C220C886, ___m_TurnAmount_17)); }
	inline float get_m_TurnAmount_17() const { return ___m_TurnAmount_17; }
	inline float* get_address_of_m_TurnAmount_17() { return &___m_TurnAmount_17; }
	inline void set_m_TurnAmount_17(float value)
	{
		___m_TurnAmount_17 = value;
	}

	inline static int32_t get_offset_of_m_ForwardAmount_18() { return static_cast<int32_t>(offsetof(CharacterControllerThirdPerson_t2CC0D121855408DC72A1CB71940065D5C220C886, ___m_ForwardAmount_18)); }
	inline float get_m_ForwardAmount_18() const { return ___m_ForwardAmount_18; }
	inline float* get_address_of_m_ForwardAmount_18() { return &___m_ForwardAmount_18; }
	inline void set_m_ForwardAmount_18(float value)
	{
		___m_ForwardAmount_18 = value;
	}

	inline static int32_t get_offset_of_m_GroundNormal_19() { return static_cast<int32_t>(offsetof(CharacterControllerThirdPerson_t2CC0D121855408DC72A1CB71940065D5C220C886, ___m_GroundNormal_19)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_GroundNormal_19() const { return ___m_GroundNormal_19; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_GroundNormal_19() { return &___m_GroundNormal_19; }
	inline void set_m_GroundNormal_19(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_GroundNormal_19 = value;
	}

	inline static int32_t get_offset_of_m_CapsuleHeight_20() { return static_cast<int32_t>(offsetof(CharacterControllerThirdPerson_t2CC0D121855408DC72A1CB71940065D5C220C886, ___m_CapsuleHeight_20)); }
	inline float get_m_CapsuleHeight_20() const { return ___m_CapsuleHeight_20; }
	inline float* get_address_of_m_CapsuleHeight_20() { return &___m_CapsuleHeight_20; }
	inline void set_m_CapsuleHeight_20(float value)
	{
		___m_CapsuleHeight_20 = value;
	}

	inline static int32_t get_offset_of_m_CapsuleCenter_21() { return static_cast<int32_t>(offsetof(CharacterControllerThirdPerson_t2CC0D121855408DC72A1CB71940065D5C220C886, ___m_CapsuleCenter_21)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_CapsuleCenter_21() const { return ___m_CapsuleCenter_21; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_CapsuleCenter_21() { return &___m_CapsuleCenter_21; }
	inline void set_m_CapsuleCenter_21(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_CapsuleCenter_21 = value;
	}

	inline static int32_t get_offset_of_m_Capsule_22() { return static_cast<int32_t>(offsetof(CharacterControllerThirdPerson_t2CC0D121855408DC72A1CB71940065D5C220C886, ___m_Capsule_22)); }
	inline CapsuleCollider_t5FD15B9E7BEEC4FFA8A2071E9FD2B8DEB3A826D1 * get_m_Capsule_22() const { return ___m_Capsule_22; }
	inline CapsuleCollider_t5FD15B9E7BEEC4FFA8A2071E9FD2B8DEB3A826D1 ** get_address_of_m_Capsule_22() { return &___m_Capsule_22; }
	inline void set_m_Capsule_22(CapsuleCollider_t5FD15B9E7BEEC4FFA8A2071E9FD2B8DEB3A826D1 * value)
	{
		___m_Capsule_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_Capsule_22), value);
	}

	inline static int32_t get_offset_of_m_Crouching_23() { return static_cast<int32_t>(offsetof(CharacterControllerThirdPerson_t2CC0D121855408DC72A1CB71940065D5C220C886, ___m_Crouching_23)); }
	inline bool get_m_Crouching_23() const { return ___m_Crouching_23; }
	inline bool* get_address_of_m_Crouching_23() { return &___m_Crouching_23; }
	inline void set_m_Crouching_23(bool value)
	{
		___m_Crouching_23 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHARACTERCONTROLLERTHIRDPERSON_T2CC0D121855408DC72A1CB71940065D5C220C886_H
#ifndef IMAGERECOGNITIONEXAMPLE_T911A7F2FD5BFFF23A30C97F8619C18A1F0757110_H
#define IMAGERECOGNITIONEXAMPLE_T911A7F2FD5BFFF23A30C97F8619C18A1F0757110_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ImageRecognitionExample
struct  ImageRecognitionExample_t911A7F2FD5BFFF23A30C97F8619C18A1F0757110  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// MarkerPrefabs[] ImageRecognitionExample::markerPrefabCombos
	MarkerPrefabsU5BU5D_t7F40C670B36EE25921E51F171FD6A68A7A1BC336* ___markerPrefabCombos_4;
	// PrefabsInVicinity[] ImageRecognitionExample::prefabsInVicinities
	PrefabsInVicinityU5BU5D_tCF6E2464E3202CBD16C9DEA043361EA7DB3A49A9* ___prefabsInVicinities_5;
	// UnityEngine.XR.ARFoundation.ARTrackedImageManager ImageRecognitionExample::_arTrackedImageManager
	ARTrackedImageManager_tC1F6C093163D1D4A5CCE83613BFCA465E2DABA9D * ____arTrackedImageManager_6;
	// UnityEngine.Camera ImageRecognitionExample::m_MainCamera
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___m_MainCamera_7;
	// System.Int32 ImageRecognitionExample::count
	int32_t ___count_8;
	// System.Boolean ImageRecognitionExample::isInTheFrame
	bool ___isInTheFrame_9;
	// System.Boolean ImageRecognitionExample::shouldObjectsBeShown
	bool ___shouldObjectsBeShown_10;
	// UnityEngine.Vector3 ImageRecognitionExample::prefabPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___prefabPosition_11;
	// UnityEngine.Vector3 ImageRecognitionExample::userPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___userPosition_12;
	// UnityEngine.GameObject ImageRecognitionExample::objectInflatedOnScreen
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___objectInflatedOnScreen_13;

public:
	inline static int32_t get_offset_of_markerPrefabCombos_4() { return static_cast<int32_t>(offsetof(ImageRecognitionExample_t911A7F2FD5BFFF23A30C97F8619C18A1F0757110, ___markerPrefabCombos_4)); }
	inline MarkerPrefabsU5BU5D_t7F40C670B36EE25921E51F171FD6A68A7A1BC336* get_markerPrefabCombos_4() const { return ___markerPrefabCombos_4; }
	inline MarkerPrefabsU5BU5D_t7F40C670B36EE25921E51F171FD6A68A7A1BC336** get_address_of_markerPrefabCombos_4() { return &___markerPrefabCombos_4; }
	inline void set_markerPrefabCombos_4(MarkerPrefabsU5BU5D_t7F40C670B36EE25921E51F171FD6A68A7A1BC336* value)
	{
		___markerPrefabCombos_4 = value;
		Il2CppCodeGenWriteBarrier((&___markerPrefabCombos_4), value);
	}

	inline static int32_t get_offset_of_prefabsInVicinities_5() { return static_cast<int32_t>(offsetof(ImageRecognitionExample_t911A7F2FD5BFFF23A30C97F8619C18A1F0757110, ___prefabsInVicinities_5)); }
	inline PrefabsInVicinityU5BU5D_tCF6E2464E3202CBD16C9DEA043361EA7DB3A49A9* get_prefabsInVicinities_5() const { return ___prefabsInVicinities_5; }
	inline PrefabsInVicinityU5BU5D_tCF6E2464E3202CBD16C9DEA043361EA7DB3A49A9** get_address_of_prefabsInVicinities_5() { return &___prefabsInVicinities_5; }
	inline void set_prefabsInVicinities_5(PrefabsInVicinityU5BU5D_tCF6E2464E3202CBD16C9DEA043361EA7DB3A49A9* value)
	{
		___prefabsInVicinities_5 = value;
		Il2CppCodeGenWriteBarrier((&___prefabsInVicinities_5), value);
	}

	inline static int32_t get_offset_of__arTrackedImageManager_6() { return static_cast<int32_t>(offsetof(ImageRecognitionExample_t911A7F2FD5BFFF23A30C97F8619C18A1F0757110, ____arTrackedImageManager_6)); }
	inline ARTrackedImageManager_tC1F6C093163D1D4A5CCE83613BFCA465E2DABA9D * get__arTrackedImageManager_6() const { return ____arTrackedImageManager_6; }
	inline ARTrackedImageManager_tC1F6C093163D1D4A5CCE83613BFCA465E2DABA9D ** get_address_of__arTrackedImageManager_6() { return &____arTrackedImageManager_6; }
	inline void set__arTrackedImageManager_6(ARTrackedImageManager_tC1F6C093163D1D4A5CCE83613BFCA465E2DABA9D * value)
	{
		____arTrackedImageManager_6 = value;
		Il2CppCodeGenWriteBarrier((&____arTrackedImageManager_6), value);
	}

	inline static int32_t get_offset_of_m_MainCamera_7() { return static_cast<int32_t>(offsetof(ImageRecognitionExample_t911A7F2FD5BFFF23A30C97F8619C18A1F0757110, ___m_MainCamera_7)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_m_MainCamera_7() const { return ___m_MainCamera_7; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_m_MainCamera_7() { return &___m_MainCamera_7; }
	inline void set_m_MainCamera_7(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___m_MainCamera_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_MainCamera_7), value);
	}

	inline static int32_t get_offset_of_count_8() { return static_cast<int32_t>(offsetof(ImageRecognitionExample_t911A7F2FD5BFFF23A30C97F8619C18A1F0757110, ___count_8)); }
	inline int32_t get_count_8() const { return ___count_8; }
	inline int32_t* get_address_of_count_8() { return &___count_8; }
	inline void set_count_8(int32_t value)
	{
		___count_8 = value;
	}

	inline static int32_t get_offset_of_isInTheFrame_9() { return static_cast<int32_t>(offsetof(ImageRecognitionExample_t911A7F2FD5BFFF23A30C97F8619C18A1F0757110, ___isInTheFrame_9)); }
	inline bool get_isInTheFrame_9() const { return ___isInTheFrame_9; }
	inline bool* get_address_of_isInTheFrame_9() { return &___isInTheFrame_9; }
	inline void set_isInTheFrame_9(bool value)
	{
		___isInTheFrame_9 = value;
	}

	inline static int32_t get_offset_of_shouldObjectsBeShown_10() { return static_cast<int32_t>(offsetof(ImageRecognitionExample_t911A7F2FD5BFFF23A30C97F8619C18A1F0757110, ___shouldObjectsBeShown_10)); }
	inline bool get_shouldObjectsBeShown_10() const { return ___shouldObjectsBeShown_10; }
	inline bool* get_address_of_shouldObjectsBeShown_10() { return &___shouldObjectsBeShown_10; }
	inline void set_shouldObjectsBeShown_10(bool value)
	{
		___shouldObjectsBeShown_10 = value;
	}

	inline static int32_t get_offset_of_prefabPosition_11() { return static_cast<int32_t>(offsetof(ImageRecognitionExample_t911A7F2FD5BFFF23A30C97F8619C18A1F0757110, ___prefabPosition_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_prefabPosition_11() const { return ___prefabPosition_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_prefabPosition_11() { return &___prefabPosition_11; }
	inline void set_prefabPosition_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___prefabPosition_11 = value;
	}

	inline static int32_t get_offset_of_userPosition_12() { return static_cast<int32_t>(offsetof(ImageRecognitionExample_t911A7F2FD5BFFF23A30C97F8619C18A1F0757110, ___userPosition_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_userPosition_12() const { return ___userPosition_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_userPosition_12() { return &___userPosition_12; }
	inline void set_userPosition_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___userPosition_12 = value;
	}

	inline static int32_t get_offset_of_objectInflatedOnScreen_13() { return static_cast<int32_t>(offsetof(ImageRecognitionExample_t911A7F2FD5BFFF23A30C97F8619C18A1F0757110, ___objectInflatedOnScreen_13)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_objectInflatedOnScreen_13() const { return ___objectInflatedOnScreen_13; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_objectInflatedOnScreen_13() { return &___objectInflatedOnScreen_13; }
	inline void set_objectInflatedOnScreen_13(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___objectInflatedOnScreen_13 = value;
		Il2CppCodeGenWriteBarrier((&___objectInflatedOnScreen_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGERECOGNITIONEXAMPLE_T911A7F2FD5BFFF23A30C97F8619C18A1F0757110_H
#ifndef WAYPOINT_T7B48E7B71CF48791746A5B866BCCB7F9FABFCA0A_H
#define WAYPOINT_T7B48E7B71CF48791746A5B866BCCB7F9FABFCA0A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TurnTheGameOn.ArrowWaypointer.Waypoint
struct  Waypoint_t7B48E7B71CF48791746A5B866BCCB7F9FABFCA0A  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Int32 TurnTheGameOn.ArrowWaypointer.Waypoint::radius
	int32_t ___radius_4;
	// TurnTheGameOn.ArrowWaypointer.WaypointController TurnTheGameOn.ArrowWaypointer.Waypoint::waypointController
	WaypointController_tD4E5882DF3A666D2E1C97E9E73061816C123AB20 * ___waypointController_5;
	// System.Int32 TurnTheGameOn.ArrowWaypointer.Waypoint::waypointNumber
	int32_t ___waypointNumber_6;

public:
	inline static int32_t get_offset_of_radius_4() { return static_cast<int32_t>(offsetof(Waypoint_t7B48E7B71CF48791746A5B866BCCB7F9FABFCA0A, ___radius_4)); }
	inline int32_t get_radius_4() const { return ___radius_4; }
	inline int32_t* get_address_of_radius_4() { return &___radius_4; }
	inline void set_radius_4(int32_t value)
	{
		___radius_4 = value;
	}

	inline static int32_t get_offset_of_waypointController_5() { return static_cast<int32_t>(offsetof(Waypoint_t7B48E7B71CF48791746A5B866BCCB7F9FABFCA0A, ___waypointController_5)); }
	inline WaypointController_tD4E5882DF3A666D2E1C97E9E73061816C123AB20 * get_waypointController_5() const { return ___waypointController_5; }
	inline WaypointController_tD4E5882DF3A666D2E1C97E9E73061816C123AB20 ** get_address_of_waypointController_5() { return &___waypointController_5; }
	inline void set_waypointController_5(WaypointController_tD4E5882DF3A666D2E1C97E9E73061816C123AB20 * value)
	{
		___waypointController_5 = value;
		Il2CppCodeGenWriteBarrier((&___waypointController_5), value);
	}

	inline static int32_t get_offset_of_waypointNumber_6() { return static_cast<int32_t>(offsetof(Waypoint_t7B48E7B71CF48791746A5B866BCCB7F9FABFCA0A, ___waypointNumber_6)); }
	inline int32_t get_waypointNumber_6() const { return ___waypointNumber_6; }
	inline int32_t* get_address_of_waypointNumber_6() { return &___waypointNumber_6; }
	inline void set_waypointNumber_6(int32_t value)
	{
		___waypointNumber_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAYPOINT_T7B48E7B71CF48791746A5B866BCCB7F9FABFCA0A_H
#ifndef WAYPOINTCONTROLLER_TD4E5882DF3A666D2E1C97E9E73061816C123AB20_H
#define WAYPOINTCONTROLLER_TD4E5882DF3A666D2E1C97E9E73061816C123AB20_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TurnTheGameOn.ArrowWaypointer.WaypointController
struct  WaypointController_tD4E5882DF3A666D2E1C97E9E73061816C123AB20  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Transform TurnTheGameOn.ArrowWaypointer.WaypointController::player
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___player_4;
	// TurnTheGameOn.ArrowWaypointer.WaypointController_Switch TurnTheGameOn.ArrowWaypointer.WaypointController::configureMode
	int32_t ___configureMode_5;
	// System.Single TurnTheGameOn.ArrowWaypointer.WaypointController::arrowTargetSmooth
	float ___arrowTargetSmooth_6;
	// System.Int32 TurnTheGameOn.ArrowWaypointer.WaypointController::TotalWaypoints
	int32_t ___TotalWaypoints_7;
	// TurnTheGameOn.ArrowWaypointer.WaypointController_WaypointComponents[] TurnTheGameOn.ArrowWaypointer.WaypointController::waypointList
	WaypointComponentsU5BU5D_t995379FD5B9F2912BDD0CA0EC38E68B64A9839FD* ___waypointList_8;
	// UnityEngine.GameObject TurnTheGameOn.ArrowWaypointer.WaypointController::newWaypoint
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___newWaypoint_9;
	// System.String TurnTheGameOn.ArrowWaypointer.WaypointController::newWaypointName
	String_t* ___newWaypointName_10;
	// System.Int32 TurnTheGameOn.ArrowWaypointer.WaypointController::nextWP
	int32_t ___nextWP_11;
	// UnityEngine.Transform TurnTheGameOn.ArrowWaypointer.WaypointController::waypointArrow
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___waypointArrow_12;
	// UnityEngine.Transform TurnTheGameOn.ArrowWaypointer.WaypointController::currentWaypoint
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___currentWaypoint_13;
	// UnityEngine.Transform TurnTheGameOn.ArrowWaypointer.WaypointController::arrowTarget
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___arrowTarget_14;

public:
	inline static int32_t get_offset_of_player_4() { return static_cast<int32_t>(offsetof(WaypointController_tD4E5882DF3A666D2E1C97E9E73061816C123AB20, ___player_4)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_player_4() const { return ___player_4; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_player_4() { return &___player_4; }
	inline void set_player_4(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___player_4 = value;
		Il2CppCodeGenWriteBarrier((&___player_4), value);
	}

	inline static int32_t get_offset_of_configureMode_5() { return static_cast<int32_t>(offsetof(WaypointController_tD4E5882DF3A666D2E1C97E9E73061816C123AB20, ___configureMode_5)); }
	inline int32_t get_configureMode_5() const { return ___configureMode_5; }
	inline int32_t* get_address_of_configureMode_5() { return &___configureMode_5; }
	inline void set_configureMode_5(int32_t value)
	{
		___configureMode_5 = value;
	}

	inline static int32_t get_offset_of_arrowTargetSmooth_6() { return static_cast<int32_t>(offsetof(WaypointController_tD4E5882DF3A666D2E1C97E9E73061816C123AB20, ___arrowTargetSmooth_6)); }
	inline float get_arrowTargetSmooth_6() const { return ___arrowTargetSmooth_6; }
	inline float* get_address_of_arrowTargetSmooth_6() { return &___arrowTargetSmooth_6; }
	inline void set_arrowTargetSmooth_6(float value)
	{
		___arrowTargetSmooth_6 = value;
	}

	inline static int32_t get_offset_of_TotalWaypoints_7() { return static_cast<int32_t>(offsetof(WaypointController_tD4E5882DF3A666D2E1C97E9E73061816C123AB20, ___TotalWaypoints_7)); }
	inline int32_t get_TotalWaypoints_7() const { return ___TotalWaypoints_7; }
	inline int32_t* get_address_of_TotalWaypoints_7() { return &___TotalWaypoints_7; }
	inline void set_TotalWaypoints_7(int32_t value)
	{
		___TotalWaypoints_7 = value;
	}

	inline static int32_t get_offset_of_waypointList_8() { return static_cast<int32_t>(offsetof(WaypointController_tD4E5882DF3A666D2E1C97E9E73061816C123AB20, ___waypointList_8)); }
	inline WaypointComponentsU5BU5D_t995379FD5B9F2912BDD0CA0EC38E68B64A9839FD* get_waypointList_8() const { return ___waypointList_8; }
	inline WaypointComponentsU5BU5D_t995379FD5B9F2912BDD0CA0EC38E68B64A9839FD** get_address_of_waypointList_8() { return &___waypointList_8; }
	inline void set_waypointList_8(WaypointComponentsU5BU5D_t995379FD5B9F2912BDD0CA0EC38E68B64A9839FD* value)
	{
		___waypointList_8 = value;
		Il2CppCodeGenWriteBarrier((&___waypointList_8), value);
	}

	inline static int32_t get_offset_of_newWaypoint_9() { return static_cast<int32_t>(offsetof(WaypointController_tD4E5882DF3A666D2E1C97E9E73061816C123AB20, ___newWaypoint_9)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_newWaypoint_9() const { return ___newWaypoint_9; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_newWaypoint_9() { return &___newWaypoint_9; }
	inline void set_newWaypoint_9(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___newWaypoint_9 = value;
		Il2CppCodeGenWriteBarrier((&___newWaypoint_9), value);
	}

	inline static int32_t get_offset_of_newWaypointName_10() { return static_cast<int32_t>(offsetof(WaypointController_tD4E5882DF3A666D2E1C97E9E73061816C123AB20, ___newWaypointName_10)); }
	inline String_t* get_newWaypointName_10() const { return ___newWaypointName_10; }
	inline String_t** get_address_of_newWaypointName_10() { return &___newWaypointName_10; }
	inline void set_newWaypointName_10(String_t* value)
	{
		___newWaypointName_10 = value;
		Il2CppCodeGenWriteBarrier((&___newWaypointName_10), value);
	}

	inline static int32_t get_offset_of_nextWP_11() { return static_cast<int32_t>(offsetof(WaypointController_tD4E5882DF3A666D2E1C97E9E73061816C123AB20, ___nextWP_11)); }
	inline int32_t get_nextWP_11() const { return ___nextWP_11; }
	inline int32_t* get_address_of_nextWP_11() { return &___nextWP_11; }
	inline void set_nextWP_11(int32_t value)
	{
		___nextWP_11 = value;
	}

	inline static int32_t get_offset_of_waypointArrow_12() { return static_cast<int32_t>(offsetof(WaypointController_tD4E5882DF3A666D2E1C97E9E73061816C123AB20, ___waypointArrow_12)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_waypointArrow_12() const { return ___waypointArrow_12; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_waypointArrow_12() { return &___waypointArrow_12; }
	inline void set_waypointArrow_12(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___waypointArrow_12 = value;
		Il2CppCodeGenWriteBarrier((&___waypointArrow_12), value);
	}

	inline static int32_t get_offset_of_currentWaypoint_13() { return static_cast<int32_t>(offsetof(WaypointController_tD4E5882DF3A666D2E1C97E9E73061816C123AB20, ___currentWaypoint_13)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_currentWaypoint_13() const { return ___currentWaypoint_13; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_currentWaypoint_13() { return &___currentWaypoint_13; }
	inline void set_currentWaypoint_13(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___currentWaypoint_13 = value;
		Il2CppCodeGenWriteBarrier((&___currentWaypoint_13), value);
	}

	inline static int32_t get_offset_of_arrowTarget_14() { return static_cast<int32_t>(offsetof(WaypointController_tD4E5882DF3A666D2E1C97E9E73061816C123AB20, ___arrowTarget_14)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_arrowTarget_14() const { return ___arrowTarget_14; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_arrowTarget_14() { return &___arrowTarget_14; }
	inline void set_arrowTarget_14(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___arrowTarget_14 = value;
		Il2CppCodeGenWriteBarrier((&___arrowTarget_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAYPOINTCONTROLLER_TD4E5882DF3A666D2E1C97E9E73061816C123AB20_H
#ifndef SMOOTHFOLLOW_T216FB57B20D1783DDB83147F7D75C14735C98316_H
#define SMOOTHFOLLOW_T216FB57B20D1783DDB83147F7D75C14735C98316_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TurnTheGameOn.NPCChat.SmoothFollow
struct  SmoothFollow_t216FB57B20D1783DDB83147F7D75C14735C98316  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Transform TurnTheGameOn.NPCChat.SmoothFollow::target
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___target_4;
	// System.Single TurnTheGameOn.NPCChat.SmoothFollow::distance
	float ___distance_5;
	// System.Single TurnTheGameOn.NPCChat.SmoothFollow::height
	float ___height_6;
	// System.Single TurnTheGameOn.NPCChat.SmoothFollow::rotationDamping
	float ___rotationDamping_7;
	// System.Single TurnTheGameOn.NPCChat.SmoothFollow::heightDamping
	float ___heightDamping_8;

public:
	inline static int32_t get_offset_of_target_4() { return static_cast<int32_t>(offsetof(SmoothFollow_t216FB57B20D1783DDB83147F7D75C14735C98316, ___target_4)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_target_4() const { return ___target_4; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_target_4() { return &___target_4; }
	inline void set_target_4(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___target_4 = value;
		Il2CppCodeGenWriteBarrier((&___target_4), value);
	}

	inline static int32_t get_offset_of_distance_5() { return static_cast<int32_t>(offsetof(SmoothFollow_t216FB57B20D1783DDB83147F7D75C14735C98316, ___distance_5)); }
	inline float get_distance_5() const { return ___distance_5; }
	inline float* get_address_of_distance_5() { return &___distance_5; }
	inline void set_distance_5(float value)
	{
		___distance_5 = value;
	}

	inline static int32_t get_offset_of_height_6() { return static_cast<int32_t>(offsetof(SmoothFollow_t216FB57B20D1783DDB83147F7D75C14735C98316, ___height_6)); }
	inline float get_height_6() const { return ___height_6; }
	inline float* get_address_of_height_6() { return &___height_6; }
	inline void set_height_6(float value)
	{
		___height_6 = value;
	}

	inline static int32_t get_offset_of_rotationDamping_7() { return static_cast<int32_t>(offsetof(SmoothFollow_t216FB57B20D1783DDB83147F7D75C14735C98316, ___rotationDamping_7)); }
	inline float get_rotationDamping_7() const { return ___rotationDamping_7; }
	inline float* get_address_of_rotationDamping_7() { return &___rotationDamping_7; }
	inline void set_rotationDamping_7(float value)
	{
		___rotationDamping_7 = value;
	}

	inline static int32_t get_offset_of_heightDamping_8() { return static_cast<int32_t>(offsetof(SmoothFollow_t216FB57B20D1783DDB83147F7D75C14735C98316, ___heightDamping_8)); }
	inline float get_heightDamping_8() const { return ___heightDamping_8; }
	inline float* get_address_of_heightDamping_8() { return &___heightDamping_8; }
	inline void set_heightDamping_8(float value)
	{
		___heightDamping_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SMOOTHFOLLOW_T216FB57B20D1783DDB83147F7D75C14735C98316_H
#ifndef ARTRACKABLE_2_T1EDE6011EE2879288E62C947FC7EF0BBD638DDF8_H
#define ARTRACKABLE_2_T1EDE6011EE2879288E62C947FC7EF0BBD638DDF8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARFoundation.ARTrackable`2<UnityEngine.XR.ARSubsystems.XRTrackedImage,UnityEngine.XR.ARFoundation.ARTrackedImage>
struct  ARTrackable_2_t1EDE6011EE2879288E62C947FC7EF0BBD638DDF8  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean UnityEngine.XR.ARFoundation.ARTrackable`2::m_DestroyOnRemoval
	bool ___m_DestroyOnRemoval_4;
	// System.Boolean UnityEngine.XR.ARFoundation.ARTrackable`2::<pending>k__BackingField
	bool ___U3CpendingU3Ek__BackingField_5;
	// TSessionRelativeData UnityEngine.XR.ARFoundation.ARTrackable`2::<sessionRelativeData>k__BackingField
	XRTrackedImage_t178EACF5BFA4228DF4EB1899685C533F3F296AA8  ___U3CsessionRelativeDataU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_m_DestroyOnRemoval_4() { return static_cast<int32_t>(offsetof(ARTrackable_2_t1EDE6011EE2879288E62C947FC7EF0BBD638DDF8, ___m_DestroyOnRemoval_4)); }
	inline bool get_m_DestroyOnRemoval_4() const { return ___m_DestroyOnRemoval_4; }
	inline bool* get_address_of_m_DestroyOnRemoval_4() { return &___m_DestroyOnRemoval_4; }
	inline void set_m_DestroyOnRemoval_4(bool value)
	{
		___m_DestroyOnRemoval_4 = value;
	}

	inline static int32_t get_offset_of_U3CpendingU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(ARTrackable_2_t1EDE6011EE2879288E62C947FC7EF0BBD638DDF8, ___U3CpendingU3Ek__BackingField_5)); }
	inline bool get_U3CpendingU3Ek__BackingField_5() const { return ___U3CpendingU3Ek__BackingField_5; }
	inline bool* get_address_of_U3CpendingU3Ek__BackingField_5() { return &___U3CpendingU3Ek__BackingField_5; }
	inline void set_U3CpendingU3Ek__BackingField_5(bool value)
	{
		___U3CpendingU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CsessionRelativeDataU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(ARTrackable_2_t1EDE6011EE2879288E62C947FC7EF0BBD638DDF8, ___U3CsessionRelativeDataU3Ek__BackingField_6)); }
	inline XRTrackedImage_t178EACF5BFA4228DF4EB1899685C533F3F296AA8  get_U3CsessionRelativeDataU3Ek__BackingField_6() const { return ___U3CsessionRelativeDataU3Ek__BackingField_6; }
	inline XRTrackedImage_t178EACF5BFA4228DF4EB1899685C533F3F296AA8 * get_address_of_U3CsessionRelativeDataU3Ek__BackingField_6() { return &___U3CsessionRelativeDataU3Ek__BackingField_6; }
	inline void set_U3CsessionRelativeDataU3Ek__BackingField_6(XRTrackedImage_t178EACF5BFA4228DF4EB1899685C533F3F296AA8  value)
	{
		___U3CsessionRelativeDataU3Ek__BackingField_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARTRACKABLE_2_T1EDE6011EE2879288E62C947FC7EF0BBD638DDF8_H
#ifndef SUBSYSTEMLIFECYCLEMANAGER_2_T10A14769CEB346237BD074E3C82BFB3673B0B6EB_H
#define SUBSYSTEMLIFECYCLEMANAGER_2_T10A14769CEB346237BD074E3C82BFB3673B0B6EB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARFoundation.SubsystemLifecycleManager`2<UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystem,UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystemDescriptor>
struct  SubsystemLifecycleManager_2_t10A14769CEB346237BD074E3C82BFB3673B0B6EB  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// TSubsystem UnityEngine.XR.ARFoundation.SubsystemLifecycleManager`2::<subsystem>k__BackingField
	XRImageTrackingSubsystem_t1DC56F01ABB0CC32D9AC26AA77E097AD9B0ECEB5 * ___U3CsubsystemU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CsubsystemU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(SubsystemLifecycleManager_2_t10A14769CEB346237BD074E3C82BFB3673B0B6EB, ___U3CsubsystemU3Ek__BackingField_4)); }
	inline XRImageTrackingSubsystem_t1DC56F01ABB0CC32D9AC26AA77E097AD9B0ECEB5 * get_U3CsubsystemU3Ek__BackingField_4() const { return ___U3CsubsystemU3Ek__BackingField_4; }
	inline XRImageTrackingSubsystem_t1DC56F01ABB0CC32D9AC26AA77E097AD9B0ECEB5 ** get_address_of_U3CsubsystemU3Ek__BackingField_4() { return &___U3CsubsystemU3Ek__BackingField_4; }
	inline void set_U3CsubsystemU3Ek__BackingField_4(XRImageTrackingSubsystem_t1DC56F01ABB0CC32D9AC26AA77E097AD9B0ECEB5 * value)
	{
		___U3CsubsystemU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsubsystemU3Ek__BackingField_4), value);
	}
};

struct SubsystemLifecycleManager_2_t10A14769CEB346237BD074E3C82BFB3673B0B6EB_StaticFields
{
public:
	// System.Collections.Generic.List`1<TSubsystemDescriptor> UnityEngine.XR.ARFoundation.SubsystemLifecycleManager`2::s_SubsystemDescriptors
	List_1_t19F02B33C2CBC6A12544FBB09A8173859BCBCCBE * ___s_SubsystemDescriptors_5;

public:
	inline static int32_t get_offset_of_s_SubsystemDescriptors_5() { return static_cast<int32_t>(offsetof(SubsystemLifecycleManager_2_t10A14769CEB346237BD074E3C82BFB3673B0B6EB_StaticFields, ___s_SubsystemDescriptors_5)); }
	inline List_1_t19F02B33C2CBC6A12544FBB09A8173859BCBCCBE * get_s_SubsystemDescriptors_5() const { return ___s_SubsystemDescriptors_5; }
	inline List_1_t19F02B33C2CBC6A12544FBB09A8173859BCBCCBE ** get_address_of_s_SubsystemDescriptors_5() { return &___s_SubsystemDescriptors_5; }
	inline void set_s_SubsystemDescriptors_5(List_1_t19F02B33C2CBC6A12544FBB09A8173859BCBCCBE * value)
	{
		___s_SubsystemDescriptors_5 = value;
		Il2CppCodeGenWriteBarrier((&___s_SubsystemDescriptors_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBSYSTEMLIFECYCLEMANAGER_2_T10A14769CEB346237BD074E3C82BFB3673B0B6EB_H
#ifndef USERCONTROLTHIRDPERSON_TA3F8F919E01EC0A7D458E6F2F3346D2BD3878DAB_H
#define USERCONTROLTHIRDPERSON_TA3F8F919E01EC0A7D458E6F2F3346D2BD3878DAB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UserControlThirdPerson
struct  UserControlThirdPerson_tA3F8F919E01EC0A7D458E6F2F3346D2BD3878DAB  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// CharacterControllerThirdPerson UserControlThirdPerson::m_Character
	CharacterControllerThirdPerson_t2CC0D121855408DC72A1CB71940065D5C220C886 * ___m_Character_4;
	// UnityEngine.Transform UserControlThirdPerson::m_Cam
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___m_Cam_5;
	// UnityEngine.Vector3 UserControlThirdPerson::m_CamForward
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_CamForward_6;
	// UnityEngine.Vector3 UserControlThirdPerson::m_Move
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Move_7;
	// System.Boolean UserControlThirdPerson::m_Jump
	bool ___m_Jump_8;

public:
	inline static int32_t get_offset_of_m_Character_4() { return static_cast<int32_t>(offsetof(UserControlThirdPerson_tA3F8F919E01EC0A7D458E6F2F3346D2BD3878DAB, ___m_Character_4)); }
	inline CharacterControllerThirdPerson_t2CC0D121855408DC72A1CB71940065D5C220C886 * get_m_Character_4() const { return ___m_Character_4; }
	inline CharacterControllerThirdPerson_t2CC0D121855408DC72A1CB71940065D5C220C886 ** get_address_of_m_Character_4() { return &___m_Character_4; }
	inline void set_m_Character_4(CharacterControllerThirdPerson_t2CC0D121855408DC72A1CB71940065D5C220C886 * value)
	{
		___m_Character_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Character_4), value);
	}

	inline static int32_t get_offset_of_m_Cam_5() { return static_cast<int32_t>(offsetof(UserControlThirdPerson_tA3F8F919E01EC0A7D458E6F2F3346D2BD3878DAB, ___m_Cam_5)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_m_Cam_5() const { return ___m_Cam_5; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_m_Cam_5() { return &___m_Cam_5; }
	inline void set_m_Cam_5(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___m_Cam_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Cam_5), value);
	}

	inline static int32_t get_offset_of_m_CamForward_6() { return static_cast<int32_t>(offsetof(UserControlThirdPerson_tA3F8F919E01EC0A7D458E6F2F3346D2BD3878DAB, ___m_CamForward_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_CamForward_6() const { return ___m_CamForward_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_CamForward_6() { return &___m_CamForward_6; }
	inline void set_m_CamForward_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_CamForward_6 = value;
	}

	inline static int32_t get_offset_of_m_Move_7() { return static_cast<int32_t>(offsetof(UserControlThirdPerson_tA3F8F919E01EC0A7D458E6F2F3346D2BD3878DAB, ___m_Move_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Move_7() const { return ___m_Move_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Move_7() { return &___m_Move_7; }
	inline void set_m_Move_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Move_7 = value;
	}

	inline static int32_t get_offset_of_m_Jump_8() { return static_cast<int32_t>(offsetof(UserControlThirdPerson_tA3F8F919E01EC0A7D458E6F2F3346D2BD3878DAB, ___m_Jump_8)); }
	inline bool get_m_Jump_8() const { return ___m_Jump_8; }
	inline bool* get_address_of_m_Jump_8() { return &___m_Jump_8; }
	inline void set_m_Jump_8(bool value)
	{
		___m_Jump_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // USERCONTROLTHIRDPERSON_TA3F8F919E01EC0A7D458E6F2F3346D2BD3878DAB_H
#ifndef ARTRACKABLEMANAGER_4_TAB5612DBDE7F8483771B0BD554243C771FE70364_H
#define ARTRACKABLEMANAGER_4_TAB5612DBDE7F8483771B0BD554243C771FE70364_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARFoundation.ARTrackableManager`4<UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystem,UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystemDescriptor,UnityEngine.XR.ARSubsystems.XRTrackedImage,UnityEngine.XR.ARFoundation.ARTrackedImage>
struct  ARTrackableManager_4_tAB5612DBDE7F8483771B0BD554243C771FE70364  : public SubsystemLifecycleManager_2_t10A14769CEB346237BD074E3C82BFB3673B0B6EB
{
public:
	// UnityEngine.XR.ARFoundation.ARSessionOrigin UnityEngine.XR.ARFoundation.ARTrackableManager`4::<sessionOrigin>k__BackingField
	ARSessionOrigin_t61463C0A24AF925CF219B6C3F1720325C96720EF * ___U3CsessionOriginU3Ek__BackingField_6;
	// System.Collections.Generic.Dictionary`2<UnityEngine.XR.ARSubsystems.TrackableId,TTrackable> UnityEngine.XR.ARFoundation.ARTrackableManager`4::m_Trackables
	Dictionary_2_t9780DA231BA452BFBD83FA24408472862BDD8A9F * ___m_Trackables_7;
	// System.Collections.Generic.Dictionary`2<UnityEngine.XR.ARSubsystems.TrackableId,TTrackable> UnityEngine.XR.ARFoundation.ARTrackableManager`4::m_PendingAdds
	Dictionary_2_t9780DA231BA452BFBD83FA24408472862BDD8A9F * ___m_PendingAdds_8;

public:
	inline static int32_t get_offset_of_U3CsessionOriginU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(ARTrackableManager_4_tAB5612DBDE7F8483771B0BD554243C771FE70364, ___U3CsessionOriginU3Ek__BackingField_6)); }
	inline ARSessionOrigin_t61463C0A24AF925CF219B6C3F1720325C96720EF * get_U3CsessionOriginU3Ek__BackingField_6() const { return ___U3CsessionOriginU3Ek__BackingField_6; }
	inline ARSessionOrigin_t61463C0A24AF925CF219B6C3F1720325C96720EF ** get_address_of_U3CsessionOriginU3Ek__BackingField_6() { return &___U3CsessionOriginU3Ek__BackingField_6; }
	inline void set_U3CsessionOriginU3Ek__BackingField_6(ARSessionOrigin_t61463C0A24AF925CF219B6C3F1720325C96720EF * value)
	{
		___U3CsessionOriginU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsessionOriginU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_m_Trackables_7() { return static_cast<int32_t>(offsetof(ARTrackableManager_4_tAB5612DBDE7F8483771B0BD554243C771FE70364, ___m_Trackables_7)); }
	inline Dictionary_2_t9780DA231BA452BFBD83FA24408472862BDD8A9F * get_m_Trackables_7() const { return ___m_Trackables_7; }
	inline Dictionary_2_t9780DA231BA452BFBD83FA24408472862BDD8A9F ** get_address_of_m_Trackables_7() { return &___m_Trackables_7; }
	inline void set_m_Trackables_7(Dictionary_2_t9780DA231BA452BFBD83FA24408472862BDD8A9F * value)
	{
		___m_Trackables_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_Trackables_7), value);
	}

	inline static int32_t get_offset_of_m_PendingAdds_8() { return static_cast<int32_t>(offsetof(ARTrackableManager_4_tAB5612DBDE7F8483771B0BD554243C771FE70364, ___m_PendingAdds_8)); }
	inline Dictionary_2_t9780DA231BA452BFBD83FA24408472862BDD8A9F * get_m_PendingAdds_8() const { return ___m_PendingAdds_8; }
	inline Dictionary_2_t9780DA231BA452BFBD83FA24408472862BDD8A9F ** get_address_of_m_PendingAdds_8() { return &___m_PendingAdds_8; }
	inline void set_m_PendingAdds_8(Dictionary_2_t9780DA231BA452BFBD83FA24408472862BDD8A9F * value)
	{
		___m_PendingAdds_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_PendingAdds_8), value);
	}
};

struct ARTrackableManager_4_tAB5612DBDE7F8483771B0BD554243C771FE70364_StaticFields
{
public:
	// System.Collections.Generic.List`1<TTrackable> UnityEngine.XR.ARFoundation.ARTrackableManager`4::s_Added
	List_1_t85456E82777B8C7F9CFFED2C540D0613542A8C8D * ___s_Added_9;
	// System.Collections.Generic.List`1<TTrackable> UnityEngine.XR.ARFoundation.ARTrackableManager`4::s_Updated
	List_1_t85456E82777B8C7F9CFFED2C540D0613542A8C8D * ___s_Updated_10;
	// System.Collections.Generic.List`1<TTrackable> UnityEngine.XR.ARFoundation.ARTrackableManager`4::s_Removed
	List_1_t85456E82777B8C7F9CFFED2C540D0613542A8C8D * ___s_Removed_11;

public:
	inline static int32_t get_offset_of_s_Added_9() { return static_cast<int32_t>(offsetof(ARTrackableManager_4_tAB5612DBDE7F8483771B0BD554243C771FE70364_StaticFields, ___s_Added_9)); }
	inline List_1_t85456E82777B8C7F9CFFED2C540D0613542A8C8D * get_s_Added_9() const { return ___s_Added_9; }
	inline List_1_t85456E82777B8C7F9CFFED2C540D0613542A8C8D ** get_address_of_s_Added_9() { return &___s_Added_9; }
	inline void set_s_Added_9(List_1_t85456E82777B8C7F9CFFED2C540D0613542A8C8D * value)
	{
		___s_Added_9 = value;
		Il2CppCodeGenWriteBarrier((&___s_Added_9), value);
	}

	inline static int32_t get_offset_of_s_Updated_10() { return static_cast<int32_t>(offsetof(ARTrackableManager_4_tAB5612DBDE7F8483771B0BD554243C771FE70364_StaticFields, ___s_Updated_10)); }
	inline List_1_t85456E82777B8C7F9CFFED2C540D0613542A8C8D * get_s_Updated_10() const { return ___s_Updated_10; }
	inline List_1_t85456E82777B8C7F9CFFED2C540D0613542A8C8D ** get_address_of_s_Updated_10() { return &___s_Updated_10; }
	inline void set_s_Updated_10(List_1_t85456E82777B8C7F9CFFED2C540D0613542A8C8D * value)
	{
		___s_Updated_10 = value;
		Il2CppCodeGenWriteBarrier((&___s_Updated_10), value);
	}

	inline static int32_t get_offset_of_s_Removed_11() { return static_cast<int32_t>(offsetof(ARTrackableManager_4_tAB5612DBDE7F8483771B0BD554243C771FE70364_StaticFields, ___s_Removed_11)); }
	inline List_1_t85456E82777B8C7F9CFFED2C540D0613542A8C8D * get_s_Removed_11() const { return ___s_Removed_11; }
	inline List_1_t85456E82777B8C7F9CFFED2C540D0613542A8C8D ** get_address_of_s_Removed_11() { return &___s_Removed_11; }
	inline void set_s_Removed_11(List_1_t85456E82777B8C7F9CFFED2C540D0613542A8C8D * value)
	{
		___s_Removed_11 = value;
		Il2CppCodeGenWriteBarrier((&___s_Removed_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARTRACKABLEMANAGER_4_TAB5612DBDE7F8483771B0BD554243C771FE70364_H
#ifndef ARTRACKEDIMAGE_TD699A1D6CA301ECE200D07D1CF1722E94AFEF01A_H
#define ARTRACKEDIMAGE_TD699A1D6CA301ECE200D07D1CF1722E94AFEF01A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARFoundation.ARTrackedImage
struct  ARTrackedImage_tD699A1D6CA301ECE200D07D1CF1722E94AFEF01A  : public ARTrackable_2_t1EDE6011EE2879288E62C947FC7EF0BBD638DDF8
{
public:
	// UnityEngine.XR.ARSubsystems.XRReferenceImage UnityEngine.XR.ARFoundation.ARTrackedImage::<referenceImage>k__BackingField
	XRReferenceImage_t5A53387AC6253D5D3DFD62BC583A45BBDFC1347E  ___U3CreferenceImageU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_U3CreferenceImageU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(ARTrackedImage_tD699A1D6CA301ECE200D07D1CF1722E94AFEF01A, ___U3CreferenceImageU3Ek__BackingField_7)); }
	inline XRReferenceImage_t5A53387AC6253D5D3DFD62BC583A45BBDFC1347E  get_U3CreferenceImageU3Ek__BackingField_7() const { return ___U3CreferenceImageU3Ek__BackingField_7; }
	inline XRReferenceImage_t5A53387AC6253D5D3DFD62BC583A45BBDFC1347E * get_address_of_U3CreferenceImageU3Ek__BackingField_7() { return &___U3CreferenceImageU3Ek__BackingField_7; }
	inline void set_U3CreferenceImageU3Ek__BackingField_7(XRReferenceImage_t5A53387AC6253D5D3DFD62BC583A45BBDFC1347E  value)
	{
		___U3CreferenceImageU3Ek__BackingField_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARTRACKEDIMAGE_TD699A1D6CA301ECE200D07D1CF1722E94AFEF01A_H
#ifndef ARTRACKEDIMAGEMANAGER_TC1F6C093163D1D4A5CCE83613BFCA465E2DABA9D_H
#define ARTRACKEDIMAGEMANAGER_TC1F6C093163D1D4A5CCE83613BFCA465E2DABA9D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARFoundation.ARTrackedImageManager
struct  ARTrackedImageManager_tC1F6C093163D1D4A5CCE83613BFCA465E2DABA9D  : public ARTrackableManager_4_tAB5612DBDE7F8483771B0BD554243C771FE70364
{
public:
	// UnityEngine.XR.ARSubsystems.XRReferenceImageLibrary UnityEngine.XR.ARFoundation.ARTrackedImageManager::m_ReferenceLibrary
	XRReferenceImageLibrary_t38B21DC6650EADA892125F045DCBF71EBC3F6A8F * ___m_ReferenceLibrary_12;
	// System.Int32 UnityEngine.XR.ARFoundation.ARTrackedImageManager::m_MaxNumberOfMovingImages
	int32_t ___m_MaxNumberOfMovingImages_13;
	// UnityEngine.GameObject UnityEngine.XR.ARFoundation.ARTrackedImageManager::m_TrackedImagePrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_TrackedImagePrefab_14;
	// System.Action`1<UnityEngine.XR.ARFoundation.ARTrackedImagesChangedEventArgs> UnityEngine.XR.ARFoundation.ARTrackedImageManager::trackedImagesChanged
	Action_1_t825CEDF2F074FBD059900D81615EFDE0AFD02CA0 * ___trackedImagesChanged_15;
	// System.Collections.Generic.Dictionary`2<System.Guid,UnityEngine.XR.ARSubsystems.XRReferenceImage> UnityEngine.XR.ARFoundation.ARTrackedImageManager::m_ReferenceImages
	Dictionary_2_tE3586D05DB2E27BC21247078C0A121A82B014D91 * ___m_ReferenceImages_16;

public:
	inline static int32_t get_offset_of_m_ReferenceLibrary_12() { return static_cast<int32_t>(offsetof(ARTrackedImageManager_tC1F6C093163D1D4A5CCE83613BFCA465E2DABA9D, ___m_ReferenceLibrary_12)); }
	inline XRReferenceImageLibrary_t38B21DC6650EADA892125F045DCBF71EBC3F6A8F * get_m_ReferenceLibrary_12() const { return ___m_ReferenceLibrary_12; }
	inline XRReferenceImageLibrary_t38B21DC6650EADA892125F045DCBF71EBC3F6A8F ** get_address_of_m_ReferenceLibrary_12() { return &___m_ReferenceLibrary_12; }
	inline void set_m_ReferenceLibrary_12(XRReferenceImageLibrary_t38B21DC6650EADA892125F045DCBF71EBC3F6A8F * value)
	{
		___m_ReferenceLibrary_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_ReferenceLibrary_12), value);
	}

	inline static int32_t get_offset_of_m_MaxNumberOfMovingImages_13() { return static_cast<int32_t>(offsetof(ARTrackedImageManager_tC1F6C093163D1D4A5CCE83613BFCA465E2DABA9D, ___m_MaxNumberOfMovingImages_13)); }
	inline int32_t get_m_MaxNumberOfMovingImages_13() const { return ___m_MaxNumberOfMovingImages_13; }
	inline int32_t* get_address_of_m_MaxNumberOfMovingImages_13() { return &___m_MaxNumberOfMovingImages_13; }
	inline void set_m_MaxNumberOfMovingImages_13(int32_t value)
	{
		___m_MaxNumberOfMovingImages_13 = value;
	}

	inline static int32_t get_offset_of_m_TrackedImagePrefab_14() { return static_cast<int32_t>(offsetof(ARTrackedImageManager_tC1F6C093163D1D4A5CCE83613BFCA465E2DABA9D, ___m_TrackedImagePrefab_14)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_TrackedImagePrefab_14() const { return ___m_TrackedImagePrefab_14; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_TrackedImagePrefab_14() { return &___m_TrackedImagePrefab_14; }
	inline void set_m_TrackedImagePrefab_14(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_TrackedImagePrefab_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_TrackedImagePrefab_14), value);
	}

	inline static int32_t get_offset_of_trackedImagesChanged_15() { return static_cast<int32_t>(offsetof(ARTrackedImageManager_tC1F6C093163D1D4A5CCE83613BFCA465E2DABA9D, ___trackedImagesChanged_15)); }
	inline Action_1_t825CEDF2F074FBD059900D81615EFDE0AFD02CA0 * get_trackedImagesChanged_15() const { return ___trackedImagesChanged_15; }
	inline Action_1_t825CEDF2F074FBD059900D81615EFDE0AFD02CA0 ** get_address_of_trackedImagesChanged_15() { return &___trackedImagesChanged_15; }
	inline void set_trackedImagesChanged_15(Action_1_t825CEDF2F074FBD059900D81615EFDE0AFD02CA0 * value)
	{
		___trackedImagesChanged_15 = value;
		Il2CppCodeGenWriteBarrier((&___trackedImagesChanged_15), value);
	}

	inline static int32_t get_offset_of_m_ReferenceImages_16() { return static_cast<int32_t>(offsetof(ARTrackedImageManager_tC1F6C093163D1D4A5CCE83613BFCA465E2DABA9D, ___m_ReferenceImages_16)); }
	inline Dictionary_2_tE3586D05DB2E27BC21247078C0A121A82B014D91 * get_m_ReferenceImages_16() const { return ___m_ReferenceImages_16; }
	inline Dictionary_2_tE3586D05DB2E27BC21247078C0A121A82B014D91 ** get_address_of_m_ReferenceImages_16() { return &___m_ReferenceImages_16; }
	inline void set_m_ReferenceImages_16(Dictionary_2_tE3586D05DB2E27BC21247078C0A121A82B014D91 * value)
	{
		___m_ReferenceImages_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_ReferenceImages_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARTRACKEDIMAGEMANAGER_TC1F6C093163D1D4A5CCE83613BFCA465E2DABA9D_H
// MarkerPrefabs[]
struct MarkerPrefabsU5BU5D_t7F40C670B36EE25921E51F171FD6A68A7A1BC336  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) MarkerPrefabs_t227A4AF77E7197AEFE5B31A217749784AA27AE6B * m_Items[1];

public:
	inline MarkerPrefabs_t227A4AF77E7197AEFE5B31A217749784AA27AE6B * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline MarkerPrefabs_t227A4AF77E7197AEFE5B31A217749784AA27AE6B ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, MarkerPrefabs_t227A4AF77E7197AEFE5B31A217749784AA27AE6B * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline MarkerPrefabs_t227A4AF77E7197AEFE5B31A217749784AA27AE6B * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline MarkerPrefabs_t227A4AF77E7197AEFE5B31A217749784AA27AE6B ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, MarkerPrefabs_t227A4AF77E7197AEFE5B31A217749784AA27AE6B * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// PrefabsInVicinity[]
struct PrefabsInVicinityU5BU5D_tCF6E2464E3202CBD16C9DEA043361EA7DB3A49A9  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) PrefabsInVicinity_tF56E3155685D05BFC0529D6C62A92A5C2B88A039 * m_Items[1];

public:
	inline PrefabsInVicinity_tF56E3155685D05BFC0529D6C62A92A5C2B88A039 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline PrefabsInVicinity_tF56E3155685D05BFC0529D6C62A92A5C2B88A039 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, PrefabsInVicinity_tF56E3155685D05BFC0529D6C62A92A5C2B88A039 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline PrefabsInVicinity_tF56E3155685D05BFC0529D6C62A92A5C2B88A039 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline PrefabsInVicinity_tF56E3155685D05BFC0529D6C62A92A5C2B88A039 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, PrefabsInVicinity_tF56E3155685D05BFC0529D6C62A92A5C2B88A039 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// TurnTheGameOn.ArrowWaypointer.WaypointController_WaypointComponents[]
struct WaypointComponentsU5BU5D_t995379FD5B9F2912BDD0CA0EC38E68B64A9839FD  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) WaypointComponents_t3DE523CAE0F5855ADF2B740DBBA68569E9034301 * m_Items[1];

public:
	inline WaypointComponents_t3DE523CAE0F5855ADF2B740DBBA68569E9034301 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline WaypointComponents_t3DE523CAE0F5855ADF2B740DBBA68569E9034301 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, WaypointComponents_t3DE523CAE0F5855ADF2B740DBBA68569E9034301 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline WaypointComponents_t3DE523CAE0F5855ADF2B740DBBA68569E9034301 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline WaypointComponents_t3DE523CAE0F5855ADF2B740DBBA68569E9034301 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, WaypointComponents_t3DE523CAE0F5855ADF2B740DBBA68569E9034301 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};


// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * Component_GetComponent_TisRuntimeObject_m3FED1FF44F93EF1C3A07526800331B638EF4105B_gshared (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Object::FindObjectOfType<System.Object>()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * Object_FindObjectOfType_TisRuntimeObject_mE3957366B74863C807E6E8A23D239A0CB079BB9C_gshared (const RuntimeMethod* method);
// System.Void System.Action`1<UnityEngine.XR.ARFoundation.ARTrackedImagesChangedEventArgs>::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void Action_1__ctor_m685B9CA8548AB276638A829E0898E1BDF2A74388_gshared (Action_1_t825CEDF2F074FBD059900D81615EFDE0AFD02CA0 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<System.Object>::GetEnumerator()
extern "C" IL2CPP_METHOD_ATTR Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD  List_1_GetEnumerator_m52CC760E475D226A2B75048D70C4E22692F9F68D_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * Enumerator_get_Current_mD7829C7E8CFBEDD463B15A951CDE9B90A12CC55C_gshared (Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
extern "C" IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_m38B1099DDAD7EEDE2F4CDAB11C095AC784AC2E34_gshared (Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::Dispose()
extern "C" IL2CPP_METHOD_ATTR void Enumerator_Dispose_m94D0DAE031619503CDA6E53C5C3CC78AF3139472_gshared (Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD * __this, const RuntimeMethod* method);
// UnityEngine.XR.ARSubsystems.TrackingState UnityEngine.XR.ARFoundation.ARTrackable`2<UnityEngine.XR.ARSubsystems.XRTrackedImage,System.Object>::get_trackingState()
extern "C" IL2CPP_METHOD_ATTR int32_t ARTrackable_2_get_trackingState_m9663AF3C3BE3CDDE19D5E9F073FF39D076031190_gshared (ARTrackable_2_tDC22DD8BD2173402258A323598A3378BE19FACCB * __this, const RuntimeMethod* method);
// System.Void System.Array::Resize<System.Object>(!!0[]&,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void Array_Resize_TisRuntimeObject_m4D085923555AC38307156B34C6F0CBC2873E38B7_gshared (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** p0, int32_t p1, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * GameObject_GetComponent_TisRuntimeObject_mD4382B2843BA9A61A01A8F9D7B9813D060F9C9CA_gshared (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method);

// !!0 UnityEngine.Component::GetComponent<UnityEngine.Animator>()
inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * Component_GetComponent_TisAnimator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A_mEA6C8FBABE309599A828D6BB8777278BCFB28719 (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method)
{
	return ((  Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * (*) (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m3FED1FF44F93EF1C3A07526800331B638EF4105B_gshared)(__this, method);
}
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Rigidbody>()
inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * Component_GetComponent_TisRigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5_m3F58A77E3F62D9C80D7B49BA04E3D4809264FD5C (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method)
{
	return ((  Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * (*) (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m3FED1FF44F93EF1C3A07526800331B638EF4105B_gshared)(__this, method);
}
// !!0 UnityEngine.Component::GetComponent<UnityEngine.CapsuleCollider>()
inline CapsuleCollider_t5FD15B9E7BEEC4FFA8A2071E9FD2B8DEB3A826D1 * Component_GetComponent_TisCapsuleCollider_t5FD15B9E7BEEC4FFA8A2071E9FD2B8DEB3A826D1_m73D2BA64E56A00BAEDB982029C042CB4CF1A0D37 (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method)
{
	return ((  CapsuleCollider_t5FD15B9E7BEEC4FFA8A2071E9FD2B8DEB3A826D1 * (*) (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m3FED1FF44F93EF1C3A07526800331B638EF4105B_gshared)(__this, method);
}
// System.Single UnityEngine.CapsuleCollider::get_height()
extern "C" IL2CPP_METHOD_ATTR float CapsuleCollider_get_height_mA0F14683CEDB4F32B59D0262AB7507574228EF75 (CapsuleCollider_t5FD15B9E7BEEC4FFA8A2071E9FD2B8DEB3A826D1 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.CapsuleCollider::get_center()
extern "C" IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  CapsuleCollider_get_center_m415B40B8ADB6B1C29F3EF4C23839D5514BBA18AE (CapsuleCollider_t5FD15B9E7BEEC4FFA8A2071E9FD2B8DEB3A826D1 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Rigidbody::set_constraints(UnityEngine.RigidbodyConstraints)
extern "C" IL2CPP_METHOD_ATTR void Rigidbody_set_constraints_m6E6AACB03165E54952E7CFE13C07188205A7061F (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * __this, int32_t p0, const RuntimeMethod* method);
// System.Single UnityEngine.Vector3::get_magnitude()
extern "C" IL2CPP_METHOD_ATTR float Vector3_get_magnitude_m9A750659B60C5FE0C30438A7F9681775D5DB1274 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Vector3::Normalize()
extern "C" IL2CPP_METHOD_ATTR void Vector3_Normalize_m174460238EC6322B9095A378AA8624B1DD9000F3 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * __this, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.Component::get_transform()
extern "C" IL2CPP_METHOD_ATTR Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9 (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::InverseTransformDirection(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Transform_InverseTransformDirection_m6F0513F2EC19C204F2077E3C68DD1D45317CB5F2 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  p0, const RuntimeMethod* method);
// System.Void CharacterControllerThirdPerson::CheckGroundStatus()
extern "C" IL2CPP_METHOD_ATTR void CharacterControllerThirdPerson_CheckGroundStatus_mDC821F214A629FE6A8E67AFFE8DE88E6B9ED6FBC (CharacterControllerThirdPerson_t2CC0D121855408DC72A1CB71940065D5C220C886 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::ProjectOnPlane(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector3_ProjectOnPlane_mAF89645654808BBD9754610879F94A2B3323D206 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  p0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  p1, const RuntimeMethod* method);
// System.Void CharacterControllerThirdPerson::ApplyExtraTurnRotation()
extern "C" IL2CPP_METHOD_ATTR void CharacterControllerThirdPerson_ApplyExtraTurnRotation_mF7EA79A525D0D136B16D4AA00A524BA395FFBFD1 (CharacterControllerThirdPerson_t2CC0D121855408DC72A1CB71940065D5C220C886 * __this, const RuntimeMethod* method);
// System.Void CharacterControllerThirdPerson::HandleGroundedMovement(System.Boolean,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void CharacterControllerThirdPerson_HandleGroundedMovement_m8BC178F170EF39BDD6F26A0B4889A3AF6C11BCEA (CharacterControllerThirdPerson_t2CC0D121855408DC72A1CB71940065D5C220C886 * __this, bool ___crouch0, bool ___jump1, const RuntimeMethod* method);
// System.Void CharacterControllerThirdPerson::HandleAirborneMovement()
extern "C" IL2CPP_METHOD_ATTR void CharacterControllerThirdPerson_HandleAirborneMovement_m0DF8799B3B9814A782A10AD5434F9B21A14D8B33 (CharacterControllerThirdPerson_t2CC0D121855408DC72A1CB71940065D5C220C886 * __this, const RuntimeMethod* method);
// System.Void CharacterControllerThirdPerson::ScaleCapsuleForCrouching(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void CharacterControllerThirdPerson_ScaleCapsuleForCrouching_m7EA7CA090344079B74749A8D6FE2A06E9946D231 (CharacterControllerThirdPerson_t2CC0D121855408DC72A1CB71940065D5C220C886 * __this, bool ___crouch0, const RuntimeMethod* method);
// System.Void CharacterControllerThirdPerson::PreventStandingInLowHeadroom()
extern "C" IL2CPP_METHOD_ATTR void CharacterControllerThirdPerson_PreventStandingInLowHeadroom_mFA01E5F165FACD66CB419F8439B763621CF2C5EF (CharacterControllerThirdPerson_t2CC0D121855408DC72A1CB71940065D5C220C886 * __this, const RuntimeMethod* method);
// System.Void CharacterControllerThirdPerson::UpdateAnimator(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR void CharacterControllerThirdPerson_UpdateAnimator_m2CCBBD592478AA52D15022D00AC3B5765466BFEA (CharacterControllerThirdPerson_t2CC0D121855408DC72A1CB71940065D5C220C886 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___move0, const RuntimeMethod* method);
// System.Void UnityEngine.CapsuleCollider::set_height(System.Single)
extern "C" IL2CPP_METHOD_ATTR void CapsuleCollider_set_height_m77D7E2FFC2A2D587B30746E00690A375D3E3D0F6 (CapsuleCollider_t5FD15B9E7BEEC4FFA8A2071E9FD2B8DEB3A826D1 * __this, float p0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Division(UnityEngine.Vector3,System.Single)
extern "C" IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector3_op_Division_mDF34F1CC445981B4D1137765BC6277419E561624 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  p0, float p1, const RuntimeMethod* method);
// System.Void UnityEngine.CapsuleCollider::set_center(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR void CapsuleCollider_set_center_mD2A261C23EB2DE8B0D9F946596BF64B72F7015C6 (CapsuleCollider_t5FD15B9E7BEEC4FFA8A2071E9FD2B8DEB3A826D1 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  p0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Rigidbody::get_position()
extern "C" IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Rigidbody_get_position_m478D060638E43DE3AE9C931A42593484B8310113 (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_up()
extern "C" IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector3_get_up_m6309EBC4E42D6D0B3D28056BD23D0331275306F7 (const RuntimeMethod* method);
// System.Single UnityEngine.CapsuleCollider::get_radius()
extern "C" IL2CPP_METHOD_ATTR float CapsuleCollider_get_radius_m8E753A625226A5AF557AAFEBF5B6D0720C00802D (CapsuleCollider_t5FD15B9E7BEEC4FFA8A2071E9FD2B8DEB3A826D1 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(UnityEngine.Vector3,System.Single)
extern "C" IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector3_op_Multiply_m1C5F07723615156ACF035D88A1280A9E8F35A04E (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  p0, float p1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Addition(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector3_op_Addition_m929F9C17E5D11B94D50B4AFF1D730B70CB59B50E (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  p0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  p1, const RuntimeMethod* method);
// System.Void UnityEngine.Ray::.ctor(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR void Ray__ctor_m695D219349B8AA4C82F96C55A27D384C07736F6B (Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  p0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  p1, const RuntimeMethod* method);
// System.Boolean UnityEngine.Physics::SphereCast(UnityEngine.Ray,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR bool Physics_SphereCast_m618453A51702CF6F34D788125CD005D803293610 (Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2  p0, float p1, float p2, const RuntimeMethod* method);
// System.Single UnityEngine.Time::get_deltaTime()
extern "C" IL2CPP_METHOD_ATTR float Time_get_deltaTime_m16F98FC9BA931581236008C288E3B25CBCB7C81E (const RuntimeMethod* method);
// System.Void UnityEngine.Animator::SetFloat(System.String,System.Single,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Animator_SetFloat_mC260C82EF3FD1DB6B66FE898C595D5F901E59681 (Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * __this, String_t* p0, float p1, float p2, float p3, const RuntimeMethod* method);
// System.Void UnityEngine.Animator::SetBool(System.String,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void Animator_SetBool_m497805BA217139E42808899782FA05C15BC9879E (Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * __this, String_t* p0, bool p1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Rigidbody::get_velocity()
extern "C" IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Rigidbody_get_velocity_m584A6D79C3657C21AE9CAA56BEE05582B8D5A2B8 (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Animator::SetFloat(System.String,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Animator_SetFloat_mE4C29F6980EBBBD954637721E6E13A0BE2B13C43 (Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * __this, String_t* p0, float p1, const RuntimeMethod* method);
// UnityEngine.AnimatorStateInfo UnityEngine.Animator::GetCurrentAnimatorStateInfo(System.Int32)
extern "C" IL2CPP_METHOD_ATTR AnimatorStateInfo_tF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2  Animator_GetCurrentAnimatorStateInfo_mBE5ED0D60A6F5CD0EDD40AF1494098D4E7BF84F2 (Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * __this, int32_t p0, const RuntimeMethod* method);
// System.Single UnityEngine.AnimatorStateInfo::get_normalizedTime()
extern "C" IL2CPP_METHOD_ATTR float AnimatorStateInfo_get_normalizedTime_m3AB6A2DB592BB9CA0B9A4EF8A382C4DF3F5F6BBD (AnimatorStateInfo_tF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Mathf::Repeat(System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR float Mathf_Repeat_m8459F4AAFF92DB770CC892BF71EE9438D9D0F779 (float p0, float p1, const RuntimeMethod* method);
// System.Void UnityEngine.Animator::set_speed(System.Single)
extern "C" IL2CPP_METHOD_ATTR void Animator_set_speed_mEA558D196D84684744A642A56AFBF22F16448813 (Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * __this, float p0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Physics::get_gravity()
extern "C" IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Physics_get_gravity_mC0F44371C49C1DD1455D6ADEC824BAE35E1E104B (const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Subtraction(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector3_op_Subtraction_mF9846B723A5034F8B9F5F5DCB78E3D67649143D3 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  p0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  p1, const RuntimeMethod* method);
// System.Void UnityEngine.Rigidbody::AddForce(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR void Rigidbody_AddForce_mC8140D90B806634A733624F671C45AD7CDBEDB38 (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  p0, const RuntimeMethod* method);
// System.Boolean UnityEngine.AnimatorStateInfo::IsName(System.String)
extern "C" IL2CPP_METHOD_ATTR bool AnimatorStateInfo_IsName_mD133471910C1ACE0979F3FD11462C9B8BB24CA96 (AnimatorStateInfo_tF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2 * __this, String_t* p0, const RuntimeMethod* method);
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * __this, float p0, float p1, float p2, const RuntimeMethod* method);
// System.Void UnityEngine.Rigidbody::set_velocity(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR void Rigidbody_set_velocity_m8D129E88E62AD02AB81CFC8BE694C4A5A2B2B380 (Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  p0, const RuntimeMethod* method);
// System.Void UnityEngine.Animator::set_applyRootMotion(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void Animator_set_applyRootMotion_mD25B7FCFCDAD7C5D107E860D2E46AB765271A982 (Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * __this, bool p0, const RuntimeMethod* method);
// System.Single UnityEngine.Mathf::Lerp(System.Single,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR float Mathf_Lerp_m9A74C5A0C37D0CDF45EE66E7774D12A9B93B1364 (float p0, float p1, float p2, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::Rotate(System.Single,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Transform_Rotate_mEEA80F3DA5A4C93611D7165DF54763E578477EF9 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, float p0, float p1, float p2, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Animator::get_deltaPosition()
extern "C" IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Animator_get_deltaPosition_m85287AA699C50B6AA7489CD313F8CFED5D998533 (Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
extern "C" IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_down()
extern "C" IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector3_get_down_m3F76A48E5B7C82B35EE047375538AFD91A305F55 (const RuntimeMethod* method);
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single)
extern "C" IL2CPP_METHOD_ATTR bool Physics_Raycast_mBEC747ED0A7660BB12AA48B663CCBF7B1EE93D6B (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  p0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  p1, RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 * p2, float p3, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.RaycastHit::get_normal()
extern "C" IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  RaycastHit_get_normal_mF736A6D09D98D63AB7E5BF10F38AEBFC177A1D94 (RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C" IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97 (MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * __this, const RuntimeMethod* method);
// UnityEngine.Camera UnityEngine.Camera::get_main()
extern "C" IL2CPP_METHOD_ATTR Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * Camera_get_main_m9256A9F84F92D7ED73F3E6C4E2694030AD8B61FA (const RuntimeMethod* method);
// !!0 UnityEngine.Object::FindObjectOfType<UnityEngine.XR.ARFoundation.ARTrackedImageManager>()
inline ARTrackedImageManager_tC1F6C093163D1D4A5CCE83613BFCA465E2DABA9D * Object_FindObjectOfType_TisARTrackedImageManager_tC1F6C093163D1D4A5CCE83613BFCA465E2DABA9D_m8E2DA26A9425734CF006C5373BEB1A4C5A896180 (const RuntimeMethod* method)
{
	return ((  ARTrackedImageManager_tC1F6C093163D1D4A5CCE83613BFCA465E2DABA9D * (*) (const RuntimeMethod*))Object_FindObjectOfType_TisRuntimeObject_mE3957366B74863C807E6E8A23D239A0CB079BB9C_gshared)(method);
}
// System.Void System.Action`1<UnityEngine.XR.ARFoundation.ARTrackedImagesChangedEventArgs>::.ctor(System.Object,System.IntPtr)
inline void Action_1__ctor_m685B9CA8548AB276638A829E0898E1BDF2A74388 (Action_1_t825CEDF2F074FBD059900D81615EFDE0AFD02CA0 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method)
{
	((  void (*) (Action_1_t825CEDF2F074FBD059900D81615EFDE0AFD02CA0 *, RuntimeObject *, intptr_t, const RuntimeMethod*))Action_1__ctor_m685B9CA8548AB276638A829E0898E1BDF2A74388_gshared)(__this, p0, p1, method);
}
// System.Void UnityEngine.XR.ARFoundation.ARTrackedImageManager::add_trackedImagesChanged(System.Action`1<UnityEngine.XR.ARFoundation.ARTrackedImagesChangedEventArgs>)
extern "C" IL2CPP_METHOD_ATTR void ARTrackedImageManager_add_trackedImagesChanged_m49E4C6CC7F6EC93050C7C084E2C779323929FCEB (ARTrackedImageManager_tC1F6C093163D1D4A5CCE83613BFCA465E2DABA9D * __this, Action_1_t825CEDF2F074FBD059900D81615EFDE0AFD02CA0 * p0, const RuntimeMethod* method);
// System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARTrackedImage> UnityEngine.XR.ARFoundation.ARTrackedImagesChangedEventArgs::get_updated()
extern "C" IL2CPP_METHOD_ATTR List_1_t85456E82777B8C7F9CFFED2C540D0613542A8C8D * ARTrackedImagesChangedEventArgs_get_updated_m6A3BB3D6B5EE300996814DA2DE6DE45C46B48DDB (ARTrackedImagesChangedEventArgs_t64F82ABE51189C62E3811D96F2C2143DB91669E4 * __this, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARTrackedImage>::GetEnumerator()
inline Enumerator_t6754DF054050219F46CEED0786A14A987A139408  List_1_GetEnumerator_m06B9ADCB9085604C5418BD1AC24ADA9237C95CF9 (List_1_t85456E82777B8C7F9CFFED2C540D0613542A8C8D * __this, const RuntimeMethod* method)
{
	return ((  Enumerator_t6754DF054050219F46CEED0786A14A987A139408  (*) (List_1_t85456E82777B8C7F9CFFED2C540D0613542A8C8D *, const RuntimeMethod*))List_1_GetEnumerator_m52CC760E475D226A2B75048D70C4E22692F9F68D_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.XR.ARFoundation.ARTrackedImage>::get_Current()
inline ARTrackedImage_tD699A1D6CA301ECE200D07D1CF1722E94AFEF01A * Enumerator_get_Current_m800C03D423EC5FCFD560CC7A2B3FF44087CEE0BF (Enumerator_t6754DF054050219F46CEED0786A14A987A139408 * __this, const RuntimeMethod* method)
{
	return ((  ARTrackedImage_tD699A1D6CA301ECE200D07D1CF1722E94AFEF01A * (*) (Enumerator_t6754DF054050219F46CEED0786A14A987A139408 *, const RuntimeMethod*))Enumerator_get_Current_mD7829C7E8CFBEDD463B15A951CDE9B90A12CC55C_gshared)(__this, method);
}
// UnityEngine.XR.ARSubsystems.XRReferenceImage UnityEngine.XR.ARFoundation.ARTrackedImage::get_referenceImage()
extern "C" IL2CPP_METHOD_ATTR XRReferenceImage_t5A53387AC6253D5D3DFD62BC583A45BBDFC1347E  ARTrackedImage_get_referenceImage_m46AAAD2FF5BF6E2A9AB8192BCE198FB69F00CC7D (ARTrackedImage_tD699A1D6CA301ECE200D07D1CF1722E94AFEF01A * __this, const RuntimeMethod* method);
// System.String UnityEngine.XR.ARSubsystems.XRReferenceImage::get_name()
extern "C" IL2CPP_METHOD_ATTR String_t* XRReferenceImage_get_name_m6DD83ECED755444645A14816918747E2EEAE92A9 (XRReferenceImage_t5A53387AC6253D5D3DFD62BC583A45BBDFC1347E * __this, const RuntimeMethod* method);
// System.String System.String::Concat(System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR String_t* String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE (String_t* p0, String_t* p1, const RuntimeMethod* method);
// System.Void UnityEngine.Debug::Log(System.Object)
extern "C" IL2CPP_METHOD_ATTR void Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708 (RuntimeObject * p0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.XR.ARFoundation.ARTrackedImage>::MoveNext()
inline bool Enumerator_MoveNext_m75BAC688340F0794E4C6C98B495BB4B9A02E6522 (Enumerator_t6754DF054050219F46CEED0786A14A987A139408 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_t6754DF054050219F46CEED0786A14A987A139408 *, const RuntimeMethod*))Enumerator_MoveNext_m38B1099DDAD7EEDE2F4CDAB11C095AC784AC2E34_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.XR.ARFoundation.ARTrackedImage>::Dispose()
inline void Enumerator_Dispose_m46C5EE3170D6F427C4379CE89AFE659555B786A7 (Enumerator_t6754DF054050219F46CEED0786A14A987A139408 * __this, const RuntimeMethod* method)
{
	((  void (*) (Enumerator_t6754DF054050219F46CEED0786A14A987A139408 *, const RuntimeMethod*))Enumerator_Dispose_m94D0DAE031619503CDA6E53C5C3CC78AF3139472_gshared)(__this, method);
}
// UnityEngine.XR.ARSubsystems.TrackingState UnityEngine.XR.ARFoundation.ARTrackable`2<UnityEngine.XR.ARSubsystems.XRTrackedImage,UnityEngine.XR.ARFoundation.ARTrackedImage>::get_trackingState()
inline int32_t ARTrackable_2_get_trackingState_mE6C9A0305C0A41DDF81E1330094981C9C1A00BCA (ARTrackable_2_t1EDE6011EE2879288E62C947FC7EF0BBD638DDF8 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (ARTrackable_2_t1EDE6011EE2879288E62C947FC7EF0BBD638DDF8 *, const RuntimeMethod*))ARTrackable_2_get_trackingState_m9663AF3C3BE3CDDE19D5E9F073FF39D076031190_gshared)(__this, method);
}
// System.Boolean System.String::op_Equality(System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR bool String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE (String_t* p0, String_t* p1, const RuntimeMethod* method);
// System.Void UnityEngine.GameObject::SetActive(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04 (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, bool p0, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.GameObject::get_transform()
extern "C" IL2CPP_METHOD_ATTR Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR void Transform_set_position_mDA89E4893F14ECA5CBEEE7FB80A5BF7C1B8EA6DC (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  p0, const RuntimeMethod* method);
// System.String UnityEngine.Vector3::ToString()
extern "C" IL2CPP_METHOD_ATTR String_t* Vector3_ToString_m2682D27AB50CD1CE4677C38D0720A302D582348D (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * __this, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Transform::get_rotation()
extern "C" IL2CPP_METHOD_ATTR Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  Transform_get_rotation_m3AB90A67403249AECCA5E02BC70FCE8C90FE9FB9 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Quaternion::Euler(System.Single,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  Quaternion_Euler_m537DD6CEAE0AD4274D8A84414C24C30730427D05 (float p0, float p1, float p2, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Quaternion::op_Multiply(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C" IL2CPP_METHOD_ATTR Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  Quaternion_op_Multiply_mDB9F738AA8160E3D85549F4FEDA23BC658B5A790 (Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  p0, Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  p1, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_rotation(UnityEngine.Quaternion)
extern "C" IL2CPP_METHOD_ATTR void Transform_set_rotation_m429694E264117C6DC682EC6AF45C7864E5155935 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  p0, const RuntimeMethod* method);
// System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARTrackedImage> UnityEngine.XR.ARFoundation.ARTrackedImagesChangedEventArgs::get_removed()
extern "C" IL2CPP_METHOD_ATTR List_1_t85456E82777B8C7F9CFFED2C540D0613542A8C8D * ARTrackedImagesChangedEventArgs_get_removed_mC4922D72993078710694F8D76A02BE9A23EFB7E8 (ARTrackedImagesChangedEventArgs_t64F82ABE51189C62E3811D96F2C2143DB91669E4 * __this, const RuntimeMethod* method);
// System.Void ImageRecognitionExample::inflateObjectsInVicinity()
extern "C" IL2CPP_METHOD_ATTR void ImageRecognitionExample_inflateObjectsInVicinity_mFEF7B58384ED8DAD668118E514CFEBE3B52635C1 (ImageRecognitionExample_t911A7F2FD5BFFF23A30C97F8619C18A1F0757110 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Camera::WorldToViewportPoint(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Camera_WorldToViewportPoint_mC939A83CE134EA2CE666C2DF5AD8FA4CB48AE3C1 (Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  p0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern "C" IL2CPP_METHOD_ATTR bool Object_op_Inequality_m31EF58E217E8F4BDD3E409DEF79E1AEE95874FC1 (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * p0, Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * p1, const RuntimeMethod* method);
// System.Single UnityEngine.Vector3::Distance(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR float Vector3_Distance_mE316E10B9B319A5C2A29F86E028740FD528149E7 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  p0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  p1, const RuntimeMethod* method);
// System.String System.String::Concat(System.Object[])
extern "C" IL2CPP_METHOD_ATTR String_t* String_Concat_mB7BA84F13912303B2E5E40FBF0109E1A328ACA07 (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* p0, const RuntimeMethod* method);
// System.Void System.Object::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Implicit(UnityEngine.Object)
extern "C" IL2CPP_METHOD_ATTR bool Object_op_Implicit_m8B2A44B4B1406ED346D1AE6D962294FD58D0D534 (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * p0, const RuntimeMethod* method);
// System.Void TurnTheGameOn.ArrowWaypointer.WaypointController::ChangeTarget()
extern "C" IL2CPP_METHOD_ATTR void WaypointController_ChangeTarget_m8D5F516033E81CC08980BAA7CE9FE4187456B63B (WaypointController_tD4E5882DF3A666D2E1C97E9E73061816C123AB20 * __this, const RuntimeMethod* method);
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C" IL2CPP_METHOD_ATTR GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method);
// System.String UnityEngine.GameObject::get_tag()
extern "C" IL2CPP_METHOD_ATTR String_t* GameObject_get_tag_mA9DC75D3D591B5E7D1ADAD14EA66CC4186580275 (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method);
// System.Void TurnTheGameOn.ArrowWaypointer.WaypointController::WaypointEvent(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void WaypointController_WaypointEvent_m69D615D2D554FDFDFCAE78015B032EB721E3BF01 (WaypointController_tD4E5882DF3A666D2E1C97E9E73061816C123AB20 * __this, int32_t ___waypointEvent0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Application::get_isPlaying()
extern "C" IL2CPP_METHOD_ATTR bool Application_get_isPlaying_mF43B519662E7433DD90D883E5AE22EC3CFB65CA5 (const RuntimeMethod* method);
// System.Void UnityEngine.GameObject::.ctor()
extern "C" IL2CPP_METHOD_ATTR void GameObject__ctor_mA4DFA8F4471418C248E95B55070665EF344B4B2D (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Object::set_name(System.String)
extern "C" IL2CPP_METHOD_ATTR void Object_set_name_m538711B144CDE30F929376BCF72D0DC8F85D0826 (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * __this, String_t* p0, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_parent(UnityEngine.Transform)
extern "C" IL2CPP_METHOD_ATTR void Transform_set_parent_m65B8E4660B2C554069C57A957D9E55FECA7AA73E (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * p0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_localPosition()
extern "C" IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Transform_get_localPosition_m812D43318E05BDCB78310EB7308785A13D85EFD8 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::Lerp(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern "C" IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector3_Lerp_m5BA75496B803820CC64079383956D73C6FD4A8A1 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  p0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  p1, float p2, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_localPosition(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR void Transform_set_localPosition_m275F5550DD939F83AFEB5E8D681131172E2E1728 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  p0, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Transform::get_localRotation()
extern "C" IL2CPP_METHOD_ATTR Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  Transform_get_localRotation_mEDA319E1B42EF12A19A95AC0824345B6574863FE (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Quaternion::Lerp(UnityEngine.Quaternion,UnityEngine.Quaternion,System.Single)
extern "C" IL2CPP_METHOD_ATTR Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  Quaternion_Lerp_m749B3988EE2EF387CC9BFB76C81B7465A7534E27 (Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  p0, Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  p1, float p2, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_localRotation(UnityEngine.Quaternion)
extern "C" IL2CPP_METHOD_ATTR void Transform_set_localRotation_mE2BECB0954FFC1D93FB631600D9A9BEFF41D9C8A (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  p0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C" IL2CPP_METHOD_ATTR bool Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * p0, Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * p1, const RuntimeMethod* method);
// System.Void TurnTheGameOn.ArrowWaypointer.WaypointController::FindArrow()
extern "C" IL2CPP_METHOD_ATTR void WaypointController_FindArrow_m95F170F1E2ADB8A718A914339C326FD87324661F (WaypointController_tD4E5882DF3A666D2E1C97E9E73061816C123AB20 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::LookAt(UnityEngine.Transform)
extern "C" IL2CPP_METHOD_ATTR void Transform_LookAt_mF2738B4AB464ABFB85D16BEB121808F6C73D669B (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * p0, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityEvent::Invoke()
extern "C" IL2CPP_METHOD_ATTR void UnityEvent_Invoke_mB2FA1C76256FE34D5E7F84ABE528AC61CE8A0325 (UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
extern "C" IL2CPP_METHOD_ATTR void Object_Destroy_m23B4562495BA35A74266D4372D45368F8C05109A (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * p0, const RuntimeMethod* method);
// System.Type System.Type::GetTypeFromHandle(System.RuntimeTypeHandle)
extern "C" IL2CPP_METHOD_ATTR Type_t * Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6 (RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  p0, const RuntimeMethod* method);
// UnityEngine.Object UnityEngine.Resources::Load(System.String,System.Type)
extern "C" IL2CPP_METHOD_ATTR Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * Resources_Load_mF0FA033BF566CDDA6A0E69BB97283B44C40726E7 (String_t* p0, Type_t * p1, const RuntimeMethod* method);
// UnityEngine.Object UnityEngine.Object::Instantiate(UnityEngine.Object)
extern "C" IL2CPP_METHOD_ATTR Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * Object_Instantiate_m17AA3123A55239124BC54A907AEEE509034F0830 (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * p0, const RuntimeMethod* method);
// UnityEngine.GameObject UnityEngine.GameObject::Find(System.String)
extern "C" IL2CPP_METHOD_ATTR GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * GameObject_Find_m1470FB04EB6DB15CCC0D9745B70EE987B318E9BD (String_t* p0, const RuntimeMethod* method);
// System.Void TurnTheGameOn.ArrowWaypointer.WaypointController::CreateArrow()
extern "C" IL2CPP_METHOD_ATTR void WaypointController_CreateArrow_m3E17F297C402A2F304D0FA081808552E733DC7BC (WaypointController_tD4E5882DF3A666D2E1C97E9E73061816C123AB20 * __this, const RuntimeMethod* method);
// System.Void System.Array::Resize<TurnTheGameOn.ArrowWaypointer.WaypointController/WaypointComponents>(!!0[]&,System.Int32)
inline void Array_Resize_TisWaypointComponents_t3DE523CAE0F5855ADF2B740DBBA68569E9034301_m9D1CC9E22BEE7FA8386256BA820CC2B8D7CAA87A (WaypointComponentsU5BU5D_t995379FD5B9F2912BDD0CA0EC38E68B64A9839FD** p0, int32_t p1, const RuntimeMethod* method)
{
	((  void (*) (WaypointComponentsU5BU5D_t995379FD5B9F2912BDD0CA0EC38E68B64A9839FD**, int32_t, const RuntimeMethod*))Array_Resize_TisRuntimeObject_m4D085923555AC38307156B34C6F0CBC2873E38B7_gshared)(p0, p1, method);
}
// System.String System.String::Concat(System.Object,System.Object)
extern "C" IL2CPP_METHOD_ATTR String_t* String_Concat_mBB19C73816BDD1C3519F248E1ADC8E11A6FDB495 (RuntimeObject * p0, RuntimeObject * p1, const RuntimeMethod* method);
// System.Collections.IEnumerator UnityEngine.Transform::GetEnumerator()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* Transform_GetEnumerator_mE98B6C5F644AE362EC1D58C10506327D6A5878FC (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, const RuntimeMethod* method);
// System.String UnityEngine.Object::get_name()
extern "C" IL2CPP_METHOD_ATTR String_t* Object_get_name_mA2D400141CB3C991C87A2556429781DE961A83CE (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<TurnTheGameOn.ArrowWaypointer.Waypoint>()
inline Waypoint_t7B48E7B71CF48791746A5B866BCCB7F9FABFCA0A * Component_GetComponent_TisWaypoint_t7B48E7B71CF48791746A5B866BCCB7F9FABFCA0A_m7AB24D399574ADB4EAD0A31828AAFDA12A50233F (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method)
{
	return ((  Waypoint_t7B48E7B71CF48791746A5B866BCCB7F9FABFCA0A * (*) (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m3FED1FF44F93EF1C3A07526800331B638EF4105B_gshared)(__this, method);
}
// UnityEngine.Object UnityEngine.Resources::Load(System.String)
extern "C" IL2CPP_METHOD_ATTR Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * Resources_Load_mDBE6B83A74A52A1A6F5A68F7E5BC112DBB81B3C1 (String_t* p0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<TurnTheGameOn.ArrowWaypointer.Waypoint>()
inline Waypoint_t7B48E7B71CF48791746A5B866BCCB7F9FABFCA0A * GameObject_GetComponent_TisWaypoint_t7B48E7B71CF48791746A5B866BCCB7F9FABFCA0A_m8DDD1ED52C9E382D4753784215506A29EDA35B32 (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method)
{
	return ((  Waypoint_t7B48E7B71CF48791746A5B866BCCB7F9FABFCA0A * (*) (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_mD4382B2843BA9A61A01A8F9D7B9813D060F9C9CA_gshared)(__this, method);
}
// System.Void TurnTheGameOn.ArrowWaypointer.WaypointController::CleanUpWaypoints()
extern "C" IL2CPP_METHOD_ATTR void WaypointController_CleanUpWaypoints_m7B8D3FEB7086B037AB945021C1D8B9753FB8C172 (WaypointController_tD4E5882DF3A666D2E1C97E9E73061816C123AB20 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Debug::LogWarning(System.Object)
extern "C" IL2CPP_METHOD_ATTR void Debug_LogWarning_m37338644DC81F640CCDFEAE35A223F0E965F0568 (RuntimeObject * p0, const RuntimeMethod* method);
// System.Int32 UnityEngine.Transform::get_childCount()
extern "C" IL2CPP_METHOD_ATTR int32_t Transform_get_childCount_m7665D779DCDB6B175FB52A254276CDF0C384A724 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Object::DestroyImmediate(UnityEngine.Object)
extern "C" IL2CPP_METHOD_ATTR void Object_DestroyImmediate_mF6F4415EF22249D6E650FAA40E403283F19B7446 (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * p0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_eulerAngles()
extern "C" IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Transform_get_eulerAngles_mF2D798FA8B18F7A1A0C4A2198329ADBAF07E37CA (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Mathf::LerpAngle(System.Single,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR float Mathf_LerpAngle_m8802FB8DCECA2D48FDD13C20F384B9430E66C99F (float p0, float p1, float p2, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_forward()
extern "C" IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector3_get_forward_m3E2E192B3302130098738C308FA1EE1439449D0D (const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Quaternion::op_Multiply(UnityEngine.Quaternion,UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Quaternion_op_Multiply_mD5999DE317D808808B72E58E7A978C4C0995879C (Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  p0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  p1, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<CharacterControllerThirdPerson>()
inline CharacterControllerThirdPerson_t2CC0D121855408DC72A1CB71940065D5C220C886 * Component_GetComponent_TisCharacterControllerThirdPerson_t2CC0D121855408DC72A1CB71940065D5C220C886_mBED54F2A08F701B5F685DC0A1DDCE406FE052B2D (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method)
{
	return ((  CharacterControllerThirdPerson_t2CC0D121855408DC72A1CB71940065D5C220C886 * (*) (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m3FED1FF44F93EF1C3A07526800331B638EF4105B_gshared)(__this, method);
}
// System.Boolean UnityEngine.Input::GetButtonDown(System.String)
extern "C" IL2CPP_METHOD_ATTR bool Input_GetButtonDown_mBCFF01DD979D2BE061A8F47082060258F2C4458B (String_t* p0, const RuntimeMethod* method);
// System.Single UnityEngine.Input::GetAxis(System.String)
extern "C" IL2CPP_METHOD_ATTR float Input_GetAxis_mF334BDB532DF82E30273ABA0ACB5116AC8244496 (String_t* p0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Input::GetKey(UnityEngine.KeyCode)
extern "C" IL2CPP_METHOD_ATTR bool Input_GetKey_m54DC93F781FFEAB1DCDFAA3D15FA2FC01BF7667A (int32_t p0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_forward()
extern "C" IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Transform_get_forward_m0BE1E88B86049ADA39391C3ACED2314A624BC67F (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::Scale(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector3_Scale_m77004B226483C7644B3F4A46B950589EE8F88775 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  p0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  p1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_normalized()
extern "C" IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector3_get_normalized_mE20796F1D2D36244FACD4D14DADB245BE579849B (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(System.Single,UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector3_op_Multiply_mC7A8D6FD19E58DBF98E30D454F59F142F7BF8839 (float p0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  p1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_right()
extern "C" IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Transform_get_right_mC32CE648E98D3D4F62F897A2751EE567C7C0CFB0 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_right()
extern "C" IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector3_get_right_m6DD9559CA0C75BBA42D9140021C4C2A9AAA9B3F5 (const RuntimeMethod* method);
// System.Void CharacterControllerThirdPerson::Move(UnityEngine.Vector3,System.Boolean,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void CharacterControllerThirdPerson_Move_mB818BF9313F1CC2571D963FEBBBBFC1765E43EF4 (CharacterControllerThirdPerson_t2CC0D121855408DC72A1CB71940065D5C220C886 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___move0, bool ___crouch1, bool ___jump2, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CharacterControllerThirdPerson::Start()
extern "C" IL2CPP_METHOD_ATTR void CharacterControllerThirdPerson_Start_m54B47B65EDA1E3776CF4EC4BAA6C297F67A44C31 (CharacterControllerThirdPerson_t2CC0D121855408DC72A1CB71940065D5C220C886 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CharacterControllerThirdPerson_Start_m54B47B65EDA1E3776CF4EC4BAA6C297F67A44C31_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * L_0 = Component_GetComponent_TisAnimator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A_mEA6C8FBABE309599A828D6BB8777278BCFB28719(__this, /*hidden argument*/Component_GetComponent_TisAnimator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A_mEA6C8FBABE309599A828D6BB8777278BCFB28719_RuntimeMethod_var);
		__this->set_m_Animator_13(L_0);
		Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * L_1 = Component_GetComponent_TisRigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5_m3F58A77E3F62D9C80D7B49BA04E3D4809264FD5C(__this, /*hidden argument*/Component_GetComponent_TisRigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5_m3F58A77E3F62D9C80D7B49BA04E3D4809264FD5C_RuntimeMethod_var);
		__this->set_m_Rigidbody_12(L_1);
		CapsuleCollider_t5FD15B9E7BEEC4FFA8A2071E9FD2B8DEB3A826D1 * L_2 = Component_GetComponent_TisCapsuleCollider_t5FD15B9E7BEEC4FFA8A2071E9FD2B8DEB3A826D1_m73D2BA64E56A00BAEDB982029C042CB4CF1A0D37(__this, /*hidden argument*/Component_GetComponent_TisCapsuleCollider_t5FD15B9E7BEEC4FFA8A2071E9FD2B8DEB3A826D1_m73D2BA64E56A00BAEDB982029C042CB4CF1A0D37_RuntimeMethod_var);
		__this->set_m_Capsule_22(L_2);
		CapsuleCollider_t5FD15B9E7BEEC4FFA8A2071E9FD2B8DEB3A826D1 * L_3 = __this->get_m_Capsule_22();
		NullCheck(L_3);
		float L_4 = CapsuleCollider_get_height_mA0F14683CEDB4F32B59D0262AB7507574228EF75(L_3, /*hidden argument*/NULL);
		__this->set_m_CapsuleHeight_20(L_4);
		CapsuleCollider_t5FD15B9E7BEEC4FFA8A2071E9FD2B8DEB3A826D1 * L_5 = __this->get_m_Capsule_22();
		NullCheck(L_5);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_6 = CapsuleCollider_get_center_m415B40B8ADB6B1C29F3EF4C23839D5514BBA18AE(L_5, /*hidden argument*/NULL);
		__this->set_m_CapsuleCenter_21(L_6);
		Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * L_7 = __this->get_m_Rigidbody_12();
		NullCheck(L_7);
		Rigidbody_set_constraints_m6E6AACB03165E54952E7CFE13C07188205A7061F(L_7, ((int32_t)112), /*hidden argument*/NULL);
		float L_8 = __this->get_m_GroundCheckDistance_11();
		__this->set_m_OrigGroundCheckDistance_15(L_8);
		return;
	}
}
// System.Void CharacterControllerThirdPerson::Move(UnityEngine.Vector3,System.Boolean,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void CharacterControllerThirdPerson_Move_mB818BF9313F1CC2571D963FEBBBBFC1765E43EF4 (CharacterControllerThirdPerson_t2CC0D121855408DC72A1CB71940065D5C220C886 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___move0, bool ___crouch1, bool ___jump2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CharacterControllerThirdPerson_Move_mB818BF9313F1CC2571D963FEBBBBFC1765E43EF4_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = Vector3_get_magnitude_m9A750659B60C5FE0C30438A7F9681775D5DB1274((Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&___move0), /*hidden argument*/NULL);
		if ((!(((float)L_0) > ((float)(1.0f)))))
		{
			goto IL_0015;
		}
	}
	{
		Vector3_Normalize_m174460238EC6322B9095A378AA8624B1DD9000F3((Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&___move0), /*hidden argument*/NULL);
	}

IL_0015:
	{
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_1 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_2 = ___move0;
		NullCheck(L_1);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_3 = Transform_InverseTransformDirection_m6F0513F2EC19C204F2077E3C68DD1D45317CB5F2(L_1, L_2, /*hidden argument*/NULL);
		___move0 = L_3;
		CharacterControllerThirdPerson_CheckGroundStatus_mDC821F214A629FE6A8E67AFFE8DE88E6B9ED6FBC(__this, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_4 = ___move0;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_5 = __this->get_m_GroundNormal_19();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_6 = Vector3_ProjectOnPlane_mAF89645654808BBD9754610879F94A2B3323D206(L_4, L_5, /*hidden argument*/NULL);
		___move0 = L_6;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_7 = ___move0;
		float L_8 = L_7.get_x_2();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_9 = ___move0;
		float L_10 = L_9.get_z_4();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var);
		float L_11 = atan2f(L_8, L_10);
		__this->set_m_TurnAmount_17(L_11);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_12 = ___move0;
		float L_13 = L_12.get_z_4();
		__this->set_m_ForwardAmount_18(L_13);
		CharacterControllerThirdPerson_ApplyExtraTurnRotation_mF7EA79A525D0D136B16D4AA00A524BA395FFBFD1(__this, /*hidden argument*/NULL);
		bool L_14 = __this->get_m_IsGrounded_14();
		if (!L_14)
		{
			goto IL_0072;
		}
	}
	{
		bool L_15 = ___crouch1;
		bool L_16 = ___jump2;
		CharacterControllerThirdPerson_HandleGroundedMovement_m8BC178F170EF39BDD6F26A0B4889A3AF6C11BCEA(__this, L_15, L_16, /*hidden argument*/NULL);
		goto IL_0078;
	}

IL_0072:
	{
		CharacterControllerThirdPerson_HandleAirborneMovement_m0DF8799B3B9814A782A10AD5434F9B21A14D8B33(__this, /*hidden argument*/NULL);
	}

IL_0078:
	{
		bool L_17 = ___crouch1;
		CharacterControllerThirdPerson_ScaleCapsuleForCrouching_m7EA7CA090344079B74749A8D6FE2A06E9946D231(__this, L_17, /*hidden argument*/NULL);
		CharacterControllerThirdPerson_PreventStandingInLowHeadroom_mFA01E5F165FACD66CB419F8439B763621CF2C5EF(__this, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_18 = ___move0;
		CharacterControllerThirdPerson_UpdateAnimator_m2CCBBD592478AA52D15022D00AC3B5765466BFEA(__this, L_18, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CharacterControllerThirdPerson::ScaleCapsuleForCrouching(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void CharacterControllerThirdPerson_ScaleCapsuleForCrouching_m7EA7CA090344079B74749A8D6FE2A06E9946D231 (CharacterControllerThirdPerson_t2CC0D121855408DC72A1CB71940065D5C220C886 * __this, bool ___crouch0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CharacterControllerThirdPerson_ScaleCapsuleForCrouching_m7EA7CA090344079B74749A8D6FE2A06E9946D231_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		bool L_0 = __this->get_m_IsGrounded_14();
		bool L_1 = ___crouch0;
		if (!((int32_t)((int32_t)L_0&(int32_t)L_1)))
		{
			goto IL_0057;
		}
	}
	{
		bool L_2 = __this->get_m_Crouching_23();
		if (!L_2)
		{
			goto IL_0013;
		}
	}
	{
		return;
	}

IL_0013:
	{
		CapsuleCollider_t5FD15B9E7BEEC4FFA8A2071E9FD2B8DEB3A826D1 * L_3 = __this->get_m_Capsule_22();
		CapsuleCollider_t5FD15B9E7BEEC4FFA8A2071E9FD2B8DEB3A826D1 * L_4 = __this->get_m_Capsule_22();
		NullCheck(L_4);
		float L_5 = CapsuleCollider_get_height_mA0F14683CEDB4F32B59D0262AB7507574228EF75(L_4, /*hidden argument*/NULL);
		NullCheck(L_3);
		CapsuleCollider_set_height_m77D7E2FFC2A2D587B30746E00690A375D3E3D0F6(L_3, ((float)((float)L_5/(float)(2.0f))), /*hidden argument*/NULL);
		CapsuleCollider_t5FD15B9E7BEEC4FFA8A2071E9FD2B8DEB3A826D1 * L_6 = __this->get_m_Capsule_22();
		CapsuleCollider_t5FD15B9E7BEEC4FFA8A2071E9FD2B8DEB3A826D1 * L_7 = __this->get_m_Capsule_22();
		NullCheck(L_7);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_8 = CapsuleCollider_get_center_m415B40B8ADB6B1C29F3EF4C23839D5514BBA18AE(L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_9 = Vector3_op_Division_mDF34F1CC445981B4D1137765BC6277419E561624(L_8, (2.0f), /*hidden argument*/NULL);
		NullCheck(L_6);
		CapsuleCollider_set_center_mD2A261C23EB2DE8B0D9F946596BF64B72F7015C6(L_6, L_9, /*hidden argument*/NULL);
		__this->set_m_Crouching_23((bool)1);
		return;
	}

IL_0057:
	{
		Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * L_10 = __this->get_m_Rigidbody_12();
		NullCheck(L_10);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_11 = Rigidbody_get_position_m478D060638E43DE3AE9C931A42593484B8310113(L_10, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_12 = Vector3_get_up_m6309EBC4E42D6D0B3D28056BD23D0331275306F7(/*hidden argument*/NULL);
		CapsuleCollider_t5FD15B9E7BEEC4FFA8A2071E9FD2B8DEB3A826D1 * L_13 = __this->get_m_Capsule_22();
		NullCheck(L_13);
		float L_14 = CapsuleCollider_get_radius_m8E753A625226A5AF557AAFEBF5B6D0720C00802D(L_13, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_15 = Vector3_op_Multiply_m1C5F07723615156ACF035D88A1280A9E8F35A04E(L_12, L_14, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_16 = Vector3_op_Multiply_m1C5F07723615156ACF035D88A1280A9E8F35A04E(L_15, (0.5f), /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_17 = Vector3_op_Addition_m929F9C17E5D11B94D50B4AFF1D730B70CB59B50E(L_11, L_16, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_18 = Vector3_get_up_m6309EBC4E42D6D0B3D28056BD23D0331275306F7(/*hidden argument*/NULL);
		Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2  L_19;
		memset(&L_19, 0, sizeof(L_19));
		Ray__ctor_m695D219349B8AA4C82F96C55A27D384C07736F6B((&L_19), L_17, L_18, /*hidden argument*/NULL);
		float L_20 = __this->get_m_CapsuleHeight_20();
		CapsuleCollider_t5FD15B9E7BEEC4FFA8A2071E9FD2B8DEB3A826D1 * L_21 = __this->get_m_Capsule_22();
		NullCheck(L_21);
		float L_22 = CapsuleCollider_get_radius_m8E753A625226A5AF557AAFEBF5B6D0720C00802D(L_21, /*hidden argument*/NULL);
		V_0 = ((float)il2cpp_codegen_subtract((float)L_20, (float)((float)il2cpp_codegen_multiply((float)L_22, (float)(0.5f)))));
		CapsuleCollider_t5FD15B9E7BEEC4FFA8A2071E9FD2B8DEB3A826D1 * L_23 = __this->get_m_Capsule_22();
		NullCheck(L_23);
		float L_24 = CapsuleCollider_get_radius_m8E753A625226A5AF557AAFEBF5B6D0720C00802D(L_23, /*hidden argument*/NULL);
		float L_25 = V_0;
		bool L_26 = Physics_SphereCast_m618453A51702CF6F34D788125CD005D803293610(L_19, ((float)il2cpp_codegen_multiply((float)L_24, (float)(0.5f))), L_25, /*hidden argument*/NULL);
		if (!L_26)
		{
			goto IL_00ca;
		}
	}
	{
		__this->set_m_Crouching_23((bool)1);
		return;
	}

IL_00ca:
	{
		CapsuleCollider_t5FD15B9E7BEEC4FFA8A2071E9FD2B8DEB3A826D1 * L_27 = __this->get_m_Capsule_22();
		float L_28 = __this->get_m_CapsuleHeight_20();
		NullCheck(L_27);
		CapsuleCollider_set_height_m77D7E2FFC2A2D587B30746E00690A375D3E3D0F6(L_27, L_28, /*hidden argument*/NULL);
		CapsuleCollider_t5FD15B9E7BEEC4FFA8A2071E9FD2B8DEB3A826D1 * L_29 = __this->get_m_Capsule_22();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_30 = __this->get_m_CapsuleCenter_21();
		NullCheck(L_29);
		CapsuleCollider_set_center_mD2A261C23EB2DE8B0D9F946596BF64B72F7015C6(L_29, L_30, /*hidden argument*/NULL);
		__this->set_m_Crouching_23((bool)0);
		return;
	}
}
// System.Void CharacterControllerThirdPerson::PreventStandingInLowHeadroom()
extern "C" IL2CPP_METHOD_ATTR void CharacterControllerThirdPerson_PreventStandingInLowHeadroom_mFA01E5F165FACD66CB419F8439B763621CF2C5EF (CharacterControllerThirdPerson_t2CC0D121855408DC72A1CB71940065D5C220C886 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CharacterControllerThirdPerson_PreventStandingInLowHeadroom_mFA01E5F165FACD66CB419F8439B763621CF2C5EF_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		bool L_0 = __this->get_m_Crouching_23();
		if (L_0)
		{
			goto IL_007a;
		}
	}
	{
		Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * L_1 = __this->get_m_Rigidbody_12();
		NullCheck(L_1);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_2 = Rigidbody_get_position_m478D060638E43DE3AE9C931A42593484B8310113(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_3 = Vector3_get_up_m6309EBC4E42D6D0B3D28056BD23D0331275306F7(/*hidden argument*/NULL);
		CapsuleCollider_t5FD15B9E7BEEC4FFA8A2071E9FD2B8DEB3A826D1 * L_4 = __this->get_m_Capsule_22();
		NullCheck(L_4);
		float L_5 = CapsuleCollider_get_radius_m8E753A625226A5AF557AAFEBF5B6D0720C00802D(L_4, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_6 = Vector3_op_Multiply_m1C5F07723615156ACF035D88A1280A9E8F35A04E(L_3, L_5, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_7 = Vector3_op_Multiply_m1C5F07723615156ACF035D88A1280A9E8F35A04E(L_6, (0.5f), /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_8 = Vector3_op_Addition_m929F9C17E5D11B94D50B4AFF1D730B70CB59B50E(L_2, L_7, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_9 = Vector3_get_up_m6309EBC4E42D6D0B3D28056BD23D0331275306F7(/*hidden argument*/NULL);
		Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2  L_10;
		memset(&L_10, 0, sizeof(L_10));
		Ray__ctor_m695D219349B8AA4C82F96C55A27D384C07736F6B((&L_10), L_8, L_9, /*hidden argument*/NULL);
		float L_11 = __this->get_m_CapsuleHeight_20();
		CapsuleCollider_t5FD15B9E7BEEC4FFA8A2071E9FD2B8DEB3A826D1 * L_12 = __this->get_m_Capsule_22();
		NullCheck(L_12);
		float L_13 = CapsuleCollider_get_radius_m8E753A625226A5AF557AAFEBF5B6D0720C00802D(L_12, /*hidden argument*/NULL);
		V_0 = ((float)il2cpp_codegen_subtract((float)L_11, (float)((float)il2cpp_codegen_multiply((float)L_13, (float)(0.5f)))));
		CapsuleCollider_t5FD15B9E7BEEC4FFA8A2071E9FD2B8DEB3A826D1 * L_14 = __this->get_m_Capsule_22();
		NullCheck(L_14);
		float L_15 = CapsuleCollider_get_radius_m8E753A625226A5AF557AAFEBF5B6D0720C00802D(L_14, /*hidden argument*/NULL);
		float L_16 = V_0;
		bool L_17 = Physics_SphereCast_m618453A51702CF6F34D788125CD005D803293610(L_10, ((float)il2cpp_codegen_multiply((float)L_15, (float)(0.5f))), L_16, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_007a;
		}
	}
	{
		__this->set_m_Crouching_23((bool)1);
	}

IL_007a:
	{
		return;
	}
}
// System.Void CharacterControllerThirdPerson::UpdateAnimator(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR void CharacterControllerThirdPerson_UpdateAnimator_m2CCBBD592478AA52D15022D00AC3B5765466BFEA (CharacterControllerThirdPerson_t2CC0D121855408DC72A1CB71940065D5C220C886 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___move0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CharacterControllerThirdPerson_UpdateAnimator_m2CCBBD592478AA52D15022D00AC3B5765466BFEA_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	AnimatorStateInfo_tF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t G_B5_0 = 0;
	{
		Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * L_0 = __this->get_m_Animator_13();
		float L_1 = __this->get_m_ForwardAmount_18();
		float L_2 = Time_get_deltaTime_m16F98FC9BA931581236008C288E3B25CBCB7C81E(/*hidden argument*/NULL);
		NullCheck(L_0);
		Animator_SetFloat_mC260C82EF3FD1DB6B66FE898C595D5F901E59681(L_0, _stringLiteralBA4E72261283258434788542BA397135C10F39D8, L_1, (0.1f), L_2, /*hidden argument*/NULL);
		Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * L_3 = __this->get_m_Animator_13();
		float L_4 = __this->get_m_TurnAmount_17();
		float L_5 = Time_get_deltaTime_m16F98FC9BA931581236008C288E3B25CBCB7C81E(/*hidden argument*/NULL);
		NullCheck(L_3);
		Animator_SetFloat_mC260C82EF3FD1DB6B66FE898C595D5F901E59681(L_3, _stringLiteral368E8081B781AAC177FD07C2415B694B1F878911, L_4, (0.1f), L_5, /*hidden argument*/NULL);
		Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * L_6 = __this->get_m_Animator_13();
		bool L_7 = __this->get_m_Crouching_23();
		NullCheck(L_6);
		Animator_SetBool_m497805BA217139E42808899782FA05C15BC9879E(L_6, _stringLiteral10720C73909715E6E3CAD0C6F55EAF64E3E4168A, L_7, /*hidden argument*/NULL);
		Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * L_8 = __this->get_m_Animator_13();
		bool L_9 = __this->get_m_IsGrounded_14();
		NullCheck(L_8);
		Animator_SetBool_m497805BA217139E42808899782FA05C15BC9879E(L_8, _stringLiteralA3EADB39916A68BB98BCFA2F32BE511DBD91782E, L_9, /*hidden argument*/NULL);
		bool L_10 = __this->get_m_IsGrounded_14();
		if (L_10)
		{
			goto IL_0094;
		}
	}
	{
		Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * L_11 = __this->get_m_Animator_13();
		Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * L_12 = __this->get_m_Rigidbody_12();
		NullCheck(L_12);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_13 = Rigidbody_get_velocity_m584A6D79C3657C21AE9CAA56BEE05582B8D5A2B8(L_12, /*hidden argument*/NULL);
		float L_14 = L_13.get_y_3();
		NullCheck(L_11);
		Animator_SetFloat_mE4C29F6980EBBBD954637721E6E13A0BE2B13C43(L_11, _stringLiteral1EBA140FDD9C6860A1730C408E3064AA417CA2A3, L_14, /*hidden argument*/NULL);
	}

IL_0094:
	{
		Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * L_15 = __this->get_m_Animator_13();
		NullCheck(L_15);
		AnimatorStateInfo_tF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2  L_16 = Animator_GetCurrentAnimatorStateInfo_mBE5ED0D60A6F5CD0EDD40AF1494098D4E7BF84F2(L_15, 0, /*hidden argument*/NULL);
		V_1 = L_16;
		float L_17 = AnimatorStateInfo_get_normalizedTime_m3AB6A2DB592BB9CA0B9A4EF8A382C4DF3F5F6BBD((AnimatorStateInfo_tF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2 *)(&V_1), /*hidden argument*/NULL);
		float L_18 = __this->get_m_RunCycleLegOffset_8();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var);
		float L_19 = Mathf_Repeat_m8459F4AAFF92DB770CC892BF71EE9438D9D0F779(((float)il2cpp_codegen_add((float)L_17, (float)L_18)), (1.0f), /*hidden argument*/NULL);
		if ((((float)L_19) < ((float)(0.5f))))
		{
			goto IL_00c3;
		}
	}
	{
		G_B5_0 = (-1);
		goto IL_00c4;
	}

IL_00c3:
	{
		G_B5_0 = 1;
	}

IL_00c4:
	{
		float L_20 = __this->get_m_ForwardAmount_18();
		V_0 = ((float)il2cpp_codegen_multiply((float)(((float)((float)G_B5_0))), (float)L_20));
		bool L_21 = __this->get_m_IsGrounded_14();
		if (!L_21)
		{
			goto IL_00e6;
		}
	}
	{
		Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * L_22 = __this->get_m_Animator_13();
		float L_23 = V_0;
		NullCheck(L_22);
		Animator_SetFloat_mE4C29F6980EBBBD954637721E6E13A0BE2B13C43(L_22, _stringLiteral97D3E6EADAC2258F84B327212E05F248C6B354A1, L_23, /*hidden argument*/NULL);
	}

IL_00e6:
	{
		bool L_24 = __this->get_m_IsGrounded_14();
		if (!L_24)
		{
			goto IL_010e;
		}
	}
	{
		float L_25 = Vector3_get_magnitude_m9A750659B60C5FE0C30438A7F9681775D5DB1274((Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&___move0), /*hidden argument*/NULL);
		if ((!(((float)L_25) > ((float)(0.0f)))))
		{
			goto IL_010e;
		}
	}
	{
		Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * L_26 = __this->get_m_Animator_13();
		float L_27 = __this->get_m_AnimSpeedMultiplier_10();
		NullCheck(L_26);
		Animator_set_speed_mEA558D196D84684744A642A56AFBF22F16448813(L_26, L_27, /*hidden argument*/NULL);
		return;
	}

IL_010e:
	{
		Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * L_28 = __this->get_m_Animator_13();
		NullCheck(L_28);
		Animator_set_speed_mEA558D196D84684744A642A56AFBF22F16448813(L_28, (1.0f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void CharacterControllerThirdPerson::HandleAirborneMovement()
extern "C" IL2CPP_METHOD_ATTR void CharacterControllerThirdPerson_HandleAirborneMovement_m0DF8799B3B9814A782A10AD5434F9B21A14D8B33 (CharacterControllerThirdPerson_t2CC0D121855408DC72A1CB71940065D5C220C886 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CharacterControllerThirdPerson_HandleAirborneMovement_m0DF8799B3B9814A782A10AD5434F9B21A14D8B33_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_0;
	memset(&V_0, 0, sizeof(V_0));
	CharacterControllerThirdPerson_t2CC0D121855408DC72A1CB71940065D5C220C886 * G_B2_0 = NULL;
	CharacterControllerThirdPerson_t2CC0D121855408DC72A1CB71940065D5C220C886 * G_B1_0 = NULL;
	float G_B3_0 = 0.0f;
	CharacterControllerThirdPerson_t2CC0D121855408DC72A1CB71940065D5C220C886 * G_B3_1 = NULL;
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_0 = Physics_get_gravity_mC0F44371C49C1DD1455D6ADEC824BAE35E1E104B(/*hidden argument*/NULL);
		float L_1 = __this->get_m_GravityMultiplier_7();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_2 = Vector3_op_Multiply_m1C5F07723615156ACF035D88A1280A9E8F35A04E(L_0, L_1, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_3 = Physics_get_gravity_mC0F44371C49C1DD1455D6ADEC824BAE35E1E104B(/*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_4 = Vector3_op_Subtraction_mF9846B723A5034F8B9F5F5DCB78E3D67649143D3(L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * L_5 = __this->get_m_Rigidbody_12();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_6 = V_0;
		NullCheck(L_5);
		Rigidbody_AddForce_mC8140D90B806634A733624F671C45AD7CDBEDB38(L_5, L_6, /*hidden argument*/NULL);
		Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * L_7 = __this->get_m_Rigidbody_12();
		NullCheck(L_7);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_8 = Rigidbody_get_velocity_m584A6D79C3657C21AE9CAA56BEE05582B8D5A2B8(L_7, /*hidden argument*/NULL);
		float L_9 = L_8.get_y_3();
		G_B1_0 = __this;
		if ((((float)L_9) < ((float)(0.0f))))
		{
			G_B2_0 = __this;
			goto IL_0046;
		}
	}
	{
		G_B3_0 = (0.01f);
		G_B3_1 = G_B1_0;
		goto IL_004c;
	}

IL_0046:
	{
		float L_10 = __this->get_m_OrigGroundCheckDistance_15();
		G_B3_0 = L_10;
		G_B3_1 = G_B2_0;
	}

IL_004c:
	{
		NullCheck(G_B3_1);
		G_B3_1->set_m_GroundCheckDistance_11(G_B3_0);
		return;
	}
}
// System.Void CharacterControllerThirdPerson::HandleGroundedMovement(System.Boolean,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void CharacterControllerThirdPerson_HandleGroundedMovement_m8BC178F170EF39BDD6F26A0B4889A3AF6C11BCEA (CharacterControllerThirdPerson_t2CC0D121855408DC72A1CB71940065D5C220C886 * __this, bool ___crouch0, bool ___jump1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CharacterControllerThirdPerson_HandleGroundedMovement_m8BC178F170EF39BDD6F26A0B4889A3AF6C11BCEA_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	AnimatorStateInfo_tF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		bool L_0 = ___jump1;
		if (!L_0)
		{
			goto IL_0075;
		}
	}
	{
		bool L_1 = ___crouch0;
		if (L_1)
		{
			goto IL_0075;
		}
	}
	{
		Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * L_2 = __this->get_m_Animator_13();
		NullCheck(L_2);
		AnimatorStateInfo_tF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2  L_3 = Animator_GetCurrentAnimatorStateInfo_mBE5ED0D60A6F5CD0EDD40AF1494098D4E7BF84F2(L_2, 0, /*hidden argument*/NULL);
		V_0 = L_3;
		bool L_4 = AnimatorStateInfo_IsName_mD133471910C1ACE0979F3FD11462C9B8BB24CA96((AnimatorStateInfo_tF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2 *)(&V_0), _stringLiteralD5580B73BFB2949AC6D9534C49ECD66CEBA94E8C, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0075;
		}
	}
	{
		Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * L_5 = __this->get_m_Rigidbody_12();
		Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * L_6 = __this->get_m_Rigidbody_12();
		NullCheck(L_6);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_7 = Rigidbody_get_velocity_m584A6D79C3657C21AE9CAA56BEE05582B8D5A2B8(L_6, /*hidden argument*/NULL);
		float L_8 = L_7.get_x_2();
		float L_9 = __this->get_m_JumpPower_6();
		Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * L_10 = __this->get_m_Rigidbody_12();
		NullCheck(L_10);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_11 = Rigidbody_get_velocity_m584A6D79C3657C21AE9CAA56BEE05582B8D5A2B8(L_10, /*hidden argument*/NULL);
		float L_12 = L_11.get_z_4();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_13;
		memset(&L_13, 0, sizeof(L_13));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_13), L_8, L_9, L_12, /*hidden argument*/NULL);
		NullCheck(L_5);
		Rigidbody_set_velocity_m8D129E88E62AD02AB81CFC8BE694C4A5A2B2B380(L_5, L_13, /*hidden argument*/NULL);
		__this->set_m_IsGrounded_14((bool)0);
		Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * L_14 = __this->get_m_Animator_13();
		NullCheck(L_14);
		Animator_set_applyRootMotion_mD25B7FCFCDAD7C5D107E860D2E46AB765271A982(L_14, (bool)0, /*hidden argument*/NULL);
		__this->set_m_GroundCheckDistance_11((0.1f));
	}

IL_0075:
	{
		return;
	}
}
// System.Void CharacterControllerThirdPerson::ApplyExtraTurnRotation()
extern "C" IL2CPP_METHOD_ATTR void CharacterControllerThirdPerson_ApplyExtraTurnRotation_mF7EA79A525D0D136B16D4AA00A524BA395FFBFD1 (CharacterControllerThirdPerson_t2CC0D121855408DC72A1CB71940065D5C220C886 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CharacterControllerThirdPerson_ApplyExtraTurnRotation_mF7EA79A525D0D136B16D4AA00A524BA395FFBFD1_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_StationaryTurnSpeed_5();
		float L_1 = __this->get_m_MovingTurnSpeed_4();
		float L_2 = __this->get_m_ForwardAmount_18();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var);
		float L_3 = Mathf_Lerp_m9A74C5A0C37D0CDF45EE66E7774D12A9B93B1364(L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_4 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		float L_5 = __this->get_m_TurnAmount_17();
		float L_6 = V_0;
		float L_7 = Time_get_deltaTime_m16F98FC9BA931581236008C288E3B25CBCB7C81E(/*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_Rotate_mEEA80F3DA5A4C93611D7165DF54763E578477EF9(L_4, (0.0f), ((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)L_5, (float)L_6)), (float)L_7)), (0.0f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void CharacterControllerThirdPerson::OnAnimatorMove()
extern "C" IL2CPP_METHOD_ATTR void CharacterControllerThirdPerson_OnAnimatorMove_m68A2195DA3689D9043DA247A18E7941EF0A53309 (CharacterControllerThirdPerson_t2CC0D121855408DC72A1CB71940065D5C220C886 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CharacterControllerThirdPerson_OnAnimatorMove_m68A2195DA3689D9043DA247A18E7941EF0A53309_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		bool L_0 = __this->get_m_IsGrounded_14();
		if (!L_0)
		{
			goto IL_0058;
		}
	}
	{
		float L_1 = Time_get_deltaTime_m16F98FC9BA931581236008C288E3B25CBCB7C81E(/*hidden argument*/NULL);
		if ((!(((float)L_1) > ((float)(0.0f)))))
		{
			goto IL_0058;
		}
	}
	{
		Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * L_2 = __this->get_m_Animator_13();
		NullCheck(L_2);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_3 = Animator_get_deltaPosition_m85287AA699C50B6AA7489CD313F8CFED5D998533(L_2, /*hidden argument*/NULL);
		float L_4 = __this->get_m_MoveSpeedMultiplier_9();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_5 = Vector3_op_Multiply_m1C5F07723615156ACF035D88A1280A9E8F35A04E(L_3, L_4, /*hidden argument*/NULL);
		float L_6 = Time_get_deltaTime_m16F98FC9BA931581236008C288E3B25CBCB7C81E(/*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_7 = Vector3_op_Division_mDF34F1CC445981B4D1137765BC6277419E561624(L_5, L_6, /*hidden argument*/NULL);
		V_0 = L_7;
		Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * L_8 = __this->get_m_Rigidbody_12();
		NullCheck(L_8);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_9 = Rigidbody_get_velocity_m584A6D79C3657C21AE9CAA56BEE05582B8D5A2B8(L_8, /*hidden argument*/NULL);
		float L_10 = L_9.get_y_3();
		(&V_0)->set_y_3(L_10);
		Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * L_11 = __this->get_m_Rigidbody_12();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_12 = V_0;
		NullCheck(L_11);
		Rigidbody_set_velocity_m8D129E88E62AD02AB81CFC8BE694C4A5A2B2B380(L_11, L_12, /*hidden argument*/NULL);
	}

IL_0058:
	{
		return;
	}
}
// System.Void CharacterControllerThirdPerson::CheckGroundStatus()
extern "C" IL2CPP_METHOD_ATTR void CharacterControllerThirdPerson_CheckGroundStatus_mDC821F214A629FE6A8E67AFFE8DE88E6B9ED6FBC (CharacterControllerThirdPerson_t2CC0D121855408DC72A1CB71940065D5C220C886 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CharacterControllerThirdPerson_CheckGroundStatus_mDC821F214A629FE6A8E67AFFE8DE88E6B9ED6FBC_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_0 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_1 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_2 = Vector3_get_up_m6309EBC4E42D6D0B3D28056BD23D0331275306F7(/*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_3 = Vector3_op_Multiply_m1C5F07723615156ACF035D88A1280A9E8F35A04E(L_2, (0.1f), /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_4 = Vector3_op_Addition_m929F9C17E5D11B94D50B4AFF1D730B70CB59B50E(L_1, L_3, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_5 = Vector3_get_down_m3F76A48E5B7C82B35EE047375538AFD91A305F55(/*hidden argument*/NULL);
		float L_6 = __this->get_m_GroundCheckDistance_11();
		bool L_7 = Physics_Raycast_mBEC747ED0A7660BB12AA48B663CCBF7B1EE93D6B(L_4, L_5, (RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 *)(&V_0), L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0054;
		}
	}
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_8 = RaycastHit_get_normal_mF736A6D09D98D63AB7E5BF10F38AEBFC177A1D94((RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 *)(&V_0), /*hidden argument*/NULL);
		__this->set_m_GroundNormal_19(L_8);
		__this->set_m_IsGrounded_14((bool)1);
		Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * L_9 = __this->get_m_Animator_13();
		NullCheck(L_9);
		Animator_set_applyRootMotion_mD25B7FCFCDAD7C5D107E860D2E46AB765271A982(L_9, (bool)1, /*hidden argument*/NULL);
		return;
	}

IL_0054:
	{
		__this->set_m_IsGrounded_14((bool)0);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_10 = Vector3_get_up_m6309EBC4E42D6D0B3D28056BD23D0331275306F7(/*hidden argument*/NULL);
		__this->set_m_GroundNormal_19(L_10);
		Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * L_11 = __this->get_m_Animator_13();
		NullCheck(L_11);
		Animator_set_applyRootMotion_mD25B7FCFCDAD7C5D107E860D2E46AB765271A982(L_11, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CharacterControllerThirdPerson::.ctor()
extern "C" IL2CPP_METHOD_ATTR void CharacterControllerThirdPerson__ctor_m1B69203EDAA3191366503C6AAECC7BC02B5C28DC (CharacterControllerThirdPerson_t2CC0D121855408DC72A1CB71940065D5C220C886 * __this, const RuntimeMethod* method)
{
	{
		__this->set_m_MovingTurnSpeed_4((360.0f));
		__this->set_m_StationaryTurnSpeed_5((180.0f));
		__this->set_m_JumpPower_6((12.0f));
		__this->set_m_GravityMultiplier_7((2.0f));
		__this->set_m_RunCycleLegOffset_8((0.2f));
		__this->set_m_MoveSpeedMultiplier_9((1.0f));
		__this->set_m_AnimSpeedMultiplier_10((1.0f));
		__this->set_m_GroundCheckDistance_11((0.1f));
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ImageRecognitionExample::Awake()
extern "C" IL2CPP_METHOD_ATTR void ImageRecognitionExample_Awake_m3A4E749AFA71E5C19CF14EAE82F8755449972A14 (ImageRecognitionExample_t911A7F2FD5BFFF23A30C97F8619C18A1F0757110 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ImageRecognitionExample_Awake_m3A4E749AFA71E5C19CF14EAE82F8755449972A14_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_0 = Camera_get_main_m9256A9F84F92D7ED73F3E6C4E2694030AD8B61FA(/*hidden argument*/NULL);
		__this->set_m_MainCamera_7(L_0);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		ARTrackedImageManager_tC1F6C093163D1D4A5CCE83613BFCA465E2DABA9D * L_1 = Object_FindObjectOfType_TisARTrackedImageManager_tC1F6C093163D1D4A5CCE83613BFCA465E2DABA9D_m8E2DA26A9425734CF006C5373BEB1A4C5A896180(/*hidden argument*/Object_FindObjectOfType_TisARTrackedImageManager_tC1F6C093163D1D4A5CCE83613BFCA465E2DABA9D_m8E2DA26A9425734CF006C5373BEB1A4C5A896180_RuntimeMethod_var);
		__this->set__arTrackedImageManager_6(L_1);
		return;
	}
}
// System.Void ImageRecognitionExample::OnEnable()
extern "C" IL2CPP_METHOD_ATTR void ImageRecognitionExample_OnEnable_m04082E6E1E765E17296B0ECB5FAEBF1FD83A1342 (ImageRecognitionExample_t911A7F2FD5BFFF23A30C97F8619C18A1F0757110 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ImageRecognitionExample_OnEnable_m04082E6E1E765E17296B0ECB5FAEBF1FD83A1342_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ARTrackedImageManager_tC1F6C093163D1D4A5CCE83613BFCA465E2DABA9D * L_0 = __this->get__arTrackedImageManager_6();
		Action_1_t825CEDF2F074FBD059900D81615EFDE0AFD02CA0 * L_1 = (Action_1_t825CEDF2F074FBD059900D81615EFDE0AFD02CA0 *)il2cpp_codegen_object_new(Action_1_t825CEDF2F074FBD059900D81615EFDE0AFD02CA0_il2cpp_TypeInfo_var);
		Action_1__ctor_m685B9CA8548AB276638A829E0898E1BDF2A74388(L_1, __this, (intptr_t)((intptr_t)ImageRecognitionExample_OnImageChanged_m0C926016A8C152B5DBDE7AAEF8C8884EB07951D7_RuntimeMethod_var), /*hidden argument*/Action_1__ctor_m685B9CA8548AB276638A829E0898E1BDF2A74388_RuntimeMethod_var);
		NullCheck(L_0);
		ARTrackedImageManager_add_trackedImagesChanged_m49E4C6CC7F6EC93050C7C084E2C779323929FCEB(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ImageRecognitionExample::OnDestroy()
extern "C" IL2CPP_METHOD_ATTR void ImageRecognitionExample_OnDestroy_m0DCA2A78CFCA2C055BD4CEAAF268E22E30E08BFC (ImageRecognitionExample_t911A7F2FD5BFFF23A30C97F8619C18A1F0757110 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ImageRecognitionExample_OnDestroy_m0DCA2A78CFCA2C055BD4CEAAF268E22E30E08BFC_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ARTrackedImageManager_tC1F6C093163D1D4A5CCE83613BFCA465E2DABA9D * L_0 = __this->get__arTrackedImageManager_6();
		Action_1_t825CEDF2F074FBD059900D81615EFDE0AFD02CA0 * L_1 = (Action_1_t825CEDF2F074FBD059900D81615EFDE0AFD02CA0 *)il2cpp_codegen_object_new(Action_1_t825CEDF2F074FBD059900D81615EFDE0AFD02CA0_il2cpp_TypeInfo_var);
		Action_1__ctor_m685B9CA8548AB276638A829E0898E1BDF2A74388(L_1, __this, (intptr_t)((intptr_t)ImageRecognitionExample_OnImageChanged_m0C926016A8C152B5DBDE7AAEF8C8884EB07951D7_RuntimeMethod_var), /*hidden argument*/Action_1__ctor_m685B9CA8548AB276638A829E0898E1BDF2A74388_RuntimeMethod_var);
		NullCheck(L_0);
		ARTrackedImageManager_add_trackedImagesChanged_m49E4C6CC7F6EC93050C7C084E2C779323929FCEB(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ImageRecognitionExample::OnImageChanged(UnityEngine.XR.ARFoundation.ARTrackedImagesChangedEventArgs)
extern "C" IL2CPP_METHOD_ATTR void ImageRecognitionExample_OnImageChanged_m0C926016A8C152B5DBDE7AAEF8C8884EB07951D7 (ImageRecognitionExample_t911A7F2FD5BFFF23A30C97F8619C18A1F0757110 * __this, ARTrackedImagesChangedEventArgs_t64F82ABE51189C62E3811D96F2C2143DB91669E4  ___args0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ImageRecognitionExample_OnImageChanged_m0C926016A8C152B5DBDE7AAEF8C8884EB07951D7_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Enumerator_t6754DF054050219F46CEED0786A14A987A139408  V_0;
	memset(&V_0, 0, sizeof(V_0));
	ARTrackedImage_tD699A1D6CA301ECE200D07D1CF1722E94AFEF01A * V_1 = NULL;
	XRReferenceImage_t5A53387AC6253D5D3DFD62BC583A45BBDFC1347E  V_2;
	memset(&V_2, 0, sizeof(V_2));
	ARTrackedImage_tD699A1D6CA301ECE200D07D1CF1722E94AFEF01A * V_3 = NULL;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  V_7;
	memset(&V_7, 0, sizeof(V_7));
	int32_t V_8 = 0;
	ARTrackedImage_tD699A1D6CA301ECE200D07D1CF1722E94AFEF01A * V_9 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = -1;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t85456E82777B8C7F9CFFED2C540D0613542A8C8D * L_0 = ARTrackedImagesChangedEventArgs_get_updated_m6A3BB3D6B5EE300996814DA2DE6DE45C46B48DDB((ARTrackedImagesChangedEventArgs_t64F82ABE51189C62E3811D96F2C2143DB91669E4 *)(&___args0), /*hidden argument*/NULL);
		NullCheck(L_0);
		Enumerator_t6754DF054050219F46CEED0786A14A987A139408  L_1 = List_1_GetEnumerator_m06B9ADCB9085604C5418BD1AC24ADA9237C95CF9(L_0, /*hidden argument*/List_1_GetEnumerator_m06B9ADCB9085604C5418BD1AC24ADA9237C95CF9_RuntimeMethod_var);
		V_0 = L_1;
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0034;
		}

IL_000f:
		{
			ARTrackedImage_tD699A1D6CA301ECE200D07D1CF1722E94AFEF01A * L_2 = Enumerator_get_Current_m800C03D423EC5FCFD560CC7A2B3FF44087CEE0BF((Enumerator_t6754DF054050219F46CEED0786A14A987A139408 *)(&V_0), /*hidden argument*/Enumerator_get_Current_m800C03D423EC5FCFD560CC7A2B3FF44087CEE0BF_RuntimeMethod_var);
			V_1 = L_2;
			ARTrackedImage_tD699A1D6CA301ECE200D07D1CF1722E94AFEF01A * L_3 = V_1;
			NullCheck(L_3);
			XRReferenceImage_t5A53387AC6253D5D3DFD62BC583A45BBDFC1347E  L_4 = ARTrackedImage_get_referenceImage_m46AAAD2FF5BF6E2A9AB8192BCE198FB69F00CC7D(L_3, /*hidden argument*/NULL);
			V_2 = L_4;
			String_t* L_5 = XRReferenceImage_get_name_m6DD83ECED755444645A14816918747E2EEAE92A9((XRReferenceImage_t5A53387AC6253D5D3DFD62BC583A45BBDFC1347E *)(&V_2), /*hidden argument*/NULL);
			String_t* L_6 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE(_stringLiteral3E5E4666125140F59F616547A0648DBF5EBE1B36, L_5, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
			Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708(L_6, /*hidden argument*/NULL);
		}

IL_0034:
		{
			bool L_7 = Enumerator_MoveNext_m75BAC688340F0794E4C6C98B495BB4B9A02E6522((Enumerator_t6754DF054050219F46CEED0786A14A987A139408 *)(&V_0), /*hidden argument*/Enumerator_MoveNext_m75BAC688340F0794E4C6C98B495BB4B9A02E6522_RuntimeMethod_var);
			if (L_7)
			{
				goto IL_000f;
			}
		}

IL_003d:
		{
			IL2CPP_LEAVE(0x4D, FINALLY_003f);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_003f;
	}

FINALLY_003f:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m46C5EE3170D6F427C4379CE89AFE659555B786A7((Enumerator_t6754DF054050219F46CEED0786A14A987A139408 *)(&V_0), /*hidden argument*/Enumerator_Dispose_m46C5EE3170D6F427C4379CE89AFE659555B786A7_RuntimeMethod_var);
		IL2CPP_RESET_LEAVE(0x4D);
		IL2CPP_END_FINALLY(63)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(63)
	{
		IL2CPP_JUMP_TBL(0x4D, IL_004d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_004d:
	{
		List_1_t85456E82777B8C7F9CFFED2C540D0613542A8C8D * L_8 = ARTrackedImagesChangedEventArgs_get_updated_m6A3BB3D6B5EE300996814DA2DE6DE45C46B48DDB((ARTrackedImagesChangedEventArgs_t64F82ABE51189C62E3811D96F2C2143DB91669E4 *)(&___args0), /*hidden argument*/NULL);
		NullCheck(L_8);
		Enumerator_t6754DF054050219F46CEED0786A14A987A139408  L_9 = List_1_GetEnumerator_m06B9ADCB9085604C5418BD1AC24ADA9237C95CF9(L_8, /*hidden argument*/List_1_GetEnumerator_m06B9ADCB9085604C5418BD1AC24ADA9237C95CF9_RuntimeMethod_var);
		V_0 = L_9;
	}

IL_005a:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0221;
		}

IL_005f:
		{
			ARTrackedImage_tD699A1D6CA301ECE200D07D1CF1722E94AFEF01A * L_10 = Enumerator_get_Current_m800C03D423EC5FCFD560CC7A2B3FF44087CEE0BF((Enumerator_t6754DF054050219F46CEED0786A14A987A139408 *)(&V_0), /*hidden argument*/Enumerator_get_Current_m800C03D423EC5FCFD560CC7A2B3FF44087CEE0BF_RuntimeMethod_var);
			V_3 = L_10;
			ARTrackedImage_tD699A1D6CA301ECE200D07D1CF1722E94AFEF01A * L_11 = V_3;
			NullCheck(L_11);
			XRReferenceImage_t5A53387AC6253D5D3DFD62BC583A45BBDFC1347E  L_12 = ARTrackedImage_get_referenceImage_m46AAAD2FF5BF6E2A9AB8192BCE198FB69F00CC7D(L_11, /*hidden argument*/NULL);
			V_2 = L_12;
			String_t* L_13 = XRReferenceImage_get_name_m6DD83ECED755444645A14816918747E2EEAE92A9((XRReferenceImage_t5A53387AC6253D5D3DFD62BC583A45BBDFC1347E *)(&V_2), /*hidden argument*/NULL);
			String_t* L_14 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE(_stringLiteral56B0664945A46455085CDE2AE967C641B320E757, L_13, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
			Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708(L_14, /*hidden argument*/NULL);
			ARTrackedImage_tD699A1D6CA301ECE200D07D1CF1722E94AFEF01A * L_15 = V_3;
			NullCheck(L_15);
			int32_t L_16 = ARTrackable_2_get_trackingState_mE6C9A0305C0A41DDF81E1330094981C9C1A00BCA(L_15, /*hidden argument*/ARTrackable_2_get_trackingState_mE6C9A0305C0A41DDF81E1330094981C9C1A00BCA_RuntimeMethod_var);
			if ((((int32_t)L_16) == ((int32_t)2)))
			{
				goto IL_0099;
			}
		}

IL_008d:
		{
			ARTrackedImage_tD699A1D6CA301ECE200D07D1CF1722E94AFEF01A * L_17 = V_3;
			NullCheck(L_17);
			int32_t L_18 = ARTrackable_2_get_trackingState_mE6C9A0305C0A41DDF81E1330094981C9C1A00BCA(L_17, /*hidden argument*/ARTrackable_2_get_trackingState_mE6C9A0305C0A41DDF81E1330094981C9C1A00BCA_RuntimeMethod_var);
			if ((!(((uint32_t)L_18) == ((uint32_t)1))))
			{
				goto IL_01c6;
			}
		}

IL_0099:
		{
			ARTrackedImage_tD699A1D6CA301ECE200D07D1CF1722E94AFEF01A * L_19 = V_3;
			NullCheck(L_19);
			int32_t L_20 = ARTrackable_2_get_trackingState_mE6C9A0305C0A41DDF81E1330094981C9C1A00BCA(L_19, /*hidden argument*/ARTrackable_2_get_trackingState_mE6C9A0305C0A41DDF81E1330094981C9C1A00BCA_RuntimeMethod_var);
			V_4 = L_20;
			RuntimeObject * L_21 = Box(TrackingState_t124D9E603E4E0453A85409CF7762EE8C946233F6_il2cpp_TypeInfo_var, (&V_4));
			NullCheck(L_21);
			String_t* L_22 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_21);
			V_4 = *(int32_t*)UnBox(L_21);
			String_t* L_23 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE(_stringLiteralA30F013D6B164520670D1289B00C9D08C846B231, L_22, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
			Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708(L_23, /*hidden argument*/NULL);
			V_5 = 0;
			goto IL_01b5;
		}

IL_00c5:
		{
			MarkerPrefabsU5BU5D_t7F40C670B36EE25921E51F171FD6A68A7A1BC336* L_24 = __this->get_markerPrefabCombos_4();
			int32_t L_25 = V_5;
			NullCheck(L_24);
			int32_t L_26 = L_25;
			MarkerPrefabs_t227A4AF77E7197AEFE5B31A217749784AA27AE6B * L_27 = (L_24)->GetAt(static_cast<il2cpp_array_size_t>(L_26));
			NullCheck(L_27);
			String_t* L_28 = L_27->get_prefabName_0();
			ARTrackedImage_tD699A1D6CA301ECE200D07D1CF1722E94AFEF01A * L_29 = V_3;
			NullCheck(L_29);
			XRReferenceImage_t5A53387AC6253D5D3DFD62BC583A45BBDFC1347E  L_30 = ARTrackedImage_get_referenceImage_m46AAAD2FF5BF6E2A9AB8192BCE198FB69F00CC7D(L_29, /*hidden argument*/NULL);
			V_2 = L_30;
			String_t* L_31 = XRReferenceImage_get_name_m6DD83ECED755444645A14816918747E2EEAE92A9((XRReferenceImage_t5A53387AC6253D5D3DFD62BC583A45BBDFC1347E *)(&V_2), /*hidden argument*/NULL);
			bool L_32 = String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE(L_28, L_31, /*hidden argument*/NULL);
			if (!L_32)
			{
				goto IL_01af;
			}
		}

IL_00eb:
		{
			__this->set_shouldObjectsBeShown_10((bool)1);
			MarkerPrefabsU5BU5D_t7F40C670B36EE25921E51F171FD6A68A7A1BC336* L_33 = __this->get_markerPrefabCombos_4();
			int32_t L_34 = V_5;
			NullCheck(L_33);
			int32_t L_35 = L_34;
			MarkerPrefabs_t227A4AF77E7197AEFE5B31A217749784AA27AE6B * L_36 = (L_33)->GetAt(static_cast<il2cpp_array_size_t>(L_35));
			NullCheck(L_36);
			GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_37 = L_36->get_targetPrefab_1();
			__this->set_objectInflatedOnScreen_13(L_37);
			GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_38 = __this->get_objectInflatedOnScreen_13();
			NullCheck(L_38);
			GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_38, (bool)1, /*hidden argument*/NULL);
			GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_39 = __this->get_objectInflatedOnScreen_13();
			NullCheck(L_39);
			Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_40 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_39, /*hidden argument*/NULL);
			ARTrackedImage_tD699A1D6CA301ECE200D07D1CF1722E94AFEF01A * L_41 = V_3;
			NullCheck(L_41);
			Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_42 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(L_41, /*hidden argument*/NULL);
			NullCheck(L_42);
			Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_43 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_42, /*hidden argument*/NULL);
			NullCheck(L_40);
			Transform_set_position_mDA89E4893F14ECA5CBEEE7FB80A5BF7C1B8EA6DC(L_40, L_43, /*hidden argument*/NULL);
			GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_44 = __this->get_objectInflatedOnScreen_13();
			NullCheck(L_44);
			Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_45 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_44, /*hidden argument*/NULL);
			NullCheck(L_45);
			Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_46 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_45, /*hidden argument*/NULL);
			__this->set_prefabPosition_11(L_46);
			Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_47 = __this->get_prefabPosition_11();
			V_6 = L_47;
			(&V_6)->set_x_2((-0.07f));
			Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * L_48 = __this->get_address_of_prefabPosition_11();
			String_t* L_49 = Vector3_ToString_m2682D27AB50CD1CE4677C38D0720A302D582348D((Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)L_48, /*hidden argument*/NULL);
			String_t* L_50 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE(_stringLiteralD32923FEEDA5736C136C007634BE98707D80C8DA, L_49, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
			Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708(L_50, /*hidden argument*/NULL);
			ARTrackedImage_tD699A1D6CA301ECE200D07D1CF1722E94AFEF01A * L_51 = V_3;
			NullCheck(L_51);
			Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_52 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(L_51, /*hidden argument*/NULL);
			NullCheck(L_52);
			Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_53 = Transform_get_rotation_m3AB90A67403249AECCA5E02BC70FCE8C90FE9FB9(L_52, /*hidden argument*/NULL);
			V_7 = L_53;
			GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_54 = __this->get_objectInflatedOnScreen_13();
			NullCheck(L_54);
			Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_55 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_54, /*hidden argument*/NULL);
			Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_56 = V_7;
			IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_il2cpp_TypeInfo_var);
			Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_57 = Quaternion_Euler_m537DD6CEAE0AD4274D8A84414C24C30730427D05((90.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
			Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_58 = Quaternion_op_Multiply_mDB9F738AA8160E3D85549F4FEDA23BC658B5A790(L_56, L_57, /*hidden argument*/NULL);
			NullCheck(L_55);
			Transform_set_rotation_m429694E264117C6DC682EC6AF45C7864E5155935(L_55, L_58, /*hidden argument*/NULL);
		}

IL_01af:
		{
			int32_t L_59 = V_5;
			V_5 = ((int32_t)il2cpp_codegen_add((int32_t)L_59, (int32_t)1));
		}

IL_01b5:
		{
			int32_t L_60 = V_5;
			MarkerPrefabsU5BU5D_t7F40C670B36EE25921E51F171FD6A68A7A1BC336* L_61 = __this->get_markerPrefabCombos_4();
			NullCheck(L_61);
			if ((((int32_t)L_60) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_61)->max_length)))))))
			{
				goto IL_00c5;
			}
		}

IL_01c4:
		{
			goto IL_0221;
		}

IL_01c6:
		{
			GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_62 = __this->get_objectInflatedOnScreen_13();
			NullCheck(L_62);
			GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_62, (bool)0, /*hidden argument*/NULL);
			ARTrackedImage_tD699A1D6CA301ECE200D07D1CF1722E94AFEF01A * L_63 = V_3;
			NullCheck(L_63);
			int32_t L_64 = ARTrackable_2_get_trackingState_mE6C9A0305C0A41DDF81E1330094981C9C1A00BCA(L_63, /*hidden argument*/ARTrackable_2_get_trackingState_mE6C9A0305C0A41DDF81E1330094981C9C1A00BCA_RuntimeMethod_var);
			V_4 = L_64;
			RuntimeObject * L_65 = Box(TrackingState_t124D9E603E4E0453A85409CF7762EE8C946233F6_il2cpp_TypeInfo_var, (&V_4));
			NullCheck(L_65);
			String_t* L_66 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_65);
			V_4 = *(int32_t*)UnBox(L_65);
			String_t* L_67 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE(_stringLiteralA30F013D6B164520670D1289B00C9D08C846B231, L_66, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
			Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708(L_67, /*hidden argument*/NULL);
			V_8 = 0;
			goto IL_0215;
		}

IL_01fb:
		{
			MarkerPrefabsU5BU5D_t7F40C670B36EE25921E51F171FD6A68A7A1BC336* L_68 = __this->get_markerPrefabCombos_4();
			int32_t L_69 = V_8;
			NullCheck(L_68);
			int32_t L_70 = L_69;
			MarkerPrefabs_t227A4AF77E7197AEFE5B31A217749784AA27AE6B * L_71 = (L_68)->GetAt(static_cast<il2cpp_array_size_t>(L_70));
			NullCheck(L_71);
			GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_72 = L_71->get_targetPrefab_1();
			NullCheck(L_72);
			GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_72, (bool)0, /*hidden argument*/NULL);
			int32_t L_73 = V_8;
			V_8 = ((int32_t)il2cpp_codegen_add((int32_t)L_73, (int32_t)1));
		}

IL_0215:
		{
			int32_t L_74 = V_8;
			MarkerPrefabsU5BU5D_t7F40C670B36EE25921E51F171FD6A68A7A1BC336* L_75 = __this->get_markerPrefabCombos_4();
			NullCheck(L_75);
			if ((((int32_t)L_74) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_75)->max_length)))))))
			{
				goto IL_01fb;
			}
		}

IL_0221:
		{
			bool L_76 = Enumerator_MoveNext_m75BAC688340F0794E4C6C98B495BB4B9A02E6522((Enumerator_t6754DF054050219F46CEED0786A14A987A139408 *)(&V_0), /*hidden argument*/Enumerator_MoveNext_m75BAC688340F0794E4C6C98B495BB4B9A02E6522_RuntimeMethod_var);
			if (L_76)
			{
				goto IL_005f;
			}
		}

IL_022d:
		{
			IL2CPP_LEAVE(0x23D, FINALLY_022f);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_022f;
	}

FINALLY_022f:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m46C5EE3170D6F427C4379CE89AFE659555B786A7((Enumerator_t6754DF054050219F46CEED0786A14A987A139408 *)(&V_0), /*hidden argument*/Enumerator_Dispose_m46C5EE3170D6F427C4379CE89AFE659555B786A7_RuntimeMethod_var);
		IL2CPP_RESET_LEAVE(0x23D);
		IL2CPP_END_FINALLY(559)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(559)
	{
		IL2CPP_JUMP_TBL(0x23D, IL_023d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_023d:
	{
		List_1_t85456E82777B8C7F9CFFED2C540D0613542A8C8D * L_77 = ARTrackedImagesChangedEventArgs_get_removed_mC4922D72993078710694F8D76A02BE9A23EFB7E8((ARTrackedImagesChangedEventArgs_t64F82ABE51189C62E3811D96F2C2143DB91669E4 *)(&___args0), /*hidden argument*/NULL);
		NullCheck(L_77);
		Enumerator_t6754DF054050219F46CEED0786A14A987A139408  L_78 = List_1_GetEnumerator_m06B9ADCB9085604C5418BD1AC24ADA9237C95CF9(L_77, /*hidden argument*/List_1_GetEnumerator_m06B9ADCB9085604C5418BD1AC24ADA9237C95CF9_RuntimeMethod_var);
		V_0 = L_78;
	}

IL_024a:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0273;
		}

IL_024c:
		{
			ARTrackedImage_tD699A1D6CA301ECE200D07D1CF1722E94AFEF01A * L_79 = Enumerator_get_Current_m800C03D423EC5FCFD560CC7A2B3FF44087CEE0BF((Enumerator_t6754DF054050219F46CEED0786A14A987A139408 *)(&V_0), /*hidden argument*/Enumerator_get_Current_m800C03D423EC5FCFD560CC7A2B3FF44087CEE0BF_RuntimeMethod_var);
			V_9 = L_79;
			ARTrackedImage_tD699A1D6CA301ECE200D07D1CF1722E94AFEF01A * L_80 = V_9;
			NullCheck(L_80);
			XRReferenceImage_t5A53387AC6253D5D3DFD62BC583A45BBDFC1347E  L_81 = ARTrackedImage_get_referenceImage_m46AAAD2FF5BF6E2A9AB8192BCE198FB69F00CC7D(L_80, /*hidden argument*/NULL);
			V_2 = L_81;
			String_t* L_82 = XRReferenceImage_get_name_m6DD83ECED755444645A14816918747E2EEAE92A9((XRReferenceImage_t5A53387AC6253D5D3DFD62BC583A45BBDFC1347E *)(&V_2), /*hidden argument*/NULL);
			String_t* L_83 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE(_stringLiteral1070D410481ACDBAF8E92F351242DA3D26ABF17A, L_82, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
			Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708(L_83, /*hidden argument*/NULL);
		}

IL_0273:
		{
			bool L_84 = Enumerator_MoveNext_m75BAC688340F0794E4C6C98B495BB4B9A02E6522((Enumerator_t6754DF054050219F46CEED0786A14A987A139408 *)(&V_0), /*hidden argument*/Enumerator_MoveNext_m75BAC688340F0794E4C6C98B495BB4B9A02E6522_RuntimeMethod_var);
			if (L_84)
			{
				goto IL_024c;
			}
		}

IL_027c:
		{
			IL2CPP_LEAVE(0x28C, FINALLY_027e);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_027e;
	}

FINALLY_027e:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m46C5EE3170D6F427C4379CE89AFE659555B786A7((Enumerator_t6754DF054050219F46CEED0786A14A987A139408 *)(&V_0), /*hidden argument*/Enumerator_Dispose_m46C5EE3170D6F427C4379CE89AFE659555B786A7_RuntimeMethod_var);
		IL2CPP_RESET_LEAVE(0x28C);
		IL2CPP_END_FINALLY(638)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(638)
	{
		IL2CPP_JUMP_TBL(0x28C, IL_028c)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_028c:
	{
		return;
	}
}
// System.Void ImageRecognitionExample::Start()
extern "C" IL2CPP_METHOD_ATTR void ImageRecognitionExample_Start_mCF6A53211748B4F3354205321894D8A83CE7AD22 (ImageRecognitionExample_t911A7F2FD5BFFF23A30C97F8619C18A1F0757110 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void ImageRecognitionExample::Update()
extern "C" IL2CPP_METHOD_ATTR void ImageRecognitionExample_Update_mC2498424F35CEF863FFF644441D85ED82E90E09E (ImageRecognitionExample_t911A7F2FD5BFFF23A30C97F8619C18A1F0757110 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ImageRecognitionExample_Update_mC2498424F35CEF863FFF644441D85ED82E90E09E_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		bool L_0 = __this->get_shouldObjectsBeShown_10();
		if (!L_0)
		{
			goto IL_000e;
		}
	}
	{
		ImageRecognitionExample_inflateObjectsInVicinity_mFEF7B58384ED8DAD668118E514CFEBE3B52635C1(__this, /*hidden argument*/NULL);
	}

IL_000e:
	{
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_1 = __this->get_m_MainCamera_7();
		NullCheck(L_1);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_2 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_3 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_2, /*hidden argument*/NULL);
		__this->set_userPosition_12(L_3);
		int32_t L_4 = __this->get_count_8();
		__this->set_count_8(((int32_t)il2cpp_codegen_add((int32_t)L_4, (int32_t)1)));
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_5 = __this->get_m_MainCamera_7();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_6 = __this->get_prefabPosition_11();
		NullCheck(L_5);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_7 = Camera_WorldToViewportPoint_mC939A83CE134EA2CE666C2DF5AD8FA4CB48AE3C1(L_5, L_6, /*hidden argument*/NULL);
		V_0 = L_7;
		String_t* L_8 = Vector3_ToString_m2682D27AB50CD1CE4677C38D0720A302D582348D((Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&V_0), /*hidden argument*/NULL);
		String_t* L_9 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE(_stringLiteral9F5957ECF885109B5CDB03117A55B5CAA759AB53, L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708(L_9, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_10 = V_0;
		float L_11 = L_10.get_z_4();
		if ((!(((float)L_11) > ((float)(0.0f)))))
		{
			goto IL_00a9;
		}
	}
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_12 = V_0;
		float L_13 = L_12.get_x_2();
		if ((!(((float)L_13) > ((float)(0.0f)))))
		{
			goto IL_00a9;
		}
	}
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_14 = V_0;
		float L_15 = L_14.get_x_2();
		if ((!(((float)L_15) < ((float)(1.0f)))))
		{
			goto IL_00a9;
		}
	}
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_16 = V_0;
		float L_17 = L_16.get_y_3();
		if ((!(((float)L_17) > ((float)(0.0f)))))
		{
			goto IL_00a9;
		}
	}
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_18 = V_0;
		float L_19 = L_18.get_y_3();
		if ((!(((float)L_19) < ((float)(1.0f)))))
		{
			goto IL_00a9;
		}
	}
	{
		__this->set_isInTheFrame_9((bool)1);
		return;
	}

IL_00a9:
	{
		__this->set_isInTheFrame_9((bool)0);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_20 = __this->get_objectInflatedOnScreen_13();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_21 = Object_op_Inequality_m31EF58E217E8F4BDD3E409DEF79E1AEE95874FC1(L_20, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_21)
		{
			goto IL_00c8;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708(_stringLiteral36FEC5D5ACCA82E76ED3E6089026A917559F8440, /*hidden argument*/NULL);
	}

IL_00c8:
	{
		return;
	}
}
// System.Void ImageRecognitionExample::inflateObjectsInVicinity()
extern "C" IL2CPP_METHOD_ATTR void ImageRecognitionExample_inflateObjectsInVicinity_mFEF7B58384ED8DAD668118E514CFEBE3B52635C1 (ImageRecognitionExample_t911A7F2FD5BFFF23A30C97F8619C18A1F0757110 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ImageRecognitionExample_inflateObjectsInVicinity_mFEF7B58384ED8DAD668118E514CFEBE3B52635C1_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	float V_1 = 0.0f;
	{
		V_0 = 0;
		goto IL_00a3;
	}

IL_0007:
	{
		PrefabsInVicinityU5BU5D_tCF6E2464E3202CBD16C9DEA043361EA7DB3A49A9* L_0 = __this->get_prefabsInVicinities_5();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		PrefabsInVicinity_tF56E3155685D05BFC0529D6C62A92A5C2B88A039 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		NullCheck(L_3);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_4 = L_3->get_prefabPresent_1();
		NullCheck(L_4);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_5 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_6 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_5, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_7 = __this->get_userPosition_12();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		float L_8 = Vector3_Distance_mE316E10B9B319A5C2A29F86E028740FD528149E7(L_6, L_7, /*hidden argument*/NULL);
		V_1 = L_8;
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_9 = (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)SZArrayNew(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A_il2cpp_TypeInfo_var, (uint32_t)4);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_10 = L_9;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral09EF5102C999B2492459D0CDEBA4014825ED07F4);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)_stringLiteral09EF5102C999B2492459D0CDEBA4014825ED07F4);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_11 = L_10;
		PrefabsInVicinityU5BU5D_tCF6E2464E3202CBD16C9DEA043361EA7DB3A49A9* L_12 = __this->get_prefabsInVicinities_5();
		int32_t L_13 = V_0;
		NullCheck(L_12);
		int32_t L_14 = L_13;
		PrefabsInVicinity_tF56E3155685D05BFC0529D6C62A92A5C2B88A039 * L_15 = (L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_14));
		NullCheck(L_15);
		String_t* L_16 = L_15->get_prefabName_0();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_16);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_16);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_17 = L_11;
		NullCheck(L_17);
		ArrayElementTypeCheck (L_17, _stringLiteral110BE4F4AA851DA91271D7D722097AD2AEEAD525);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)_stringLiteral110BE4F4AA851DA91271D7D722097AD2AEEAD525);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_18 = L_17;
		float L_19 = V_1;
		float L_20 = L_19;
		RuntimeObject * L_21 = Box(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_il2cpp_TypeInfo_var, &L_20);
		NullCheck(L_18);
		ArrayElementTypeCheck (L_18, L_21);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_21);
		String_t* L_22 = String_Concat_mB7BA84F13912303B2E5E40FBF0109E1A328ACA07(L_18, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708(L_22, /*hidden argument*/NULL);
		float L_23 = V_1;
		if ((!(((double)(((double)((double)L_23)))) <= ((double)(2.0)))))
		{
			goto IL_009f;
		}
	}
	{
		PrefabsInVicinityU5BU5D_tCF6E2464E3202CBD16C9DEA043361EA7DB3A49A9* L_24 = __this->get_prefabsInVicinities_5();
		int32_t L_25 = V_0;
		NullCheck(L_24);
		int32_t L_26 = L_25;
		PrefabsInVicinity_tF56E3155685D05BFC0529D6C62A92A5C2B88A039 * L_27 = (L_24)->GetAt(static_cast<il2cpp_array_size_t>(L_26));
		NullCheck(L_27);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_28 = L_27->get_prefabPresent_1();
		NullCheck(L_28);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_28, (bool)1, /*hidden argument*/NULL);
		PrefabsInVicinityU5BU5D_tCF6E2464E3202CBD16C9DEA043361EA7DB3A49A9* L_29 = __this->get_prefabsInVicinities_5();
		int32_t L_30 = V_0;
		NullCheck(L_29);
		int32_t L_31 = L_30;
		PrefabsInVicinity_tF56E3155685D05BFC0529D6C62A92A5C2B88A039 * L_32 = (L_29)->GetAt(static_cast<il2cpp_array_size_t>(L_31));
		NullCheck(L_32);
		String_t* L_33 = L_32->get_prefabName_0();
		String_t* L_34 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE(_stringLiteralB50A7D3B0F99300A231D81BA74CEE18CD3BF5EC3, L_33, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708(L_34, /*hidden argument*/NULL);
	}

IL_009f:
	{
		int32_t L_35 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_35, (int32_t)1));
	}

IL_00a3:
	{
		int32_t L_36 = V_0;
		PrefabsInVicinityU5BU5D_tCF6E2464E3202CBD16C9DEA043361EA7DB3A49A9* L_37 = __this->get_prefabsInVicinities_5();
		NullCheck(L_37);
		if ((((int32_t)L_36) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_37)->max_length)))))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void ImageRecognitionExample::.ctor()
extern "C" IL2CPP_METHOD_ATTR void ImageRecognitionExample__ctor_m3C9C0E440AAC1D33A5D764ECF85B7985853153FC (ImageRecognitionExample_t911A7F2FD5BFFF23A30C97F8619C18A1F0757110 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MarkerPrefabs::.ctor()
extern "C" IL2CPP_METHOD_ATTR void MarkerPrefabs__ctor_mC6DD4C2AB87114BC90C2AB0522199A85E4958569 (MarkerPrefabs_t227A4AF77E7197AEFE5B31A217749784AA27AE6B * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void PrefabsInVicinity::.ctor()
extern "C" IL2CPP_METHOD_ATTR void PrefabsInVicinity__ctor_m3C33D68FCD42085C26E8D12F2CFC89F781E72F0C (PrefabsInVicinity_tF56E3155685D05BFC0529D6C62A92A5C2B88A039 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TurnTheGameOn.ArrowWaypointer.Waypoint::Update()
extern "C" IL2CPP_METHOD_ATTR void Waypoint_Update_mB8767078754CB1D93447A1142A45618F7AE8B3F6 (Waypoint_t7B48E7B71CF48791746A5B866BCCB7F9FABFCA0A * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Waypoint_Update_mB8767078754CB1D93447A1142A45618F7AE8B3F6_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		WaypointController_tD4E5882DF3A666D2E1C97E9E73061816C123AB20 * L_0 = __this->get_waypointController_5();
		NullCheck(L_0);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_1 = L_0->get_player_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Implicit_m8B2A44B4B1406ED346D1AE6D962294FD58D0D534(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0046;
		}
	}
	{
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_3 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_4 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_3, /*hidden argument*/NULL);
		WaypointController_tD4E5882DF3A666D2E1C97E9E73061816C123AB20 * L_5 = __this->get_waypointController_5();
		NullCheck(L_5);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_6 = L_5->get_player_4();
		NullCheck(L_6);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_7 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		float L_8 = Vector3_Distance_mE316E10B9B319A5C2A29F86E028740FD528149E7(L_4, L_7, /*hidden argument*/NULL);
		int32_t L_9 = __this->get_radius_4();
		if ((!(((float)L_8) < ((float)(((float)((float)L_9)))))))
		{
			goto IL_0046;
		}
	}
	{
		WaypointController_tD4E5882DF3A666D2E1C97E9E73061816C123AB20 * L_10 = __this->get_waypointController_5();
		NullCheck(L_10);
		WaypointController_ChangeTarget_m8D5F516033E81CC08980BAA7CE9FE4187456B63B(L_10, /*hidden argument*/NULL);
	}

IL_0046:
	{
		return;
	}
}
// System.Void TurnTheGameOn.ArrowWaypointer.Waypoint::OnTriggerEnter(UnityEngine.Collider)
extern "C" IL2CPP_METHOD_ATTR void Waypoint_OnTriggerEnter_mE727467660A29DBBAD9152E21605AF2879F2AC28 (Waypoint_t7B48E7B71CF48791746A5B866BCCB7F9FABFCA0A * __this, Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * ___col0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Waypoint_OnTriggerEnter_mE727467660A29DBBAD9152E21605AF2879F2AC28_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * L_0 = ___col0;
		NullCheck(L_0);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_1 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_2 = GameObject_get_tag_mA9DC75D3D591B5E7D1ADAD14EA66CC4186580275(L_1, /*hidden argument*/NULL);
		bool L_3 = String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE(L_2, _stringLiteralE53407CFE1A5156B9F0D1EED3BAB5EF3AE75CFD8, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0033;
		}
	}
	{
		WaypointController_tD4E5882DF3A666D2E1C97E9E73061816C123AB20 * L_4 = __this->get_waypointController_5();
		int32_t L_5 = __this->get_waypointNumber_6();
		NullCheck(L_4);
		WaypointController_WaypointEvent_m69D615D2D554FDFDFCAE78015B032EB721E3BF01(L_4, L_5, /*hidden argument*/NULL);
		WaypointController_tD4E5882DF3A666D2E1C97E9E73061816C123AB20 * L_6 = __this->get_waypointController_5();
		NullCheck(L_6);
		WaypointController_ChangeTarget_m8D5F516033E81CC08980BAA7CE9FE4187456B63B(L_6, /*hidden argument*/NULL);
	}

IL_0033:
	{
		return;
	}
}
// System.Void TurnTheGameOn.ArrowWaypointer.Waypoint::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Waypoint__ctor_m7B9AE07A50077AECAD82787C043BE5345EB7C786 (Waypoint_t7B48E7B71CF48791746A5B866BCCB7F9FABFCA0A * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TurnTheGameOn.ArrowWaypointer.WaypointController::Start()
extern "C" IL2CPP_METHOD_ATTR void WaypointController_Start_m1D3C90981C9D75F4796FF21DBF8DEF2202B3D6BD (WaypointController_tD4E5882DF3A666D2E1C97E9E73061816C123AB20 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WaypointController_Start_m1D3C90981C9D75F4796FF21DBF8DEF2202B3D6BD_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * V_0 = NULL;
	{
		bool L_0 = Application_get_isPlaying_mF43B519662E7433DD90D883E5AE22EC3CFB65CA5(/*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_003c;
		}
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_1 = (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *)il2cpp_codegen_object_new(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_il2cpp_TypeInfo_var);
		GameObject__ctor_mA4DFA8F4471418C248E95B55070665EF344B4B2D(L_1, /*hidden argument*/NULL);
		V_0 = L_1;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_2 = V_0;
		NullCheck(L_2);
		Object_set_name_m538711B144CDE30F929376BCF72D0DC8F85D0826(L_2, _stringLiteralF79EAD34F827E5E12513B175430AD4243748F1E0, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_3 = V_0;
		NullCheck(L_3);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_4 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_3, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_5 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_6 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_set_parent_m65B8E4660B2C554069C57A957D9E55FECA7AA73E(L_4, L_6, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_7 = V_0;
		NullCheck(L_7);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_8 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_7, /*hidden argument*/NULL);
		__this->set_arrowTarget_14(L_8);
		V_0 = (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *)NULL;
	}

IL_003c:
	{
		__this->set_nextWP_11(0);
		WaypointController_ChangeTarget_m8D5F516033E81CC08980BAA7CE9FE4187456B63B(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TurnTheGameOn.ArrowWaypointer.WaypointController::Update()
extern "C" IL2CPP_METHOD_ATTR void WaypointController_Update_mDBBE6585C09BAF5459BDD47D3891570E3DEC5C6E (WaypointController_tD4E5882DF3A666D2E1C97E9E73061816C123AB20 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WaypointController_Update_mDBBE6585C09BAF5459BDD47D3891570E3DEC5C6E_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get_configureMode_5();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		WaypointComponentsU5BU5D_t995379FD5B9F2912BDD0CA0EC38E68B64A9839FD* L_1 = __this->get_waypointList_8();
		NullCheck(L_1);
		__this->set_TotalWaypoints_7((((int32_t)((int32_t)(((RuntimeArray *)L_1)->max_length)))));
	}

IL_0016:
	{
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_2 = __this->get_arrowTarget_14();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Inequality_m31EF58E217E8F4BDD3E409DEF79E1AEE95874FC1(L_2, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_008a;
		}
	}
	{
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_4 = __this->get_arrowTarget_14();
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_5 = __this->get_arrowTarget_14();
		NullCheck(L_5);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_6 = Transform_get_localPosition_m812D43318E05BDCB78310EB7308785A13D85EFD8(L_5, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_7 = __this->get_currentWaypoint_13();
		NullCheck(L_7);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_8 = Transform_get_localPosition_m812D43318E05BDCB78310EB7308785A13D85EFD8(L_7, /*hidden argument*/NULL);
		float L_9 = __this->get_arrowTargetSmooth_6();
		float L_10 = Time_get_deltaTime_m16F98FC9BA931581236008C288E3B25CBCB7C81E(/*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_11 = Vector3_Lerp_m5BA75496B803820CC64079383956D73C6FD4A8A1(L_6, L_8, ((float)il2cpp_codegen_multiply((float)L_9, (float)L_10)), /*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_set_localPosition_m275F5550DD939F83AFEB5E8D681131172E2E1728(L_4, L_11, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_12 = __this->get_arrowTarget_14();
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_13 = __this->get_arrowTarget_14();
		NullCheck(L_13);
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_14 = Transform_get_localRotation_mEDA319E1B42EF12A19A95AC0824345B6574863FE(L_13, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_15 = __this->get_currentWaypoint_13();
		NullCheck(L_15);
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_16 = Transform_get_localRotation_mEDA319E1B42EF12A19A95AC0824345B6574863FE(L_15, /*hidden argument*/NULL);
		float L_17 = __this->get_arrowTargetSmooth_6();
		float L_18 = Time_get_deltaTime_m16F98FC9BA931581236008C288E3B25CBCB7C81E(/*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_il2cpp_TypeInfo_var);
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_19 = Quaternion_Lerp_m749B3988EE2EF387CC9BFB76C81B7465A7534E27(L_14, L_16, ((float)il2cpp_codegen_multiply((float)L_17, (float)L_18)), /*hidden argument*/NULL);
		NullCheck(L_12);
		Transform_set_localRotation_mE2BECB0954FFC1D93FB631600D9A9BEFF41D9C8A(L_12, L_19, /*hidden argument*/NULL);
		goto IL_0096;
	}

IL_008a:
	{
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_20 = __this->get_currentWaypoint_13();
		__this->set_arrowTarget_14(L_20);
	}

IL_0096:
	{
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_21 = __this->get_waypointArrow_12();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_22 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_21, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_00aa;
		}
	}
	{
		WaypointController_FindArrow_m95F170F1E2ADB8A718A914339C326FD87324661F(__this, /*hidden argument*/NULL);
	}

IL_00aa:
	{
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_23 = __this->get_waypointArrow_12();
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_24 = __this->get_arrowTarget_14();
		NullCheck(L_23);
		Transform_LookAt_mF2738B4AB464ABFB85D16BEB121808F6C73D669B(L_23, L_24, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TurnTheGameOn.ArrowWaypointer.WaypointController::WaypointEvent(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void WaypointController_WaypointEvent_m69D615D2D554FDFDFCAE78015B032EB721E3BF01 (WaypointController_tD4E5882DF3A666D2E1C97E9E73061816C123AB20 * __this, int32_t ___waypointEvent0, const RuntimeMethod* method)
{
	{
		WaypointComponentsU5BU5D_t995379FD5B9F2912BDD0CA0EC38E68B64A9839FD* L_0 = __this->get_waypointList_8();
		int32_t L_1 = ___waypointEvent0;
		NullCheck(L_0);
		int32_t L_2 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_1, (int32_t)1));
		WaypointComponents_t3DE523CAE0F5855ADF2B740DBBA68569E9034301 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		NullCheck(L_3);
		UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * L_4 = L_3->get_waypointEvent_2();
		NullCheck(L_4);
		UnityEvent_Invoke_mB2FA1C76256FE34D5E7F84ABE528AC61CE8A0325(L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TurnTheGameOn.ArrowWaypointer.WaypointController::ChangeTarget()
extern "C" IL2CPP_METHOD_ATTR void WaypointController_ChangeTarget_m8D5F516033E81CC08980BAA7CE9FE4187456B63B (WaypointController_tD4E5882DF3A666D2E1C97E9E73061816C123AB20 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WaypointController_ChangeTarget_m8D5F516033E81CC08980BAA7CE9FE4187456B63B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B4_0 = 0;
	int32_t G_B1_0 = 0;
	int32_t G_B3_0 = 0;
	int32_t G_B2_0 = 0;
	{
		int32_t L_0 = __this->get_nextWP_11();
		int32_t L_1 = L_0;
		int32_t L_2 = __this->get_TotalWaypoints_7();
		G_B1_0 = L_1;
		if ((((int32_t)L_1) >= ((int32_t)L_2)))
		{
			G_B4_0 = L_1;
			goto IL_0082;
		}
	}
	{
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_3 = __this->get_currentWaypoint_13();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_3, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		G_B2_0 = G_B1_0;
		if (!L_4)
		{
			G_B3_0 = G_B1_0;
			goto IL_0035;
		}
	}
	{
		WaypointComponentsU5BU5D_t995379FD5B9F2912BDD0CA0EC38E68B64A9839FD* L_5 = __this->get_waypointList_8();
		NullCheck(L_5);
		int32_t L_6 = 0;
		WaypointComponents_t3DE523CAE0F5855ADF2B740DBBA68569E9034301 * L_7 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		NullCheck(L_7);
		Waypoint_t7B48E7B71CF48791746A5B866BCCB7F9FABFCA0A * L_8 = L_7->get_waypoint_1();
		NullCheck(L_8);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_9 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(L_8, /*hidden argument*/NULL);
		__this->set_currentWaypoint_13(L_9);
		G_B3_0 = G_B2_0;
	}

IL_0035:
	{
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_10 = __this->get_currentWaypoint_13();
		NullCheck(L_10);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_11 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_11, (bool)0, /*hidden argument*/NULL);
		WaypointComponentsU5BU5D_t995379FD5B9F2912BDD0CA0EC38E68B64A9839FD* L_12 = __this->get_waypointList_8();
		int32_t L_13 = __this->get_nextWP_11();
		NullCheck(L_12);
		int32_t L_14 = L_13;
		WaypointComponents_t3DE523CAE0F5855ADF2B740DBBA68569E9034301 * L_15 = (L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_14));
		NullCheck(L_15);
		Waypoint_t7B48E7B71CF48791746A5B866BCCB7F9FABFCA0A * L_16 = L_15->get_waypoint_1();
		NullCheck(L_16);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_17 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(L_16, /*hidden argument*/NULL);
		__this->set_currentWaypoint_13(L_17);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_18 = __this->get_currentWaypoint_13();
		NullCheck(L_18);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_19 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(L_18, /*hidden argument*/NULL);
		NullCheck(L_19);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_19, (bool)1, /*hidden argument*/NULL);
		int32_t L_20 = __this->get_nextWP_11();
		__this->set_nextWP_11(((int32_t)il2cpp_codegen_add((int32_t)L_20, (int32_t)1)));
		G_B4_0 = G_B3_0;
	}

IL_0082:
	{
		int32_t L_21 = __this->get_TotalWaypoints_7();
		if ((!(((uint32_t)G_B4_0) == ((uint32_t)L_21))))
		{
			goto IL_00a5;
		}
	}
	{
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_22 = __this->get_waypointArrow_12();
		NullCheck(L_22);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_23 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(L_22, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		Object_Destroy_m23B4562495BA35A74266D4372D45368F8C05109A(L_23, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_24 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		Object_Destroy_m23B4562495BA35A74266D4372D45368F8C05109A(L_24, /*hidden argument*/NULL);
	}

IL_00a5:
	{
		return;
	}
}
// System.Void TurnTheGameOn.ArrowWaypointer.WaypointController::CreateArrow()
extern "C" IL2CPP_METHOD_ATTR void WaypointController_CreateArrow_m3E17F297C402A2F304D0FA081808552E733DC7BC (WaypointController_tD4E5882DF3A666D2E1C97E9E73061816C123AB20 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WaypointController_CreateArrow_m3E17F297C402A2F304D0FA081808552E733DC7BC_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_0 = { reinterpret_cast<intptr_t> (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6(L_0, /*hidden argument*/NULL);
		Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * L_2 = Resources_Load_mF0FA033BF566CDDA6A0E69BB97283B44C40726E7(_stringLiteral6AE43D92255E63B6460986C81E8F68153762E449, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * L_3 = Object_Instantiate_m17AA3123A55239124BC54A907AEEE509034F0830(L_2, /*hidden argument*/NULL);
		NullCheck(((GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *)IsInstSealed((RuntimeObject*)L_3, GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_il2cpp_TypeInfo_var)));
		Object_set_name_m538711B144CDE30F929376BCF72D0DC8F85D0826(((GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *)IsInstSealed((RuntimeObject*)L_3, GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_il2cpp_TypeInfo_var)), _stringLiteral6AE43D92255E63B6460986C81E8F68153762E449, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TurnTheGameOn.ArrowWaypointer.WaypointController::FindArrow()
extern "C" IL2CPP_METHOD_ATTR void WaypointController_FindArrow_m95F170F1E2ADB8A718A914339C326FD87324661F (WaypointController_tD4E5882DF3A666D2E1C97E9E73061816C123AB20 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WaypointController_FindArrow_m95F170F1E2ADB8A718A914339C326FD87324661F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * V_0 = NULL;
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_0 = GameObject_Find_m1470FB04EB6DB15CCC0D9745B70EE987B318E9BD(_stringLiteral6AE43D92255E63B6460986C81E8F68153762E449, /*hidden argument*/NULL);
		V_0 = L_0;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_1, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0030;
		}
	}
	{
		WaypointController_CreateArrow_m3E17F297C402A2F304D0FA081808552E733DC7BC(__this, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_3 = GameObject_Find_m1470FB04EB6DB15CCC0D9745B70EE987B318E9BD(_stringLiteral6AE43D92255E63B6460986C81E8F68153762E449, /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_4 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_3, /*hidden argument*/NULL);
		__this->set_waypointArrow_12(L_4);
		return;
	}

IL_0030:
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_5 = V_0;
		NullCheck(L_5);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_6 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_5, /*hidden argument*/NULL);
		__this->set_waypointArrow_12(L_6);
		return;
	}
}
// System.Void TurnTheGameOn.ArrowWaypointer.WaypointController::CalculateWaypoints()
extern "C" IL2CPP_METHOD_ATTR void WaypointController_CalculateWaypoints_m88CB2133663AC7FF257CED5CACCE8641523D7910 (WaypointController_tD4E5882DF3A666D2E1C97E9E73061816C123AB20 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WaypointController_CalculateWaypoints_m88CB2133663AC7FF257CED5CACCE8641523D7910_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	RuntimeObject* V_1 = NULL;
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * V_2 = NULL;
	RuntimeObject* V_3 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = -1;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = __this->get_configureMode_5();
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_01c2;
		}
	}
	{
		WaypointComponentsU5BU5D_t995379FD5B9F2912BDD0CA0EC38E68B64A9839FD** L_1 = __this->get_address_of_waypointList_8();
		int32_t L_2 = __this->get_TotalWaypoints_7();
		Array_Resize_TisWaypointComponents_t3DE523CAE0F5855ADF2B740DBBA68569E9034301_m9D1CC9E22BEE7FA8386256BA820CC2B8D7CAA87A((WaypointComponentsU5BU5D_t995379FD5B9F2912BDD0CA0EC38E68B64A9839FD**)L_1, L_2, /*hidden argument*/Array_Resize_TisWaypointComponents_t3DE523CAE0F5855ADF2B740DBBA68569E9034301_m9D1CC9E22BEE7FA8386256BA820CC2B8D7CAA87A_RuntimeMethod_var);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_3 = __this->get_waypointArrow_12();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_3, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0031;
		}
	}
	{
		WaypointController_FindArrow_m95F170F1E2ADB8A718A914339C326FD87324661F(__this, /*hidden argument*/NULL);
	}

IL_0031:
	{
		V_0 = 0;
		goto IL_01b0;
	}

IL_0038:
	{
		WaypointComponentsU5BU5D_t995379FD5B9F2912BDD0CA0EC38E68B64A9839FD* L_5 = __this->get_waypointList_8();
		int32_t L_6 = V_0;
		NullCheck(L_5);
		int32_t L_7 = L_6;
		WaypointComponents_t3DE523CAE0F5855ADF2B740DBBA68569E9034301 * L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		if (!L_8)
		{
			goto IL_01ac;
		}
	}
	{
		WaypointComponentsU5BU5D_t995379FD5B9F2912BDD0CA0EC38E68B64A9839FD* L_9 = __this->get_waypointList_8();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		int32_t L_11 = L_10;
		WaypointComponents_t3DE523CAE0F5855ADF2B740DBBA68569E9034301 * L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		NullCheck(L_12);
		Waypoint_t7B48E7B71CF48791746A5B866BCCB7F9FABFCA0A * L_13 = L_12->get_waypoint_1();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_14 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_13, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_01ac;
		}
	}
	{
		int32_t L_15 = V_0;
		int32_t L_16 = ((int32_t)il2cpp_codegen_add((int32_t)L_15, (int32_t)1));
		RuntimeObject * L_17 = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &L_16);
		String_t* L_18 = String_Concat_mBB19C73816BDD1C3519F248E1ADC8E11A6FDB495(_stringLiteral4F45D2FEA2B49D472D55F53F0CFAC6EDD8065D8D, L_17, /*hidden argument*/NULL);
		__this->set_newWaypointName_10(L_18);
		WaypointComponentsU5BU5D_t995379FD5B9F2912BDD0CA0EC38E68B64A9839FD* L_19 = __this->get_waypointList_8();
		int32_t L_20 = V_0;
		NullCheck(L_19);
		int32_t L_21 = L_20;
		WaypointComponents_t3DE523CAE0F5855ADF2B740DBBA68569E9034301 * L_22 = (L_19)->GetAt(static_cast<il2cpp_array_size_t>(L_21));
		String_t* L_23 = __this->get_newWaypointName_10();
		NullCheck(L_22);
		L_22->set_waypointName_0(L_23);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_24 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		NullCheck(L_24);
		RuntimeObject* L_25 = Transform_GetEnumerator_mE98B6C5F644AE362EC1D58C10506327D6A5878FC(L_24, /*hidden argument*/NULL);
		V_1 = L_25;
	}

IL_0094:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00c8;
		}

IL_0096:
		{
			RuntimeObject* L_26 = V_1;
			NullCheck(L_26);
			RuntimeObject * L_27 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(1 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t8789118187258CC88B77AFAC6315B5AF87D3E18A_il2cpp_TypeInfo_var, L_26);
			V_2 = ((Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA *)CastclassClass((RuntimeObject*)L_27, Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA_il2cpp_TypeInfo_var));
			Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_28 = V_2;
			NullCheck(L_28);
			String_t* L_29 = Object_get_name_mA2D400141CB3C991C87A2556429781DE961A83CE(L_28, /*hidden argument*/NULL);
			String_t* L_30 = __this->get_newWaypointName_10();
			bool L_31 = String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE(L_29, L_30, /*hidden argument*/NULL);
			if (!L_31)
			{
				goto IL_00c8;
			}
		}

IL_00b5:
		{
			WaypointComponentsU5BU5D_t995379FD5B9F2912BDD0CA0EC38E68B64A9839FD* L_32 = __this->get_waypointList_8();
			int32_t L_33 = V_0;
			NullCheck(L_32);
			int32_t L_34 = L_33;
			WaypointComponents_t3DE523CAE0F5855ADF2B740DBBA68569E9034301 * L_35 = (L_32)->GetAt(static_cast<il2cpp_array_size_t>(L_34));
			Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_36 = V_2;
			NullCheck(L_36);
			Waypoint_t7B48E7B71CF48791746A5B866BCCB7F9FABFCA0A * L_37 = Component_GetComponent_TisWaypoint_t7B48E7B71CF48791746A5B866BCCB7F9FABFCA0A_m7AB24D399574ADB4EAD0A31828AAFDA12A50233F(L_36, /*hidden argument*/Component_GetComponent_TisWaypoint_t7B48E7B71CF48791746A5B866BCCB7F9FABFCA0A_m7AB24D399574ADB4EAD0A31828AAFDA12A50233F_RuntimeMethod_var);
			NullCheck(L_35);
			L_35->set_waypoint_1(L_37);
		}

IL_00c8:
		{
			RuntimeObject* L_38 = V_1;
			NullCheck(L_38);
			bool L_39 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t8789118187258CC88B77AFAC6315B5AF87D3E18A_il2cpp_TypeInfo_var, L_38);
			if (L_39)
			{
				goto IL_0096;
			}
		}

IL_00d0:
		{
			IL2CPP_LEAVE(0xE3, FINALLY_00d2);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_00d2;
	}

FINALLY_00d2:
	{ // begin finally (depth: 1)
		{
			RuntimeObject* L_40 = V_1;
			V_3 = ((RuntimeObject*)IsInst((RuntimeObject*)L_40, IDisposable_t7218B22548186B208D65EA5B7870503810A2D15A_il2cpp_TypeInfo_var));
			RuntimeObject* L_41 = V_3;
			if (!L_41)
			{
				goto IL_00e2;
			}
		}

IL_00dc:
		{
			RuntimeObject* L_42 = V_3;
			NullCheck(L_42);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t7218B22548186B208D65EA5B7870503810A2D15A_il2cpp_TypeInfo_var, L_42);
		}

IL_00e2:
		{
			IL2CPP_RESET_LEAVE(0xE3);
			IL2CPP_END_FINALLY(210)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(210)
	{
		IL2CPP_JUMP_TBL(0xE3, IL_00e3)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_00e3:
	{
		WaypointComponentsU5BU5D_t995379FD5B9F2912BDD0CA0EC38E68B64A9839FD* L_43 = __this->get_waypointList_8();
		int32_t L_44 = V_0;
		NullCheck(L_43);
		int32_t L_45 = L_44;
		WaypointComponents_t3DE523CAE0F5855ADF2B740DBBA68569E9034301 * L_46 = (L_43)->GetAt(static_cast<il2cpp_array_size_t>(L_45));
		NullCheck(L_46);
		Waypoint_t7B48E7B71CF48791746A5B866BCCB7F9FABFCA0A * L_47 = L_46->get_waypoint_1();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_48 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_47, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_48)
		{
			goto IL_0194;
		}
	}
	{
		Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * L_49 = Resources_Load_mDBE6B83A74A52A1A6F5A68F7E5BC112DBB81B3C1(_stringLiteral71648DD8C0E0AB094578F2E030DB26C525C7BA67, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * L_50 = Object_Instantiate_m17AA3123A55239124BC54A907AEEE509034F0830(L_49, /*hidden argument*/NULL);
		__this->set_newWaypoint_9(((GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *)IsInstSealed((RuntimeObject*)L_50, GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_il2cpp_TypeInfo_var)));
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_51 = __this->get_newWaypoint_9();
		String_t* L_52 = __this->get_newWaypointName_10();
		NullCheck(L_51);
		Object_set_name_m538711B144CDE30F929376BCF72D0DC8F85D0826(L_51, L_52, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_53 = __this->get_newWaypoint_9();
		NullCheck(L_53);
		Waypoint_t7B48E7B71CF48791746A5B866BCCB7F9FABFCA0A * L_54 = GameObject_GetComponent_TisWaypoint_t7B48E7B71CF48791746A5B866BCCB7F9FABFCA0A_m8DDD1ED52C9E382D4753784215506A29EDA35B32(L_53, /*hidden argument*/GameObject_GetComponent_TisWaypoint_t7B48E7B71CF48791746A5B866BCCB7F9FABFCA0A_m8DDD1ED52C9E382D4753784215506A29EDA35B32_RuntimeMethod_var);
		int32_t L_55 = V_0;
		NullCheck(L_54);
		L_54->set_waypointNumber_6(((int32_t)il2cpp_codegen_add((int32_t)L_55, (int32_t)1)));
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_56 = __this->get_newWaypoint_9();
		NullCheck(L_56);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_57 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_56, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_58 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		NullCheck(L_58);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_59 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_58, /*hidden argument*/NULL);
		NullCheck(L_57);
		Transform_set_parent_m65B8E4660B2C554069C57A957D9E55FECA7AA73E(L_57, L_59, /*hidden argument*/NULL);
		WaypointComponentsU5BU5D_t995379FD5B9F2912BDD0CA0EC38E68B64A9839FD* L_60 = __this->get_waypointList_8();
		int32_t L_61 = V_0;
		NullCheck(L_60);
		int32_t L_62 = L_61;
		WaypointComponents_t3DE523CAE0F5855ADF2B740DBBA68569E9034301 * L_63 = (L_60)->GetAt(static_cast<il2cpp_array_size_t>(L_62));
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_64 = __this->get_newWaypoint_9();
		NullCheck(L_64);
		Waypoint_t7B48E7B71CF48791746A5B866BCCB7F9FABFCA0A * L_65 = GameObject_GetComponent_TisWaypoint_t7B48E7B71CF48791746A5B866BCCB7F9FABFCA0A_m8DDD1ED52C9E382D4753784215506A29EDA35B32(L_64, /*hidden argument*/GameObject_GetComponent_TisWaypoint_t7B48E7B71CF48791746A5B866BCCB7F9FABFCA0A_m8DDD1ED52C9E382D4753784215506A29EDA35B32_RuntimeMethod_var);
		NullCheck(L_63);
		L_63->set_waypoint_1(L_65);
		WaypointComponentsU5BU5D_t995379FD5B9F2912BDD0CA0EC38E68B64A9839FD* L_66 = __this->get_waypointList_8();
		int32_t L_67 = V_0;
		NullCheck(L_66);
		int32_t L_68 = L_67;
		WaypointComponents_t3DE523CAE0F5855ADF2B740DBBA68569E9034301 * L_69 = (L_66)->GetAt(static_cast<il2cpp_array_size_t>(L_68));
		NullCheck(L_69);
		Waypoint_t7B48E7B71CF48791746A5B866BCCB7F9FABFCA0A * L_70 = L_69->get_waypoint_1();
		NullCheck(L_70);
		L_70->set_waypointController_5(__this);
		String_t* L_71 = __this->get_newWaypointName_10();
		String_t* L_72 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE(_stringLiteral08DEE733AAADFC66234A4C80E52749BADDA1805D, L_71, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708(L_72, /*hidden argument*/NULL);
	}

IL_0194:
	{
		WaypointComponentsU5BU5D_t995379FD5B9F2912BDD0CA0EC38E68B64A9839FD* L_73 = __this->get_waypointList_8();
		NullCheck(L_73);
		int32_t L_74 = 0;
		WaypointComponents_t3DE523CAE0F5855ADF2B740DBBA68569E9034301 * L_75 = (L_73)->GetAt(static_cast<il2cpp_array_size_t>(L_74));
		NullCheck(L_75);
		Waypoint_t7B48E7B71CF48791746A5B866BCCB7F9FABFCA0A * L_76 = L_75->get_waypoint_1();
		NullCheck(L_76);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_77 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(L_76, /*hidden argument*/NULL);
		__this->set_currentWaypoint_13(L_77);
	}

IL_01ac:
	{
		int32_t L_78 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_78, (int32_t)1));
	}

IL_01b0:
	{
		int32_t L_79 = V_0;
		int32_t L_80 = __this->get_TotalWaypoints_7();
		if ((((int32_t)L_79) < ((int32_t)L_80)))
		{
			goto IL_0038;
		}
	}
	{
		WaypointController_CleanUpWaypoints_m7B8D3FEB7086B037AB945021C1D8B9753FB8C172(__this, /*hidden argument*/NULL);
	}

IL_01c2:
	{
		return;
	}
}
// System.Void TurnTheGameOn.ArrowWaypointer.WaypointController::CleanUpWaypoints()
extern "C" IL2CPP_METHOD_ATTR void WaypointController_CleanUpWaypoints_m7B8D3FEB7086B037AB945021C1D8B9753FB8C172 (WaypointController_tD4E5882DF3A666D2E1C97E9E73061816C123AB20 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WaypointController_CleanUpWaypoints_m7B8D3FEB7086B037AB945021C1D8B9753FB8C172_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * V_1 = NULL;
	RuntimeObject* V_2 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = -1;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = __this->get_configureMode_5();
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_0084;
		}
	}
	{
		bool L_1 = Application_get_isPlaying_mF43B519662E7433DD90D883E5AE22EC3CFB65CA5(/*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_LogWarning_m37338644DC81F640CCDFEAE35A223F0E965F0568(_stringLiteralEC315164409BDAD2E1AB675BF92EFBF8B3EB019E, /*hidden argument*/NULL);
	}

IL_001a:
	{
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_2 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		int32_t L_3 = Transform_get_childCount_m7665D779DCDB6B175FB52A254276CDF0C384A724(L_2, /*hidden argument*/NULL);
		WaypointComponentsU5BU5D_t995379FD5B9F2912BDD0CA0EC38E68B64A9839FD* L_4 = __this->get_waypointList_8();
		NullCheck(L_4);
		if ((((int32_t)L_3) <= ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_4)->max_length)))))))
		{
			goto IL_0084;
		}
	}
	{
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_5 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		RuntimeObject* L_6 = Transform_GetEnumerator_mE98B6C5F644AE362EC1D58C10506327D6A5878FC(L_5, /*hidden argument*/NULL);
		V_0 = L_6;
	}

IL_003b:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0069;
		}

IL_003d:
		{
			RuntimeObject* L_7 = V_0;
			NullCheck(L_7);
			RuntimeObject * L_8 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(1 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t8789118187258CC88B77AFAC6315B5AF87D3E18A_il2cpp_TypeInfo_var, L_7);
			V_1 = ((Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA *)CastclassClass((RuntimeObject*)L_8, Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA_il2cpp_TypeInfo_var));
			Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_9 = V_1;
			NullCheck(L_9);
			Waypoint_t7B48E7B71CF48791746A5B866BCCB7F9FABFCA0A * L_10 = Component_GetComponent_TisWaypoint_t7B48E7B71CF48791746A5B866BCCB7F9FABFCA0A_m7AB24D399574ADB4EAD0A31828AAFDA12A50233F(L_9, /*hidden argument*/Component_GetComponent_TisWaypoint_t7B48E7B71CF48791746A5B866BCCB7F9FABFCA0A_m7AB24D399574ADB4EAD0A31828AAFDA12A50233F_RuntimeMethod_var);
			NullCheck(L_10);
			int32_t L_11 = L_10->get_waypointNumber_6();
			WaypointComponentsU5BU5D_t995379FD5B9F2912BDD0CA0EC38E68B64A9839FD* L_12 = __this->get_waypointList_8();
			NullCheck(L_12);
			if ((((int32_t)L_11) <= ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_12)->max_length)))))))
			{
				goto IL_0069;
			}
		}

IL_005e:
		{
			Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_13 = V_1;
			NullCheck(L_13);
			GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_14 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(L_13, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
			Object_DestroyImmediate_mF6F4415EF22249D6E650FAA40E403283F19B7446(L_14, /*hidden argument*/NULL);
		}

IL_0069:
		{
			RuntimeObject* L_15 = V_0;
			NullCheck(L_15);
			bool L_16 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t8789118187258CC88B77AFAC6315B5AF87D3E18A_il2cpp_TypeInfo_var, L_15);
			if (L_16)
			{
				goto IL_003d;
			}
		}

IL_0071:
		{
			IL2CPP_LEAVE(0x84, FINALLY_0073);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0073;
	}

FINALLY_0073:
	{ // begin finally (depth: 1)
		{
			RuntimeObject* L_17 = V_0;
			V_2 = ((RuntimeObject*)IsInst((RuntimeObject*)L_17, IDisposable_t7218B22548186B208D65EA5B7870503810A2D15A_il2cpp_TypeInfo_var));
			RuntimeObject* L_18 = V_2;
			if (!L_18)
			{
				goto IL_0083;
			}
		}

IL_007d:
		{
			RuntimeObject* L_19 = V_2;
			NullCheck(L_19);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t7218B22548186B208D65EA5B7870503810A2D15A_il2cpp_TypeInfo_var, L_19);
		}

IL_0083:
		{
			IL2CPP_RESET_LEAVE(0x84);
			IL2CPP_END_FINALLY(115)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(115)
	{
		IL2CPP_JUMP_TBL(0x84, IL_0084)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0084:
	{
		return;
	}
}
// System.Void TurnTheGameOn.ArrowWaypointer.WaypointController::.ctor()
extern "C" IL2CPP_METHOD_ATTR void WaypointController__ctor_m29538729F4929E37996F31DE68CD6DFF5B1AA7E2 (WaypointController_tD4E5882DF3A666D2E1C97E9E73061816C123AB20 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TurnTheGameOn.ArrowWaypointer.WaypointController_WaypointComponents::.ctor()
extern "C" IL2CPP_METHOD_ATTR void WaypointComponents__ctor_m1F01D8B6FA5D14EA5F49E6BC0E11998407C61499 (WaypointComponents_t3DE523CAE0F5855ADF2B740DBBA68569E9034301 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WaypointComponents__ctor_m1F01D8B6FA5D14EA5F49E6BC0E11998407C61499_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_waypointName_0(_stringLiteral8C3B68BB4A07759ACDAC90DBED3433E8AADDA35B);
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TurnTheGameOn.NPCChat.SmoothFollow::LateUpdate()
extern "C" IL2CPP_METHOD_ATTR void SmoothFollow_LateUpdate_mC6D45143622DF2CA6DB06FA1053C6F973E59FA3B (SmoothFollow_t216FB57B20D1783DDB83147F7D75C14735C98316 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SmoothFollow_LateUpdate_mC6D45143622DF2CA6DB06FA1053C6F973E59FA3B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  V_4;
	memset(&V_4, 0, sizeof(V_4));
	{
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_0 = __this->get_target_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m8B2A44B4B1406ED346D1AE6D962294FD58D0D534(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_000e;
		}
	}
	{
		return;
	}

IL_000e:
	{
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_2 = __this->get_target_4();
		NullCheck(L_2);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_3 = Transform_get_eulerAngles_mF2D798FA8B18F7A1A0C4A2198329ADBAF07E37CA(L_2, /*hidden argument*/NULL);
		float L_4 = L_3.get_y_3();
		V_0 = L_4;
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_5 = __this->get_target_4();
		NullCheck(L_5);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_6 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_5, /*hidden argument*/NULL);
		float L_7 = L_6.get_y_3();
		float L_8 = __this->get_height_6();
		V_1 = ((float)il2cpp_codegen_add((float)L_7, (float)L_8));
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_9 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_10 = Transform_get_eulerAngles_mF2D798FA8B18F7A1A0C4A2198329ADBAF07E37CA(L_9, /*hidden argument*/NULL);
		float L_11 = L_10.get_y_3();
		V_2 = L_11;
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_12 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_13 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_12, /*hidden argument*/NULL);
		float L_14 = L_13.get_y_3();
		V_3 = L_14;
		float L_15 = V_2;
		float L_16 = V_0;
		float L_17 = __this->get_rotationDamping_7();
		float L_18 = Time_get_deltaTime_m16F98FC9BA931581236008C288E3B25CBCB7C81E(/*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var);
		float L_19 = Mathf_LerpAngle_m8802FB8DCECA2D48FDD13C20F384B9430E66C99F(L_15, L_16, ((float)il2cpp_codegen_multiply((float)L_17, (float)L_18)), /*hidden argument*/NULL);
		V_2 = L_19;
		float L_20 = V_3;
		float L_21 = V_1;
		float L_22 = __this->get_heightDamping_8();
		float L_23 = Time_get_deltaTime_m16F98FC9BA931581236008C288E3B25CBCB7C81E(/*hidden argument*/NULL);
		float L_24 = Mathf_Lerp_m9A74C5A0C37D0CDF45EE66E7774D12A9B93B1364(L_20, L_21, ((float)il2cpp_codegen_multiply((float)L_22, (float)L_23)), /*hidden argument*/NULL);
		V_3 = L_24;
		float L_25 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_il2cpp_TypeInfo_var);
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_26 = Quaternion_Euler_m537DD6CEAE0AD4274D8A84414C24C30730427D05((0.0f), L_25, (0.0f), /*hidden argument*/NULL);
		V_4 = L_26;
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_27 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_28 = __this->get_target_4();
		NullCheck(L_28);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_29 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_28, /*hidden argument*/NULL);
		NullCheck(L_27);
		Transform_set_position_mDA89E4893F14ECA5CBEEE7FB80A5BF7C1B8EA6DC(L_27, L_29, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_30 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_31 = L_30;
		NullCheck(L_31);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_32 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_31, /*hidden argument*/NULL);
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_33 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_34 = Vector3_get_forward_m3E2E192B3302130098738C308FA1EE1439449D0D(/*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_35 = Quaternion_op_Multiply_mD5999DE317D808808B72E58E7A978C4C0995879C(L_33, L_34, /*hidden argument*/NULL);
		float L_36 = __this->get_distance_5();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_37 = Vector3_op_Multiply_m1C5F07723615156ACF035D88A1280A9E8F35A04E(L_35, L_36, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_38 = Vector3_op_Subtraction_mF9846B723A5034F8B9F5F5DCB78E3D67649143D3(L_32, L_37, /*hidden argument*/NULL);
		NullCheck(L_31);
		Transform_set_position_mDA89E4893F14ECA5CBEEE7FB80A5BF7C1B8EA6DC(L_31, L_38, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_39 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_40 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		NullCheck(L_40);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_41 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_40, /*hidden argument*/NULL);
		float L_42 = L_41.get_x_2();
		float L_43 = V_3;
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_44 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		NullCheck(L_44);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_45 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_44, /*hidden argument*/NULL);
		float L_46 = L_45.get_z_4();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_47;
		memset(&L_47, 0, sizeof(L_47));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_47), L_42, L_43, L_46, /*hidden argument*/NULL);
		NullCheck(L_39);
		Transform_set_position_mDA89E4893F14ECA5CBEEE7FB80A5BF7C1B8EA6DC(L_39, L_47, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_48 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_49 = __this->get_target_4();
		NullCheck(L_48);
		Transform_LookAt_mF2738B4AB464ABFB85D16BEB121808F6C73D669B(L_48, L_49, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TurnTheGameOn.NPCChat.SmoothFollow::.ctor()
extern "C" IL2CPP_METHOD_ATTR void SmoothFollow__ctor_m813573DCA4677C6B34C88CEE42738B4CB4204410 (SmoothFollow_t216FB57B20D1783DDB83147F7D75C14735C98316 * __this, const RuntimeMethod* method)
{
	{
		__this->set_distance_5((10.0f));
		__this->set_height_6((5.0f));
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UserControlThirdPerson::Start()
extern "C" IL2CPP_METHOD_ATTR void UserControlThirdPerson_Start_mDF67BE424627A659DF4048B044008D31CFEB8F79 (UserControlThirdPerson_tA3F8F919E01EC0A7D458E6F2F3346D2BD3878DAB * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UserControlThirdPerson_Start_mDF67BE424627A659DF4048B044008D31CFEB8F79_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_0 = Camera_get_main_m9256A9F84F92D7ED73F3E6C4E2694030AD8B61FA(/*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m31EF58E217E8F4BDD3E409DEF79E1AEE95874FC1(L_0, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001f;
		}
	}
	{
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_2 = Camera_get_main_m9256A9F84F92D7ED73F3E6C4E2694030AD8B61FA(/*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_3 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(L_2, /*hidden argument*/NULL);
		__this->set_m_Cam_5(L_3);
		goto IL_0029;
	}

IL_001f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_LogWarning_m37338644DC81F640CCDFEAE35A223F0E965F0568(_stringLiteral34AC72CAAD5414CF545E1146885DC571771361D9, /*hidden argument*/NULL);
	}

IL_0029:
	{
		CharacterControllerThirdPerson_t2CC0D121855408DC72A1CB71940065D5C220C886 * L_4 = Component_GetComponent_TisCharacterControllerThirdPerson_t2CC0D121855408DC72A1CB71940065D5C220C886_mBED54F2A08F701B5F685DC0A1DDCE406FE052B2D(__this, /*hidden argument*/Component_GetComponent_TisCharacterControllerThirdPerson_t2CC0D121855408DC72A1CB71940065D5C220C886_mBED54F2A08F701B5F685DC0A1DDCE406FE052B2D_RuntimeMethod_var);
		__this->set_m_Character_4(L_4);
		return;
	}
}
// System.Void UserControlThirdPerson::Update()
extern "C" IL2CPP_METHOD_ATTR void UserControlThirdPerson_Update_mB28C29866F8A6825FAF4D8D2CBE82A0F20F4E096 (UserControlThirdPerson_tA3F8F919E01EC0A7D458E6F2F3346D2BD3878DAB * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UserControlThirdPerson_Update_mB28C29866F8A6825FAF4D8D2CBE82A0F20F4E096_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_m_Jump_8();
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		bool L_1 = Input_GetButtonDown_mBCFF01DD979D2BE061A8F47082060258F2C4458B(_stringLiteral1EBA140FDD9C6860A1730C408E3064AA417CA2A3, /*hidden argument*/NULL);
		__this->set_m_Jump_8(L_1);
	}

IL_0018:
	{
		return;
	}
}
// System.Void UserControlThirdPerson::FixedUpdate()
extern "C" IL2CPP_METHOD_ATTR void UserControlThirdPerson_FixedUpdate_mD8844CAD5BD762F1E6CF9624AA695F0E1CA770A7 (UserControlThirdPerson_tA3F8F919E01EC0A7D458E6F2F3346D2BD3878DAB * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UserControlThirdPerson_FixedUpdate_mD8844CAD5BD762F1E6CF9624AA695F0E1CA770A7_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	bool V_2 = false;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		float L_0 = Input_GetAxis_mF334BDB532DF82E30273ABA0ACB5116AC8244496(_stringLiteral4F57A1CE99E68A7B05C42D0A7EA0070EAFABD31C, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = Input_GetAxis_mF334BDB532DF82E30273ABA0ACB5116AC8244496(_stringLiteral4B937CC841D82F8936CEF1EFB88708AB5B0F1EE5, /*hidden argument*/NULL);
		V_1 = L_1;
		bool L_2 = Input_GetKey_m54DC93F781FFEAB1DCDFAA3D15FA2FC01BF7667A(((int32_t)99), /*hidden argument*/NULL);
		V_2 = L_2;
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_3 = __this->get_m_Cam_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Inequality_m31EF58E217E8F4BDD3E409DEF79E1AEE95874FC1(L_3, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0088;
		}
	}
	{
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_5 = __this->get_m_Cam_5();
		NullCheck(L_5);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_6 = Transform_get_forward_m0BE1E88B86049ADA39391C3ACED2314A624BC67F(L_5, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_7;
		memset(&L_7, 0, sizeof(L_7));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_7), (1.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_8 = Vector3_Scale_m77004B226483C7644B3F4A46B950589EE8F88775(L_6, L_7, /*hidden argument*/NULL);
		V_3 = L_8;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_9 = Vector3_get_normalized_mE20796F1D2D36244FACD4D14DADB245BE579849B((Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&V_3), /*hidden argument*/NULL);
		__this->set_m_CamForward_6(L_9);
		float L_10 = V_1;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_11 = __this->get_m_CamForward_6();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_12 = Vector3_op_Multiply_mC7A8D6FD19E58DBF98E30D454F59F142F7BF8839(L_10, L_11, /*hidden argument*/NULL);
		float L_13 = V_0;
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_14 = __this->get_m_Cam_5();
		NullCheck(L_14);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_15 = Transform_get_right_mC32CE648E98D3D4F62F897A2751EE567C7C0CFB0(L_14, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_16 = Vector3_op_Multiply_mC7A8D6FD19E58DBF98E30D454F59F142F7BF8839(L_13, L_15, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_17 = Vector3_op_Addition_m929F9C17E5D11B94D50B4AFF1D730B70CB59B50E(L_12, L_16, /*hidden argument*/NULL);
		__this->set_m_Move_7(L_17);
		goto IL_00a9;
	}

IL_0088:
	{
		float L_18 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_19 = Vector3_get_forward_m3E2E192B3302130098738C308FA1EE1439449D0D(/*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_20 = Vector3_op_Multiply_mC7A8D6FD19E58DBF98E30D454F59F142F7BF8839(L_18, L_19, /*hidden argument*/NULL);
		float L_21 = V_0;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_22 = Vector3_get_right_m6DD9559CA0C75BBA42D9140021C4C2A9AAA9B3F5(/*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_23 = Vector3_op_Multiply_mC7A8D6FD19E58DBF98E30D454F59F142F7BF8839(L_21, L_22, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_24 = Vector3_op_Addition_m929F9C17E5D11B94D50B4AFF1D730B70CB59B50E(L_20, L_23, /*hidden argument*/NULL);
		__this->set_m_Move_7(L_24);
	}

IL_00a9:
	{
		bool L_25 = Input_GetKey_m54DC93F781FFEAB1DCDFAA3D15FA2FC01BF7667A(((int32_t)304), /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_00cb;
		}
	}
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_26 = __this->get_m_Move_7();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_27 = Vector3_op_Multiply_m1C5F07723615156ACF035D88A1280A9E8F35A04E(L_26, (0.5f), /*hidden argument*/NULL);
		__this->set_m_Move_7(L_27);
	}

IL_00cb:
	{
		CharacterControllerThirdPerson_t2CC0D121855408DC72A1CB71940065D5C220C886 * L_28 = __this->get_m_Character_4();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_29 = __this->get_m_Move_7();
		bool L_30 = V_2;
		bool L_31 = __this->get_m_Jump_8();
		NullCheck(L_28);
		CharacterControllerThirdPerson_Move_mB818BF9313F1CC2571D963FEBBBBFC1765E43EF4(L_28, L_29, L_30, L_31, /*hidden argument*/NULL);
		__this->set_m_Jump_8((bool)0);
		return;
	}
}
// System.Void UserControlThirdPerson::.ctor()
extern "C" IL2CPP_METHOD_ATTR void UserControlThirdPerson__ctor_mFC8F1D4DE219B3D13304F1990F0C260B9E12F872 (UserControlThirdPerson_tA3F8F919E01EC0A7D458E6F2F3346D2BD3878DAB * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
