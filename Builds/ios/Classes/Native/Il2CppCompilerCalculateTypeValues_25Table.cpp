﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// CharacterControllerThirdPerson
struct CharacterControllerThirdPerson_t2CC0D121855408DC72A1CB71940065D5C220C886;
// MarkerPrefabs[]
struct MarkerPrefabsU5BU5D_t7F40C670B36EE25921E51F171FD6A68A7A1BC336;
// PrefabsInVicinity[]
struct PrefabsInVicinityU5BU5D_tCF6E2464E3202CBD16C9DEA043361EA7DB3A49A9;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2;
// System.String
struct String_t;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// TurnTheGameOn.ArrowWaypointer.Waypoint
struct Waypoint_t7B48E7B71CF48791746A5B866BCCB7F9FABFCA0A;
// TurnTheGameOn.ArrowWaypointer.WaypointController
struct WaypointController_tD4E5882DF3A666D2E1C97E9E73061816C123AB20;
// TurnTheGameOn.ArrowWaypointer.WaypointController/WaypointComponents[]
struct WaypointComponentsU5BU5D_t995379FD5B9F2912BDD0CA0EC38E68B64A9839FD;
// UnityEngine.Animator
struct Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A;
// UnityEngine.Camera
struct Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34;
// UnityEngine.CapsuleCollider
struct CapsuleCollider_t5FD15B9E7BEEC4FFA8A2071E9FD2B8DEB3A826D1;
// UnityEngine.Events.UnityEvent
struct UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F;
// UnityEngine.Experimental.ISubsystemDescriptor
struct ISubsystemDescriptor_tDF5EB3ED639A15690D2CB9993789BB21F24D3934;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.Rigidbody
struct Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// UnityEngine.XR.ARFoundation.ARTrackedImageManager
struct ARTrackedImageManager_tC1F6C093163D1D4A5CCE83613BFCA465E2DABA9D;
// UnityEngine.XR.ARKit.ARKitSessionSubsystem/NativeApi/OnAsyncConversionCompleteDelegate
struct OnAsyncConversionCompleteDelegate_t841FB5BE19010FE3AFBEDEA37C52A468755B19FF;
// UnityEngine.XR.ARSubsystems.XRDepthSubsystem/IDepthApi
struct IDepthApi_t7CEE33C76911E53220D0242D5E38503D64736BE4;
// UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystem/IProvider
struct IProvider_t7463EA752A2B2BCE8B6D256E89A24C48FE593D91;
// UnityEngine.XR.ARSubsystems.XRPlaneSubsystem/IProvider
struct IProvider_t40982CEC3244CC0C726CED2C27336E95321F7469;
// UnityEngine.XR.ARSubsystems.XRReferenceImageLibrary
struct XRReferenceImageLibrary_t38B21DC6650EADA892125F045DCBF71EBC3F6A8F;
// UnityEngine.XR.ARSubsystems.XRReferencePointSubsystem/IProvider
struct IProvider_tA80639246D0ED291E8332BE2FBC3FE3EA13A4459;
// UnityEngine.XR.ARSubsystems.XRSessionSubsystem/IProvider
struct IProvider_tCE3A36B52F24E47DF8D9945EAB79493A822E9428;

struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;



#ifndef U3CMODULEU3E_T6CDDDF959E7E18A6744E43B613F41CDAC780256A_H
#define U3CMODULEU3E_T6CDDDF959E7E18A6744E43B613F41CDAC780256A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t6CDDDF959E7E18A6744E43B613F41CDAC780256A 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T6CDDDF959E7E18A6744E43B613F41CDAC780256A_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef MARKERPREFABS_T227A4AF77E7197AEFE5B31A217749784AA27AE6B_H
#define MARKERPREFABS_T227A4AF77E7197AEFE5B31A217749784AA27AE6B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MarkerPrefabs
struct  MarkerPrefabs_t227A4AF77E7197AEFE5B31A217749784AA27AE6B  : public RuntimeObject
{
public:
	// System.String MarkerPrefabs::prefabName
	String_t* ___prefabName_0;
	// UnityEngine.GameObject MarkerPrefabs::targetPrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___targetPrefab_1;

public:
	inline static int32_t get_offset_of_prefabName_0() { return static_cast<int32_t>(offsetof(MarkerPrefabs_t227A4AF77E7197AEFE5B31A217749784AA27AE6B, ___prefabName_0)); }
	inline String_t* get_prefabName_0() const { return ___prefabName_0; }
	inline String_t** get_address_of_prefabName_0() { return &___prefabName_0; }
	inline void set_prefabName_0(String_t* value)
	{
		___prefabName_0 = value;
		Il2CppCodeGenWriteBarrier((&___prefabName_0), value);
	}

	inline static int32_t get_offset_of_targetPrefab_1() { return static_cast<int32_t>(offsetof(MarkerPrefabs_t227A4AF77E7197AEFE5B31A217749784AA27AE6B, ___targetPrefab_1)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_targetPrefab_1() const { return ___targetPrefab_1; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_targetPrefab_1() { return &___targetPrefab_1; }
	inline void set_targetPrefab_1(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___targetPrefab_1 = value;
		Il2CppCodeGenWriteBarrier((&___targetPrefab_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MARKERPREFABS_T227A4AF77E7197AEFE5B31A217749784AA27AE6B_H
#ifndef PREFABSINVICINITY_TF56E3155685D05BFC0529D6C62A92A5C2B88A039_H
#define PREFABSINVICINITY_TF56E3155685D05BFC0529D6C62A92A5C2B88A039_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PrefabsInVicinity
struct  PrefabsInVicinity_tF56E3155685D05BFC0529D6C62A92A5C2B88A039  : public RuntimeObject
{
public:
	// System.String PrefabsInVicinity::prefabName
	String_t* ___prefabName_0;
	// UnityEngine.GameObject PrefabsInVicinity::prefabPresent
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___prefabPresent_1;

public:
	inline static int32_t get_offset_of_prefabName_0() { return static_cast<int32_t>(offsetof(PrefabsInVicinity_tF56E3155685D05BFC0529D6C62A92A5C2B88A039, ___prefabName_0)); }
	inline String_t* get_prefabName_0() const { return ___prefabName_0; }
	inline String_t** get_address_of_prefabName_0() { return &___prefabName_0; }
	inline void set_prefabName_0(String_t* value)
	{
		___prefabName_0 = value;
		Il2CppCodeGenWriteBarrier((&___prefabName_0), value);
	}

	inline static int32_t get_offset_of_prefabPresent_1() { return static_cast<int32_t>(offsetof(PrefabsInVicinity_tF56E3155685D05BFC0529D6C62A92A5C2B88A039, ___prefabPresent_1)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_prefabPresent_1() const { return ___prefabPresent_1; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_prefabPresent_1() { return &___prefabPresent_1; }
	inline void set_prefabPresent_1(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___prefabPresent_1 = value;
		Il2CppCodeGenWriteBarrier((&___prefabPresent_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREFABSINVICINITY_TF56E3155685D05BFC0529D6C62A92A5C2B88A039_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef WAYPOINTCOMPONENTS_T3DE523CAE0F5855ADF2B740DBBA68569E9034301_H
#define WAYPOINTCOMPONENTS_T3DE523CAE0F5855ADF2B740DBBA68569E9034301_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TurnTheGameOn.ArrowWaypointer.WaypointController_WaypointComponents
struct  WaypointComponents_t3DE523CAE0F5855ADF2B740DBBA68569E9034301  : public RuntimeObject
{
public:
	// System.String TurnTheGameOn.ArrowWaypointer.WaypointController_WaypointComponents::waypointName
	String_t* ___waypointName_0;
	// TurnTheGameOn.ArrowWaypointer.Waypoint TurnTheGameOn.ArrowWaypointer.WaypointController_WaypointComponents::waypoint
	Waypoint_t7B48E7B71CF48791746A5B866BCCB7F9FABFCA0A * ___waypoint_1;
	// UnityEngine.Events.UnityEvent TurnTheGameOn.ArrowWaypointer.WaypointController_WaypointComponents::waypointEvent
	UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * ___waypointEvent_2;

public:
	inline static int32_t get_offset_of_waypointName_0() { return static_cast<int32_t>(offsetof(WaypointComponents_t3DE523CAE0F5855ADF2B740DBBA68569E9034301, ___waypointName_0)); }
	inline String_t* get_waypointName_0() const { return ___waypointName_0; }
	inline String_t** get_address_of_waypointName_0() { return &___waypointName_0; }
	inline void set_waypointName_0(String_t* value)
	{
		___waypointName_0 = value;
		Il2CppCodeGenWriteBarrier((&___waypointName_0), value);
	}

	inline static int32_t get_offset_of_waypoint_1() { return static_cast<int32_t>(offsetof(WaypointComponents_t3DE523CAE0F5855ADF2B740DBBA68569E9034301, ___waypoint_1)); }
	inline Waypoint_t7B48E7B71CF48791746A5B866BCCB7F9FABFCA0A * get_waypoint_1() const { return ___waypoint_1; }
	inline Waypoint_t7B48E7B71CF48791746A5B866BCCB7F9FABFCA0A ** get_address_of_waypoint_1() { return &___waypoint_1; }
	inline void set_waypoint_1(Waypoint_t7B48E7B71CF48791746A5B866BCCB7F9FABFCA0A * value)
	{
		___waypoint_1 = value;
		Il2CppCodeGenWriteBarrier((&___waypoint_1), value);
	}

	inline static int32_t get_offset_of_waypointEvent_2() { return static_cast<int32_t>(offsetof(WaypointComponents_t3DE523CAE0F5855ADF2B740DBBA68569E9034301, ___waypointEvent_2)); }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * get_waypointEvent_2() const { return ___waypointEvent_2; }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F ** get_address_of_waypointEvent_2() { return &___waypointEvent_2; }
	inline void set_waypointEvent_2(UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * value)
	{
		___waypointEvent_2 = value;
		Il2CppCodeGenWriteBarrier((&___waypointEvent_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAYPOINTCOMPONENTS_T3DE523CAE0F5855ADF2B740DBBA68569E9034301_H
#ifndef SUBSYSTEM_T9D40133A9FEB984E2C3695B077DA4DDFC91CA181_H
#define SUBSYSTEM_T9D40133A9FEB984E2C3695B077DA4DDFC91CA181_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Subsystem
struct  Subsystem_t9D40133A9FEB984E2C3695B077DA4DDFC91CA181  : public RuntimeObject
{
public:
	// UnityEngine.Experimental.ISubsystemDescriptor UnityEngine.Experimental.Subsystem::m_subsystemDescriptor
	RuntimeObject* ___m_subsystemDescriptor_0;

public:
	inline static int32_t get_offset_of_m_subsystemDescriptor_0() { return static_cast<int32_t>(offsetof(Subsystem_t9D40133A9FEB984E2C3695B077DA4DDFC91CA181, ___m_subsystemDescriptor_0)); }
	inline RuntimeObject* get_m_subsystemDescriptor_0() const { return ___m_subsystemDescriptor_0; }
	inline RuntimeObject** get_address_of_m_subsystemDescriptor_0() { return &___m_subsystemDescriptor_0; }
	inline void set_m_subsystemDescriptor_0(RuntimeObject* value)
	{
		___m_subsystemDescriptor_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_subsystemDescriptor_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBSYSTEM_T9D40133A9FEB984E2C3695B077DA4DDFC91CA181_H
#ifndef NATIVEAPI_T164DECAC3F6004936824870871CC817A16AC9050_H
#define NATIVEAPI_T164DECAC3F6004936824870871CC817A16AC9050_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARKit.ARKitSessionSubsystem_NativeApi
struct  NativeApi_t164DECAC3F6004936824870871CC817A16AC9050  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NATIVEAPI_T164DECAC3F6004936824870871CC817A16AC9050_H
#ifndef ARWORLDMAPREQUESTSTATUSEXTENSIONS_T8FC86F2BC224C9CCC808FF3B4610B22BE5F8051F_H
#define ARWORLDMAPREQUESTSTATUSEXTENSIONS_T8FC86F2BC224C9CCC808FF3B4610B22BE5F8051F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARKit.ARWorldMapRequestStatusExtensions
struct  ARWorldMapRequestStatusExtensions_t8FC86F2BC224C9CCC808FF3B4610B22BE5F8051F  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARWORLDMAPREQUESTSTATUSEXTENSIONS_T8FC86F2BC224C9CCC808FF3B4610B22BE5F8051F_H
#ifndef ENVIRONMENTPROBEAPI_T7835EE6A5EB5249E9C0ED63CD4AC8AA1B21D76D8_H
#define ENVIRONMENTPROBEAPI_T7835EE6A5EB5249E9C0ED63CD4AC8AA1B21D76D8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARKit.EnvironmentProbeApi
struct  EnvironmentProbeApi_t7835EE6A5EB5249E9C0ED63CD4AC8AA1B21D76D8  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENVIRONMENTPROBEAPI_T7835EE6A5EB5249E9C0ED63CD4AC8AA1B21D76D8_H
#ifndef IDEPTHAPI_T7CEE33C76911E53220D0242D5E38503D64736BE4_H
#define IDEPTHAPI_T7CEE33C76911E53220D0242D5E38503D64736BE4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARSubsystems.XRDepthSubsystem_IDepthApi
struct  IDepthApi_t7CEE33C76911E53220D0242D5E38503D64736BE4  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IDEPTHAPI_T7CEE33C76911E53220D0242D5E38503D64736BE4_H
#ifndef IPROVIDER_T7463EA752A2B2BCE8B6D256E89A24C48FE593D91_H
#define IPROVIDER_T7463EA752A2B2BCE8B6D256E89A24C48FE593D91_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystem_IProvider
struct  IProvider_t7463EA752A2B2BCE8B6D256E89A24C48FE593D91  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IPROVIDER_T7463EA752A2B2BCE8B6D256E89A24C48FE593D91_H
#ifndef IPROVIDER_T40982CEC3244CC0C726CED2C27336E95321F7469_H
#define IPROVIDER_T40982CEC3244CC0C726CED2C27336E95321F7469_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARSubsystems.XRPlaneSubsystem_IProvider
struct  IProvider_t40982CEC3244CC0C726CED2C27336E95321F7469  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IPROVIDER_T40982CEC3244CC0C726CED2C27336E95321F7469_H
#ifndef IPROVIDER_TA80639246D0ED291E8332BE2FBC3FE3EA13A4459_H
#define IPROVIDER_TA80639246D0ED291E8332BE2FBC3FE3EA13A4459_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARSubsystems.XRReferencePointSubsystem_IProvider
struct  IProvider_tA80639246D0ED291E8332BE2FBC3FE3EA13A4459  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IPROVIDER_TA80639246D0ED291E8332BE2FBC3FE3EA13A4459_H
#ifndef IPROVIDER_TCE3A36B52F24E47DF8D9945EAB79493A822E9428_H
#define IPROVIDER_TCE3A36B52F24E47DF8D9945EAB79493A822E9428_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARSubsystems.XRSessionSubsystem_IProvider
struct  IProvider_tCE3A36B52F24E47DF8D9945EAB79493A822E9428  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IPROVIDER_TCE3A36B52F24E47DF8D9945EAB79493A822E9428_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef GUID_T_H
#define GUID_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Guid
struct  Guid_t 
{
public:
	// System.Int32 System.Guid::_a
	int32_t ____a_1;
	// System.Int16 System.Guid::_b
	int16_t ____b_2;
	// System.Int16 System.Guid::_c
	int16_t ____c_3;
	// System.Byte System.Guid::_d
	uint8_t ____d_4;
	// System.Byte System.Guid::_e
	uint8_t ____e_5;
	// System.Byte System.Guid::_f
	uint8_t ____f_6;
	// System.Byte System.Guid::_g
	uint8_t ____g_7;
	// System.Byte System.Guid::_h
	uint8_t ____h_8;
	// System.Byte System.Guid::_i
	uint8_t ____i_9;
	// System.Byte System.Guid::_j
	uint8_t ____j_10;
	// System.Byte System.Guid::_k
	uint8_t ____k_11;

public:
	inline static int32_t get_offset_of__a_1() { return static_cast<int32_t>(offsetof(Guid_t, ____a_1)); }
	inline int32_t get__a_1() const { return ____a_1; }
	inline int32_t* get_address_of__a_1() { return &____a_1; }
	inline void set__a_1(int32_t value)
	{
		____a_1 = value;
	}

	inline static int32_t get_offset_of__b_2() { return static_cast<int32_t>(offsetof(Guid_t, ____b_2)); }
	inline int16_t get__b_2() const { return ____b_2; }
	inline int16_t* get_address_of__b_2() { return &____b_2; }
	inline void set__b_2(int16_t value)
	{
		____b_2 = value;
	}

	inline static int32_t get_offset_of__c_3() { return static_cast<int32_t>(offsetof(Guid_t, ____c_3)); }
	inline int16_t get__c_3() const { return ____c_3; }
	inline int16_t* get_address_of__c_3() { return &____c_3; }
	inline void set__c_3(int16_t value)
	{
		____c_3 = value;
	}

	inline static int32_t get_offset_of__d_4() { return static_cast<int32_t>(offsetof(Guid_t, ____d_4)); }
	inline uint8_t get__d_4() const { return ____d_4; }
	inline uint8_t* get_address_of__d_4() { return &____d_4; }
	inline void set__d_4(uint8_t value)
	{
		____d_4 = value;
	}

	inline static int32_t get_offset_of__e_5() { return static_cast<int32_t>(offsetof(Guid_t, ____e_5)); }
	inline uint8_t get__e_5() const { return ____e_5; }
	inline uint8_t* get_address_of__e_5() { return &____e_5; }
	inline void set__e_5(uint8_t value)
	{
		____e_5 = value;
	}

	inline static int32_t get_offset_of__f_6() { return static_cast<int32_t>(offsetof(Guid_t, ____f_6)); }
	inline uint8_t get__f_6() const { return ____f_6; }
	inline uint8_t* get_address_of__f_6() { return &____f_6; }
	inline void set__f_6(uint8_t value)
	{
		____f_6 = value;
	}

	inline static int32_t get_offset_of__g_7() { return static_cast<int32_t>(offsetof(Guid_t, ____g_7)); }
	inline uint8_t get__g_7() const { return ____g_7; }
	inline uint8_t* get_address_of__g_7() { return &____g_7; }
	inline void set__g_7(uint8_t value)
	{
		____g_7 = value;
	}

	inline static int32_t get_offset_of__h_8() { return static_cast<int32_t>(offsetof(Guid_t, ____h_8)); }
	inline uint8_t get__h_8() const { return ____h_8; }
	inline uint8_t* get_address_of__h_8() { return &____h_8; }
	inline void set__h_8(uint8_t value)
	{
		____h_8 = value;
	}

	inline static int32_t get_offset_of__i_9() { return static_cast<int32_t>(offsetof(Guid_t, ____i_9)); }
	inline uint8_t get__i_9() const { return ____i_9; }
	inline uint8_t* get_address_of__i_9() { return &____i_9; }
	inline void set__i_9(uint8_t value)
	{
		____i_9 = value;
	}

	inline static int32_t get_offset_of__j_10() { return static_cast<int32_t>(offsetof(Guid_t, ____j_10)); }
	inline uint8_t get__j_10() const { return ____j_10; }
	inline uint8_t* get_address_of__j_10() { return &____j_10; }
	inline void set__j_10(uint8_t value)
	{
		____j_10 = value;
	}

	inline static int32_t get_offset_of__k_11() { return static_cast<int32_t>(offsetof(Guid_t, ____k_11)); }
	inline uint8_t get__k_11() const { return ____k_11; }
	inline uint8_t* get_address_of__k_11() { return &____k_11; }
	inline void set__k_11(uint8_t value)
	{
		____k_11 = value;
	}
};

struct Guid_t_StaticFields
{
public:
	// System.Guid System.Guid::Empty
	Guid_t  ___Empty_0;
	// System.Object System.Guid::_rngAccess
	RuntimeObject * ____rngAccess_12;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_rng
	RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * ____rng_13;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_fastRng
	RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * ____fastRng_14;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ___Empty_0)); }
	inline Guid_t  get_Empty_0() const { return ___Empty_0; }
	inline Guid_t * get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(Guid_t  value)
	{
		___Empty_0 = value;
	}

	inline static int32_t get_offset_of__rngAccess_12() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rngAccess_12)); }
	inline RuntimeObject * get__rngAccess_12() const { return ____rngAccess_12; }
	inline RuntimeObject ** get_address_of__rngAccess_12() { return &____rngAccess_12; }
	inline void set__rngAccess_12(RuntimeObject * value)
	{
		____rngAccess_12 = value;
		Il2CppCodeGenWriteBarrier((&____rngAccess_12), value);
	}

	inline static int32_t get_offset_of__rng_13() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rng_13)); }
	inline RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * get__rng_13() const { return ____rng_13; }
	inline RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 ** get_address_of__rng_13() { return &____rng_13; }
	inline void set__rng_13(RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * value)
	{
		____rng_13 = value;
		Il2CppCodeGenWriteBarrier((&____rng_13), value);
	}

	inline static int32_t get_offset_of__fastRng_14() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____fastRng_14)); }
	inline RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * get__fastRng_14() const { return ____fastRng_14; }
	inline RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 ** get_address_of__fastRng_14() { return &____fastRng_14; }
	inline void set__fastRng_14(RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * value)
	{
		____fastRng_14 = value;
		Il2CppCodeGenWriteBarrier((&____fastRng_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUID_T_H
#ifndef INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#define INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t585191389E07734F19F3156FF88FB3EF4800D102 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t585191389E07734F19F3156FF88FB3EF4800D102, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#define VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifndef SUBSYSTEM_1_TC858FB8A173FE66E4825248BD7D6CFEB152A2FF7_H
#define SUBSYSTEM_1_TC858FB8A173FE66E4825248BD7D6CFEB152A2FF7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Subsystem`1<UnityEngine.XR.ARSubsystems.XRDepthSubsystemDescriptor>
struct  Subsystem_1_tC858FB8A173FE66E4825248BD7D6CFEB152A2FF7  : public Subsystem_t9D40133A9FEB984E2C3695B077DA4DDFC91CA181
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBSYSTEM_1_TC858FB8A173FE66E4825248BD7D6CFEB152A2FF7_H
#ifndef SUBSYSTEM_1_TC1D092A41F919B743080F469FDB5690DA55488C7_H
#define SUBSYSTEM_1_TC1D092A41F919B743080F469FDB5690DA55488C7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Subsystem`1<UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystemDescriptor>
struct  Subsystem_1_tC1D092A41F919B743080F469FDB5690DA55488C7  : public Subsystem_t9D40133A9FEB984E2C3695B077DA4DDFC91CA181
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBSYSTEM_1_TC1D092A41F919B743080F469FDB5690DA55488C7_H
#ifndef SUBSYSTEM_1_TA80E63F4E3ED973F7E4926636F00EEF1AC454773_H
#define SUBSYSTEM_1_TA80E63F4E3ED973F7E4926636F00EEF1AC454773_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Subsystem`1<UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor>
struct  Subsystem_1_tA80E63F4E3ED973F7E4926636F00EEF1AC454773  : public Subsystem_t9D40133A9FEB984E2C3695B077DA4DDFC91CA181
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBSYSTEM_1_TA80E63F4E3ED973F7E4926636F00EEF1AC454773_H
#ifndef SUBSYSTEM_1_T35AA453451CA1D5D679E6EBF39EBDA236764F5E2_H
#define SUBSYSTEM_1_T35AA453451CA1D5D679E6EBF39EBDA236764F5E2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Subsystem`1<UnityEngine.XR.ARSubsystems.XRReferencePointSubsystemDescriptor>
struct  Subsystem_1_t35AA453451CA1D5D679E6EBF39EBDA236764F5E2  : public Subsystem_t9D40133A9FEB984E2C3695B077DA4DDFC91CA181
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBSYSTEM_1_T35AA453451CA1D5D679E6EBF39EBDA236764F5E2_H
#ifndef SUBSYSTEM_1_T30A0E0214407A620CECF452A54D6676F5A6574B9_H
#define SUBSYSTEM_1_T30A0E0214407A620CECF452A54D6676F5A6574B9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Subsystem`1<UnityEngine.XR.ARSubsystems.XRSessionSubsystemDescriptor>
struct  Subsystem_1_t30A0E0214407A620CECF452A54D6676F5A6574B9  : public Subsystem_t9D40133A9FEB984E2C3695B077DA4DDFC91CA181
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBSYSTEM_1_T30A0E0214407A620CECF452A54D6676F5A6574B9_H
#ifndef QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#define QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifndef VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#define VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifndef VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#define VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifndef PROVIDER_T529D13A815B277469267A1CA5C25A0FB0246AD46_H
#define PROVIDER_T529D13A815B277469267A1CA5C25A0FB0246AD46_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARKit.ARKitImageTrackingSubsystem_Provider
struct  Provider_t529D13A815B277469267A1CA5C25A0FB0246AD46  : public IProvider_t7463EA752A2B2BCE8B6D256E89A24C48FE593D91
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROVIDER_T529D13A815B277469267A1CA5C25A0FB0246AD46_H
#ifndef PROVIDER_T509F1B75CECA94E702BEAA9F7C67EA2313C58986_H
#define PROVIDER_T509F1B75CECA94E702BEAA9F7C67EA2313C58986_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARKit.ARKitReferencePointSubsystem_Provider
struct  Provider_t509F1B75CECA94E702BEAA9F7C67EA2313C58986  : public IProvider_tA80639246D0ED291E8332BE2FBC3FE3EA13A4459
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROVIDER_T509F1B75CECA94E702BEAA9F7C67EA2313C58986_H
#ifndef PROVIDER_T475F303CC6F0955D8B266D4CD5B7022F3658389B_H
#define PROVIDER_T475F303CC6F0955D8B266D4CD5B7022F3658389B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARKit.ARKitSessionSubsystem_Provider
struct  Provider_t475F303CC6F0955D8B266D4CD5B7022F3658389B  : public IProvider_tCE3A36B52F24E47DF8D9945EAB79493A822E9428
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROVIDER_T475F303CC6F0955D8B266D4CD5B7022F3658389B_H
#ifndef PROVIDER_T38A44526F6D5F1EBFDA048E10377C8F3FA82EF70_H
#define PROVIDER_T38A44526F6D5F1EBFDA048E10377C8F3FA82EF70_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARKit.ARKitXRDepthSubsystem_Provider
struct  Provider_t38A44526F6D5F1EBFDA048E10377C8F3FA82EF70  : public IDepthApi_t7CEE33C76911E53220D0242D5E38503D64736BE4
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROVIDER_T38A44526F6D5F1EBFDA048E10377C8F3FA82EF70_H
#ifndef PROVIDER_TE828C43D91B7E57F44E04A10F068C304DBAE5A6A_H
#define PROVIDER_TE828C43D91B7E57F44E04A10F068C304DBAE5A6A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARKit.ARKitXRPlaneSubsystem_Provider
struct  Provider_tE828C43D91B7E57F44E04A10F068C304DBAE5A6A  : public IProvider_t40982CEC3244CC0C726CED2C27336E95321F7469
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROVIDER_TE828C43D91B7E57F44E04A10F068C304DBAE5A6A_H
#ifndef ARWORLDMAP_T8BAE5D083A023D7DD23C29E4082B6BBD329010DE_H
#define ARWORLDMAP_T8BAE5D083A023D7DD23C29E4082B6BBD329010DE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARKit.ARWorldMap
struct  ARWorldMap_t8BAE5D083A023D7DD23C29E4082B6BBD329010DE 
{
public:
	// System.Int32 UnityEngine.XR.ARKit.ARWorldMap::<nativeHandle>k__BackingField
	int32_t ___U3CnativeHandleU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CnativeHandleU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ARWorldMap_t8BAE5D083A023D7DD23C29E4082B6BBD329010DE, ___U3CnativeHandleU3Ek__BackingField_0)); }
	inline int32_t get_U3CnativeHandleU3Ek__BackingField_0() const { return ___U3CnativeHandleU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CnativeHandleU3Ek__BackingField_0() { return &___U3CnativeHandleU3Ek__BackingField_0; }
	inline void set_U3CnativeHandleU3Ek__BackingField_0(int32_t value)
	{
		___U3CnativeHandleU3Ek__BackingField_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARWORLDMAP_T8BAE5D083A023D7DD23C29E4082B6BBD329010DE_H
#ifndef OSVERSION_T4FEA9BCD71B65A146585E5F00B36F839EEF89FEA_H
#define OSVERSION_T4FEA9BCD71B65A146585E5F00B36F839EEF89FEA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARKit.OSVersion
struct  OSVersion_t4FEA9BCD71B65A146585E5F00B36F839EEF89FEA 
{
public:
	// System.Int32 UnityEngine.XR.ARKit.OSVersion::<major>k__BackingField
	int32_t ___U3CmajorU3Ek__BackingField_0;
	// System.Int32 UnityEngine.XR.ARKit.OSVersion::<minor>k__BackingField
	int32_t ___U3CminorU3Ek__BackingField_1;
	// System.Int32 UnityEngine.XR.ARKit.OSVersion::<point>k__BackingField
	int32_t ___U3CpointU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CmajorU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(OSVersion_t4FEA9BCD71B65A146585E5F00B36F839EEF89FEA, ___U3CmajorU3Ek__BackingField_0)); }
	inline int32_t get_U3CmajorU3Ek__BackingField_0() const { return ___U3CmajorU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CmajorU3Ek__BackingField_0() { return &___U3CmajorU3Ek__BackingField_0; }
	inline void set_U3CmajorU3Ek__BackingField_0(int32_t value)
	{
		___U3CmajorU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CminorU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(OSVersion_t4FEA9BCD71B65A146585E5F00B36F839EEF89FEA, ___U3CminorU3Ek__BackingField_1)); }
	inline int32_t get_U3CminorU3Ek__BackingField_1() const { return ___U3CminorU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CminorU3Ek__BackingField_1() { return &___U3CminorU3Ek__BackingField_1; }
	inline void set_U3CminorU3Ek__BackingField_1(int32_t value)
	{
		___U3CminorU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CpointU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(OSVersion_t4FEA9BCD71B65A146585E5F00B36F839EEF89FEA, ___U3CpointU3Ek__BackingField_2)); }
	inline int32_t get_U3CpointU3Ek__BackingField_2() const { return ___U3CpointU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CpointU3Ek__BackingField_2() { return &___U3CpointU3Ek__BackingField_2; }
	inline void set_U3CpointU3Ek__BackingField_2(int32_t value)
	{
		___U3CpointU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OSVERSION_T4FEA9BCD71B65A146585E5F00B36F839EEF89FEA_H
#ifndef TRACKABLEID_TA7E19AFE62176E25E3759548887E9068E1E4AE47_H
#define TRACKABLEID_TA7E19AFE62176E25E3759548887E9068E1E4AE47_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARSubsystems.TrackableId
struct  TrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47 
{
public:
	// System.UInt64 UnityEngine.XR.ARSubsystems.TrackableId::m_SubId1
	uint64_t ___m_SubId1_1;
	// System.UInt64 UnityEngine.XR.ARSubsystems.TrackableId::m_SubId2
	uint64_t ___m_SubId2_2;

public:
	inline static int32_t get_offset_of_m_SubId1_1() { return static_cast<int32_t>(offsetof(TrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47, ___m_SubId1_1)); }
	inline uint64_t get_m_SubId1_1() const { return ___m_SubId1_1; }
	inline uint64_t* get_address_of_m_SubId1_1() { return &___m_SubId1_1; }
	inline void set_m_SubId1_1(uint64_t value)
	{
		___m_SubId1_1 = value;
	}

	inline static int32_t get_offset_of_m_SubId2_2() { return static_cast<int32_t>(offsetof(TrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47, ___m_SubId2_2)); }
	inline uint64_t get_m_SubId2_2() const { return ___m_SubId2_2; }
	inline uint64_t* get_address_of_m_SubId2_2() { return &___m_SubId2_2; }
	inline void set_m_SubId2_2(uint64_t value)
	{
		___m_SubId2_2 = value;
	}
};

struct TrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47_StaticFields
{
public:
	// UnityEngine.XR.ARSubsystems.TrackableId UnityEngine.XR.ARSubsystems.TrackableId::s_InvalidId
	TrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47  ___s_InvalidId_0;

public:
	inline static int32_t get_offset_of_s_InvalidId_0() { return static_cast<int32_t>(offsetof(TrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47_StaticFields, ___s_InvalidId_0)); }
	inline TrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47  get_s_InvalidId_0() const { return ___s_InvalidId_0; }
	inline TrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47 * get_address_of_s_InvalidId_0() { return &___s_InvalidId_0; }
	inline void set_s_InvalidId_0(TrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47  value)
	{
		___s_InvalidId_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLEID_TA7E19AFE62176E25E3759548887E9068E1E4AE47_H
#ifndef DELEGATE_T_H
#define DELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T_H
#ifndef SWITCH_T477EAF7EDB3539E13C2AB958F50647A0D1892299_H
#define SWITCH_T477EAF7EDB3539E13C2AB958F50647A0D1892299_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TurnTheGameOn.ArrowWaypointer.WaypointController_Switch
struct  Switch_t477EAF7EDB3539E13C2AB958F50647A0D1892299 
{
public:
	// System.Int32 TurnTheGameOn.ArrowWaypointer.WaypointController_Switch::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Switch_t477EAF7EDB3539E13C2AB958F50647A0D1892299, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWITCH_T477EAF7EDB3539E13C2AB958F50647A0D1892299_H
#ifndef ALLOCATOR_T62A091275262E7067EAAD565B67764FA877D58D6_H
#define ALLOCATOR_T62A091275262E7067EAAD565B67764FA877D58D6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Unity.Collections.Allocator
struct  Allocator_t62A091275262E7067EAAD565B67764FA877D58D6 
{
public:
	// System.Int32 Unity.Collections.Allocator::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Allocator_t62A091275262E7067EAAD565B67764FA877D58D6, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ALLOCATOR_T62A091275262E7067EAAD565B67764FA877D58D6_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef POSE_T2997DE3CB3863E4D78FCF42B46FC481818823F29_H
#define POSE_T2997DE3CB3863E4D78FCF42B46FC481818823F29_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Pose
struct  Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29 
{
public:
	// UnityEngine.Vector3 UnityEngine.Pose::position
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___position_0;
	// UnityEngine.Quaternion UnityEngine.Pose::rotation
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___rotation_1;

public:
	inline static int32_t get_offset_of_position_0() { return static_cast<int32_t>(offsetof(Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29, ___position_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_position_0() const { return ___position_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_position_0() { return &___position_0; }
	inline void set_position_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___position_0 = value;
	}

	inline static int32_t get_offset_of_rotation_1() { return static_cast<int32_t>(offsetof(Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29, ___rotation_1)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_rotation_1() const { return ___rotation_1; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_rotation_1() { return &___rotation_1; }
	inline void set_rotation_1(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___rotation_1 = value;
	}
};

struct Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29_StaticFields
{
public:
	// UnityEngine.Pose UnityEngine.Pose::k_Identity
	Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29  ___k_Identity_2;

public:
	inline static int32_t get_offset_of_k_Identity_2() { return static_cast<int32_t>(offsetof(Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29_StaticFields, ___k_Identity_2)); }
	inline Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29  get_k_Identity_2() const { return ___k_Identity_2; }
	inline Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29 * get_address_of_k_Identity_2() { return &___k_Identity_2; }
	inline void set_k_Identity_2(Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29  value)
	{
		___k_Identity_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSE_T2997DE3CB3863E4D78FCF42B46FC481818823F29_H
#ifndef AVAILABILITY_TFD76A2177DDFE5A5A8CDB75EB9CF784CDD9A7487_H
#define AVAILABILITY_TFD76A2177DDFE5A5A8CDB75EB9CF784CDD9A7487_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARKit.ARKitSessionSubsystem_NativeApi_Availability
struct  Availability_tFD76A2177DDFE5A5A8CDB75EB9CF784CDD9A7487 
{
public:
	// System.Int32 UnityEngine.XR.ARKit.ARKitSessionSubsystem_NativeApi_Availability::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Availability_tFD76A2177DDFE5A5A8CDB75EB9CF784CDD9A7487, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AVAILABILITY_TFD76A2177DDFE5A5A8CDB75EB9CF784CDD9A7487_H
#ifndef ARWORLDMAPREQUESTSTATUS_TF71BE763C5F9644F3D7377ACE110F7FFBBE3D5DC_H
#define ARWORLDMAPREQUESTSTATUS_TF71BE763C5F9644F3D7377ACE110F7FFBBE3D5DC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARKit.ARWorldMapRequestStatus
struct  ARWorldMapRequestStatus_tF71BE763C5F9644F3D7377ACE110F7FFBBE3D5DC 
{
public:
	// System.Int32 UnityEngine.XR.ARKit.ARWorldMapRequestStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ARWorldMapRequestStatus_tF71BE763C5F9644F3D7377ACE110F7FFBBE3D5DC, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARWORLDMAPREQUESTSTATUS_TF71BE763C5F9644F3D7377ACE110F7FFBBE3D5DC_H
#ifndef SETREFERENCELIBRARYRESULT_T60A3252A5AE8F09F503ABF5103B415DED845A779_H
#define SETREFERENCELIBRARYRESULT_T60A3252A5AE8F09F503ABF5103B415DED845A779_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARKit.SetReferenceLibraryResult
struct  SetReferenceLibraryResult_t60A3252A5AE8F09F503ABF5103B415DED845A779 
{
public:
	// System.Int32 UnityEngine.XR.ARKit.SetReferenceLibraryResult::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SetReferenceLibraryResult_t60A3252A5AE8F09F503ABF5103B415DED845A779, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETREFERENCELIBRARYRESULT_T60A3252A5AE8F09F503ABF5103B415DED845A779_H
#ifndef PLANEALIGNMENT_T8959E33A181E8A5B46387DDC30F957DD14B48783_H
#define PLANEALIGNMENT_T8959E33A181E8A5B46387DDC30F957DD14B48783_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARSubsystems.PlaneAlignment
struct  PlaneAlignment_t8959E33A181E8A5B46387DDC30F957DD14B48783 
{
public:
	// System.Int32 UnityEngine.XR.ARSubsystems.PlaneAlignment::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PlaneAlignment_t8959E33A181E8A5B46387DDC30F957DD14B48783, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLANEALIGNMENT_T8959E33A181E8A5B46387DDC30F957DD14B48783_H
#ifndef TRACKINGSTATE_T124D9E603E4E0453A85409CF7762EE8C946233F6_H
#define TRACKINGSTATE_T124D9E603E4E0453A85409CF7762EE8C946233F6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARSubsystems.TrackingState
struct  TrackingState_t124D9E603E4E0453A85409CF7762EE8C946233F6 
{
public:
	// System.Int32 UnityEngine.XR.ARSubsystems.TrackingState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TrackingState_t124D9E603E4E0453A85409CF7762EE8C946233F6, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKINGSTATE_T124D9E603E4E0453A85409CF7762EE8C946233F6_H
#ifndef TRACKINGSUBSYSTEM_2_T758E80FC2D0E4A224AA733F5ED6419083CD56389_H
#define TRACKINGSUBSYSTEM_2_T758E80FC2D0E4A224AA733F5ED6419083CD56389_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARSubsystems.TrackingSubsystem`2<UnityEngine.XR.ARSubsystems.BoundedPlane,UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor>
struct  TrackingSubsystem_2_t758E80FC2D0E4A224AA733F5ED6419083CD56389  : public Subsystem_1_tA80E63F4E3ED973F7E4926636F00EEF1AC454773
{
public:
	// System.Boolean UnityEngine.XR.ARSubsystems.TrackingSubsystem`2::m_Running
	bool ___m_Running_1;

public:
	inline static int32_t get_offset_of_m_Running_1() { return static_cast<int32_t>(offsetof(TrackingSubsystem_2_t758E80FC2D0E4A224AA733F5ED6419083CD56389, ___m_Running_1)); }
	inline bool get_m_Running_1() const { return ___m_Running_1; }
	inline bool* get_address_of_m_Running_1() { return &___m_Running_1; }
	inline void set_m_Running_1(bool value)
	{
		___m_Running_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKINGSUBSYSTEM_2_T758E80FC2D0E4A224AA733F5ED6419083CD56389_H
#ifndef TRACKINGSUBSYSTEM_2_T70B6F8BBDCEA193299D7F4FC34F9D63A1996CE0C_H
#define TRACKINGSUBSYSTEM_2_T70B6F8BBDCEA193299D7F4FC34F9D63A1996CE0C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARSubsystems.TrackingSubsystem`2<UnityEngine.XR.ARSubsystems.XRPointCloud,UnityEngine.XR.ARSubsystems.XRDepthSubsystemDescriptor>
struct  TrackingSubsystem_2_t70B6F8BBDCEA193299D7F4FC34F9D63A1996CE0C  : public Subsystem_1_tC858FB8A173FE66E4825248BD7D6CFEB152A2FF7
{
public:
	// System.Boolean UnityEngine.XR.ARSubsystems.TrackingSubsystem`2::m_Running
	bool ___m_Running_1;

public:
	inline static int32_t get_offset_of_m_Running_1() { return static_cast<int32_t>(offsetof(TrackingSubsystem_2_t70B6F8BBDCEA193299D7F4FC34F9D63A1996CE0C, ___m_Running_1)); }
	inline bool get_m_Running_1() const { return ___m_Running_1; }
	inline bool* get_address_of_m_Running_1() { return &___m_Running_1; }
	inline void set_m_Running_1(bool value)
	{
		___m_Running_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKINGSUBSYSTEM_2_T70B6F8BBDCEA193299D7F4FC34F9D63A1996CE0C_H
#ifndef TRACKINGSUBSYSTEM_2_T62AFA2295FCEFC2C3818E3B9EDB3C1AF80509899_H
#define TRACKINGSUBSYSTEM_2_T62AFA2295FCEFC2C3818E3B9EDB3C1AF80509899_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARSubsystems.TrackingSubsystem`2<UnityEngine.XR.ARSubsystems.XRReferencePoint,UnityEngine.XR.ARSubsystems.XRReferencePointSubsystemDescriptor>
struct  TrackingSubsystem_2_t62AFA2295FCEFC2C3818E3B9EDB3C1AF80509899  : public Subsystem_1_t35AA453451CA1D5D679E6EBF39EBDA236764F5E2
{
public:
	// System.Boolean UnityEngine.XR.ARSubsystems.TrackingSubsystem`2::m_Running
	bool ___m_Running_1;

public:
	inline static int32_t get_offset_of_m_Running_1() { return static_cast<int32_t>(offsetof(TrackingSubsystem_2_t62AFA2295FCEFC2C3818E3B9EDB3C1AF80509899, ___m_Running_1)); }
	inline bool get_m_Running_1() const { return ___m_Running_1; }
	inline bool* get_address_of_m_Running_1() { return &___m_Running_1; }
	inline void set_m_Running_1(bool value)
	{
		___m_Running_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKINGSUBSYSTEM_2_T62AFA2295FCEFC2C3818E3B9EDB3C1AF80509899_H
#ifndef TRACKINGSUBSYSTEM_2_TCF858475272A4880E6335A2B92788052D333E1E8_H
#define TRACKINGSUBSYSTEM_2_TCF858475272A4880E6335A2B92788052D333E1E8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARSubsystems.TrackingSubsystem`2<UnityEngine.XR.ARSubsystems.XRTrackedImage,UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystemDescriptor>
struct  TrackingSubsystem_2_tCF858475272A4880E6335A2B92788052D333E1E8  : public Subsystem_1_tC1D092A41F919B743080F469FDB5690DA55488C7
{
public:
	// System.Boolean UnityEngine.XR.ARSubsystems.TrackingSubsystem`2::m_Running
	bool ___m_Running_1;

public:
	inline static int32_t get_offset_of_m_Running_1() { return static_cast<int32_t>(offsetof(TrackingSubsystem_2_tCF858475272A4880E6335A2B92788052D333E1E8, ___m_Running_1)); }
	inline bool get_m_Running_1() const { return ___m_Running_1; }
	inline bool* get_address_of_m_Running_1() { return &___m_Running_1; }
	inline void set_m_Running_1(bool value)
	{
		___m_Running_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKINGSUBSYSTEM_2_TCF858475272A4880E6335A2B92788052D333E1E8_H
#ifndef XRSESSIONSUBSYSTEM_T9B9C16B4BDB611559FB6FA728BE399001E47EFF0_H
#define XRSESSIONSUBSYSTEM_T9B9C16B4BDB611559FB6FA728BE399001E47EFF0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARSubsystems.XRSessionSubsystem
struct  XRSessionSubsystem_t9B9C16B4BDB611559FB6FA728BE399001E47EFF0  : public Subsystem_1_t30A0E0214407A620CECF452A54D6676F5A6574B9
{
public:
	// System.Boolean UnityEngine.XR.ARSubsystems.XRSessionSubsystem::m_Running
	bool ___m_Running_1;
	// UnityEngine.XR.ARSubsystems.XRSessionSubsystem_IProvider UnityEngine.XR.ARSubsystems.XRSessionSubsystem::m_Provider
	IProvider_tCE3A36B52F24E47DF8D9945EAB79493A822E9428 * ___m_Provider_2;

public:
	inline static int32_t get_offset_of_m_Running_1() { return static_cast<int32_t>(offsetof(XRSessionSubsystem_t9B9C16B4BDB611559FB6FA728BE399001E47EFF0, ___m_Running_1)); }
	inline bool get_m_Running_1() const { return ___m_Running_1; }
	inline bool* get_address_of_m_Running_1() { return &___m_Running_1; }
	inline void set_m_Running_1(bool value)
	{
		___m_Running_1 = value;
	}

	inline static int32_t get_offset_of_m_Provider_2() { return static_cast<int32_t>(offsetof(XRSessionSubsystem_t9B9C16B4BDB611559FB6FA728BE399001E47EFF0, ___m_Provider_2)); }
	inline IProvider_tCE3A36B52F24E47DF8D9945EAB79493A822E9428 * get_m_Provider_2() const { return ___m_Provider_2; }
	inline IProvider_tCE3A36B52F24E47DF8D9945EAB79493A822E9428 ** get_address_of_m_Provider_2() { return &___m_Provider_2; }
	inline void set_m_Provider_2(IProvider_tCE3A36B52F24E47DF8D9945EAB79493A822E9428 * value)
	{
		___m_Provider_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Provider_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XRSESSIONSUBSYSTEM_T9B9C16B4BDB611559FB6FA728BE399001E47EFF0_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef NATIVEARRAY_1_T9C70B1A7759D3AEB5D78FECCCDB8DCDEB9CCA684_H
#define NATIVEARRAY_1_T9C70B1A7759D3AEB5D78FECCCDB8DCDEB9CCA684_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Unity.Collections.NativeArray`1<UnityEngine.Quaternion>
struct  NativeArray_1_t9C70B1A7759D3AEB5D78FECCCDB8DCDEB9CCA684 
{
public:
	// System.Void* Unity.Collections.NativeArray`1::m_Buffer
	void* ___m_Buffer_0;
	// System.Int32 Unity.Collections.NativeArray`1::m_Length
	int32_t ___m_Length_1;
	// Unity.Collections.Allocator Unity.Collections.NativeArray`1::m_AllocatorLabel
	int32_t ___m_AllocatorLabel_2;

public:
	inline static int32_t get_offset_of_m_Buffer_0() { return static_cast<int32_t>(offsetof(NativeArray_1_t9C70B1A7759D3AEB5D78FECCCDB8DCDEB9CCA684, ___m_Buffer_0)); }
	inline void* get_m_Buffer_0() const { return ___m_Buffer_0; }
	inline void** get_address_of_m_Buffer_0() { return &___m_Buffer_0; }
	inline void set_m_Buffer_0(void* value)
	{
		___m_Buffer_0 = value;
	}

	inline static int32_t get_offset_of_m_Length_1() { return static_cast<int32_t>(offsetof(NativeArray_1_t9C70B1A7759D3AEB5D78FECCCDB8DCDEB9CCA684, ___m_Length_1)); }
	inline int32_t get_m_Length_1() const { return ___m_Length_1; }
	inline int32_t* get_address_of_m_Length_1() { return &___m_Length_1; }
	inline void set_m_Length_1(int32_t value)
	{
		___m_Length_1 = value;
	}

	inline static int32_t get_offset_of_m_AllocatorLabel_2() { return static_cast<int32_t>(offsetof(NativeArray_1_t9C70B1A7759D3AEB5D78FECCCDB8DCDEB9CCA684, ___m_AllocatorLabel_2)); }
	inline int32_t get_m_AllocatorLabel_2() const { return ___m_AllocatorLabel_2; }
	inline int32_t* get_address_of_m_AllocatorLabel_2() { return &___m_AllocatorLabel_2; }
	inline void set_m_AllocatorLabel_2(int32_t value)
	{
		___m_AllocatorLabel_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Unity.Collections.NativeArray`1
#ifndef NativeArray_1_t350F3793D2FE9D9CD5A50725BE978ED846FE3098_marshaled_pinvoke_define
#define NativeArray_1_t350F3793D2FE9D9CD5A50725BE978ED846FE3098_marshaled_pinvoke_define
struct NativeArray_1_t350F3793D2FE9D9CD5A50725BE978ED846FE3098_marshaled_pinvoke
{
	void* ___m_Buffer_0;
	int32_t ___m_Length_1;
	int32_t ___m_AllocatorLabel_2;
};
#endif
// Native definition for COM marshalling of Unity.Collections.NativeArray`1
#ifndef NativeArray_1_t350F3793D2FE9D9CD5A50725BE978ED846FE3098_marshaled_com_define
#define NativeArray_1_t350F3793D2FE9D9CD5A50725BE978ED846FE3098_marshaled_com_define
struct NativeArray_1_t350F3793D2FE9D9CD5A50725BE978ED846FE3098_marshaled_com
{
	void* ___m_Buffer_0;
	int32_t ___m_Length_1;
	int32_t ___m_AllocatorLabel_2;
};
#endif
#endif // NATIVEARRAY_1_T9C70B1A7759D3AEB5D78FECCCDB8DCDEB9CCA684_H
#ifndef NATIVEARRAY_1_T86AEDFC03CDAC131E54ED6A68B102EBD947A3F71_H
#define NATIVEARRAY_1_T86AEDFC03CDAC131E54ED6A68B102EBD947A3F71_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Unity.Collections.NativeArray`1<UnityEngine.Vector2>
struct  NativeArray_1_t86AEDFC03CDAC131E54ED6A68B102EBD947A3F71 
{
public:
	// System.Void* Unity.Collections.NativeArray`1::m_Buffer
	void* ___m_Buffer_0;
	// System.Int32 Unity.Collections.NativeArray`1::m_Length
	int32_t ___m_Length_1;
	// Unity.Collections.Allocator Unity.Collections.NativeArray`1::m_AllocatorLabel
	int32_t ___m_AllocatorLabel_2;

public:
	inline static int32_t get_offset_of_m_Buffer_0() { return static_cast<int32_t>(offsetof(NativeArray_1_t86AEDFC03CDAC131E54ED6A68B102EBD947A3F71, ___m_Buffer_0)); }
	inline void* get_m_Buffer_0() const { return ___m_Buffer_0; }
	inline void** get_address_of_m_Buffer_0() { return &___m_Buffer_0; }
	inline void set_m_Buffer_0(void* value)
	{
		___m_Buffer_0 = value;
	}

	inline static int32_t get_offset_of_m_Length_1() { return static_cast<int32_t>(offsetof(NativeArray_1_t86AEDFC03CDAC131E54ED6A68B102EBD947A3F71, ___m_Length_1)); }
	inline int32_t get_m_Length_1() const { return ___m_Length_1; }
	inline int32_t* get_address_of_m_Length_1() { return &___m_Length_1; }
	inline void set_m_Length_1(int32_t value)
	{
		___m_Length_1 = value;
	}

	inline static int32_t get_offset_of_m_AllocatorLabel_2() { return static_cast<int32_t>(offsetof(NativeArray_1_t86AEDFC03CDAC131E54ED6A68B102EBD947A3F71, ___m_AllocatorLabel_2)); }
	inline int32_t get_m_AllocatorLabel_2() const { return ___m_AllocatorLabel_2; }
	inline int32_t* get_address_of_m_AllocatorLabel_2() { return &___m_AllocatorLabel_2; }
	inline void set_m_AllocatorLabel_2(int32_t value)
	{
		___m_AllocatorLabel_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Unity.Collections.NativeArray`1
#ifndef NativeArray_1_t350F3793D2FE9D9CD5A50725BE978ED846FE3098_marshaled_pinvoke_define
#define NativeArray_1_t350F3793D2FE9D9CD5A50725BE978ED846FE3098_marshaled_pinvoke_define
struct NativeArray_1_t350F3793D2FE9D9CD5A50725BE978ED846FE3098_marshaled_pinvoke
{
	void* ___m_Buffer_0;
	int32_t ___m_Length_1;
	int32_t ___m_AllocatorLabel_2;
};
#endif
// Native definition for COM marshalling of Unity.Collections.NativeArray`1
#ifndef NativeArray_1_t350F3793D2FE9D9CD5A50725BE978ED846FE3098_marshaled_com_define
#define NativeArray_1_t350F3793D2FE9D9CD5A50725BE978ED846FE3098_marshaled_com_define
struct NativeArray_1_t350F3793D2FE9D9CD5A50725BE978ED846FE3098_marshaled_com
{
	void* ___m_Buffer_0;
	int32_t ___m_Length_1;
	int32_t ___m_AllocatorLabel_2;
};
#endif
#endif // NATIVEARRAY_1_T86AEDFC03CDAC131E54ED6A68B102EBD947A3F71_H
#ifndef NATIVEARRAY_1_T20B0E97D613353CCC2889E9B3D97A5612374FE74_H
#define NATIVEARRAY_1_T20B0E97D613353CCC2889E9B3D97A5612374FE74_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Unity.Collections.NativeArray`1<UnityEngine.Vector3>
struct  NativeArray_1_t20B0E97D613353CCC2889E9B3D97A5612374FE74 
{
public:
	// System.Void* Unity.Collections.NativeArray`1::m_Buffer
	void* ___m_Buffer_0;
	// System.Int32 Unity.Collections.NativeArray`1::m_Length
	int32_t ___m_Length_1;
	// Unity.Collections.Allocator Unity.Collections.NativeArray`1::m_AllocatorLabel
	int32_t ___m_AllocatorLabel_2;

public:
	inline static int32_t get_offset_of_m_Buffer_0() { return static_cast<int32_t>(offsetof(NativeArray_1_t20B0E97D613353CCC2889E9B3D97A5612374FE74, ___m_Buffer_0)); }
	inline void* get_m_Buffer_0() const { return ___m_Buffer_0; }
	inline void** get_address_of_m_Buffer_0() { return &___m_Buffer_0; }
	inline void set_m_Buffer_0(void* value)
	{
		___m_Buffer_0 = value;
	}

	inline static int32_t get_offset_of_m_Length_1() { return static_cast<int32_t>(offsetof(NativeArray_1_t20B0E97D613353CCC2889E9B3D97A5612374FE74, ___m_Length_1)); }
	inline int32_t get_m_Length_1() const { return ___m_Length_1; }
	inline int32_t* get_address_of_m_Length_1() { return &___m_Length_1; }
	inline void set_m_Length_1(int32_t value)
	{
		___m_Length_1 = value;
	}

	inline static int32_t get_offset_of_m_AllocatorLabel_2() { return static_cast<int32_t>(offsetof(NativeArray_1_t20B0E97D613353CCC2889E9B3D97A5612374FE74, ___m_AllocatorLabel_2)); }
	inline int32_t get_m_AllocatorLabel_2() const { return ___m_AllocatorLabel_2; }
	inline int32_t* get_address_of_m_AllocatorLabel_2() { return &___m_AllocatorLabel_2; }
	inline void set_m_AllocatorLabel_2(int32_t value)
	{
		___m_AllocatorLabel_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Unity.Collections.NativeArray`1
#ifndef NativeArray_1_t350F3793D2FE9D9CD5A50725BE978ED846FE3098_marshaled_pinvoke_define
#define NativeArray_1_t350F3793D2FE9D9CD5A50725BE978ED846FE3098_marshaled_pinvoke_define
struct NativeArray_1_t350F3793D2FE9D9CD5A50725BE978ED846FE3098_marshaled_pinvoke
{
	void* ___m_Buffer_0;
	int32_t ___m_Length_1;
	int32_t ___m_AllocatorLabel_2;
};
#endif
// Native definition for COM marshalling of Unity.Collections.NativeArray`1
#ifndef NativeArray_1_t350F3793D2FE9D9CD5A50725BE978ED846FE3098_marshaled_com_define
#define NativeArray_1_t350F3793D2FE9D9CD5A50725BE978ED846FE3098_marshaled_com_define
struct NativeArray_1_t350F3793D2FE9D9CD5A50725BE978ED846FE3098_marshaled_com
{
	void* ___m_Buffer_0;
	int32_t ___m_Length_1;
	int32_t ___m_AllocatorLabel_2;
};
#endif
#endif // NATIVEARRAY_1_T20B0E97D613353CCC2889E9B3D97A5612374FE74_H
#ifndef NATIVEARRAY_1_T32E6297AF854BD125529357115F7C02BA25C4C96_H
#define NATIVEARRAY_1_T32E6297AF854BD125529357115F7C02BA25C4C96_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Unity.Collections.NativeArray`1<UnityEngine.Vector4>
struct  NativeArray_1_t32E6297AF854BD125529357115F7C02BA25C4C96 
{
public:
	// System.Void* Unity.Collections.NativeArray`1::m_Buffer
	void* ___m_Buffer_0;
	// System.Int32 Unity.Collections.NativeArray`1::m_Length
	int32_t ___m_Length_1;
	// Unity.Collections.Allocator Unity.Collections.NativeArray`1::m_AllocatorLabel
	int32_t ___m_AllocatorLabel_2;

public:
	inline static int32_t get_offset_of_m_Buffer_0() { return static_cast<int32_t>(offsetof(NativeArray_1_t32E6297AF854BD125529357115F7C02BA25C4C96, ___m_Buffer_0)); }
	inline void* get_m_Buffer_0() const { return ___m_Buffer_0; }
	inline void** get_address_of_m_Buffer_0() { return &___m_Buffer_0; }
	inline void set_m_Buffer_0(void* value)
	{
		___m_Buffer_0 = value;
	}

	inline static int32_t get_offset_of_m_Length_1() { return static_cast<int32_t>(offsetof(NativeArray_1_t32E6297AF854BD125529357115F7C02BA25C4C96, ___m_Length_1)); }
	inline int32_t get_m_Length_1() const { return ___m_Length_1; }
	inline int32_t* get_address_of_m_Length_1() { return &___m_Length_1; }
	inline void set_m_Length_1(int32_t value)
	{
		___m_Length_1 = value;
	}

	inline static int32_t get_offset_of_m_AllocatorLabel_2() { return static_cast<int32_t>(offsetof(NativeArray_1_t32E6297AF854BD125529357115F7C02BA25C4C96, ___m_AllocatorLabel_2)); }
	inline int32_t get_m_AllocatorLabel_2() const { return ___m_AllocatorLabel_2; }
	inline int32_t* get_address_of_m_AllocatorLabel_2() { return &___m_AllocatorLabel_2; }
	inline void set_m_AllocatorLabel_2(int32_t value)
	{
		___m_AllocatorLabel_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Unity.Collections.NativeArray`1
#ifndef NativeArray_1_t350F3793D2FE9D9CD5A50725BE978ED846FE3098_marshaled_pinvoke_define
#define NativeArray_1_t350F3793D2FE9D9CD5A50725BE978ED846FE3098_marshaled_pinvoke_define
struct NativeArray_1_t350F3793D2FE9D9CD5A50725BE978ED846FE3098_marshaled_pinvoke
{
	void* ___m_Buffer_0;
	int32_t ___m_Length_1;
	int32_t ___m_AllocatorLabel_2;
};
#endif
// Native definition for COM marshalling of Unity.Collections.NativeArray`1
#ifndef NativeArray_1_t350F3793D2FE9D9CD5A50725BE978ED846FE3098_marshaled_com_define
#define NativeArray_1_t350F3793D2FE9D9CD5A50725BE978ED846FE3098_marshaled_com_define
struct NativeArray_1_t350F3793D2FE9D9CD5A50725BE978ED846FE3098_marshaled_com
{
	void* ___m_Buffer_0;
	int32_t ___m_Length_1;
	int32_t ___m_AllocatorLabel_2;
};
#endif
#endif // NATIVEARRAY_1_T32E6297AF854BD125529357115F7C02BA25C4C96_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef ARKITSESSIONSUBSYSTEM_TD9267F6454E65E2C795C8CAF65C9CCB0BBE1636A_H
#define ARKITSESSIONSUBSYSTEM_TD9267F6454E65E2C795C8CAF65C9CCB0BBE1636A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARKit.ARKitSessionSubsystem
struct  ARKitSessionSubsystem_tD9267F6454E65E2C795C8CAF65C9CCB0BBE1636A  : public XRSessionSubsystem_t9B9C16B4BDB611559FB6FA728BE399001E47EFF0
{
public:

public:
};

struct ARKitSessionSubsystem_tD9267F6454E65E2C795C8CAF65C9CCB0BBE1636A_StaticFields
{
public:
	// UnityEngine.XR.ARKit.ARKitSessionSubsystem_NativeApi_OnAsyncConversionCompleteDelegate UnityEngine.XR.ARKit.ARKitSessionSubsystem::s_OnAsyncWorldMapCompleted
	OnAsyncConversionCompleteDelegate_t841FB5BE19010FE3AFBEDEA37C52A468755B19FF * ___s_OnAsyncWorldMapCompleted_3;

public:
	inline static int32_t get_offset_of_s_OnAsyncWorldMapCompleted_3() { return static_cast<int32_t>(offsetof(ARKitSessionSubsystem_tD9267F6454E65E2C795C8CAF65C9CCB0BBE1636A_StaticFields, ___s_OnAsyncWorldMapCompleted_3)); }
	inline OnAsyncConversionCompleteDelegate_t841FB5BE19010FE3AFBEDEA37C52A468755B19FF * get_s_OnAsyncWorldMapCompleted_3() const { return ___s_OnAsyncWorldMapCompleted_3; }
	inline OnAsyncConversionCompleteDelegate_t841FB5BE19010FE3AFBEDEA37C52A468755B19FF ** get_address_of_s_OnAsyncWorldMapCompleted_3() { return &___s_OnAsyncWorldMapCompleted_3; }
	inline void set_s_OnAsyncWorldMapCompleted_3(OnAsyncConversionCompleteDelegate_t841FB5BE19010FE3AFBEDEA37C52A468755B19FF * value)
	{
		___s_OnAsyncWorldMapCompleted_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_OnAsyncWorldMapCompleted_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARKITSESSIONSUBSYSTEM_TD9267F6454E65E2C795C8CAF65C9CCB0BBE1636A_H
#ifndef BOUNDEDPLANE_T5FBCC289E172852D4A9C51F6581B3C7EEE17A227_H
#define BOUNDEDPLANE_T5FBCC289E172852D4A9C51F6581B3C7EEE17A227_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARSubsystems.BoundedPlane
struct  BoundedPlane_t5FBCC289E172852D4A9C51F6581B3C7EEE17A227 
{
public:
	// UnityEngine.XR.ARSubsystems.TrackableId UnityEngine.XR.ARSubsystems.BoundedPlane::m_TrackableId
	TrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47  ___m_TrackableId_0;
	// UnityEngine.XR.ARSubsystems.TrackableId UnityEngine.XR.ARSubsystems.BoundedPlane::m_SubsumedById
	TrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47  ___m_SubsumedById_1;
	// UnityEngine.Vector2 UnityEngine.XR.ARSubsystems.BoundedPlane::m_Center
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_Center_2;
	// UnityEngine.Pose UnityEngine.XR.ARSubsystems.BoundedPlane::m_Pose
	Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29  ___m_Pose_3;
	// UnityEngine.Vector2 UnityEngine.XR.ARSubsystems.BoundedPlane::m_Size
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_Size_4;
	// UnityEngine.XR.ARSubsystems.PlaneAlignment UnityEngine.XR.ARSubsystems.BoundedPlane::m_Alignment
	int32_t ___m_Alignment_5;
	// UnityEngine.XR.ARSubsystems.TrackingState UnityEngine.XR.ARSubsystems.BoundedPlane::m_TrackingState
	int32_t ___m_TrackingState_6;
	// System.IntPtr UnityEngine.XR.ARSubsystems.BoundedPlane::m_NativePtr
	intptr_t ___m_NativePtr_7;

public:
	inline static int32_t get_offset_of_m_TrackableId_0() { return static_cast<int32_t>(offsetof(BoundedPlane_t5FBCC289E172852D4A9C51F6581B3C7EEE17A227, ___m_TrackableId_0)); }
	inline TrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47  get_m_TrackableId_0() const { return ___m_TrackableId_0; }
	inline TrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47 * get_address_of_m_TrackableId_0() { return &___m_TrackableId_0; }
	inline void set_m_TrackableId_0(TrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47  value)
	{
		___m_TrackableId_0 = value;
	}

	inline static int32_t get_offset_of_m_SubsumedById_1() { return static_cast<int32_t>(offsetof(BoundedPlane_t5FBCC289E172852D4A9C51F6581B3C7EEE17A227, ___m_SubsumedById_1)); }
	inline TrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47  get_m_SubsumedById_1() const { return ___m_SubsumedById_1; }
	inline TrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47 * get_address_of_m_SubsumedById_1() { return &___m_SubsumedById_1; }
	inline void set_m_SubsumedById_1(TrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47  value)
	{
		___m_SubsumedById_1 = value;
	}

	inline static int32_t get_offset_of_m_Center_2() { return static_cast<int32_t>(offsetof(BoundedPlane_t5FBCC289E172852D4A9C51F6581B3C7EEE17A227, ___m_Center_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_Center_2() const { return ___m_Center_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_Center_2() { return &___m_Center_2; }
	inline void set_m_Center_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_Center_2 = value;
	}

	inline static int32_t get_offset_of_m_Pose_3() { return static_cast<int32_t>(offsetof(BoundedPlane_t5FBCC289E172852D4A9C51F6581B3C7EEE17A227, ___m_Pose_3)); }
	inline Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29  get_m_Pose_3() const { return ___m_Pose_3; }
	inline Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29 * get_address_of_m_Pose_3() { return &___m_Pose_3; }
	inline void set_m_Pose_3(Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29  value)
	{
		___m_Pose_3 = value;
	}

	inline static int32_t get_offset_of_m_Size_4() { return static_cast<int32_t>(offsetof(BoundedPlane_t5FBCC289E172852D4A9C51F6581B3C7EEE17A227, ___m_Size_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_Size_4() const { return ___m_Size_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_Size_4() { return &___m_Size_4; }
	inline void set_m_Size_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_Size_4 = value;
	}

	inline static int32_t get_offset_of_m_Alignment_5() { return static_cast<int32_t>(offsetof(BoundedPlane_t5FBCC289E172852D4A9C51F6581B3C7EEE17A227, ___m_Alignment_5)); }
	inline int32_t get_m_Alignment_5() const { return ___m_Alignment_5; }
	inline int32_t* get_address_of_m_Alignment_5() { return &___m_Alignment_5; }
	inline void set_m_Alignment_5(int32_t value)
	{
		___m_Alignment_5 = value;
	}

	inline static int32_t get_offset_of_m_TrackingState_6() { return static_cast<int32_t>(offsetof(BoundedPlane_t5FBCC289E172852D4A9C51F6581B3C7EEE17A227, ___m_TrackingState_6)); }
	inline int32_t get_m_TrackingState_6() const { return ___m_TrackingState_6; }
	inline int32_t* get_address_of_m_TrackingState_6() { return &___m_TrackingState_6; }
	inline void set_m_TrackingState_6(int32_t value)
	{
		___m_TrackingState_6 = value;
	}

	inline static int32_t get_offset_of_m_NativePtr_7() { return static_cast<int32_t>(offsetof(BoundedPlane_t5FBCC289E172852D4A9C51F6581B3C7EEE17A227, ___m_NativePtr_7)); }
	inline intptr_t get_m_NativePtr_7() const { return ___m_NativePtr_7; }
	inline intptr_t* get_address_of_m_NativePtr_7() { return &___m_NativePtr_7; }
	inline void set_m_NativePtr_7(intptr_t value)
	{
		___m_NativePtr_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNDEDPLANE_T5FBCC289E172852D4A9C51F6581B3C7EEE17A227_H
#ifndef XRIMAGETRACKINGSUBSYSTEM_T1DC56F01ABB0CC32D9AC26AA77E097AD9B0ECEB5_H
#define XRIMAGETRACKINGSUBSYSTEM_T1DC56F01ABB0CC32D9AC26AA77E097AD9B0ECEB5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystem
struct  XRImageTrackingSubsystem_t1DC56F01ABB0CC32D9AC26AA77E097AD9B0ECEB5  : public TrackingSubsystem_2_tCF858475272A4880E6335A2B92788052D333E1E8
{
public:
	// UnityEngine.XR.ARSubsystems.XRReferenceImageLibrary UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystem::m_ImageLibrary
	XRReferenceImageLibrary_t38B21DC6650EADA892125F045DCBF71EBC3F6A8F * ___m_ImageLibrary_2;
	// UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystem_IProvider UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystem::m_Provider
	IProvider_t7463EA752A2B2BCE8B6D256E89A24C48FE593D91 * ___m_Provider_3;

public:
	inline static int32_t get_offset_of_m_ImageLibrary_2() { return static_cast<int32_t>(offsetof(XRImageTrackingSubsystem_t1DC56F01ABB0CC32D9AC26AA77E097AD9B0ECEB5, ___m_ImageLibrary_2)); }
	inline XRReferenceImageLibrary_t38B21DC6650EADA892125F045DCBF71EBC3F6A8F * get_m_ImageLibrary_2() const { return ___m_ImageLibrary_2; }
	inline XRReferenceImageLibrary_t38B21DC6650EADA892125F045DCBF71EBC3F6A8F ** get_address_of_m_ImageLibrary_2() { return &___m_ImageLibrary_2; }
	inline void set_m_ImageLibrary_2(XRReferenceImageLibrary_t38B21DC6650EADA892125F045DCBF71EBC3F6A8F * value)
	{
		___m_ImageLibrary_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_ImageLibrary_2), value);
	}

	inline static int32_t get_offset_of_m_Provider_3() { return static_cast<int32_t>(offsetof(XRImageTrackingSubsystem_t1DC56F01ABB0CC32D9AC26AA77E097AD9B0ECEB5, ___m_Provider_3)); }
	inline IProvider_t7463EA752A2B2BCE8B6D256E89A24C48FE593D91 * get_m_Provider_3() const { return ___m_Provider_3; }
	inline IProvider_t7463EA752A2B2BCE8B6D256E89A24C48FE593D91 ** get_address_of_m_Provider_3() { return &___m_Provider_3; }
	inline void set_m_Provider_3(IProvider_t7463EA752A2B2BCE8B6D256E89A24C48FE593D91 * value)
	{
		___m_Provider_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Provider_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XRIMAGETRACKINGSUBSYSTEM_T1DC56F01ABB0CC32D9AC26AA77E097AD9B0ECEB5_H
#ifndef XRPOINTCLOUD_TA4A412DE503530E1B2953919F1463B9B48504ED0_H
#define XRPOINTCLOUD_TA4A412DE503530E1B2953919F1463B9B48504ED0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARSubsystems.XRPointCloud
struct  XRPointCloud_tA4A412DE503530E1B2953919F1463B9B48504ED0 
{
public:
	// UnityEngine.XR.ARSubsystems.TrackableId UnityEngine.XR.ARSubsystems.XRPointCloud::m_TrackableId
	TrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47  ___m_TrackableId_0;
	// UnityEngine.Pose UnityEngine.XR.ARSubsystems.XRPointCloud::m_Pose
	Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29  ___m_Pose_1;
	// UnityEngine.XR.ARSubsystems.TrackingState UnityEngine.XR.ARSubsystems.XRPointCloud::m_TrackingState
	int32_t ___m_TrackingState_2;
	// System.IntPtr UnityEngine.XR.ARSubsystems.XRPointCloud::m_NativePtr
	intptr_t ___m_NativePtr_3;

public:
	inline static int32_t get_offset_of_m_TrackableId_0() { return static_cast<int32_t>(offsetof(XRPointCloud_tA4A412DE503530E1B2953919F1463B9B48504ED0, ___m_TrackableId_0)); }
	inline TrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47  get_m_TrackableId_0() const { return ___m_TrackableId_0; }
	inline TrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47 * get_address_of_m_TrackableId_0() { return &___m_TrackableId_0; }
	inline void set_m_TrackableId_0(TrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47  value)
	{
		___m_TrackableId_0 = value;
	}

	inline static int32_t get_offset_of_m_Pose_1() { return static_cast<int32_t>(offsetof(XRPointCloud_tA4A412DE503530E1B2953919F1463B9B48504ED0, ___m_Pose_1)); }
	inline Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29  get_m_Pose_1() const { return ___m_Pose_1; }
	inline Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29 * get_address_of_m_Pose_1() { return &___m_Pose_1; }
	inline void set_m_Pose_1(Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29  value)
	{
		___m_Pose_1 = value;
	}

	inline static int32_t get_offset_of_m_TrackingState_2() { return static_cast<int32_t>(offsetof(XRPointCloud_tA4A412DE503530E1B2953919F1463B9B48504ED0, ___m_TrackingState_2)); }
	inline int32_t get_m_TrackingState_2() const { return ___m_TrackingState_2; }
	inline int32_t* get_address_of_m_TrackingState_2() { return &___m_TrackingState_2; }
	inline void set_m_TrackingState_2(int32_t value)
	{
		___m_TrackingState_2 = value;
	}

	inline static int32_t get_offset_of_m_NativePtr_3() { return static_cast<int32_t>(offsetof(XRPointCloud_tA4A412DE503530E1B2953919F1463B9B48504ED0, ___m_NativePtr_3)); }
	inline intptr_t get_m_NativePtr_3() const { return ___m_NativePtr_3; }
	inline intptr_t* get_address_of_m_NativePtr_3() { return &___m_NativePtr_3; }
	inline void set_m_NativePtr_3(intptr_t value)
	{
		___m_NativePtr_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XRPOINTCLOUD_TA4A412DE503530E1B2953919F1463B9B48504ED0_H
#ifndef XRREFERENCEPOINT_TA8592C08A27EC91D9B1FB3B083C95C5D372FF1F9_H
#define XRREFERENCEPOINT_TA8592C08A27EC91D9B1FB3B083C95C5D372FF1F9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARSubsystems.XRReferencePoint
struct  XRReferencePoint_tA8592C08A27EC91D9B1FB3B083C95C5D372FF1F9 
{
public:
	// UnityEngine.XR.ARSubsystems.TrackableId UnityEngine.XR.ARSubsystems.XRReferencePoint::m_Id
	TrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47  ___m_Id_0;
	// UnityEngine.Pose UnityEngine.XR.ARSubsystems.XRReferencePoint::m_Pose
	Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29  ___m_Pose_1;
	// UnityEngine.XR.ARSubsystems.TrackingState UnityEngine.XR.ARSubsystems.XRReferencePoint::m_TrackingState
	int32_t ___m_TrackingState_2;
	// System.IntPtr UnityEngine.XR.ARSubsystems.XRReferencePoint::m_NativePtr
	intptr_t ___m_NativePtr_3;
	// System.Guid UnityEngine.XR.ARSubsystems.XRReferencePoint::m_SessionId
	Guid_t  ___m_SessionId_4;

public:
	inline static int32_t get_offset_of_m_Id_0() { return static_cast<int32_t>(offsetof(XRReferencePoint_tA8592C08A27EC91D9B1FB3B083C95C5D372FF1F9, ___m_Id_0)); }
	inline TrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47  get_m_Id_0() const { return ___m_Id_0; }
	inline TrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47 * get_address_of_m_Id_0() { return &___m_Id_0; }
	inline void set_m_Id_0(TrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47  value)
	{
		___m_Id_0 = value;
	}

	inline static int32_t get_offset_of_m_Pose_1() { return static_cast<int32_t>(offsetof(XRReferencePoint_tA8592C08A27EC91D9B1FB3B083C95C5D372FF1F9, ___m_Pose_1)); }
	inline Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29  get_m_Pose_1() const { return ___m_Pose_1; }
	inline Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29 * get_address_of_m_Pose_1() { return &___m_Pose_1; }
	inline void set_m_Pose_1(Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29  value)
	{
		___m_Pose_1 = value;
	}

	inline static int32_t get_offset_of_m_TrackingState_2() { return static_cast<int32_t>(offsetof(XRReferencePoint_tA8592C08A27EC91D9B1FB3B083C95C5D372FF1F9, ___m_TrackingState_2)); }
	inline int32_t get_m_TrackingState_2() const { return ___m_TrackingState_2; }
	inline int32_t* get_address_of_m_TrackingState_2() { return &___m_TrackingState_2; }
	inline void set_m_TrackingState_2(int32_t value)
	{
		___m_TrackingState_2 = value;
	}

	inline static int32_t get_offset_of_m_NativePtr_3() { return static_cast<int32_t>(offsetof(XRReferencePoint_tA8592C08A27EC91D9B1FB3B083C95C5D372FF1F9, ___m_NativePtr_3)); }
	inline intptr_t get_m_NativePtr_3() const { return ___m_NativePtr_3; }
	inline intptr_t* get_address_of_m_NativePtr_3() { return &___m_NativePtr_3; }
	inline void set_m_NativePtr_3(intptr_t value)
	{
		___m_NativePtr_3 = value;
	}

	inline static int32_t get_offset_of_m_SessionId_4() { return static_cast<int32_t>(offsetof(XRReferencePoint_tA8592C08A27EC91D9B1FB3B083C95C5D372FF1F9, ___m_SessionId_4)); }
	inline Guid_t  get_m_SessionId_4() const { return ___m_SessionId_4; }
	inline Guid_t * get_address_of_m_SessionId_4() { return &___m_SessionId_4; }
	inline void set_m_SessionId_4(Guid_t  value)
	{
		___m_SessionId_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XRREFERENCEPOINT_TA8592C08A27EC91D9B1FB3B083C95C5D372FF1F9_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef ARKITIMAGETRACKINGSUBSYSTEM_TE365CBF82726F48ED703BACA30C2B8FD797D6960_H
#define ARKITIMAGETRACKINGSUBSYSTEM_TE365CBF82726F48ED703BACA30C2B8FD797D6960_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARKit.ARKitImageTrackingSubsystem
struct  ARKitImageTrackingSubsystem_tE365CBF82726F48ED703BACA30C2B8FD797D6960  : public XRImageTrackingSubsystem_t1DC56F01ABB0CC32D9AC26AA77E097AD9B0ECEB5
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARKITIMAGETRACKINGSUBSYSTEM_TE365CBF82726F48ED703BACA30C2B8FD797D6960_H
#ifndef ONASYNCCONVERSIONCOMPLETEDELEGATE_T841FB5BE19010FE3AFBEDEA37C52A468755B19FF_H
#define ONASYNCCONVERSIONCOMPLETEDELEGATE_T841FB5BE19010FE3AFBEDEA37C52A468755B19FF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARKit.ARKitSessionSubsystem_NativeApi_OnAsyncConversionCompleteDelegate
struct  OnAsyncConversionCompleteDelegate_t841FB5BE19010FE3AFBEDEA37C52A468755B19FF  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONASYNCCONVERSIONCOMPLETEDELEGATE_T841FB5BE19010FE3AFBEDEA37C52A468755B19FF_H
#ifndef TRANSFORMPOSITIONSJOB_T4BCA4844CF5EFB6C0A19B9E5059390B2E499E283_H
#define TRANSFORMPOSITIONSJOB_T4BCA4844CF5EFB6C0A19B9E5059390B2E499E283_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARKit.ARKitXRDepthSubsystem_TransformPositionsJob
struct  TransformPositionsJob_t4BCA4844CF5EFB6C0A19B9E5059390B2E499E283 
{
public:
	// Unity.Collections.NativeArray`1<UnityEngine.Quaternion> UnityEngine.XR.ARKit.ARKitXRDepthSubsystem_TransformPositionsJob::positionsIn
	NativeArray_1_t9C70B1A7759D3AEB5D78FECCCDB8DCDEB9CCA684  ___positionsIn_0;
	// Unity.Collections.NativeArray`1<UnityEngine.Vector3> UnityEngine.XR.ARKit.ARKitXRDepthSubsystem_TransformPositionsJob::positionsOut
	NativeArray_1_t20B0E97D613353CCC2889E9B3D97A5612374FE74  ___positionsOut_1;

public:
	inline static int32_t get_offset_of_positionsIn_0() { return static_cast<int32_t>(offsetof(TransformPositionsJob_t4BCA4844CF5EFB6C0A19B9E5059390B2E499E283, ___positionsIn_0)); }
	inline NativeArray_1_t9C70B1A7759D3AEB5D78FECCCDB8DCDEB9CCA684  get_positionsIn_0() const { return ___positionsIn_0; }
	inline NativeArray_1_t9C70B1A7759D3AEB5D78FECCCDB8DCDEB9CCA684 * get_address_of_positionsIn_0() { return &___positionsIn_0; }
	inline void set_positionsIn_0(NativeArray_1_t9C70B1A7759D3AEB5D78FECCCDB8DCDEB9CCA684  value)
	{
		___positionsIn_0 = value;
	}

	inline static int32_t get_offset_of_positionsOut_1() { return static_cast<int32_t>(offsetof(TransformPositionsJob_t4BCA4844CF5EFB6C0A19B9E5059390B2E499E283, ___positionsOut_1)); }
	inline NativeArray_1_t20B0E97D613353CCC2889E9B3D97A5612374FE74  get_positionsOut_1() const { return ___positionsOut_1; }
	inline NativeArray_1_t20B0E97D613353CCC2889E9B3D97A5612374FE74 * get_address_of_positionsOut_1() { return &___positionsOut_1; }
	inline void set_positionsOut_1(NativeArray_1_t20B0E97D613353CCC2889E9B3D97A5612374FE74  value)
	{
		___positionsOut_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.XR.ARKit.ARKitXRDepthSubsystem/TransformPositionsJob
struct TransformPositionsJob_t4BCA4844CF5EFB6C0A19B9E5059390B2E499E283_marshaled_pinvoke
{
	NativeArray_1_t9C70B1A7759D3AEB5D78FECCCDB8DCDEB9CCA684  ___positionsIn_0;
	NativeArray_1_t20B0E97D613353CCC2889E9B3D97A5612374FE74  ___positionsOut_1;
};
// Native definition for COM marshalling of UnityEngine.XR.ARKit.ARKitXRDepthSubsystem/TransformPositionsJob
struct TransformPositionsJob_t4BCA4844CF5EFB6C0A19B9E5059390B2E499E283_marshaled_com
{
	NativeArray_1_t9C70B1A7759D3AEB5D78FECCCDB8DCDEB9CCA684  ___positionsIn_0;
	NativeArray_1_t20B0E97D613353CCC2889E9B3D97A5612374FE74  ___positionsOut_1;
};
#endif // TRANSFORMPOSITIONSJOB_T4BCA4844CF5EFB6C0A19B9E5059390B2E499E283_H
#ifndef FLIPBOUNDARYWINDINGJOB_TAB484E8E0F98EEA68CD01FECAEBD5BFA7C75B312_H
#define FLIPBOUNDARYWINDINGJOB_TAB484E8E0F98EEA68CD01FECAEBD5BFA7C75B312_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARKit.ARKitXRPlaneSubsystem_Provider_FlipBoundaryWindingJob
struct  FlipBoundaryWindingJob_tAB484E8E0F98EEA68CD01FECAEBD5BFA7C75B312 
{
public:
	// Unity.Collections.NativeArray`1<UnityEngine.Vector2> UnityEngine.XR.ARKit.ARKitXRPlaneSubsystem_Provider_FlipBoundaryWindingJob::positions
	NativeArray_1_t86AEDFC03CDAC131E54ED6A68B102EBD947A3F71  ___positions_0;

public:
	inline static int32_t get_offset_of_positions_0() { return static_cast<int32_t>(offsetof(FlipBoundaryWindingJob_tAB484E8E0F98EEA68CD01FECAEBD5BFA7C75B312, ___positions_0)); }
	inline NativeArray_1_t86AEDFC03CDAC131E54ED6A68B102EBD947A3F71  get_positions_0() const { return ___positions_0; }
	inline NativeArray_1_t86AEDFC03CDAC131E54ED6A68B102EBD947A3F71 * get_address_of_positions_0() { return &___positions_0; }
	inline void set_positions_0(NativeArray_1_t86AEDFC03CDAC131E54ED6A68B102EBD947A3F71  value)
	{
		___positions_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.XR.ARKit.ARKitXRPlaneSubsystem/Provider/FlipBoundaryWindingJob
struct FlipBoundaryWindingJob_tAB484E8E0F98EEA68CD01FECAEBD5BFA7C75B312_marshaled_pinvoke
{
	NativeArray_1_t86AEDFC03CDAC131E54ED6A68B102EBD947A3F71  ___positions_0;
};
// Native definition for COM marshalling of UnityEngine.XR.ARKit.ARKitXRPlaneSubsystem/Provider/FlipBoundaryWindingJob
struct FlipBoundaryWindingJob_tAB484E8E0F98EEA68CD01FECAEBD5BFA7C75B312_marshaled_com
{
	NativeArray_1_t86AEDFC03CDAC131E54ED6A68B102EBD947A3F71  ___positions_0;
};
#endif // FLIPBOUNDARYWINDINGJOB_TAB484E8E0F98EEA68CD01FECAEBD5BFA7C75B312_H
#ifndef TRANSFORMBOUNDARYPOSITIONSJOB_T42DE86BF3E6AB9CAC98E5C1772288A28226EC59A_H
#define TRANSFORMBOUNDARYPOSITIONSJOB_T42DE86BF3E6AB9CAC98E5C1772288A28226EC59A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARKit.ARKitXRPlaneSubsystem_Provider_TransformBoundaryPositionsJob
struct  TransformBoundaryPositionsJob_t42DE86BF3E6AB9CAC98E5C1772288A28226EC59A 
{
public:
	// Unity.Collections.NativeArray`1<UnityEngine.Vector4> UnityEngine.XR.ARKit.ARKitXRPlaneSubsystem_Provider_TransformBoundaryPositionsJob::positionsIn
	NativeArray_1_t32E6297AF854BD125529357115F7C02BA25C4C96  ___positionsIn_0;
	// Unity.Collections.NativeArray`1<UnityEngine.Vector2> UnityEngine.XR.ARKit.ARKitXRPlaneSubsystem_Provider_TransformBoundaryPositionsJob::positionsOut
	NativeArray_1_t86AEDFC03CDAC131E54ED6A68B102EBD947A3F71  ___positionsOut_1;

public:
	inline static int32_t get_offset_of_positionsIn_0() { return static_cast<int32_t>(offsetof(TransformBoundaryPositionsJob_t42DE86BF3E6AB9CAC98E5C1772288A28226EC59A, ___positionsIn_0)); }
	inline NativeArray_1_t32E6297AF854BD125529357115F7C02BA25C4C96  get_positionsIn_0() const { return ___positionsIn_0; }
	inline NativeArray_1_t32E6297AF854BD125529357115F7C02BA25C4C96 * get_address_of_positionsIn_0() { return &___positionsIn_0; }
	inline void set_positionsIn_0(NativeArray_1_t32E6297AF854BD125529357115F7C02BA25C4C96  value)
	{
		___positionsIn_0 = value;
	}

	inline static int32_t get_offset_of_positionsOut_1() { return static_cast<int32_t>(offsetof(TransformBoundaryPositionsJob_t42DE86BF3E6AB9CAC98E5C1772288A28226EC59A, ___positionsOut_1)); }
	inline NativeArray_1_t86AEDFC03CDAC131E54ED6A68B102EBD947A3F71  get_positionsOut_1() const { return ___positionsOut_1; }
	inline NativeArray_1_t86AEDFC03CDAC131E54ED6A68B102EBD947A3F71 * get_address_of_positionsOut_1() { return &___positionsOut_1; }
	inline void set_positionsOut_1(NativeArray_1_t86AEDFC03CDAC131E54ED6A68B102EBD947A3F71  value)
	{
		___positionsOut_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.XR.ARKit.ARKitXRPlaneSubsystem/Provider/TransformBoundaryPositionsJob
struct TransformBoundaryPositionsJob_t42DE86BF3E6AB9CAC98E5C1772288A28226EC59A_marshaled_pinvoke
{
	NativeArray_1_t32E6297AF854BD125529357115F7C02BA25C4C96  ___positionsIn_0;
	NativeArray_1_t86AEDFC03CDAC131E54ED6A68B102EBD947A3F71  ___positionsOut_1;
};
// Native definition for COM marshalling of UnityEngine.XR.ARKit.ARKitXRPlaneSubsystem/Provider/TransformBoundaryPositionsJob
struct TransformBoundaryPositionsJob_t42DE86BF3E6AB9CAC98E5C1772288A28226EC59A_marshaled_com
{
	NativeArray_1_t32E6297AF854BD125529357115F7C02BA25C4C96  ___positionsIn_0;
	NativeArray_1_t86AEDFC03CDAC131E54ED6A68B102EBD947A3F71  ___positionsOut_1;
};
#endif // TRANSFORMBOUNDARYPOSITIONSJOB_T42DE86BF3E6AB9CAC98E5C1772288A28226EC59A_H
#ifndef XRDEPTHSUBSYSTEM_TA1188AEE9FA009EAF2BDA064BEA1482FF4CD8AD7_H
#define XRDEPTHSUBSYSTEM_TA1188AEE9FA009EAF2BDA064BEA1482FF4CD8AD7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARSubsystems.XRDepthSubsystem
struct  XRDepthSubsystem_tA1188AEE9FA009EAF2BDA064BEA1482FF4CD8AD7  : public TrackingSubsystem_2_t70B6F8BBDCEA193299D7F4FC34F9D63A1996CE0C
{
public:
	// UnityEngine.XR.ARSubsystems.XRDepthSubsystem_IDepthApi UnityEngine.XR.ARSubsystems.XRDepthSubsystem::m_Interface
	IDepthApi_t7CEE33C76911E53220D0242D5E38503D64736BE4 * ___m_Interface_2;
	// UnityEngine.XR.ARSubsystems.XRPointCloud UnityEngine.XR.ARSubsystems.XRDepthSubsystem::m_DefaultPointCloud
	XRPointCloud_tA4A412DE503530E1B2953919F1463B9B48504ED0  ___m_DefaultPointCloud_3;

public:
	inline static int32_t get_offset_of_m_Interface_2() { return static_cast<int32_t>(offsetof(XRDepthSubsystem_tA1188AEE9FA009EAF2BDA064BEA1482FF4CD8AD7, ___m_Interface_2)); }
	inline IDepthApi_t7CEE33C76911E53220D0242D5E38503D64736BE4 * get_m_Interface_2() const { return ___m_Interface_2; }
	inline IDepthApi_t7CEE33C76911E53220D0242D5E38503D64736BE4 ** get_address_of_m_Interface_2() { return &___m_Interface_2; }
	inline void set_m_Interface_2(IDepthApi_t7CEE33C76911E53220D0242D5E38503D64736BE4 * value)
	{
		___m_Interface_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Interface_2), value);
	}

	inline static int32_t get_offset_of_m_DefaultPointCloud_3() { return static_cast<int32_t>(offsetof(XRDepthSubsystem_tA1188AEE9FA009EAF2BDA064BEA1482FF4CD8AD7, ___m_DefaultPointCloud_3)); }
	inline XRPointCloud_tA4A412DE503530E1B2953919F1463B9B48504ED0  get_m_DefaultPointCloud_3() const { return ___m_DefaultPointCloud_3; }
	inline XRPointCloud_tA4A412DE503530E1B2953919F1463B9B48504ED0 * get_address_of_m_DefaultPointCloud_3() { return &___m_DefaultPointCloud_3; }
	inline void set_m_DefaultPointCloud_3(XRPointCloud_tA4A412DE503530E1B2953919F1463B9B48504ED0  value)
	{
		___m_DefaultPointCloud_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XRDEPTHSUBSYSTEM_TA1188AEE9FA009EAF2BDA064BEA1482FF4CD8AD7_H
#ifndef XRPLANESUBSYSTEM_TCAFBDE4A030D5EB56FC6D968DB31FA2852836909_H
#define XRPLANESUBSYSTEM_TCAFBDE4A030D5EB56FC6D968DB31FA2852836909_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARSubsystems.XRPlaneSubsystem
struct  XRPlaneSubsystem_tCAFBDE4A030D5EB56FC6D968DB31FA2852836909  : public TrackingSubsystem_2_t758E80FC2D0E4A224AA733F5ED6419083CD56389
{
public:
	// UnityEngine.XR.ARSubsystems.XRPlaneSubsystem_IProvider UnityEngine.XR.ARSubsystems.XRPlaneSubsystem::m_Provider
	IProvider_t40982CEC3244CC0C726CED2C27336E95321F7469 * ___m_Provider_2;
	// UnityEngine.XR.ARSubsystems.BoundedPlane UnityEngine.XR.ARSubsystems.XRPlaneSubsystem::m_DefaultPlane
	BoundedPlane_t5FBCC289E172852D4A9C51F6581B3C7EEE17A227  ___m_DefaultPlane_3;

public:
	inline static int32_t get_offset_of_m_Provider_2() { return static_cast<int32_t>(offsetof(XRPlaneSubsystem_tCAFBDE4A030D5EB56FC6D968DB31FA2852836909, ___m_Provider_2)); }
	inline IProvider_t40982CEC3244CC0C726CED2C27336E95321F7469 * get_m_Provider_2() const { return ___m_Provider_2; }
	inline IProvider_t40982CEC3244CC0C726CED2C27336E95321F7469 ** get_address_of_m_Provider_2() { return &___m_Provider_2; }
	inline void set_m_Provider_2(IProvider_t40982CEC3244CC0C726CED2C27336E95321F7469 * value)
	{
		___m_Provider_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Provider_2), value);
	}

	inline static int32_t get_offset_of_m_DefaultPlane_3() { return static_cast<int32_t>(offsetof(XRPlaneSubsystem_tCAFBDE4A030D5EB56FC6D968DB31FA2852836909, ___m_DefaultPlane_3)); }
	inline BoundedPlane_t5FBCC289E172852D4A9C51F6581B3C7EEE17A227  get_m_DefaultPlane_3() const { return ___m_DefaultPlane_3; }
	inline BoundedPlane_t5FBCC289E172852D4A9C51F6581B3C7EEE17A227 * get_address_of_m_DefaultPlane_3() { return &___m_DefaultPlane_3; }
	inline void set_m_DefaultPlane_3(BoundedPlane_t5FBCC289E172852D4A9C51F6581B3C7EEE17A227  value)
	{
		___m_DefaultPlane_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XRPLANESUBSYSTEM_TCAFBDE4A030D5EB56FC6D968DB31FA2852836909_H
#ifndef XRREFERENCEPOINTSUBSYSTEM_TF175EC0188CC6EFB0A0633BA11FCA4D81A6A88E3_H
#define XRREFERENCEPOINTSUBSYSTEM_TF175EC0188CC6EFB0A0633BA11FCA4D81A6A88E3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARSubsystems.XRReferencePointSubsystem
struct  XRReferencePointSubsystem_tF175EC0188CC6EFB0A0633BA11FCA4D81A6A88E3  : public TrackingSubsystem_2_t62AFA2295FCEFC2C3818E3B9EDB3C1AF80509899
{
public:
	// UnityEngine.XR.ARSubsystems.XRReferencePointSubsystem_IProvider UnityEngine.XR.ARSubsystems.XRReferencePointSubsystem::m_Provider
	IProvider_tA80639246D0ED291E8332BE2FBC3FE3EA13A4459 * ___m_Provider_2;
	// UnityEngine.XR.ARSubsystems.XRReferencePoint UnityEngine.XR.ARSubsystems.XRReferencePointSubsystem::m_DefaultReferencePoint
	XRReferencePoint_tA8592C08A27EC91D9B1FB3B083C95C5D372FF1F9  ___m_DefaultReferencePoint_3;

public:
	inline static int32_t get_offset_of_m_Provider_2() { return static_cast<int32_t>(offsetof(XRReferencePointSubsystem_tF175EC0188CC6EFB0A0633BA11FCA4D81A6A88E3, ___m_Provider_2)); }
	inline IProvider_tA80639246D0ED291E8332BE2FBC3FE3EA13A4459 * get_m_Provider_2() const { return ___m_Provider_2; }
	inline IProvider_tA80639246D0ED291E8332BE2FBC3FE3EA13A4459 ** get_address_of_m_Provider_2() { return &___m_Provider_2; }
	inline void set_m_Provider_2(IProvider_tA80639246D0ED291E8332BE2FBC3FE3EA13A4459 * value)
	{
		___m_Provider_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Provider_2), value);
	}

	inline static int32_t get_offset_of_m_DefaultReferencePoint_3() { return static_cast<int32_t>(offsetof(XRReferencePointSubsystem_tF175EC0188CC6EFB0A0633BA11FCA4D81A6A88E3, ___m_DefaultReferencePoint_3)); }
	inline XRReferencePoint_tA8592C08A27EC91D9B1FB3B083C95C5D372FF1F9  get_m_DefaultReferencePoint_3() const { return ___m_DefaultReferencePoint_3; }
	inline XRReferencePoint_tA8592C08A27EC91D9B1FB3B083C95C5D372FF1F9 * get_address_of_m_DefaultReferencePoint_3() { return &___m_DefaultReferencePoint_3; }
	inline void set_m_DefaultReferencePoint_3(XRReferencePoint_tA8592C08A27EC91D9B1FB3B083C95C5D372FF1F9  value)
	{
		___m_DefaultReferencePoint_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XRREFERENCEPOINTSUBSYSTEM_TF175EC0188CC6EFB0A0633BA11FCA4D81A6A88E3_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef ARKITREFERENCEPOINTSUBSYSTEM_T7883B8562F4226A121B744F99A8CE262F2D0E017_H
#define ARKITREFERENCEPOINTSUBSYSTEM_T7883B8562F4226A121B744F99A8CE262F2D0E017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARKit.ARKitReferencePointSubsystem
struct  ARKitReferencePointSubsystem_t7883B8562F4226A121B744F99A8CE262F2D0E017  : public XRReferencePointSubsystem_tF175EC0188CC6EFB0A0633BA11FCA4D81A6A88E3
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARKITREFERENCEPOINTSUBSYSTEM_T7883B8562F4226A121B744F99A8CE262F2D0E017_H
#ifndef ARKITXRDEPTHSUBSYSTEM_TA5A5BCEDB5F2217FEE76B4751167757193534501_H
#define ARKITXRDEPTHSUBSYSTEM_TA5A5BCEDB5F2217FEE76B4751167757193534501_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARKit.ARKitXRDepthSubsystem
struct  ARKitXRDepthSubsystem_tA5A5BCEDB5F2217FEE76B4751167757193534501  : public XRDepthSubsystem_tA1188AEE9FA009EAF2BDA064BEA1482FF4CD8AD7
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARKITXRDEPTHSUBSYSTEM_TA5A5BCEDB5F2217FEE76B4751167757193534501_H
#ifndef ARKITXRPLANESUBSYSTEM_TBCCDC8EA086FD3B3AD556F50AECA1BBFA9A8272A_H
#define ARKITXRPLANESUBSYSTEM_TBCCDC8EA086FD3B3AD556F50AECA1BBFA9A8272A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARKit.ARKitXRPlaneSubsystem
struct  ARKitXRPlaneSubsystem_tBCCDC8EA086FD3B3AD556F50AECA1BBFA9A8272A  : public XRPlaneSubsystem_tCAFBDE4A030D5EB56FC6D968DB31FA2852836909
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARKITXRPLANESUBSYSTEM_TBCCDC8EA086FD3B3AD556F50AECA1BBFA9A8272A_H
#ifndef CHARACTERCONTROLLERTHIRDPERSON_T2CC0D121855408DC72A1CB71940065D5C220C886_H
#define CHARACTERCONTROLLERTHIRDPERSON_T2CC0D121855408DC72A1CB71940065D5C220C886_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CharacterControllerThirdPerson
struct  CharacterControllerThirdPerson_t2CC0D121855408DC72A1CB71940065D5C220C886  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single CharacterControllerThirdPerson::m_MovingTurnSpeed
	float ___m_MovingTurnSpeed_4;
	// System.Single CharacterControllerThirdPerson::m_StationaryTurnSpeed
	float ___m_StationaryTurnSpeed_5;
	// System.Single CharacterControllerThirdPerson::m_JumpPower
	float ___m_JumpPower_6;
	// System.Single CharacterControllerThirdPerson::m_GravityMultiplier
	float ___m_GravityMultiplier_7;
	// System.Single CharacterControllerThirdPerson::m_RunCycleLegOffset
	float ___m_RunCycleLegOffset_8;
	// System.Single CharacterControllerThirdPerson::m_MoveSpeedMultiplier
	float ___m_MoveSpeedMultiplier_9;
	// System.Single CharacterControllerThirdPerson::m_AnimSpeedMultiplier
	float ___m_AnimSpeedMultiplier_10;
	// System.Single CharacterControllerThirdPerson::m_GroundCheckDistance
	float ___m_GroundCheckDistance_11;
	// UnityEngine.Rigidbody CharacterControllerThirdPerson::m_Rigidbody
	Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * ___m_Rigidbody_12;
	// UnityEngine.Animator CharacterControllerThirdPerson::m_Animator
	Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * ___m_Animator_13;
	// System.Boolean CharacterControllerThirdPerson::m_IsGrounded
	bool ___m_IsGrounded_14;
	// System.Single CharacterControllerThirdPerson::m_OrigGroundCheckDistance
	float ___m_OrigGroundCheckDistance_15;
	// System.Single CharacterControllerThirdPerson::m_TurnAmount
	float ___m_TurnAmount_17;
	// System.Single CharacterControllerThirdPerson::m_ForwardAmount
	float ___m_ForwardAmount_18;
	// UnityEngine.Vector3 CharacterControllerThirdPerson::m_GroundNormal
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_GroundNormal_19;
	// System.Single CharacterControllerThirdPerson::m_CapsuleHeight
	float ___m_CapsuleHeight_20;
	// UnityEngine.Vector3 CharacterControllerThirdPerson::m_CapsuleCenter
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_CapsuleCenter_21;
	// UnityEngine.CapsuleCollider CharacterControllerThirdPerson::m_Capsule
	CapsuleCollider_t5FD15B9E7BEEC4FFA8A2071E9FD2B8DEB3A826D1 * ___m_Capsule_22;
	// System.Boolean CharacterControllerThirdPerson::m_Crouching
	bool ___m_Crouching_23;

public:
	inline static int32_t get_offset_of_m_MovingTurnSpeed_4() { return static_cast<int32_t>(offsetof(CharacterControllerThirdPerson_t2CC0D121855408DC72A1CB71940065D5C220C886, ___m_MovingTurnSpeed_4)); }
	inline float get_m_MovingTurnSpeed_4() const { return ___m_MovingTurnSpeed_4; }
	inline float* get_address_of_m_MovingTurnSpeed_4() { return &___m_MovingTurnSpeed_4; }
	inline void set_m_MovingTurnSpeed_4(float value)
	{
		___m_MovingTurnSpeed_4 = value;
	}

	inline static int32_t get_offset_of_m_StationaryTurnSpeed_5() { return static_cast<int32_t>(offsetof(CharacterControllerThirdPerson_t2CC0D121855408DC72A1CB71940065D5C220C886, ___m_StationaryTurnSpeed_5)); }
	inline float get_m_StationaryTurnSpeed_5() const { return ___m_StationaryTurnSpeed_5; }
	inline float* get_address_of_m_StationaryTurnSpeed_5() { return &___m_StationaryTurnSpeed_5; }
	inline void set_m_StationaryTurnSpeed_5(float value)
	{
		___m_StationaryTurnSpeed_5 = value;
	}

	inline static int32_t get_offset_of_m_JumpPower_6() { return static_cast<int32_t>(offsetof(CharacterControllerThirdPerson_t2CC0D121855408DC72A1CB71940065D5C220C886, ___m_JumpPower_6)); }
	inline float get_m_JumpPower_6() const { return ___m_JumpPower_6; }
	inline float* get_address_of_m_JumpPower_6() { return &___m_JumpPower_6; }
	inline void set_m_JumpPower_6(float value)
	{
		___m_JumpPower_6 = value;
	}

	inline static int32_t get_offset_of_m_GravityMultiplier_7() { return static_cast<int32_t>(offsetof(CharacterControllerThirdPerson_t2CC0D121855408DC72A1CB71940065D5C220C886, ___m_GravityMultiplier_7)); }
	inline float get_m_GravityMultiplier_7() const { return ___m_GravityMultiplier_7; }
	inline float* get_address_of_m_GravityMultiplier_7() { return &___m_GravityMultiplier_7; }
	inline void set_m_GravityMultiplier_7(float value)
	{
		___m_GravityMultiplier_7 = value;
	}

	inline static int32_t get_offset_of_m_RunCycleLegOffset_8() { return static_cast<int32_t>(offsetof(CharacterControllerThirdPerson_t2CC0D121855408DC72A1CB71940065D5C220C886, ___m_RunCycleLegOffset_8)); }
	inline float get_m_RunCycleLegOffset_8() const { return ___m_RunCycleLegOffset_8; }
	inline float* get_address_of_m_RunCycleLegOffset_8() { return &___m_RunCycleLegOffset_8; }
	inline void set_m_RunCycleLegOffset_8(float value)
	{
		___m_RunCycleLegOffset_8 = value;
	}

	inline static int32_t get_offset_of_m_MoveSpeedMultiplier_9() { return static_cast<int32_t>(offsetof(CharacterControllerThirdPerson_t2CC0D121855408DC72A1CB71940065D5C220C886, ___m_MoveSpeedMultiplier_9)); }
	inline float get_m_MoveSpeedMultiplier_9() const { return ___m_MoveSpeedMultiplier_9; }
	inline float* get_address_of_m_MoveSpeedMultiplier_9() { return &___m_MoveSpeedMultiplier_9; }
	inline void set_m_MoveSpeedMultiplier_9(float value)
	{
		___m_MoveSpeedMultiplier_9 = value;
	}

	inline static int32_t get_offset_of_m_AnimSpeedMultiplier_10() { return static_cast<int32_t>(offsetof(CharacterControllerThirdPerson_t2CC0D121855408DC72A1CB71940065D5C220C886, ___m_AnimSpeedMultiplier_10)); }
	inline float get_m_AnimSpeedMultiplier_10() const { return ___m_AnimSpeedMultiplier_10; }
	inline float* get_address_of_m_AnimSpeedMultiplier_10() { return &___m_AnimSpeedMultiplier_10; }
	inline void set_m_AnimSpeedMultiplier_10(float value)
	{
		___m_AnimSpeedMultiplier_10 = value;
	}

	inline static int32_t get_offset_of_m_GroundCheckDistance_11() { return static_cast<int32_t>(offsetof(CharacterControllerThirdPerson_t2CC0D121855408DC72A1CB71940065D5C220C886, ___m_GroundCheckDistance_11)); }
	inline float get_m_GroundCheckDistance_11() const { return ___m_GroundCheckDistance_11; }
	inline float* get_address_of_m_GroundCheckDistance_11() { return &___m_GroundCheckDistance_11; }
	inline void set_m_GroundCheckDistance_11(float value)
	{
		___m_GroundCheckDistance_11 = value;
	}

	inline static int32_t get_offset_of_m_Rigidbody_12() { return static_cast<int32_t>(offsetof(CharacterControllerThirdPerson_t2CC0D121855408DC72A1CB71940065D5C220C886, ___m_Rigidbody_12)); }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * get_m_Rigidbody_12() const { return ___m_Rigidbody_12; }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 ** get_address_of_m_Rigidbody_12() { return &___m_Rigidbody_12; }
	inline void set_m_Rigidbody_12(Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * value)
	{
		___m_Rigidbody_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rigidbody_12), value);
	}

	inline static int32_t get_offset_of_m_Animator_13() { return static_cast<int32_t>(offsetof(CharacterControllerThirdPerson_t2CC0D121855408DC72A1CB71940065D5C220C886, ___m_Animator_13)); }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * get_m_Animator_13() const { return ___m_Animator_13; }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A ** get_address_of_m_Animator_13() { return &___m_Animator_13; }
	inline void set_m_Animator_13(Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * value)
	{
		___m_Animator_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_Animator_13), value);
	}

	inline static int32_t get_offset_of_m_IsGrounded_14() { return static_cast<int32_t>(offsetof(CharacterControllerThirdPerson_t2CC0D121855408DC72A1CB71940065D5C220C886, ___m_IsGrounded_14)); }
	inline bool get_m_IsGrounded_14() const { return ___m_IsGrounded_14; }
	inline bool* get_address_of_m_IsGrounded_14() { return &___m_IsGrounded_14; }
	inline void set_m_IsGrounded_14(bool value)
	{
		___m_IsGrounded_14 = value;
	}

	inline static int32_t get_offset_of_m_OrigGroundCheckDistance_15() { return static_cast<int32_t>(offsetof(CharacterControllerThirdPerson_t2CC0D121855408DC72A1CB71940065D5C220C886, ___m_OrigGroundCheckDistance_15)); }
	inline float get_m_OrigGroundCheckDistance_15() const { return ___m_OrigGroundCheckDistance_15; }
	inline float* get_address_of_m_OrigGroundCheckDistance_15() { return &___m_OrigGroundCheckDistance_15; }
	inline void set_m_OrigGroundCheckDistance_15(float value)
	{
		___m_OrigGroundCheckDistance_15 = value;
	}

	inline static int32_t get_offset_of_m_TurnAmount_17() { return static_cast<int32_t>(offsetof(CharacterControllerThirdPerson_t2CC0D121855408DC72A1CB71940065D5C220C886, ___m_TurnAmount_17)); }
	inline float get_m_TurnAmount_17() const { return ___m_TurnAmount_17; }
	inline float* get_address_of_m_TurnAmount_17() { return &___m_TurnAmount_17; }
	inline void set_m_TurnAmount_17(float value)
	{
		___m_TurnAmount_17 = value;
	}

	inline static int32_t get_offset_of_m_ForwardAmount_18() { return static_cast<int32_t>(offsetof(CharacterControllerThirdPerson_t2CC0D121855408DC72A1CB71940065D5C220C886, ___m_ForwardAmount_18)); }
	inline float get_m_ForwardAmount_18() const { return ___m_ForwardAmount_18; }
	inline float* get_address_of_m_ForwardAmount_18() { return &___m_ForwardAmount_18; }
	inline void set_m_ForwardAmount_18(float value)
	{
		___m_ForwardAmount_18 = value;
	}

	inline static int32_t get_offset_of_m_GroundNormal_19() { return static_cast<int32_t>(offsetof(CharacterControllerThirdPerson_t2CC0D121855408DC72A1CB71940065D5C220C886, ___m_GroundNormal_19)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_GroundNormal_19() const { return ___m_GroundNormal_19; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_GroundNormal_19() { return &___m_GroundNormal_19; }
	inline void set_m_GroundNormal_19(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_GroundNormal_19 = value;
	}

	inline static int32_t get_offset_of_m_CapsuleHeight_20() { return static_cast<int32_t>(offsetof(CharacterControllerThirdPerson_t2CC0D121855408DC72A1CB71940065D5C220C886, ___m_CapsuleHeight_20)); }
	inline float get_m_CapsuleHeight_20() const { return ___m_CapsuleHeight_20; }
	inline float* get_address_of_m_CapsuleHeight_20() { return &___m_CapsuleHeight_20; }
	inline void set_m_CapsuleHeight_20(float value)
	{
		___m_CapsuleHeight_20 = value;
	}

	inline static int32_t get_offset_of_m_CapsuleCenter_21() { return static_cast<int32_t>(offsetof(CharacterControllerThirdPerson_t2CC0D121855408DC72A1CB71940065D5C220C886, ___m_CapsuleCenter_21)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_CapsuleCenter_21() const { return ___m_CapsuleCenter_21; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_CapsuleCenter_21() { return &___m_CapsuleCenter_21; }
	inline void set_m_CapsuleCenter_21(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_CapsuleCenter_21 = value;
	}

	inline static int32_t get_offset_of_m_Capsule_22() { return static_cast<int32_t>(offsetof(CharacterControllerThirdPerson_t2CC0D121855408DC72A1CB71940065D5C220C886, ___m_Capsule_22)); }
	inline CapsuleCollider_t5FD15B9E7BEEC4FFA8A2071E9FD2B8DEB3A826D1 * get_m_Capsule_22() const { return ___m_Capsule_22; }
	inline CapsuleCollider_t5FD15B9E7BEEC4FFA8A2071E9FD2B8DEB3A826D1 ** get_address_of_m_Capsule_22() { return &___m_Capsule_22; }
	inline void set_m_Capsule_22(CapsuleCollider_t5FD15B9E7BEEC4FFA8A2071E9FD2B8DEB3A826D1 * value)
	{
		___m_Capsule_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_Capsule_22), value);
	}

	inline static int32_t get_offset_of_m_Crouching_23() { return static_cast<int32_t>(offsetof(CharacterControllerThirdPerson_t2CC0D121855408DC72A1CB71940065D5C220C886, ___m_Crouching_23)); }
	inline bool get_m_Crouching_23() const { return ___m_Crouching_23; }
	inline bool* get_address_of_m_Crouching_23() { return &___m_Crouching_23; }
	inline void set_m_Crouching_23(bool value)
	{
		___m_Crouching_23 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHARACTERCONTROLLERTHIRDPERSON_T2CC0D121855408DC72A1CB71940065D5C220C886_H
#ifndef IMAGERECOGNITIONEXAMPLE_T911A7F2FD5BFFF23A30C97F8619C18A1F0757110_H
#define IMAGERECOGNITIONEXAMPLE_T911A7F2FD5BFFF23A30C97F8619C18A1F0757110_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ImageRecognitionExample
struct  ImageRecognitionExample_t911A7F2FD5BFFF23A30C97F8619C18A1F0757110  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// MarkerPrefabs[] ImageRecognitionExample::markerPrefabCombos
	MarkerPrefabsU5BU5D_t7F40C670B36EE25921E51F171FD6A68A7A1BC336* ___markerPrefabCombos_4;
	// PrefabsInVicinity[] ImageRecognitionExample::prefabsInVicinities
	PrefabsInVicinityU5BU5D_tCF6E2464E3202CBD16C9DEA043361EA7DB3A49A9* ___prefabsInVicinities_5;
	// UnityEngine.XR.ARFoundation.ARTrackedImageManager ImageRecognitionExample::_arTrackedImageManager
	ARTrackedImageManager_tC1F6C093163D1D4A5CCE83613BFCA465E2DABA9D * ____arTrackedImageManager_6;
	// UnityEngine.Camera ImageRecognitionExample::m_MainCamera
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___m_MainCamera_7;
	// System.Int32 ImageRecognitionExample::count
	int32_t ___count_8;
	// System.Boolean ImageRecognitionExample::isInTheFrame
	bool ___isInTheFrame_9;
	// System.Boolean ImageRecognitionExample::shouldObjectsBeShown
	bool ___shouldObjectsBeShown_10;
	// UnityEngine.Vector3 ImageRecognitionExample::prefabPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___prefabPosition_11;
	// UnityEngine.Vector3 ImageRecognitionExample::userPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___userPosition_12;
	// UnityEngine.GameObject ImageRecognitionExample::objectInflatedOnScreen
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___objectInflatedOnScreen_13;

public:
	inline static int32_t get_offset_of_markerPrefabCombos_4() { return static_cast<int32_t>(offsetof(ImageRecognitionExample_t911A7F2FD5BFFF23A30C97F8619C18A1F0757110, ___markerPrefabCombos_4)); }
	inline MarkerPrefabsU5BU5D_t7F40C670B36EE25921E51F171FD6A68A7A1BC336* get_markerPrefabCombos_4() const { return ___markerPrefabCombos_4; }
	inline MarkerPrefabsU5BU5D_t7F40C670B36EE25921E51F171FD6A68A7A1BC336** get_address_of_markerPrefabCombos_4() { return &___markerPrefabCombos_4; }
	inline void set_markerPrefabCombos_4(MarkerPrefabsU5BU5D_t7F40C670B36EE25921E51F171FD6A68A7A1BC336* value)
	{
		___markerPrefabCombos_4 = value;
		Il2CppCodeGenWriteBarrier((&___markerPrefabCombos_4), value);
	}

	inline static int32_t get_offset_of_prefabsInVicinities_5() { return static_cast<int32_t>(offsetof(ImageRecognitionExample_t911A7F2FD5BFFF23A30C97F8619C18A1F0757110, ___prefabsInVicinities_5)); }
	inline PrefabsInVicinityU5BU5D_tCF6E2464E3202CBD16C9DEA043361EA7DB3A49A9* get_prefabsInVicinities_5() const { return ___prefabsInVicinities_5; }
	inline PrefabsInVicinityU5BU5D_tCF6E2464E3202CBD16C9DEA043361EA7DB3A49A9** get_address_of_prefabsInVicinities_5() { return &___prefabsInVicinities_5; }
	inline void set_prefabsInVicinities_5(PrefabsInVicinityU5BU5D_tCF6E2464E3202CBD16C9DEA043361EA7DB3A49A9* value)
	{
		___prefabsInVicinities_5 = value;
		Il2CppCodeGenWriteBarrier((&___prefabsInVicinities_5), value);
	}

	inline static int32_t get_offset_of__arTrackedImageManager_6() { return static_cast<int32_t>(offsetof(ImageRecognitionExample_t911A7F2FD5BFFF23A30C97F8619C18A1F0757110, ____arTrackedImageManager_6)); }
	inline ARTrackedImageManager_tC1F6C093163D1D4A5CCE83613BFCA465E2DABA9D * get__arTrackedImageManager_6() const { return ____arTrackedImageManager_6; }
	inline ARTrackedImageManager_tC1F6C093163D1D4A5CCE83613BFCA465E2DABA9D ** get_address_of__arTrackedImageManager_6() { return &____arTrackedImageManager_6; }
	inline void set__arTrackedImageManager_6(ARTrackedImageManager_tC1F6C093163D1D4A5CCE83613BFCA465E2DABA9D * value)
	{
		____arTrackedImageManager_6 = value;
		Il2CppCodeGenWriteBarrier((&____arTrackedImageManager_6), value);
	}

	inline static int32_t get_offset_of_m_MainCamera_7() { return static_cast<int32_t>(offsetof(ImageRecognitionExample_t911A7F2FD5BFFF23A30C97F8619C18A1F0757110, ___m_MainCamera_7)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_m_MainCamera_7() const { return ___m_MainCamera_7; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_m_MainCamera_7() { return &___m_MainCamera_7; }
	inline void set_m_MainCamera_7(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___m_MainCamera_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_MainCamera_7), value);
	}

	inline static int32_t get_offset_of_count_8() { return static_cast<int32_t>(offsetof(ImageRecognitionExample_t911A7F2FD5BFFF23A30C97F8619C18A1F0757110, ___count_8)); }
	inline int32_t get_count_8() const { return ___count_8; }
	inline int32_t* get_address_of_count_8() { return &___count_8; }
	inline void set_count_8(int32_t value)
	{
		___count_8 = value;
	}

	inline static int32_t get_offset_of_isInTheFrame_9() { return static_cast<int32_t>(offsetof(ImageRecognitionExample_t911A7F2FD5BFFF23A30C97F8619C18A1F0757110, ___isInTheFrame_9)); }
	inline bool get_isInTheFrame_9() const { return ___isInTheFrame_9; }
	inline bool* get_address_of_isInTheFrame_9() { return &___isInTheFrame_9; }
	inline void set_isInTheFrame_9(bool value)
	{
		___isInTheFrame_9 = value;
	}

	inline static int32_t get_offset_of_shouldObjectsBeShown_10() { return static_cast<int32_t>(offsetof(ImageRecognitionExample_t911A7F2FD5BFFF23A30C97F8619C18A1F0757110, ___shouldObjectsBeShown_10)); }
	inline bool get_shouldObjectsBeShown_10() const { return ___shouldObjectsBeShown_10; }
	inline bool* get_address_of_shouldObjectsBeShown_10() { return &___shouldObjectsBeShown_10; }
	inline void set_shouldObjectsBeShown_10(bool value)
	{
		___shouldObjectsBeShown_10 = value;
	}

	inline static int32_t get_offset_of_prefabPosition_11() { return static_cast<int32_t>(offsetof(ImageRecognitionExample_t911A7F2FD5BFFF23A30C97F8619C18A1F0757110, ___prefabPosition_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_prefabPosition_11() const { return ___prefabPosition_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_prefabPosition_11() { return &___prefabPosition_11; }
	inline void set_prefabPosition_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___prefabPosition_11 = value;
	}

	inline static int32_t get_offset_of_userPosition_12() { return static_cast<int32_t>(offsetof(ImageRecognitionExample_t911A7F2FD5BFFF23A30C97F8619C18A1F0757110, ___userPosition_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_userPosition_12() const { return ___userPosition_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_userPosition_12() { return &___userPosition_12; }
	inline void set_userPosition_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___userPosition_12 = value;
	}

	inline static int32_t get_offset_of_objectInflatedOnScreen_13() { return static_cast<int32_t>(offsetof(ImageRecognitionExample_t911A7F2FD5BFFF23A30C97F8619C18A1F0757110, ___objectInflatedOnScreen_13)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_objectInflatedOnScreen_13() const { return ___objectInflatedOnScreen_13; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_objectInflatedOnScreen_13() { return &___objectInflatedOnScreen_13; }
	inline void set_objectInflatedOnScreen_13(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___objectInflatedOnScreen_13 = value;
		Il2CppCodeGenWriteBarrier((&___objectInflatedOnScreen_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGERECOGNITIONEXAMPLE_T911A7F2FD5BFFF23A30C97F8619C18A1F0757110_H
#ifndef WAYPOINT_T7B48E7B71CF48791746A5B866BCCB7F9FABFCA0A_H
#define WAYPOINT_T7B48E7B71CF48791746A5B866BCCB7F9FABFCA0A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TurnTheGameOn.ArrowWaypointer.Waypoint
struct  Waypoint_t7B48E7B71CF48791746A5B866BCCB7F9FABFCA0A  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Int32 TurnTheGameOn.ArrowWaypointer.Waypoint::radius
	int32_t ___radius_4;
	// TurnTheGameOn.ArrowWaypointer.WaypointController TurnTheGameOn.ArrowWaypointer.Waypoint::waypointController
	WaypointController_tD4E5882DF3A666D2E1C97E9E73061816C123AB20 * ___waypointController_5;
	// System.Int32 TurnTheGameOn.ArrowWaypointer.Waypoint::waypointNumber
	int32_t ___waypointNumber_6;

public:
	inline static int32_t get_offset_of_radius_4() { return static_cast<int32_t>(offsetof(Waypoint_t7B48E7B71CF48791746A5B866BCCB7F9FABFCA0A, ___radius_4)); }
	inline int32_t get_radius_4() const { return ___radius_4; }
	inline int32_t* get_address_of_radius_4() { return &___radius_4; }
	inline void set_radius_4(int32_t value)
	{
		___radius_4 = value;
	}

	inline static int32_t get_offset_of_waypointController_5() { return static_cast<int32_t>(offsetof(Waypoint_t7B48E7B71CF48791746A5B866BCCB7F9FABFCA0A, ___waypointController_5)); }
	inline WaypointController_tD4E5882DF3A666D2E1C97E9E73061816C123AB20 * get_waypointController_5() const { return ___waypointController_5; }
	inline WaypointController_tD4E5882DF3A666D2E1C97E9E73061816C123AB20 ** get_address_of_waypointController_5() { return &___waypointController_5; }
	inline void set_waypointController_5(WaypointController_tD4E5882DF3A666D2E1C97E9E73061816C123AB20 * value)
	{
		___waypointController_5 = value;
		Il2CppCodeGenWriteBarrier((&___waypointController_5), value);
	}

	inline static int32_t get_offset_of_waypointNumber_6() { return static_cast<int32_t>(offsetof(Waypoint_t7B48E7B71CF48791746A5B866BCCB7F9FABFCA0A, ___waypointNumber_6)); }
	inline int32_t get_waypointNumber_6() const { return ___waypointNumber_6; }
	inline int32_t* get_address_of_waypointNumber_6() { return &___waypointNumber_6; }
	inline void set_waypointNumber_6(int32_t value)
	{
		___waypointNumber_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAYPOINT_T7B48E7B71CF48791746A5B866BCCB7F9FABFCA0A_H
#ifndef WAYPOINTCONTROLLER_TD4E5882DF3A666D2E1C97E9E73061816C123AB20_H
#define WAYPOINTCONTROLLER_TD4E5882DF3A666D2E1C97E9E73061816C123AB20_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TurnTheGameOn.ArrowWaypointer.WaypointController
struct  WaypointController_tD4E5882DF3A666D2E1C97E9E73061816C123AB20  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Transform TurnTheGameOn.ArrowWaypointer.WaypointController::player
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___player_4;
	// TurnTheGameOn.ArrowWaypointer.WaypointController_Switch TurnTheGameOn.ArrowWaypointer.WaypointController::configureMode
	int32_t ___configureMode_5;
	// System.Single TurnTheGameOn.ArrowWaypointer.WaypointController::arrowTargetSmooth
	float ___arrowTargetSmooth_6;
	// System.Int32 TurnTheGameOn.ArrowWaypointer.WaypointController::TotalWaypoints
	int32_t ___TotalWaypoints_7;
	// TurnTheGameOn.ArrowWaypointer.WaypointController_WaypointComponents[] TurnTheGameOn.ArrowWaypointer.WaypointController::waypointList
	WaypointComponentsU5BU5D_t995379FD5B9F2912BDD0CA0EC38E68B64A9839FD* ___waypointList_8;
	// UnityEngine.GameObject TurnTheGameOn.ArrowWaypointer.WaypointController::newWaypoint
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___newWaypoint_9;
	// System.String TurnTheGameOn.ArrowWaypointer.WaypointController::newWaypointName
	String_t* ___newWaypointName_10;
	// System.Int32 TurnTheGameOn.ArrowWaypointer.WaypointController::nextWP
	int32_t ___nextWP_11;
	// UnityEngine.Transform TurnTheGameOn.ArrowWaypointer.WaypointController::waypointArrow
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___waypointArrow_12;
	// UnityEngine.Transform TurnTheGameOn.ArrowWaypointer.WaypointController::currentWaypoint
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___currentWaypoint_13;
	// UnityEngine.Transform TurnTheGameOn.ArrowWaypointer.WaypointController::arrowTarget
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___arrowTarget_14;

public:
	inline static int32_t get_offset_of_player_4() { return static_cast<int32_t>(offsetof(WaypointController_tD4E5882DF3A666D2E1C97E9E73061816C123AB20, ___player_4)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_player_4() const { return ___player_4; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_player_4() { return &___player_4; }
	inline void set_player_4(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___player_4 = value;
		Il2CppCodeGenWriteBarrier((&___player_4), value);
	}

	inline static int32_t get_offset_of_configureMode_5() { return static_cast<int32_t>(offsetof(WaypointController_tD4E5882DF3A666D2E1C97E9E73061816C123AB20, ___configureMode_5)); }
	inline int32_t get_configureMode_5() const { return ___configureMode_5; }
	inline int32_t* get_address_of_configureMode_5() { return &___configureMode_5; }
	inline void set_configureMode_5(int32_t value)
	{
		___configureMode_5 = value;
	}

	inline static int32_t get_offset_of_arrowTargetSmooth_6() { return static_cast<int32_t>(offsetof(WaypointController_tD4E5882DF3A666D2E1C97E9E73061816C123AB20, ___arrowTargetSmooth_6)); }
	inline float get_arrowTargetSmooth_6() const { return ___arrowTargetSmooth_6; }
	inline float* get_address_of_arrowTargetSmooth_6() { return &___arrowTargetSmooth_6; }
	inline void set_arrowTargetSmooth_6(float value)
	{
		___arrowTargetSmooth_6 = value;
	}

	inline static int32_t get_offset_of_TotalWaypoints_7() { return static_cast<int32_t>(offsetof(WaypointController_tD4E5882DF3A666D2E1C97E9E73061816C123AB20, ___TotalWaypoints_7)); }
	inline int32_t get_TotalWaypoints_7() const { return ___TotalWaypoints_7; }
	inline int32_t* get_address_of_TotalWaypoints_7() { return &___TotalWaypoints_7; }
	inline void set_TotalWaypoints_7(int32_t value)
	{
		___TotalWaypoints_7 = value;
	}

	inline static int32_t get_offset_of_waypointList_8() { return static_cast<int32_t>(offsetof(WaypointController_tD4E5882DF3A666D2E1C97E9E73061816C123AB20, ___waypointList_8)); }
	inline WaypointComponentsU5BU5D_t995379FD5B9F2912BDD0CA0EC38E68B64A9839FD* get_waypointList_8() const { return ___waypointList_8; }
	inline WaypointComponentsU5BU5D_t995379FD5B9F2912BDD0CA0EC38E68B64A9839FD** get_address_of_waypointList_8() { return &___waypointList_8; }
	inline void set_waypointList_8(WaypointComponentsU5BU5D_t995379FD5B9F2912BDD0CA0EC38E68B64A9839FD* value)
	{
		___waypointList_8 = value;
		Il2CppCodeGenWriteBarrier((&___waypointList_8), value);
	}

	inline static int32_t get_offset_of_newWaypoint_9() { return static_cast<int32_t>(offsetof(WaypointController_tD4E5882DF3A666D2E1C97E9E73061816C123AB20, ___newWaypoint_9)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_newWaypoint_9() const { return ___newWaypoint_9; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_newWaypoint_9() { return &___newWaypoint_9; }
	inline void set_newWaypoint_9(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___newWaypoint_9 = value;
		Il2CppCodeGenWriteBarrier((&___newWaypoint_9), value);
	}

	inline static int32_t get_offset_of_newWaypointName_10() { return static_cast<int32_t>(offsetof(WaypointController_tD4E5882DF3A666D2E1C97E9E73061816C123AB20, ___newWaypointName_10)); }
	inline String_t* get_newWaypointName_10() const { return ___newWaypointName_10; }
	inline String_t** get_address_of_newWaypointName_10() { return &___newWaypointName_10; }
	inline void set_newWaypointName_10(String_t* value)
	{
		___newWaypointName_10 = value;
		Il2CppCodeGenWriteBarrier((&___newWaypointName_10), value);
	}

	inline static int32_t get_offset_of_nextWP_11() { return static_cast<int32_t>(offsetof(WaypointController_tD4E5882DF3A666D2E1C97E9E73061816C123AB20, ___nextWP_11)); }
	inline int32_t get_nextWP_11() const { return ___nextWP_11; }
	inline int32_t* get_address_of_nextWP_11() { return &___nextWP_11; }
	inline void set_nextWP_11(int32_t value)
	{
		___nextWP_11 = value;
	}

	inline static int32_t get_offset_of_waypointArrow_12() { return static_cast<int32_t>(offsetof(WaypointController_tD4E5882DF3A666D2E1C97E9E73061816C123AB20, ___waypointArrow_12)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_waypointArrow_12() const { return ___waypointArrow_12; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_waypointArrow_12() { return &___waypointArrow_12; }
	inline void set_waypointArrow_12(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___waypointArrow_12 = value;
		Il2CppCodeGenWriteBarrier((&___waypointArrow_12), value);
	}

	inline static int32_t get_offset_of_currentWaypoint_13() { return static_cast<int32_t>(offsetof(WaypointController_tD4E5882DF3A666D2E1C97E9E73061816C123AB20, ___currentWaypoint_13)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_currentWaypoint_13() const { return ___currentWaypoint_13; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_currentWaypoint_13() { return &___currentWaypoint_13; }
	inline void set_currentWaypoint_13(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___currentWaypoint_13 = value;
		Il2CppCodeGenWriteBarrier((&___currentWaypoint_13), value);
	}

	inline static int32_t get_offset_of_arrowTarget_14() { return static_cast<int32_t>(offsetof(WaypointController_tD4E5882DF3A666D2E1C97E9E73061816C123AB20, ___arrowTarget_14)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_arrowTarget_14() const { return ___arrowTarget_14; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_arrowTarget_14() { return &___arrowTarget_14; }
	inline void set_arrowTarget_14(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___arrowTarget_14 = value;
		Il2CppCodeGenWriteBarrier((&___arrowTarget_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAYPOINTCONTROLLER_TD4E5882DF3A666D2E1C97E9E73061816C123AB20_H
#ifndef SMOOTHFOLLOW_T216FB57B20D1783DDB83147F7D75C14735C98316_H
#define SMOOTHFOLLOW_T216FB57B20D1783DDB83147F7D75C14735C98316_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TurnTheGameOn.NPCChat.SmoothFollow
struct  SmoothFollow_t216FB57B20D1783DDB83147F7D75C14735C98316  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Transform TurnTheGameOn.NPCChat.SmoothFollow::target
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___target_4;
	// System.Single TurnTheGameOn.NPCChat.SmoothFollow::distance
	float ___distance_5;
	// System.Single TurnTheGameOn.NPCChat.SmoothFollow::height
	float ___height_6;
	// System.Single TurnTheGameOn.NPCChat.SmoothFollow::rotationDamping
	float ___rotationDamping_7;
	// System.Single TurnTheGameOn.NPCChat.SmoothFollow::heightDamping
	float ___heightDamping_8;

public:
	inline static int32_t get_offset_of_target_4() { return static_cast<int32_t>(offsetof(SmoothFollow_t216FB57B20D1783DDB83147F7D75C14735C98316, ___target_4)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_target_4() const { return ___target_4; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_target_4() { return &___target_4; }
	inline void set_target_4(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___target_4 = value;
		Il2CppCodeGenWriteBarrier((&___target_4), value);
	}

	inline static int32_t get_offset_of_distance_5() { return static_cast<int32_t>(offsetof(SmoothFollow_t216FB57B20D1783DDB83147F7D75C14735C98316, ___distance_5)); }
	inline float get_distance_5() const { return ___distance_5; }
	inline float* get_address_of_distance_5() { return &___distance_5; }
	inline void set_distance_5(float value)
	{
		___distance_5 = value;
	}

	inline static int32_t get_offset_of_height_6() { return static_cast<int32_t>(offsetof(SmoothFollow_t216FB57B20D1783DDB83147F7D75C14735C98316, ___height_6)); }
	inline float get_height_6() const { return ___height_6; }
	inline float* get_address_of_height_6() { return &___height_6; }
	inline void set_height_6(float value)
	{
		___height_6 = value;
	}

	inline static int32_t get_offset_of_rotationDamping_7() { return static_cast<int32_t>(offsetof(SmoothFollow_t216FB57B20D1783DDB83147F7D75C14735C98316, ___rotationDamping_7)); }
	inline float get_rotationDamping_7() const { return ___rotationDamping_7; }
	inline float* get_address_of_rotationDamping_7() { return &___rotationDamping_7; }
	inline void set_rotationDamping_7(float value)
	{
		___rotationDamping_7 = value;
	}

	inline static int32_t get_offset_of_heightDamping_8() { return static_cast<int32_t>(offsetof(SmoothFollow_t216FB57B20D1783DDB83147F7D75C14735C98316, ___heightDamping_8)); }
	inline float get_heightDamping_8() const { return ___heightDamping_8; }
	inline float* get_address_of_heightDamping_8() { return &___heightDamping_8; }
	inline void set_heightDamping_8(float value)
	{
		___heightDamping_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SMOOTHFOLLOW_T216FB57B20D1783DDB83147F7D75C14735C98316_H
#ifndef USERCONTROLTHIRDPERSON_TA3F8F919E01EC0A7D458E6F2F3346D2BD3878DAB_H
#define USERCONTROLTHIRDPERSON_TA3F8F919E01EC0A7D458E6F2F3346D2BD3878DAB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UserControlThirdPerson
struct  UserControlThirdPerson_tA3F8F919E01EC0A7D458E6F2F3346D2BD3878DAB  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// CharacterControllerThirdPerson UserControlThirdPerson::m_Character
	CharacterControllerThirdPerson_t2CC0D121855408DC72A1CB71940065D5C220C886 * ___m_Character_4;
	// UnityEngine.Transform UserControlThirdPerson::m_Cam
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___m_Cam_5;
	// UnityEngine.Vector3 UserControlThirdPerson::m_CamForward
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_CamForward_6;
	// UnityEngine.Vector3 UserControlThirdPerson::m_Move
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Move_7;
	// System.Boolean UserControlThirdPerson::m_Jump
	bool ___m_Jump_8;

public:
	inline static int32_t get_offset_of_m_Character_4() { return static_cast<int32_t>(offsetof(UserControlThirdPerson_tA3F8F919E01EC0A7D458E6F2F3346D2BD3878DAB, ___m_Character_4)); }
	inline CharacterControllerThirdPerson_t2CC0D121855408DC72A1CB71940065D5C220C886 * get_m_Character_4() const { return ___m_Character_4; }
	inline CharacterControllerThirdPerson_t2CC0D121855408DC72A1CB71940065D5C220C886 ** get_address_of_m_Character_4() { return &___m_Character_4; }
	inline void set_m_Character_4(CharacterControllerThirdPerson_t2CC0D121855408DC72A1CB71940065D5C220C886 * value)
	{
		___m_Character_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Character_4), value);
	}

	inline static int32_t get_offset_of_m_Cam_5() { return static_cast<int32_t>(offsetof(UserControlThirdPerson_tA3F8F919E01EC0A7D458E6F2F3346D2BD3878DAB, ___m_Cam_5)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_m_Cam_5() const { return ___m_Cam_5; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_m_Cam_5() { return &___m_Cam_5; }
	inline void set_m_Cam_5(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___m_Cam_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Cam_5), value);
	}

	inline static int32_t get_offset_of_m_CamForward_6() { return static_cast<int32_t>(offsetof(UserControlThirdPerson_tA3F8F919E01EC0A7D458E6F2F3346D2BD3878DAB, ___m_CamForward_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_CamForward_6() const { return ___m_CamForward_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_CamForward_6() { return &___m_CamForward_6; }
	inline void set_m_CamForward_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_CamForward_6 = value;
	}

	inline static int32_t get_offset_of_m_Move_7() { return static_cast<int32_t>(offsetof(UserControlThirdPerson_tA3F8F919E01EC0A7D458E6F2F3346D2BD3878DAB, ___m_Move_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Move_7() const { return ___m_Move_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Move_7() { return &___m_Move_7; }
	inline void set_m_Move_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Move_7 = value;
	}

	inline static int32_t get_offset_of_m_Jump_8() { return static_cast<int32_t>(offsetof(UserControlThirdPerson_tA3F8F919E01EC0A7D458E6F2F3346D2BD3878DAB, ___m_Jump_8)); }
	inline bool get_m_Jump_8() const { return ___m_Jump_8; }
	inline bool* get_address_of_m_Jump_8() { return &___m_Jump_8; }
	inline void set_m_Jump_8(bool value)
	{
		___m_Jump_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // USERCONTROLTHIRDPERSON_TA3F8F919E01EC0A7D458E6F2F3346D2BD3878DAB_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2500 = { sizeof (ARKitReferencePointSubsystem_t7883B8562F4226A121B744F99A8CE262F2D0E017), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2501 = { sizeof (Provider_t509F1B75CECA94E702BEAA9F7C67EA2313C58986), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2502 = { sizeof (ARKitSessionSubsystem_tD9267F6454E65E2C795C8CAF65C9CCB0BBE1636A), -1, sizeof(ARKitSessionSubsystem_tD9267F6454E65E2C795C8CAF65C9CCB0BBE1636A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2502[1] = 
{
	ARKitSessionSubsystem_tD9267F6454E65E2C795C8CAF65C9CCB0BBE1636A_StaticFields::get_offset_of_s_OnAsyncWorldMapCompleted_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2503 = { sizeof (Provider_t475F303CC6F0955D8B266D4CD5B7022F3658389B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2504 = { sizeof (NativeApi_t164DECAC3F6004936824870871CC817A16AC9050), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2505 = { sizeof (Availability_tFD76A2177DDFE5A5A8CDB75EB9CF784CDD9A7487)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2505[3] = 
{
	Availability_tFD76A2177DDFE5A5A8CDB75EB9CF784CDD9A7487::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2506 = { sizeof (OnAsyncConversionCompleteDelegate_t841FB5BE19010FE3AFBEDEA37C52A468755B19FF), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2507 = { sizeof (ARWorldMap_t8BAE5D083A023D7DD23C29E4082B6BBD329010DE)+ sizeof (RuntimeObject), sizeof(ARWorldMap_t8BAE5D083A023D7DD23C29E4082B6BBD329010DE ), 0, 0 };
extern const int32_t g_FieldOffsetTable2507[1] = 
{
	ARWorldMap_t8BAE5D083A023D7DD23C29E4082B6BBD329010DE::get_offset_of_U3CnativeHandleU3Ek__BackingField_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2508 = { sizeof (ARWorldMapRequestStatus_tF71BE763C5F9644F3D7377ACE110F7FFBBE3D5DC)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2508[8] = 
{
	ARWorldMapRequestStatus_tF71BE763C5F9644F3D7377ACE110F7FFBBE3D5DC::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2509 = { sizeof (ARWorldMapRequestStatusExtensions_t8FC86F2BC224C9CCC808FF3B4610B22BE5F8051F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2510 = { sizeof (ARKitXRDepthSubsystem_tA5A5BCEDB5F2217FEE76B4751167757193534501), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2511 = { sizeof (Provider_t38A44526F6D5F1EBFDA048E10377C8F3FA82EF70), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2512 = { sizeof (TransformPositionsJob_t4BCA4844CF5EFB6C0A19B9E5059390B2E499E283)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2512[2] = 
{
	TransformPositionsJob_t4BCA4844CF5EFB6C0A19B9E5059390B2E499E283::get_offset_of_positionsIn_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TransformPositionsJob_t4BCA4844CF5EFB6C0A19B9E5059390B2E499E283::get_offset_of_positionsOut_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2513 = { sizeof (EnvironmentProbeApi_t7835EE6A5EB5249E9C0ED63CD4AC8AA1B21D76D8), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2514 = { sizeof (ARKitImageTrackingSubsystem_tE365CBF82726F48ED703BACA30C2B8FD797D6960), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2515 = { sizeof (Provider_t529D13A815B277469267A1CA5C25A0FB0246AD46), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2516 = { sizeof (OSVersion_t4FEA9BCD71B65A146585E5F00B36F839EEF89FEA)+ sizeof (RuntimeObject), sizeof(OSVersion_t4FEA9BCD71B65A146585E5F00B36F839EEF89FEA ), 0, 0 };
extern const int32_t g_FieldOffsetTable2516[3] = 
{
	OSVersion_t4FEA9BCD71B65A146585E5F00B36F839EEF89FEA::get_offset_of_U3CmajorU3Ek__BackingField_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	OSVersion_t4FEA9BCD71B65A146585E5F00B36F839EEF89FEA::get_offset_of_U3CminorU3Ek__BackingField_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	OSVersion_t4FEA9BCD71B65A146585E5F00B36F839EEF89FEA::get_offset_of_U3CpointU3Ek__BackingField_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2517 = { sizeof (ARKitXRPlaneSubsystem_tBCCDC8EA086FD3B3AD556F50AECA1BBFA9A8272A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2518 = { sizeof (Provider_tE828C43D91B7E57F44E04A10F068C304DBAE5A6A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2519 = { sizeof (FlipBoundaryWindingJob_tAB484E8E0F98EEA68CD01FECAEBD5BFA7C75B312)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2519[1] = 
{
	FlipBoundaryWindingJob_tAB484E8E0F98EEA68CD01FECAEBD5BFA7C75B312::get_offset_of_positions_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2520 = { sizeof (TransformBoundaryPositionsJob_t42DE86BF3E6AB9CAC98E5C1772288A28226EC59A)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2520[2] = 
{
	TransformBoundaryPositionsJob_t42DE86BF3E6AB9CAC98E5C1772288A28226EC59A::get_offset_of_positionsIn_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TransformBoundaryPositionsJob_t42DE86BF3E6AB9CAC98E5C1772288A28226EC59A::get_offset_of_positionsOut_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2521 = { sizeof (SetReferenceLibraryResult_t60A3252A5AE8F09F503ABF5103B415DED845A779)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2521[4] = 
{
	SetReferenceLibraryResult_t60A3252A5AE8F09F503ABF5103B415DED845A779::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2522 = { sizeof (U3CModuleU3E_t6CDDDF959E7E18A6744E43B613F41CDAC780256A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2523 = { sizeof (PrefabsInVicinity_tF56E3155685D05BFC0529D6C62A92A5C2B88A039), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2523[2] = 
{
	PrefabsInVicinity_tF56E3155685D05BFC0529D6C62A92A5C2B88A039::get_offset_of_prefabName_0(),
	PrefabsInVicinity_tF56E3155685D05BFC0529D6C62A92A5C2B88A039::get_offset_of_prefabPresent_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2524 = { sizeof (MarkerPrefabs_t227A4AF77E7197AEFE5B31A217749784AA27AE6B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2524[2] = 
{
	MarkerPrefabs_t227A4AF77E7197AEFE5B31A217749784AA27AE6B::get_offset_of_prefabName_0(),
	MarkerPrefabs_t227A4AF77E7197AEFE5B31A217749784AA27AE6B::get_offset_of_targetPrefab_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2525 = { sizeof (ImageRecognitionExample_t911A7F2FD5BFFF23A30C97F8619C18A1F0757110), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2525[10] = 
{
	ImageRecognitionExample_t911A7F2FD5BFFF23A30C97F8619C18A1F0757110::get_offset_of_markerPrefabCombos_4(),
	ImageRecognitionExample_t911A7F2FD5BFFF23A30C97F8619C18A1F0757110::get_offset_of_prefabsInVicinities_5(),
	ImageRecognitionExample_t911A7F2FD5BFFF23A30C97F8619C18A1F0757110::get_offset_of__arTrackedImageManager_6(),
	ImageRecognitionExample_t911A7F2FD5BFFF23A30C97F8619C18A1F0757110::get_offset_of_m_MainCamera_7(),
	ImageRecognitionExample_t911A7F2FD5BFFF23A30C97F8619C18A1F0757110::get_offset_of_count_8(),
	ImageRecognitionExample_t911A7F2FD5BFFF23A30C97F8619C18A1F0757110::get_offset_of_isInTheFrame_9(),
	ImageRecognitionExample_t911A7F2FD5BFFF23A30C97F8619C18A1F0757110::get_offset_of_shouldObjectsBeShown_10(),
	ImageRecognitionExample_t911A7F2FD5BFFF23A30C97F8619C18A1F0757110::get_offset_of_prefabPosition_11(),
	ImageRecognitionExample_t911A7F2FD5BFFF23A30C97F8619C18A1F0757110::get_offset_of_userPosition_12(),
	ImageRecognitionExample_t911A7F2FD5BFFF23A30C97F8619C18A1F0757110::get_offset_of_objectInflatedOnScreen_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2526 = { sizeof (CharacterControllerThirdPerson_t2CC0D121855408DC72A1CB71940065D5C220C886), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2526[20] = 
{
	CharacterControllerThirdPerson_t2CC0D121855408DC72A1CB71940065D5C220C886::get_offset_of_m_MovingTurnSpeed_4(),
	CharacterControllerThirdPerson_t2CC0D121855408DC72A1CB71940065D5C220C886::get_offset_of_m_StationaryTurnSpeed_5(),
	CharacterControllerThirdPerson_t2CC0D121855408DC72A1CB71940065D5C220C886::get_offset_of_m_JumpPower_6(),
	CharacterControllerThirdPerson_t2CC0D121855408DC72A1CB71940065D5C220C886::get_offset_of_m_GravityMultiplier_7(),
	CharacterControllerThirdPerson_t2CC0D121855408DC72A1CB71940065D5C220C886::get_offset_of_m_RunCycleLegOffset_8(),
	CharacterControllerThirdPerson_t2CC0D121855408DC72A1CB71940065D5C220C886::get_offset_of_m_MoveSpeedMultiplier_9(),
	CharacterControllerThirdPerson_t2CC0D121855408DC72A1CB71940065D5C220C886::get_offset_of_m_AnimSpeedMultiplier_10(),
	CharacterControllerThirdPerson_t2CC0D121855408DC72A1CB71940065D5C220C886::get_offset_of_m_GroundCheckDistance_11(),
	CharacterControllerThirdPerson_t2CC0D121855408DC72A1CB71940065D5C220C886::get_offset_of_m_Rigidbody_12(),
	CharacterControllerThirdPerson_t2CC0D121855408DC72A1CB71940065D5C220C886::get_offset_of_m_Animator_13(),
	CharacterControllerThirdPerson_t2CC0D121855408DC72A1CB71940065D5C220C886::get_offset_of_m_IsGrounded_14(),
	CharacterControllerThirdPerson_t2CC0D121855408DC72A1CB71940065D5C220C886::get_offset_of_m_OrigGroundCheckDistance_15(),
	0,
	CharacterControllerThirdPerson_t2CC0D121855408DC72A1CB71940065D5C220C886::get_offset_of_m_TurnAmount_17(),
	CharacterControllerThirdPerson_t2CC0D121855408DC72A1CB71940065D5C220C886::get_offset_of_m_ForwardAmount_18(),
	CharacterControllerThirdPerson_t2CC0D121855408DC72A1CB71940065D5C220C886::get_offset_of_m_GroundNormal_19(),
	CharacterControllerThirdPerson_t2CC0D121855408DC72A1CB71940065D5C220C886::get_offset_of_m_CapsuleHeight_20(),
	CharacterControllerThirdPerson_t2CC0D121855408DC72A1CB71940065D5C220C886::get_offset_of_m_CapsuleCenter_21(),
	CharacterControllerThirdPerson_t2CC0D121855408DC72A1CB71940065D5C220C886::get_offset_of_m_Capsule_22(),
	CharacterControllerThirdPerson_t2CC0D121855408DC72A1CB71940065D5C220C886::get_offset_of_m_Crouching_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2527 = { sizeof (UserControlThirdPerson_tA3F8F919E01EC0A7D458E6F2F3346D2BD3878DAB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2527[5] = 
{
	UserControlThirdPerson_tA3F8F919E01EC0A7D458E6F2F3346D2BD3878DAB::get_offset_of_m_Character_4(),
	UserControlThirdPerson_tA3F8F919E01EC0A7D458E6F2F3346D2BD3878DAB::get_offset_of_m_Cam_5(),
	UserControlThirdPerson_tA3F8F919E01EC0A7D458E6F2F3346D2BD3878DAB::get_offset_of_m_CamForward_6(),
	UserControlThirdPerson_tA3F8F919E01EC0A7D458E6F2F3346D2BD3878DAB::get_offset_of_m_Move_7(),
	UserControlThirdPerson_tA3F8F919E01EC0A7D458E6F2F3346D2BD3878DAB::get_offset_of_m_Jump_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2528 = { sizeof (Waypoint_t7B48E7B71CF48791746A5B866BCCB7F9FABFCA0A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2528[3] = 
{
	Waypoint_t7B48E7B71CF48791746A5B866BCCB7F9FABFCA0A::get_offset_of_radius_4(),
	Waypoint_t7B48E7B71CF48791746A5B866BCCB7F9FABFCA0A::get_offset_of_waypointController_5(),
	Waypoint_t7B48E7B71CF48791746A5B866BCCB7F9FABFCA0A::get_offset_of_waypointNumber_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2529 = { sizeof (WaypointController_tD4E5882DF3A666D2E1C97E9E73061816C123AB20), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2529[11] = 
{
	WaypointController_tD4E5882DF3A666D2E1C97E9E73061816C123AB20::get_offset_of_player_4(),
	WaypointController_tD4E5882DF3A666D2E1C97E9E73061816C123AB20::get_offset_of_configureMode_5(),
	WaypointController_tD4E5882DF3A666D2E1C97E9E73061816C123AB20::get_offset_of_arrowTargetSmooth_6(),
	WaypointController_tD4E5882DF3A666D2E1C97E9E73061816C123AB20::get_offset_of_TotalWaypoints_7(),
	WaypointController_tD4E5882DF3A666D2E1C97E9E73061816C123AB20::get_offset_of_waypointList_8(),
	WaypointController_tD4E5882DF3A666D2E1C97E9E73061816C123AB20::get_offset_of_newWaypoint_9(),
	WaypointController_tD4E5882DF3A666D2E1C97E9E73061816C123AB20::get_offset_of_newWaypointName_10(),
	WaypointController_tD4E5882DF3A666D2E1C97E9E73061816C123AB20::get_offset_of_nextWP_11(),
	WaypointController_tD4E5882DF3A666D2E1C97E9E73061816C123AB20::get_offset_of_waypointArrow_12(),
	WaypointController_tD4E5882DF3A666D2E1C97E9E73061816C123AB20::get_offset_of_currentWaypoint_13(),
	WaypointController_tD4E5882DF3A666D2E1C97E9E73061816C123AB20::get_offset_of_arrowTarget_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2530 = { sizeof (Switch_t477EAF7EDB3539E13C2AB958F50647A0D1892299)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2530[3] = 
{
	Switch_t477EAF7EDB3539E13C2AB958F50647A0D1892299::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2531 = { sizeof (WaypointComponents_t3DE523CAE0F5855ADF2B740DBBA68569E9034301), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2531[3] = 
{
	WaypointComponents_t3DE523CAE0F5855ADF2B740DBBA68569E9034301::get_offset_of_waypointName_0(),
	WaypointComponents_t3DE523CAE0F5855ADF2B740DBBA68569E9034301::get_offset_of_waypoint_1(),
	WaypointComponents_t3DE523CAE0F5855ADF2B740DBBA68569E9034301::get_offset_of_waypointEvent_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2532 = { sizeof (SmoothFollow_t216FB57B20D1783DDB83147F7D75C14735C98316), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2532[5] = 
{
	SmoothFollow_t216FB57B20D1783DDB83147F7D75C14735C98316::get_offset_of_target_4(),
	SmoothFollow_t216FB57B20D1783DDB83147F7D75C14735C98316::get_offset_of_distance_5(),
	SmoothFollow_t216FB57B20D1783DDB83147F7D75C14735C98316::get_offset_of_height_6(),
	SmoothFollow_t216FB57B20D1783DDB83147F7D75C14735C98316::get_offset_of_rotationDamping_7(),
	SmoothFollow_t216FB57B20D1783DDB83147F7D75C14735C98316::get_offset_of_heightDamping_8(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
